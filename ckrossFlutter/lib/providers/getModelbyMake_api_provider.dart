import 'dart:convert' as JSON;

import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/database/all_tables_queries/model_vdDB.dart';
import 'package:ckrossFlutter/models/ResponseModels/getUniqueColumnData.dart';
import 'package:ckrossFlutter/models/modelVd.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class modelUTData{
  getModelsBasedOnSelectedMake(BuildContext context, String selectedMake,  Map<String,dynamic> subProcessField,placeholdervslue)async
  {
    print(selectedMake);

    print(subProcessField);
    print(placeholdervslue);
     print("Start of getModelsBasedOnSelectedMake()");

      Map jObject = new Map();
      jObject["collectionName"] =  "Price_Master_UTW";
      jObject["columnName"] =  "Model_Variant";

//      if (subProcessField.containsKey("isMobileInputRequired")) {
        print("in if");
        if (subProcessField["mobileInputAPIKey"] != null) {
          print("in if.......");
          print(subProcessField["mobileInputAPIKey"]);
           List<String> mobileInputAPIKeysList = getMobileInptAPIKeysList(subProcessField);
         // List<String>mobileInputAPIKeysList = getMobileInptAPIKeysList(subProcessField);
          print("............");
          print(mobileInputAPIKeysList);
          List jArray = getDependentFieldJsonArray(mobileInputAPIKeysList, selectedMake,placeholdervslue);
          print("jarry..............");
          print(jArray);
          jObject["selectedDDKey"] = jArray;
          print("jobject................");
          print(jObject);

          String requestJson = JSON.jsonEncode(jObject);
          print("requestjson.........");
          print(requestJson);
          ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
          if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
            var response =
           await APIServices.GetUniqueColumnData(requestJson);
            if ((response != null) && (response != "null") &&  (response != "{}")) {
              print("response of make model.......");
              print(response.body);
              //await handleGetReport(context, response);
              if (response.statusCode == 200)
              {
                if (response != null)
                {
                  print("i am here ...........");

                  GetUniqueColumnDataModel responseModel = getUniqueColumnDataModelFromJson(response.body);
                  print(responseModel.details);
                 // await  SaveModelsBasedOnSelectedMake(responseModel.details,selectedMake);

                }
                else
                {

                  await showdialog(context,"Alert", "Model not found.", "OK");
                }
              }
              else
              {

                await showdialog(context,"Alert", "Model not found.", "OK");
              }
            } else {
              //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
              await showdialog(context,
                  AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
            }
          } else {
            CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          }

        }
//      }else{
//        print("not in if");
//      }


  }

  SaveModelsBasedOnSelectedMake(List<dynamic>details, String makeid)async
  {
    try
    {
      //CKrossStore<ModelVD> cKrossModelVD = new SQLiteModelVD(DependencyService.Get<ISQLiteDb>());
      print("savemodl....");
      print(details);
      print(makeid);

      ModelVdDB modelVdDB = new ModelVdDB();
      for (int i = 0; i < details.length; i++)
      {
        ModelVD modelVD = new ModelVD();
        modelVD.make_id = makeid;
        modelVD.model_name = details[i].toString();
        int id = await ModelVdDB().insertModelVD(modelVD);
        print("id........");
        print(id);
      }
    }
    on Exception catch (e)
    {
   print("Exception in SaveModelsBasedOnSelectedMake() : " + e.toString());
   // e.GetBaseException();
    }
  }


  List getDependentFieldJsonArray(List<String> mobileInputAPIKeysList, String selected,placeholdervslue)
  {

    print("at getdependend fieldjson...........");
    print( mobileInputAPIKeysList);
    print(selected);
    print(placeholdervslue);
    List jArray = new List();

    try
    {
      for (int i = 0; i < mobileInputAPIKeysList.length; i++)
      {
        Map jObject1 = new Map();
        jObject1["selectedKey"] =  mobileInputAPIKeysList[i].trim();


       // List<String> arrayListEntryFields = new List<placeholdervslue>();
       // FindEntryByKey(mainParent, _subProcessField, mobileInputAPIKeysList[i].Trim());

        if (placeholdervslue != 0)
        {
          if (placeholdervslue == (mobileInputAPIKeysList[i].trim().toLowerCase()))
          {
            String temp = placeholdervslue;
            if (temp.toLowerCase() == ("P2P".toLowerCase()))
            {
              temp = "Used TW";
            }
            jObject1[mobileInputAPIKeysList[i].trim()] = temp;
          }
          else
          {
            jObject1[mobileInputAPIKeysList[i].trim()] == selected;
            print(jObject1);
            print("jobject at selcted.....");
          }
        }

        if (mobileInputAPIKeysList[i].trim().toLowerCase() == ("product_Id".toLowerCase()))
        {
          print("product id if");
          print(mobileInputAPIKeysList[i].trim().toLowerCase());
          jObject1[mobileInputAPIKeysList[i].trim()] == 2;
        }

        jArray.add(jObject1);
     }
    }
    on Exception catch ( e)
    {
    print("Exception in getDependentFieldJsonArray() : " + e.toString());
   // e.GetBaseException();
    }

    return jArray;
  }


  List<String> getMobileInptAPIKeysList(Map<String,dynamic>subProcessField)
  {

    print("getMobile.............");
    List<String> mobileInputAPIKeysList = new List<String>();

    String mobileInputAPIKeys = subProcessField["mobileInputAPIKey"].toString();

    print(mobileInputAPIKeys);
    if (mobileInputAPIKeys.toString().contains("["))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("[", "");
    }
    if (mobileInputAPIKeys.toString().contains("]"))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("]", "");
    }
    if (mobileInputAPIKeys.toString().contains("\""))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("\"", "");
    }
    if (mobileInputAPIKeys.toString().contains("\n"))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("\n", "");
    }

    mobileInputAPIKeysList = mobileInputAPIKeys.toString().split(',').toList();
    if (mobileInputAPIKeysList.length == 1 && mobileInputAPIKeysList.contains(""))
    {
      mobileInputAPIKeysList.remove("");
    }
    print("i am mobile input api key......");
    print(mobileInputAPIKeysList);
    return mobileInputAPIKeysList;
  }

  showdialog(BuildContext context,String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }


}