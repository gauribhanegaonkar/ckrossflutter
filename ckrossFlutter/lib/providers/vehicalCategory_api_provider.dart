import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/YearVdDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/modelDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/subLoanTypeDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ResponseModels/getUniqueColumnData.dart';
import 'package:ckrossFlutter/models/subLoanType.dart';
import 'package:ckrossFlutter/models/yearVD.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as JSON;
import '../models/statusReportDeatailsModel.dart';
import 'package:flutter/foundation.dart';

class VehicalCategoryUTData {


  Future  GetVehicleCategoryBasedOnModel(BuildContext context,String selectedModel,  Map<String,dynamic> subProcessField) async
  {
    try{
      print("In GetSubLoanTypes");
      Map jObject = new Map();
      jObject["collectionName"] =  "Price_Master_UTW";
      jObject["columnName"] =  "VehicleCategory";


      print("in if");
      if (subProcessField.containsKey("mobileInputAPIKey") != null) {
        print("in if.......");
        // List<string> mobileInputAPIKeysList = getMobileInptAPIKeysList(subProcessField.mobileInputAPIKey);
        List<String>mobileInputAPIKeysList = getMobileInptAPIKeysList(
            subProcessField);
//        List jArray = getDependentFieldJsonArray(
//            mobileInputAPIKeysList, selectedMake);
        ///  jObject["selectedDDKey"] = jArray;
      }

      String requestJson = JSON.jsonEncode(jObject);

      print("requestJson at in class.............................");
      print(requestJson);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response =
        await APIServices.GetUniqueColumnData(requestJson);
        if ((response != null) && (response != "null") &&  (response != "{}")) {
          print("response of date filter");
          print(response.body);
          //await handleGetReport(context, response);
          if (response.statusCode == 200)
          {
            if (response != null)
            {
              print("i am here ...........");
              //print(response.body);
              // Map<String,dynamic> results = JSON.jsonDecode(response.body);
              //print(results["details"]);
              GetUniqueColumnDataModel responseModel = getUniqueColumnDataModelFromJson(response.body);

              //List<String> results = JSON.jsonDecode(responseModel.details);
              print(responseModel.details);
              //List<String> listData = responseModel.details;
              // await SaveYearOnModelType(responseModel.details);
            }
            else
            {

              await showdialog(context,"Alert", "Data not found.", "OK");
            }
          }
          else
          {

            await showdialog(context,"Alert", "Data not found.", "OK");
          }
        } else {
          //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          await showdialog(context,
              AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }

    }on Exception catch (e) {
      print("Exception in GetSub-loan Data " + e.toString());
    }


  }//end of get sub loan data

  SaveYearOnModelType(List<dynamic>detailssses,String LoanType) async
  {
    print('detailssses............');
    print(detailssses);
    DatabaseHelper db = DatabaseHelper();
    try {
      print("Start of TruncatesubloanTable");
      String query =
      DatabaseQueries.truncateTable(DatabaseQueries.TABLE_SUB_LOAN_TYPE);
      print(query);
      db.execution(query);
      print("End of TruncateSubloanTable");
    } on Exception catch (_) {
      print("Exception occured in TruncateSubloanTable ");
    }

    List<String> arr = [];
    for(int i =0; i < detailssses.length; i++){
      print(detailssses[i]);
      arr.add(detailssses[i]);
    }
    print("arrrrr");
    print(arr);
    YearVDDB yearVDDB = YearVDDB();

    for(int i =0; i < arr.length; i++) {
      print("arrrrr..................");
      print(arr);
      YearVD yearVD = new YearVD();

      yearVD.model_id = arr[i];
      yearVD.year = arr[i].toString();
      print("sub loan type");
      // print(subLoanType.sub_loan_type);
      int id = await yearVDDB.insertYearVD(yearVD);
      print(id);
      print("insert sub loan ");
    }
  }

  List<String> getMobileInptAPIKeysList(Map<String,dynamic>mobileInputAPIKey)
  {

    print("getMobile.............");
    List<String> mobileInputAPIKeysList = new List<String>();

    String mobileInputAPIKeys = mobileInputAPIKey.toString();

    print(mobileInputAPIKeys);
    if (mobileInputAPIKeys.toString().contains("["))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("[", "");
    }
    if (mobileInputAPIKeys.toString().contains("]"))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("]", "");
    }
    if (mobileInputAPIKeys.toString().contains("\""))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("\"", "");
    }
    if (mobileInputAPIKeys.toString().contains("\n"))
    {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("\n", "");
    }

    mobileInputAPIKeysList = mobileInputAPIKeys.toString().split(',').toList();
    if (mobileInputAPIKeysList.length == 1 && mobileInputAPIKeysList.contains(""))
    {
      mobileInputAPIKeysList.remove("");
    }
    print(mobileInputAPIKeysList);
    return mobileInputAPIKeysList;
  }


  showdialog(BuildContext context,String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/statusReport');
              },
            ),
          ],
        );
      },
    );
  }

}