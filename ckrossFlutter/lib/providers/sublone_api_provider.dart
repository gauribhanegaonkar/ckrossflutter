import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/subLoanTypeDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ResponseModels/getUniqueColumnData.dart';
import 'package:ckrossFlutter/models/subLoanType.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as JSON;
import '../models/statusReportDeatailsModel.dart';
import 'package:flutter/foundation.dart';

class SubloanUTData {


 Future GetSubLoanTypes(BuildContext context,String loanType,subProcessField) async
  {
    try{
      print("In GetSubLoanTypes");
      Map jObject = new Map();
      jObject["collectionName"] =  "Used_TW_Loan_Types";
      jObject["columnName"] =  "subLoanType";


      List selectedDDKeyArray = new List();
      Map selectedDDKeyObject1 = new Map();
      selectedDDKeyObject1["selectedKey"] = "LoanType";
      print("loan type");
      print(loanType);
//      if(loanType == ' Used TW'){
//        print("i am here usd tw.............");
//
//        selectedDDKeyObject1["LoanType"] = "Used TW";
//        SharedPreferences sp = await SharedPreferences.getInstance();
//        sp.setString('loan type', selectedDDKeyObject1["LoanType"]);
//      }else{
        selectedDDKeyObject1["LoanType"] = loanType;
     // }

      selectedDDKeyArray.add(selectedDDKeyObject1);
      jObject["selectedDDKey"] =  selectedDDKeyArray;

      String requestJson = JSON.jsonEncode(jObject);

      print("requestJson at in class.............................");
      print(requestJson);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response =
            await APIServices.GetUniqueColumnData(requestJson);
        if ((response != null) && (response != "null") &&  (response != "{}")) {
          print("response of date filter");
          print(response.body);

          if (response.statusCode == 200)
          {
            if (response != null)
            {
              print("i am here ...........");
              GetUniqueColumnDataModel responseModel = getUniqueColumnDataModelFromJson(response.body);
              print(responseModel.details);
              await SaveSubLoanTypes(responseModel.details,loanType);
            }
            else
            {
              await showdialog(context,"Alert", "Sub Loan Type not found.", "OK");
            }
          }
          else
          {
            await showdialog(context,"Alert", "Sub Loan Type not found.", "OK");
          }
        } else {
          //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          await showdialog(context,
              AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }

    }on Exception catch (e) {
      print("Exception in GetSub-loan Data " + e.toString());
    }


  }//end of get sub loan data

  SaveSubLoanTypes(List<dynamic>detailssses,String LoanType) async
 {
   print('detailssses............');
   print(detailssses);
    DatabaseHelper db = DatabaseHelper();
   try {
     print("Start of TruncateClientTemplateTable");
     String query =
     DatabaseQueries.truncateTable(DatabaseQueries.TABLE_SUB_LOAN_TYPE);
     print(query);
     db.execution(query);
     print("End of TruncateClientTemplateTable");
   } on Exception catch (_) {
     print("Exception occured in TruncateTemplateJsonTable ");
   }

   List<String> arr = [];
   for(int i =0; i < detailssses.length; i++){
     print(detailssses[i]);
     arr.add(detailssses[i]);
     }
     print("arrrrr");
     print(arr);
   SubLoanTypeDB subLoanTypeDB = SubLoanTypeDB();

   for(int i =0; i < arr.length; i++) {
     print("arrrrr..................");
     print(arr);
     SubLoanType subLoanType = new SubLoanType();
     subLoanType.sub_loan_type = arr[i];
     subLoanType.loan_type = LoanType;
     print("sub loan type...........");
     print(subLoanType.sub_loan_type);
     int id = await subLoanTypeDB.insertSubLoanType(subLoanType);
     print(id);
     print("insert sub loan ");
   }
 }


 showdialog(BuildContext context,String title, String text, String text1) {
   showDialog(
     context: context,
     // barrierDismissible: false,
     builder: (BuildContext context) {
       return AlertDialog(
         title: Text(title),
         content: new Text(text),
         actions: <Widget>[
           FlatButton(
             child: new Text(text1),
             onPressed: () {
               Navigator.pushReplacementNamed(context, '/statusReport');
             },
           ),
         ],
       );
     },
   );
 }

}