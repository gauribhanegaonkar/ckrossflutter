import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as JSON;
import '../models/statusReportDeatailsModel.dart';
import 'package:flutter/foundation.dart';

class PostDisbursmentUTData with ChangeNotifier {
  SharedPreferences sp;
  List<NEWTWReport> _listViewData = [];

  List<NEWTWReport> get reportItems {
    return [..._listViewData];
  }

  PostDisbursmentUTData() {
    initData();
  }
  void initData() async {
    sp = await SharedPreferences.getInstance();
  }

  Future<void> getReportsFromAPI(BuildContext context) async {
    _listViewData.clear();
    print("start getStatusreport data");

    try {
      final currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
      String url = 'reports' +
          "/" +
          currentProductID +
          "/" +
          sp.getString(AppConstants.POST_DISBURSEMENT_KEY);
      print("key:" + sp.getString(AppConstants.POST_DISBURSEMENT_KEY));
      print("requestJson");
      print(url);
      FileUtils.printLog("requestJson" + url);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      // Dialogs.showSimpleDialog(context, "Please Wait");
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.GetReports(url);
        if ((response != null) && (response != "null") &&  (response != "{}")) {
          await handleGetReport(context, response);
        } else {
          //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          await showdialog(
              context, AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
      //main if

    } on Exception catch (e) {
      print("Exception in GetpostDisbustmentdocUploadReportData " + e.toString());
    }
  }

  Future handleGetReport(BuildContext context, Response response) async {
    try {
      if (response.statusCode == 200) {
        var results = JSON.jsonDecode(response.body);
        if (results != null) {
          // print("result");
          //print(results);
          // JObject results = JObject.Parse(responseContent);
          List reportHeadersArray = results["reportHeaders"];
          //print(reportHeadersArray);

          int editIndex = -1;
          for (int i = 0; i < reportHeadersArray.length; i++) {
            if (reportHeadersArray[i] == "Edit".toLowerCase()) {
              editIndex = i;
            }
          }
          if (editIndex > 0) {
            reportHeadersArray.remove(editIndex);
          }
          List reportDataArray = results["reportData"];
          List reportHeadersUIArray = results["reportHeadersUI"];
          //  print("arrays");
          //print(reportDataArray);
          // print(reportHeadersUIArray);
          if (reportDataArray != null) {
            if (reportDataArray.length > 0) {
              List<List<String>> arrayListDataValues =
              await GetValuesFromReportDataArrayJson(
                  reportDataArray, reportHeadersUIArray);
              // print("array list ..." + arrayListDataValues.toString());
              await CreateExpandableReportUI(reportHeadersArray,
                  arrayListDataValues, reportHeadersUIArray);
              // Dialogs.dismissSimpleDialog(context);
            } else {
              await showdialog(context, AppConstants.POST_DISBURSEMENT_REPORT,
                  "No Data Available", "OK");
              // Navigation.RemovePage(this);
            }
          }
        } else {
          await showdialog(
              context, AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
          // Navigation.RemovePage(this);
        }
      } else if (response.statusCode == 401) {
        await showdialog(context, AppConstants.ALERT,
            AppConstants.TOKEN_EXPIRED_RELOGIN, AppConstants.OK);
        //AppUtils.LogOutUser(this);
      } else if (response.statusCode == 403 || response.statusCode == 800) {
        await showdialog(context, AppConstants.ALERT,
            AppConstants.SOMETHING_WENT_WRONG_TRY, AppConstants.OK);
        // Navigation.RemovePage(this);
      } else {
        await showdialog(context, AppConstants.ALERT,
            AppConstants.SOMETHING_WENT_WRONG_TRY, AppConstants.OK);
        // Navigation.RemovePage(this);
      }
    } on Exception catch (e) {
      FileUtils.printLog("Exception in GetpostDisbustmentdocUploadReportData " + e.toString());
    }
  }

  Future GetValuesFromReportDataArrayJson(
      List reportDataArray, List reportHeadersUIArray) async {
    List<List<String>> arrayListDataValues = new List<List<String>>();
    try {
      print("GetValuesFromReportDataArrayJson Adding elements in report cards");

      for (int i = 0; i < reportDataArray.length; i++) {
        try {
          List<String> values = new List<String>();
          for (int j = 0; j < reportHeadersUIArray.length; j++) {
            Map headersUIJsonObject = reportHeadersUIArray[j];

            String uiField = headersUIJsonObject["data"];

            //  print(reportDataArray);
            Map<String, dynamic> jsonObject = reportDataArray[i];
            //print("jsonobject...." + jsonObject.toString());
            if (uiField != null && uiField.contains(".")) {
              try {
                List<String> fieldDataArray = uiField.split('.');
                String fieldDataVal = fieldDataArray[0];
                Map dataJsonObject = jsonObject[fieldDataVal];
                Map applicationJsonObject = dataJsonObject[fieldDataArray[
                (fieldDataArray.length) - (fieldDataArray.length - 1)]];
                String fieldFinalVal = applicationJsonObject[
                fieldDataArray[fieldDataArray.length - 1]];
                values.add(fieldFinalVal.toString());
              } on Exception catch (e) {
                // print("GetValuesFromReportDataArrayJson " + e.toString());
                values.add("");
              }
            } else {
              try {
                String fieldFinalVal = jsonObject[uiField].toString();
                values.add(fieldFinalVal);
              } on Exception catch (e) {
                // print("GetValuesFromReportDataArrayJson " + e.toString());
                values.add("");
              }
            }
          }
          arrayListDataValues.add(values);
        } on Exception catch (e) {
          FileUtils.printLog("GetValuesFromReportDataArrayJson " + e.toString());
        }
      } // end for loop

    } on Exception catch (e) {
      FileUtils.printLog("GetValuesFromReportDataArrayJson " + e.toString());
    }
    return arrayListDataValues;
  } //end of

  CreateExpandableReportUI(List reportHeadersArray,
      List<List<String>> reportDataArray, List reportHeadersUIArray) async {
    print(reportHeadersArray);
    print(reportDataArray);
    print(reportHeadersUIArray);
    Map<String, String> z = new Map<String, String>();
    List<String> mapData = new List<String>();
    try {
      //  setState(() {
      for (var i = 0; i < reportDataArray.length; i++) {
        var dataList = reportDataArray[i];

        for (var j = 0; j < dataList.length; j++) {
          print("datalist" + dataList.toString());
          for (var k = 0; k < reportHeadersArray.length; k++) {
            z[reportHeadersArray[k]] = dataList[j];
            j++;
          }
        }
        print("z..." + z.toString());
        mapData.add(z.toString());
        print("map data");
        print(mapData);
      }

      //  [{Lead ID: 782559, Customer First Name: Rizwan, Customer Last Name: ., Lead Stage: Conditional DO, Status: Processed, Edit: null, Source By: 18-WEMI, Dealer: VIPIN VAIH, Sales Officer: Abhishek Shrivastav, Last Update Time: 2020-09-14 16:15:13}, {Lead ID: 782765, Customer First Name: Brijendra Kumar, Customer Last Name: ., Lead Stage: Conditional DO, Status: Processed, Edit: null, Source By: 18-WEMI, Dealer: VIPIN VAIH, Sales Officer: Abhishek Shrivastav, Last Update Time: 2020-09-15 18:00:38}]

      mapData.forEach((k) => _listViewData.add(NEWTWReport(
          k.substring(k.indexOf("{"), k.indexOf("Edit")).replaceAll("{", " "),
          [k.substring(k.indexOf(" Source"), k.indexOf("}"))])));

      // Dialogs.dismissSimpleDialog(context);

    } on Exception catch (e) {
      FileUtils.printLog("Exception in CreateExpandableReportUI " + e.toString());
    }
  }

  showdialog(BuildContext context, String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
