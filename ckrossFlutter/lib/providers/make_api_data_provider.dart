
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/database/all_tables_queries/makeDB.dart';
import 'package:ckrossFlutter/models/ResponseModels/getUniqueColumnData.dart';
import 'package:ckrossFlutter/models/make.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as JSON;

class makeUTData{

  Future GetMake(BuildContext context) async
  {
    try{
      print("In GetMake");
      Map jObject = new Map();
      jObject["collectionName"] =  "Price_Master_UTW";
      jObject["columnName"] = "Make";
      String requestJson = JSON.jsonEncode(jObject);

      print("requestJson at in class.............................");
      print(requestJson);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response =
        await APIServices.GetUniqueColumnData(requestJson);
        if ((response != null) && (response != "null") &&  (response != "{}")) {
          print("response MAKE");
          print(response.body);
          //await handleGetReport(context, response);
          if (response.statusCode == 200)
          {
            if (response != null)
            {
              print("i am here ...........");
              GetUniqueColumnDataModel responseModel = getUniqueColumnDataModelFromJson(response.body);
              print(responseModel.details);
              SaveMakesDataAsync(responseModel.details);

            }
            else
            {

              await showdialog(context,"Alert", "Make Data Not found.", "OK");
            }
          }
          else
          {

            CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          }
        } else {
          //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          await showdialog(context,
              AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }

    }on Exception catch (e) {
      print("Exception in GetSub-loan Data " + e.toString());
    }


  }//end oF MAKE GET

  SaveMakesDataAsync (List jArrayMakes) async
  {
    MakeDB makeDB = new MakeDB();
    for (int y = 0; y < jArrayMakes.length; y++)
    {
      MakeModel make = new MakeModel();
      make.product_id = '-1';
      make.make_name = jArrayMakes[y].toString();
      await makeDB.insertMake(make);
    }
  }


  showdialog(BuildContext context,String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }


}