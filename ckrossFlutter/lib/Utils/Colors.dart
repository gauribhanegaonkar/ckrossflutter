import 'package:flutter/material.dart';


const Color loginGradientStart = const Color(0xFF0064EA);
const Color loginGradientEnd = const Color(0xFF00C3FA);


const primaryGradient = const LinearGradient(
  colors: const [loginGradientStart, loginGradientEnd],
  stops: const [0.0, 1.0],
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
);


