import 'dart:convert';

import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/database/all_tables_queries/dealer_SOdataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/makeDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/make_dealer_dataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/productDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/statesDB.dart';
import 'package:ckrossFlutter/models/dealerso_data.dart';
import 'package:ckrossFlutter/models/make_dealer_data.dart';
import 'package:ckrossFlutter/models/states.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show Platform;

class DropDownFile extends StatefulWidget {
  // State<StatefulWidget> createState() {
  //   return DropDown();
  // }
  State<DropDownFile> createState() => new DropDown();
}
class DropDown  extends State<DropDownFile> {
    static String userRole = "";
    static String currentProductName = "";
    static String currentProductID = "";
    var activePlaceholderColor;
    var placeholderColor;
    var placeholder;
  String text;
  var keyboard;
  bool isEnabled;
bool isData = false;
   bool isValid = true;
  
  
   
 // var dropdown;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return null;
 
  }
 
 
  init(){
    super.initState();
    
    getspvalues();
  }
  getspvalues() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    userRole=sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
  }
createDropDown(subProcessField, applicantJsonString)  {
  var subProcessFieldValue=subProcessField["value"];
    print("Start of CreateDropdown()");
            print(applicantJsonString);

    InitializeAppData();
    String classId = subProcessField["key"];
    String textColor = Colors.black.toString();
    int fontSize = 18;
     List<String> _items = [""].toList();
       
var _selection ;
  //  if (subProcessField.Key.ToLower().Equals("product"))
  //           {
  //               xfx_entry.StyleId = "Make";
  //           }
  //           else if (subProcessField.Key.ToLower().Equals("make"))
  //           {
  //               xfx_entry.StyleId = "Model_Variant";
  //           }
  //           else if (subProcessField.Key.ToLower().Equals("model_variant"))
  //           {
  //               xfx_entry.StyleId = "orp";
  //           }
  //           else
  //           {
  //               xfx_entry.StyleId = "";
  //           }
   if (Platform.isAndroid)
            {
                if (subProcessField["isMandatory"])
                {
                    activePlaceholderColor = Colors.red.toString();
                    placeholderColor = Colors.red.toString();
                }
                else
                {
                    activePlaceholderColor = Colors.grey[500].toString();
                    placeholderColor = Colors.grey[500].toString();
                }
            }
            else if (Platform.isIOS)
            {
                if (subProcessField.IsMandatory)
                {
                    placeholderColor = Colors.red.toString();;
                    activePlaceholderColor = Colors.red.toString();
                }
                else
                {
                    activePlaceholderColor = Colors.grey[500].toString();
                    placeholderColor = Colors.grey[500].toString();
                }
            }
 try
            {
               // JObject jsonObject = JObject.Parse(applicant);
               // String text = (string)jsonObject.SelectToken(subProcessField.Key);
                if (subProcessField != null && !(subProcessField==""))
                { print("itttttt");
                text = subProcessField["key"];
                    var user = <String,dynamic>{};
                    user.addAll(subProcessField);
                   // print(subProcessField);
                  //  print(subProcessField["Dealership_Name"]);
                   // print(user[text]);
                    
                  //  print(user[text]);
                   // Map map = <String,dynamic>{};
                   // map.addAll(subProcessField["key"]);
                  //  print(user[text]);
                  //  print(text);
                    placeholder = subProcessField["lable"];//added by nilima 15 may 2018
                    print("classid"+classId);
                    print(subProcessField["key"]);
                    //Added by Rohit July 22, 2019
                    if (classId=="Dealership_Name")
                    {
                        if (userRole.toLowerCase().startsWith("SO".toLowerCase())  && currentProductID=="1")
                        {
                          print("dealershippppp name");
                          print(subProcessField["Dealership_Name"]);
                          print(subProcessField);

                           // SetDealerData(text, subProcessField);
                        }
                    }
                }
                else
                {
                    placeholder = subProcessField["lable"];
                }
            }
            catch ( e)
            {
                print("Exception 1 in CreateDropDown() : ");
                
                text = "";
                placeholder = subProcessField["lable"];
            }
        print("checking");
        print(subProcessField["validation"]);
      if (subProcessField["validation"]=="Numeric" || subProcessField["validation"]=="Numbers" || subProcessField["validation"]=="Decimal")
            {
              print("Numeric validation");
                keyboard = TextInputType.number;
                print("keyboard"+keyboard);
            }
            else if (subProcessField["validation"]=="alphaNumeric")
            {
              print("alphaNumeric validation");
                keyboard = TextInputType.text;
                print("keyboard"+keyboard.toString());
            }
             if (subProcessField["validationPattern"] != null && !(subProcessField["validationPattern"]==""))
            {
              print(subProcessField["validationPattern"]);
               // xfx_entry.Behaviors.Add(new ValidationPatternBehavior(subProcessField.ValidationPattern, subProcessField.ValidationMessage, subProcessField.Maxlength));
            }
            if (subProcessField["isreadonly"] == "true")
            {

                isEnabled = false;
                //stackLayout.IsEnabled = false;
            }
            print("validation...");
            isEnabled = false;
   // if (stackLayout.IsEnabled == true)
    //        { 
                //   stackLayout.GestureRecognizers.Add(new TapGestureRecognizer()
                // {
                    //   Command = new Command(async () =>
                    // {
                      ondropdownSelect()    async {

                   
                          try
                        {
                            if (subProcessField["value"].toString().contains("["))
                            {
                                subProcessField["value"] = subProcessField["value"].toString().replaceAll("[", "");
                            }
                            if (subProcessField["value"].toString().contains("]"))
                            {
                                subProcessField["value"] = subProcessField["value"].toString().replaceAll("]", "");
                            }
                            if (subProcessField["value"].toString().contains("\""))
                            {
                                subProcessField["value"] = subProcessField["value"].toString().replaceAll("\"", "");
                            }
                            if (subProcessField["value"].toString().contains("\n"))
                            {
                                subProcessField["value"] = subProcessField["value"].toString().replaceAll("\n", "");

                            }
                            print("objectList before");
                            
                            List<String> objectList = subProcessField["value"].toString().split(',').toList();
                           print(objectList.toString());
                            if (objectList.length == 1 && objectList.contains(""))
                            {
                                objectList.remove("");
                            }
                            // objectList = objectList.Select(x => x.Trim()).ToArray().ToList();
                           // SharedPreferences sp = await SharedPreferences.getInstance();
                            //var userRole = Application.Current.Properties[AppConstants.LOGGED_IN_USER_ROLE];
                            //var userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE)
                            print("classId"+classId);
                             if (classId=="Product")
                            {
                                try
                                {
                                   // StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                                    //arrayListpicker = new List<XfxEntry>();
                                   // FindAllddlorp(mainParentLayout, "Make");
                                    //XfxEntry orpXfxEntry3 = arrayListpicker[0];
                                    //orpXfxEntry3.Text = "";

                                    // arrayListpicker = new List<XfxEntry>();
                                    // FindAllddlorp(mainParentLayout, "Model_Variant");
                                    // XfxEntry orpXfxEntry = arrayListpicker[0];
                                    // orpXfxEntry.Text = "";

                                    // arrayListpicker = new List<XfxEntry>();
                                    // FindAllddlorp(mainParentLayout, "orp");
                                    // XfxEntry orpXfxEntry1 = arrayListpicker[0];
                                    // orpXfxEntry1.Text = "";
                                }
                                catch ( e)
                                {
                                   print("exception in branch id prduct");
                                }
                            }
                           print("iserrole"+userRole);
                              if (objectList.length == 0 && !userRole.toString().toLowerCase().startsWith("VD".toLowerCase()))
                            //if (objectList.Count == 0 && currentProductID.Equals("1"))
                            {
                              print("startsWithV");
                                if (objectList.length == 0 && currentProductID=="2")
                                {
                                  print("currentProductID==");
                        //             //get data to display in popup list
                        //             //check on Dealer_Type is LAV or UsedTW or P2P as told by Shweta on 04th July, 2019
                        //             if (subProcessField.Key.ToLower().Equals("dealership_name"))
                        //             {
                        //                 //StackLayout mainParentLayout1 = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
                        //                 //arrayListpicker = new List<XfxEntry>();
                        //                 //FindAllddlorp(mainParentLayout1, "Dealership_Name");
                        //                 //XfxEntry orpXfxEntry3 = arrayListpicker[0];

                        //                 CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
                        //                 List<DealerSO> dealerList = (List<DealerSO>)await cKrossStoreDealerSO.GetAllDataAsync();

                        //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout, "LoanType");
                        //                 XfxEntry xfxEntry = arrayListpicker[0];
                        //                 string LoanType = xfxEntry.Text;

                        //                 if (!String.IsNullOrEmpty(LoanType))
                        //                 {
                        //                     if (LoanType.ToLower().Equals("Used TW".ToLower()) || LoanType.ToLower().Equals("P2P".ToLower()))
                        //                     {
                        //                         LoanType = "Used TW";
                        //                     }

                        //                     for (int i = 0; i < dealerList.Count; i++)
                        //                     {
                        //                         string dealershipName = dealerList[i].dealership_name;

                        //                         DealerSO dealerSO = dealerList[i];
                        //                         JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
                        //                         if (dealerObject.ContainsKey("Dealer_Type"))
                        //                         {
                        //                             string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
                        //                             if (DealerType.ToLower().Equals(LoanType.ToLower()))
                        //                             {
                        //                                 objectList.Add(dealershipName);
                        //                             }
                        //                         }
                        //                     }

                        //                     if (objectList.Count == 0)
                        //                     {
                        //                         await _page.DisplayAlert("Alert", "No Dealers Found", "Ok");
                        //                     }
                        //                 }
                        //                 else
                        //                 {
                        //                     await _page.DisplayAlert("Alert", "Please Select Loan Type", "Ok");
                        //                 }
                        //             }
                        //             else if (subProcessField.Key.ToLower().Equals("dealer_State".ToLower()))
                        //             {
                        //                 //get from state database table
                        //                 CKrossStore<States> cKrossStates = new SQLiteState(DependencyService.Get<ISQLiteDb>());
                        //                 List<States> stateList = (List<States>)await cKrossStates.GetAllDataAsync();
                        //                 //dataList = stateList;
                        //                 for (int i = 0; i < stateList.Count; i++)
                        //                 {
                        //                     string stateName = stateList[i].state_name;
                        //                     objectList.Add(stateName);
                        //                 }
                        //                 dataList = stateList;

                        //                 if (currentProductID.Equals("2"))
                        //                 {
                        //                     if (stateList.Count == 0 && subProcessField.isMobileInputRequired)
                        //                     {
                        //                         await doCheckIsValid(parent, subProcessField);
                        //                     }
                        //                 }
                        //                 else if (currentProductID.Equals("1"))
                        //                 {
                        //                     if (stateList.Count == 0)
                        //                     {
                        //                         await _page.DisplayAlert("Alert", "No Dealer States Found", "Ok");
                        //                     }
                        //                 }
                        //             }
                        //             else if (subProcessField.Key.ToLower().Equals("Make".ToLower()))
                        //             {
                        //                 //get from make database table
                        //                 CKrossStore<Make> cKrossMake = new SQLiteMake(DependencyService.Get<ISQLiteDb>());
                        //                 List<Make> makeList = (List<Make>)await cKrossMake.GetAllDataAsync();
                        //                 for (int i = 0; i < makeList.Count; i++)
                        //                 {
                        //                     string makeName = makeList[i].make_name;
                        //                     objectList.Add(makeName);
                        //                 }
                        //                 dataList = makeList;
                        //             }
                        //             else if (subProcessField.Key.ToLower().Equals("Model_Variant".ToLower()))
                        //             {
                        //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout, "Make");
                        //                 var selectedMakeId = arrayListpicker[0].StyleId;

                        //                 if (!selectedMakeId.ToLower().Equals("Make".ToLower()))
                        //                 {
                        //                     string query = DatabaseQueries.GetModelVDNameByMakeId(selectedMakeId);
                        //                     CKrossStore<ModelVD> cKrossStoreModelVD = new SQLiteModelVD(DependencyService.Get<ISQLiteDb>());
                        //                     List<ModelVD> modelVDList = await cKrossStoreModelVD.ExecuteQueryAsync(query);

                        //                     for (int i = 0; i < modelVDList.Count; i++)
                        //                     {
                        //                         string modelName = modelVDList[i].model_name;
                        //                         objectList.Add(modelName);
                        //                     }
                        //                     dataList = modelVDList;

                        //                     if (modelVDList.Count == 0 && subProcessField.isMobileInputRequired)
                        //                     {
                        //                         doCheckIsValid(parent, subProcessField);
                        //                     }
                        //                 }
                        //             }
                        //             else if (subProcessField.Key.ToLower().Equals("year_of_manufacturing".ToLower()))
                        //             {
                        //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout, "Model_Variant");
                        //                 var selectedModelId = arrayListpicker[0].StyleId;

                        //                 if (!selectedModelId.ToLower().Equals("Model_Variant".ToLower()))
                        //                 {
                        //                     CKrossStore<YearVD> cKrossStoreYearVD = new SQLiteYearVD(DependencyService.Get<ISQLiteDb>());
                        //                     string query = DatabaseQueries.GetYearVDByModelId(selectedModelId);
                        //                     List<YearVD> yearVDList = await cKrossStoreYearVD.ExecuteQueryAsync(query);
                        //                     for (int i = 0; i < yearVDList.Count; i++)
                        //                     {
                        //                         string year = yearVDList[i].year;
                        //                         if (!objectList.Contains(year))
                        //                         {
                        //                             objectList.Add(year);
                        //                         }
                        //                         //objectList.Add(year);
                        //                     }
                        //                     dataList = yearVDList;

                        //                     if (yearVDList.Count == 0 && subProcessField.isMobileInputRequired)
                        //                     {
                        //                         doCheckIsValid(parent, subProcessField);
                        //                     }
                        //                 }
                        //             }
                        //             else if (subProcessField.Key.ToLower().Equals("dealerBranch".ToLower()))
                        //             {
                        //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout, "dealerBranch");
                        //                 CKrossStore<City> cKrossStorecity = new SQLiteCity(DependencyService.Get<ISQLiteDb>());
                        //                 string SQLquery = DatabaseQueries.GetCityNames();
                        //                 List<City> cityList = await cKrossStorecity.ExecuteQueryAsync(SQLquery);
                        //                 for (int i = 0; i < cityList.Count; i++)
                        //                 {
                        //                     string year = cityList[i].city_name;
                        //                     objectList.Add(year);
                        //                 }
                        //                 dataList = cityList;
                        //             }
                        //             //this else added on 22 Aug, 2019 -- addtion of MainDealer Dropdown
                        //             else if (subProcessField.Key.ToLower().Equals("Main_DealershipName".ToLower()))
                        //             {
                        //                 CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
                        //                 List<DealerSO> dealerList = (List<DealerSO>)await cKrossStoreDealerSO.GetAllDataAsync();

                        //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout, "LoanType");
                        //                 XfxEntry xfxEntry = arrayListpicker[0];
                        //                 string LoanType = xfxEntry.Text;

                        //                 if (!String.IsNullOrEmpty(LoanType))
                        //                 {
                        //                     if (LoanType.ToLower().Equals("P2P".ToLower()))
                        //                     {
                        //                         LoanType = "Used TW";
                        //                     } 

                        //                     for (int i = 0; i < dealerList.Count; i++)
                        //                     {
                        //                         string dealershipName = dealerList[i].dealership_name;

                        //                         DealerSO dealerSO = dealerList[i];
                        //                         JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
                        //                         if (dealerObject.ContainsKey("Dealer_Type"))
                        //                         {
                        //                             string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
                        //                             if (DealerType.ToLower().Equals(LoanType.ToLower()))
                        //                             {
                        //                                 string DealerCategorization = dealerObject.SelectToken("Dealer_Categorisation").ToString();
                        //                                 if (DealerCategorization.ToLower().Equals("Main Dealer".ToLower()))
                        //                                 {
                        //                                     objectList.Add(dealershipName);
                        //                                 }
                        //                             }
                        //                         }
                        //                     }

                        //                     if (objectList.Count == 0)
                        //                     {
                        //                         await _page.DisplayAlert("Alert", "No Dealers Found", "Ok");
                        //                     }
                        //                 }
                        //                 else
                        //                 {
                        //                     await _page.DisplayAlert("Alert", "Please Select Loan Type", "Ok");
                        //                 }
                        //             }
                        //             //this else added on 29nd Aug, 2019 -- addtion of SubLoanType Dropdown
                        //             else if (subProcessField.Key.ToLower().Equals("subLoanType".ToLower()))
                        //             {
                        //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout, "LoanType");
                        //                 XfxEntry xfxEntry = arrayListpicker[0];
                        //                 string LoanType = xfxEntry.Text;

                        //                 if (!String.IsNullOrEmpty(LoanType))
                        //                 {
                        //                     CKrossStore<SubLoanType> cKrossStore = new SQLiteSubLoanType(DependencyService.Get<ISQLiteDb>());
                        //                     string query = DatabaseQueries.GetSubLoanTypes(LoanType);
                        //                     List<SubLoanType> subLoanList = (List<SubLoanType>)await cKrossStore.GetAllDataAsync();

                        //                     for (int i = 0; i < subLoanList.Count; i++)
                        //                     {
                        //                         string type = subLoanList[i].sub_loan_type;
                        //                         objectList.Add(type);
                        //                     }       

                        //                     if (objectList.Count == 0)
                        //                     {
												// if (_page != null)
												// {
												// 	await _page.DisplayAlert("Alert", "No Sub Loan Type Found", "Ok");
												// }
                        //                         else
												// {
												//     DependencyService.Get<IToastInterface>().ShortAlert("No Sub Loan Type Found");
												// }
                        //                     }
                        //                 }
                        //                 else
                        //                 {
                        //                     await _page.DisplayAlert("Alert", "Please Select Loan Type", "Ok");
                        //                 }
                        //             }
                                 }
                                else if (objectList.length == 0 && currentProductID=="1")
                                {
                                  print("currentProductID111111111");
                        //             CKrossStore<Product> cKrossStoreProduct = new SQLiteProduct(DependencyService.Get<ISQLiteDb>());
                        //             CKrossStore<Make> cKrossStoreMake = new SQLiteMake(DependencyService.Get<ISQLiteDb>());
                        //             CKrossStore<Model> cKrossStoreModel = new SQLiteModel(DependencyService.Get<ISQLiteDb>());
                        //             StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //             arrayListpicker = new List<XfxEntry>();
                        //             FindAllddlnew(mainParentLayout, subProcessField, xfx_entry.ClassId);
                        //             if (arrayListpicker.Count > 0)
                        //             {
                        //                 XfxEntry parenXfxEntry = arrayListpicker[0];


                        //                 if (parenXfxEntry.StyleId.ToLower().Equals("make"))
                        //                 {
                        //                     if (!(string.IsNullOrEmpty(parenXfxEntry.Text)))
                        //                     {
                                                 String queryGetProduct = DatabaseQueries.getproduct("NewTW");
                                                 Future<List> productlistddl ;
                                                 ProductDataDB.executeQuery(queryGetProduct).then((val){
                                                 print("productlistddl");
                                                 print(productlistddl.toString());   
                                                 });
                                                
                        //                         if (productlistddl.Count > 0)
                        //                         {
                        //                             Product product = productlistddl[0];
                        //                             string selectedpid = product.product_id.ToString();
                        //                             // string queryGetMake = DatabaseQueries.getmakebyproductid(selectedpid);
                                                     String queryGetMake = DatabaseQueries.getmakebyproductid("2");//added by nilima on 24 july 2018
                                                        print("queryGetMake");
                                                       
                                                   Future<List> makelistddl ;
                                                   MakeDB.executeQuery(queryGetMake).then((val){
                                                   print(makelistddl);  
                                                   });
                                                   
                        //                             objectList = new List<string>();
                        //                             for (int i = 0; i < makelistddl.Count; i++)
                        //                             {
                                                            List makename = ["New TW"];
                        //                                 string makename = makelistddl[i].make_name;
                        //                                 if (!objectList.Contains(makename))
                        //                                 {
                                                              //  return  makename
                                                              //   .map((user) => DropdownMenuItem<String>(
                                                              //         child: Text(user),
                                                              //         value: user,
                                                              //       ))
                                                              //   .toList();
                        //                                     objectList.Add(makename);
                        //                                 }
                        //                             }
                        //                         }
                        //                         else
                        //                         {
                        //                             await _page.DisplayAlert("Ckross", "Make List Empty", "Ok");
                        //                         }
                        //                     }
                        //                     else
                        //                     {
                        //                         await _page.DisplayAlert("Ckross", "Please Select Product", "Ok");
                        //                     }
                        //                 }
                        //                 else if (parenXfxEntry.StyleId.ToLower().Equals("model_variant"))
                        //                 {
                        //                     if (!(string.IsNullOrEmpty(parenXfxEntry.Text)))
                        //                     {
                        //                         string queryGetMakebyMakeName = DatabaseQueries.getmake(parenXfxEntry.Text.Trim());
                        //                         List<Make> makelistddlByMakeName = await cKrossStoreMake.ExecuteQueryAsync(queryGetMakebyMakeName);
                        //                         Make make = makelistddlByMakeName[0];
                        //                         string selectedmkid = make.make_id.ToString();
                        //                         string queryGetModel = DatabaseQueries.getmodelbymakeid(selectedmkid);
                        //                         List<Model> modellistddl = await cKrossStoreModel.ExecuteQueryAsync(queryGetModel);
                        //                         objectList = new List<string>();
                        //                         for (int i = 0; i < modellistddl.Count; i++)
                        //                         {
                        //                             string modelname = modellistddl[i].model_name;
                        //                             if (!objectList.Contains(modelname))
                        //                             {
                        //                                 objectList.Add(modelname);
                        //                             }
                        //                             //objectList.Add(modelname);
                        //                         }
                        //                     }
                        //                     else
                        //                     {
                        //                         await _page.DisplayAlert("Ckross", "Please Select Make", "Ok");

                        //                     }
                        //                 }
                        //             }
                        print("111111111");
                        print(subProcessField["key"]);
                                     if (subProcessField["key"]=="Dealership_Name")
                                    {
                                            print("DealerSODBkeygetDat");

                                        //CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
                                        List dealerList =  await DealerSODB.getData();
                                       
                                        for (int i = 0; i < dealerList.length; i++)
                                        {
                                          Map map = {};
                                          map.addAll(dealerList[i]);
                                    //       print("dealerrrrr");
                                           var dealershipName = map["dealership_name"];
                                           var type = map["Dealer_Type"];
                                           print(dealershipName);
                                          // MakeDealerDataDB obj = map["dealership_data"];
                                          // print(obj.dealer_Type);
                                          //  final parsed = jsonDecode(map["dealership_data"]).cast<Map<String, dynamic>>();
                                          //  print(parsed);
                                           var  maps=jsonEncode(map["dealership_data"]);
                                           print("mappppp");
                                          // var abc= map["dealership_data"];
                                           var pqr = Map<String, dynamic>.from(jsonDecode(map["dealership_data"]));
                                           print(pqr);
                                         
                                        //     if (dealerObject.ContainsKey("Dealer_Type"))
                                        //     {
                                        //         string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
                                        //         if (DealerType.ToLower().Equals("New TW".ToLower()))
                                        //         {
                                        //             objectList.Add(dealershipName);
                                        //         }
                                        //     }
                                         }
                                    }
                                    //following all else if added for UTW for SO login by Rohit on 1st July, 2019
                        //             else if (subProcessField.Key.ToLower().Equals("dealer_State".ToLower()))
                        //             {
                        //                 //get from state database table
                        //                 CKrossStore<States> cKrossStates = new SQLiteState(DependencyService.Get<ISQLiteDb>());
                        //                 List<States> stateList = (List<States>)await cKrossStates.GetAllDataAsync();
                        //                 //dataList = stateList;
                        //                 for (int i = 0; i < stateList.Count; i++)
                        //                 {
                        //                     string stateName = stateList[i].state_name;
                        //                     objectList.Add(stateName);
                        //                 }
                        //                 dataList = stateList;

                        //                 if (currentProductID.Equals("2"))
                        //                 {
                        //                     if (stateList.Count == 0 && subProcessField.isMobileInputRequired)
                        //                     {
                        //                         doCheckIsValid(parent, subProcessField);
                        //                     }
                        //                 }
                        //                 else if (currentProductID.Equals("1"))
                        //                 {
                        //                     if (stateList.Count == 0)
                        //                     {
                        //                         _page.DisplayAlert("Alert", "No Dealer States Found", "Ok");
                        //                     }
                        //                 }
                        //             }
                        //             else if (subProcessField.Key.ToLower().Equals("year_of_manufacturing".ToLower()))
                        //             {
                        //                 StackLayout mainParentLayout1 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout1, "Model_Variant");
                        //                 var selectedModelId = arrayListpicker[0].StyleId;

                        //                 if (!selectedModelId.ToLower().Equals("Model_Variant".ToLower()))
                        //                 {
                        //                     CKrossStore<YearVD> cKrossStoreYearVD = new SQLiteYearVD(DependencyService.Get<ISQLiteDb>());
                        //                     string query = DatabaseQueries.GetYearVDByModelId(selectedModelId);
                        //                     List<YearVD> yearVDList = await cKrossStoreYearVD.ExecuteQueryAsync(query);
                        //                     for (int i = 0; i < yearVDList.Count; i++)
                        //                     {
                        //                         string year = yearVDList[i].year;
                        //                         if (!objectList.Contains(year))
                        //                         {
                        //                             objectList.Add(year);
                        //                         }
                        //                         //objectList.Add(year);
                        //                     }
                        //                     dataList = yearVDList;

                        //                     if (yearVDList.Count == 0 && subProcessField.isMobileInputRequired)
                        //                     {
                        //                         await doCheckIsValid(parent, subProcessField);
                        //                     }
                        //                 }
                        //             }
                        //             else if (subProcessField.Key.ToLower().Equals("dealerBranch".ToLower()))
                        //             {
                        //                 StackLayout mainParentLayout2 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout2, "dealerBranch");
                        //                 CKrossStore<City> cKrossStorecity = new SQLiteCity(DependencyService.Get<ISQLiteDb>());
                        //                 string SQLquery = DatabaseQueries.GetCityNames();
                        //                 List<City> cityList = await cKrossStorecity.ExecuteQueryAsync(SQLquery);
                        //                 for (int i = 0; i < cityList.Count; i++)
                        //                 {
                        //                     string year = cityList[i].city_name;
                        //                     objectList.Add(year);
                        //                 }
                        //                 dataList = cityList;
                        //             }
                        //             //this else added on 22nd Aug, 2019 -- addtion of MainDealer Dropdown
                        //             else if (subProcessField.Key.ToLower().Equals("Main_DealershipName".ToLower()))
                        //             {
                        //                 if (subProcessField.dependentFieldMobList != null)
                        //                 {
                        //                     StackLayout mainParentLayout2 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //                     ClearDependantFields(mainParentLayout2, subProcessField.dependentFieldMobList);
                        //                 }

                        //                 CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
                        //                 List<DealerSO> dealerList = (List<DealerSO>)await cKrossStoreDealerSO.GetAllDataAsync();
                        //                 for (int i = 0; i < dealerList.Count; i++)
                        //                 {
                        //                     string dealershipName = dealerList[i].dealership_name;

                        //                     DealerSO dealerSO = dealerList[i];
                        //                     JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
                        //                     if (dealerObject.ContainsKey("Dealer_Type"))
                        //                     {
                        //                         string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
                        //                         if (DealerType.ToLower().Equals("New TW".ToLower()))
                        //                         {
                        //                             string DealerCategorization = dealerObject.SelectToken("Dealer_Categorisation").ToString();
                        //                             //code as per shweta told for adding iaActive condition
                        //                             if (dealerObject.ContainsKey("isActive"))
                        //                             {
                        //                                 bool isActive = (bool)dealerObject.SelectToken("isActive");
                        //                                 if (isActive)
                        //                                 {
                        //                                     if (DealerCategorization.ToLower().Equals("Main Dealer".ToLower()))
                        //                                     {
                        //                                         objectList.Add(dealershipName);
                        //                                     }
                        //                                 }
                        //                             }
                        //                         }
                        //                     }
                        //                 }
                        //             }
                        //             //this else added on 29nd Aug, 2019 -- addtion of MainDealerCode Dropdown
                        //             else if (subProcessField.Key.ToLower().Equals("Main_DealerCode".ToLower()))
                        //             {
                        //                 StackLayout mainParentLayout2 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
                        //                 arrayListpicker = new List<XfxEntry>();
                        //                 FindAllddlorp(mainParentLayout2, "Main_DealershipName");

                        //                 if (arrayListpicker.Count > 0)
                        //                 {
                        //                     XfxEntry parenXfxEntry = arrayListpicker[0];
                        //                     string Main_DealershipName = parenXfxEntry.Text.Trim();

                        //                     CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
                        //                     string query = DatabaseQueries.GetDealershipDataByName(Main_DealershipName);
                        //                     List<DealerSO> dealerList = await cKrossStoreDealerSO.ExecuteQueryAsync(query);

                        //                     for (int i = 0; i < dealerList.Count; i++)
                        //                     {
                        //                         DealerSO dealerSO = dealerList[i];
                        //                         JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
                        //                         if (dealerObject.ContainsKey("Dealer_Type"))
                        //                         {
                        //                             string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
                        //                             if (DealerType.ToLower().Equals("New TW".ToLower()))
                        //                             {
                        //                                 string DealerCategorization = dealerObject.SelectToken("Dealer_Categorisation").ToString();
                        //                                 if (DealerCategorization.ToLower().Equals("Main Dealer".ToLower()))
                        //                                 {
                        //                                     string DealerCode = dealerObject.SelectToken("Dealer_Code").ToString();
                        //                                     objectList.Add(DealerCode);
                        //                                 }
                        //                             }
                        //                         }
                        //                     }                                          
                        //                 }
                        //             }
                                 }
                             }







                                     print("objectList@@@@@@@@@");
                                    print(objectList);
                                     if (objectList != null && objectList.length > 0)
                            {
                              print("dfgdfgdfgdfgdf");
                              print(currentProductID);
                                //if (userRole.toString().toLowerCase().startsWith("VD".toLowerCase()))
                                if (currentProductID=="2")
                                {
                                    if (isValid)
                                    {
                                             
                                       // showPopup(objectList, xfx_entry, subProcessField.Lable, stackLayout, subProcessField, parent, dataList);
                                    }
                                    isValid = true;
                                }
                                else
                                {
                                    if (userRole.toString().toLowerCase().startsWith("VD".toLowerCase()))
                                    {
                                        if (isValid)
                                        {
                                          print("in vd");
                                           // showPopup(objectList, xfx_entry, subProcessField.Lable, stackLayout, subProcessField, parent, dataList);
                                        }
                                        isValid = true;
                                    }
                                    else
                                    {
                                      
                                       if(mounted){
                                         setState(() {
                                    isData=true;
                                           
                                         });
                                       }
                                       
                                     //});
                                      //obj = objectList;
                                    return  objectList
                                    .map((user) => DropdownMenuItem<String>(
                                          child: Text(user),
                                          value: user,
                                        ))
                                    .toList();
                                     
                                       // ShowPicker(objectList, xfx_entry, subProcessField.Lable, stackLayout, subProcessField, parent);
                                    }
                          //     }
                       //    }
                    //    }
                                 }
                             }
                        }
                      
                        catch(e){
                          print(e);
                        }
                         }
        if (subProcessField["key"]=="Main_DealershipName")
            {
              print("applicantJsonString");
              print(applicantJsonString);
                if (applicantJsonString == null || applicantJsonString=="{}")
                {
                    isEnabled = false;
                }
                else
                {
                   // JObject jsonObject = JObject.Parse(applicant);
                 //   String dealerType = (string)jsonObject.SelectToken("Dealer_Categorisation");
                    // if (dealerType != null && !String.IsNullOrEmpty(dealerType))
                    // {
                    //     if (dealerType.Equals("MBO"))
                    //     {
                    //         stackLayout.IsEnabled = true;
                    //     }
                    //     else
                    //     {
                    //         stackLayout.IsEnabled = false;
                    //     }
                    // }
                    // else
                    // {
                    //     stackLayout.IsEnabled = false;
                    // }
                }
            }
                     // }
                   // });
                // }
             
    //        }
   
  // _selection = _items.first;
 // _selection = obj[0];
 showitems(){
   ondropdownSelect();
 }
    var dropdown = DropdownButtonFormField<String>(
      hint:Text(placeholder,style: TextStyle(color:Colors.red),),
      iconEnabledColor: Colors.red,
      decoration: new InputDecoration(
        enabledBorder: UnderlineInputBorder(      
                            borderSide: BorderSide(color: Colors.red),   
                            ),
      ),
        value: _selection,
        items: showitems(),
        
        onChanged: (String s) {
          print(s);
          if(mounted){
            print(mounted);
            setState(() {
                        _selection = s;
            });
         
          }          //  ondropdownSelect();
         
        }
    );
       
    
    // var dropdown= TextFormField(
            
    //          // controller: lanController,
    //           decoration: InputDecoration(
    //             labelText: placeholder,
    //          //  border: OutlineInputBorder(),
    //             labelStyle: TextStyle(color: Colors.red),
    //             contentPadding: EdgeInsets.only(top: 15, left: 0, right: 5),
    //             suffixIcon: IconButton(
    //               icon: Icon(Icons.arrow_drop_down),
    //             ),
    //           ),
    //           showCursor: false,
    //           readOnly: true,
    //           style: TextStyle(fontSize: 18),
    //           onTap: () => {
    //             // _showDialogLanList(
    //             //     context, 'Select Loan Account Number', _allLanList)
    //           },
              
    //           validator: (value) {
    //             if (value.isEmpty || value.length == 0) {
    //               return 'Please select Loan Account Number';
    //             }
    //             return null;
    //           },
    //         );
             return dropdown;

}

  InitializeAppData()
       async {
            try
            {
                print(" In InitializeAppData");
                SharedPreferences sp = await SharedPreferences.getInstance();
                userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
                currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
                currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
            }
            catch ( e)
            { 
                print("Exception In InitializeAppData " + e.Message);
            }
        }

           SetDealerData(String dealerName,  subProcessField)
       async {
             print("in SetDealerData method");
            try
            {
                //CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
               print(dealerName);
                String query = DatabaseQueries.GetDealershipDataByName(dealerName);
                print(query);
                List dealerSOList = await DealerSODB.executeClientTemplateData(query);
                print("LIsted DealerSODB");
                print(dealerSOList);
              //   if (dealerSOList.Count > 0)
              //   {
              //       DealerSO dealerSO = dealerSOList[0];
              //       JObject dealerObject = JObject.Parse(dealerSO.dealership_data);

              //       await UITemplateOperations.InitDatabase();
              //       await UITemplateOperations.TruncateMakeDealerDataTable();
              //       await SaveMakeFromDealerData(dealerObject);
              //  }
            }
            catch ( e)
            {
                print("Exception in SetDealerData() : $e");
                
            }
             
        }
       
       
}

class dataTable{
   String dealer_location_id ;
  String dealer_name ;
  String location;
  String branch;
  String dealer_code;
  String vehicle_city;

  dataTable(this.dealer_location_id,this.dealer_name,this.location);


  Map<String,dynamic> toMap(){
    var map = Map<String, dynamic>();

   // map['dealer_location_id'] = dealer_location_id;
    map['dealer_name'] = dealer_name;
    map['location'] = location;
    

    return map;
  }

  dataTable.fromMapObject(Map<String,dynamic>map){

    this.dealer_location_id= map['dealer_location_id'];
    this.dealer_name = map['dealer_name'];
    this.location = map['location'];
   

  }
}