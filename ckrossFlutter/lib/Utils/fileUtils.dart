import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as path;

class FileUtils {
  static Future<String> get _localPath async {
    final directory = '/storage/emulated/0/CKross_Wheels_EMI/Logs';
    return directory;
  }

  static Future<File> get _localFile async {
    final path = await _localPath;
    var now = new DateTime.now();
    print(now.day);
    //var androidInfo = await DeviceInfoPlugin().androidInfo;
    var day = DateFormat('EEEE').format(now);
    String date = new DateFormat("dd-MM-yyyy").format(now);
    var filePath = '$path/Log$date.txt';
    return File(filePath);
  }
  static Future<String> getFileNameWithExtension(File file)async{

    if(await file.exists()){
      //To get file name without extension
      return path.basenameWithoutExtension(file.path);

//      return file with file extension
//      return path.basename(file.path);
    }else{
      return null;
    }
  }

  static Future<List<File>> getFile() async {
    var dir = new Directory('/storage/emulated/0/CKross_Wheels_EMI/Logs');
    List contents = dir.listSync();
    print("print start file");
    print(dir);
    print(contents);
    List<File> fileList = new List<File>();
    for (var fileOrDir in contents) {
      if (fileOrDir is File) {
        print("dile or dir");
        print(fileOrDir);
        String filename = await FileUtils.getFileNameWithExtension(fileOrDir);
        print(filename);
        String dateInString = filename.replaceAll('Log','');
        print("dateIN string...");
        print(dateInString);
        DateTime date = new DateFormat("dd-MM-yyyy").parse(dateInString);
        DateTime currentDate = DateTime.now();
        print("before if");
        print(currentDate);
        print(date);

        int diffDays = DateTime.now().difference(date).inDays;

          print("in current if date");
          var current = new DateTime( currentDate.year, currentDate.month , currentDate.day );
          print(current);
          var dayBefore = new DateTime( currentDate.year, currentDate.month , currentDate.day - 1);
          print(dayBefore);
          var oneDayBefore = new DateTime( currentDate.year, currentDate.month , currentDate.day - 2);
          print(oneDayBefore);
         var DayBefore = new DateTime( currentDate.year, currentDate.month , currentDate.day - 3);
          print(oneDayBefore);
          print("end");

        print("date before if");
        print(date);
            if (current.compareTo(date) == 0) {
              final file = await fileOrDir;
              fileList.add(file);
              print("file i");
              //return file;
            }
            else if (dayBefore.compareTo(date) == 0) {
              final file = await fileOrDir;
              fileList.add(file);
              print("file ii");
              //return file;
            }
            else if (oneDayBefore.compareTo(date) == 0) {
              final file = await fileOrDir;
              fileList.add(file);
              print("file iii");
              //return file;
            }else{
              print("no file found");
            }

          print("date after if");
          print(date);

      }
    }
    print("end loop");
    return fileList;

  }

  static Future<String> readFile() async {
    try {
      final file = await _localFile;
      String content = await file.readAsString();
      return content;
    } catch (e) {
      return '';
    }
  }

  static Future<File> writeFile(String text) async {
    final file = await _localFile;
    print("fileeee"+file.toString());
  
    return file.writeAsString('$text\r\n', mode: FileMode.append, flush: true);
  }

  static Future<File> addLog(String Tag, String text) async {
    String content = await FileUtils.readFile();
    if (content != '') {
      final file = await _localFile;
      var now = new DateTime.now();
      String date = new DateFormat("dd-MM-yyyy").format(now);
      String time = new DateFormat("H:m:s").format(now);
      return file.writeAsString('$Tag  $date  $time  $text\r\n',
          mode: FileMode.append, flush: true);
    }
  }

  static Future<File> printLog(String text) async {
    String content = await FileUtils.readFile();
    if (content != '') {
      final file = await _localFile;
      var now = new DateTime.now();
      String date = new DateFormat("dd-MM-yyyy").format(now);
      String time = new DateFormat("H:m:s").format(now);
      return file.writeAsString('$date  $time  $text\r\n\n\n',
          mode: FileMode.append, flush: true);
    }
  }

  static Future<File> cleanFile() async {
    final file = await _localFile;
    return file.writeAsString('');
  }

  static Future<String> createFolderInAppDocDir(String folderName, var logFile) async {
    try {//Get this App Document Directory
      final String _appDocDir = '/storage/emulated/0/CKross_Wheels_EMI';
      //App Document Directory + folder name
      final Directory _appDocDirFolder = Directory('${_appDocDir}/$folderName/');

      if (await _appDocDirFolder.exists()) {
        //if folder already exists return path
        final file = await _localFile;
        if (!await file.exists())
          logFile = await _localFile;
        logFile.create();
        return _appDocDirFolder.path;
      } else {
        //if folder not exists create folder and then return its path
        final Directory _appDocDirNewFolder =
        await _appDocDirFolder.create(recursive: true);

        logFile = await _localFile;
        logFile.create();
        return _appDocDirNewFolder.path;
      }
    }catch(e){
      e.printStackTrace();
    }
  }



  static Future<File> deleteLogFiles() async {
    var dir = new Directory('/storage/emulated/0/CKross_Wheels_EMI/Logs');
    List contents = dir.listSync();
    print("print start file");
    print(dir);
    print(contents);
    for (var fileOrDir in contents) {
      if (fileOrDir is File) {
       String filename = await FileUtils.getFileNameWithExtension(fileOrDir);
        print(filename);
       const start = "(";
       const end = ")";


       final startIndex = filename.indexOf(start);

       final endIndex = filename.indexOf(end, startIndex + start.length);
       String dateInString = filename.substring(startIndex + start.length, endIndex);
       DateTime date = new DateFormat("dd-MM-yyyy").parse(dateInString);
       // DateTime date =  DateTime.parse(dateFormat);
       int diffDays = DateTime.now().difference(date).inDays;

      if(diffDays > 5){
        try {
          await fileOrDir.delete();
          print("file delete successfully");
          print(fileOrDir);
        } catch (e) {
          print("error in delete file");
          print(e);
        }
      }
       print(dateInString);
       print("diff date");
       print(diffDays);
        print(fileOrDir);

      } else if (fileOrDir is Directory) {
        print(fileOrDir.path);
      }
    }
   print("print file");
  }


//  static void deleteOldFiles(Directory dir) {
//    var now = new DateTime.now();
//    var currunt_day = now.day;
//    var older_day = DateTime.now().subtract(new Duration(days: 5));
//    print(older_day);
//    dir
//        .list(recursive: true, followLinks: false)
//        .listen((FileSystemEntity entity) {
//      if (entity == null) {
//        print("The Folder is Empty");
//      } else {
//        try {
//          entity.delete();
//          print("Files Deleted");
//        } catch (_) {}
//      }
//    });
//  }

  static Future<File> saveLogFile() async {
    print("save log file");
//    var now = new DateTime.now();
    var androidInfo = await DeviceInfoPlugin().androidInfo;
    var IMEI = await ImeiPlugin.getImei();

    try {
      var logFile;
      final String _appDocDir = '/storage/emulated/0/CKross_Wheels_EMI/Logs';
      //App Document Directory + folder name
      final Directory _appDocDirFolder = Directory('${_appDocDir}');

      if (await _appDocDirFolder.exists()) {
        //if folder already exists return path
        final file = await _localFile;
        if (!await file.exists()) {
          logFile = await _localFile;
          logFile.create();
        }else{
          return null;
        }

      } else {
        //if folder not exists create folder and then return its path
        final Directory _appDocDirNewFolder =
        await _appDocDirFolder.create(recursive: true);
        logFile = await _localFile;
        logFile.create();
      }

      if (Platform.isAndroid) {
        String version = androidInfo.version.release;
        String model = androidInfo.model;
        String manufacturer = androidInfo.manufacturer;
        String sdk = androidInfo.version.sdkInt.toString();
        writeFile(
            '*****************************************************************\nAPI LEVEL:$sdk\n'
            'MANUFACTURER:$manufacturer\nMobile Model No.:$model\nAndroid OS Version:$version\nIMEI NO.:$IMEI\n*****************************************************************');
        /*    writeFile('API LEVEL 			    :$sdk');
        writeFile('MANUFACTURER 		  :$manufacturer');
        writeFile('Mobile Model No.   :$model');
        writeFile('Android OS Version	: $version');
        writeFile('IMEI NO.			      : $IMEI');
        writeFile(
            '*****************************************************************');*/
      }

      if (Platform.isIOS) {
        var iosInfo = await DeviceInfoPlugin().iosInfo;
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString(
            "*****************************************************************",
            mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("App Build Version	:  " + iosInfo.systemName,
            mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("API LEVEL 			: " + iosInfo.systemVersion,
            mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("MANUFACTURER 		: " + iosInfo.name,
            mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("Mobile Model No. 	: " + iosInfo.model,
            mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("IMEI NO.			: " + IMEI, mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
        logFile.writeAsString(
            "*****************************************************************",
            mode: FileMode.append);
        logFile.writeAsString("\n", mode: FileMode.append);
      }
      print("File Written");
      printLog("text");
      return logFile;
    } catch (e) {
      e.printStackTrace();
    }
  }
}
