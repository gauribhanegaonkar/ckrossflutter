
import 'dart:convert';
import 'dart:io';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/screens/leadCreationImagePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image/image.dart' as img;
import 'package:shared_preferences/shared_preferences.dart';


class ImageUtils {
 static int count=0;
  static Future<bool> getAndSaveImageFromCamera(int index ,String imageDirName) async {
   // Navigator.pop(context, true);
    final File imageFile = await ImagePicker.pickImage(source: ImageSource.camera); 
    FileUtils.printLog("Image captured from camera.");
    bool result = await _cropImage(index,imageFile, imageDirName);
    return result;
  }

  static Future<bool> getAndSaveImageFromGallary(int index, String imageDirName) async {
   // Navigator.pop(context, true);
    final File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    FileUtils.printLog("Image picked from gallry.");
   bool result = await _cropImage(index,image,imageDirName);
    return result;
  }

  static Future<bool> _cropImage(int index,File image, String imageDirName) async {
    
    print("in crop image");
    print(imageDirName);
    File imageNew;
    
    var removeSpace = imageDirName.replaceAll(" ", "");
    var imageFinal =removeSpace.replaceAll("/", "_");
    File croppedImage = await ImageCropper.cropImage(
      sourcePath: image.path,
      maxWidth: 1080,
      maxHeight: 1080,
        aspectRatioPresets: Platform.isAndroid
            ? [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ]
            : [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio5x3,
          CropAspectRatioPreset.ratio5x4,
          CropAspectRatioPreset.ratio7x5,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: '',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: '',
        )  );
    if (croppedImage != null) {
      imageNew = croppedImage;
      FileUtils.printLog("Image cropped.");
    }

      final String directory = '/storage/emulated/0/CKross_Wheels_EMI/Images/$imageFinal';
      final Directory _appDocDirFolder = Directory('${directory}');
      int counts = await getCount( _appDocDirFolder);
      var now = new DateTime.now();
      String date = new DateFormat("dd-MM-yyyy").format(now);
      String time = new DateFormat("H:m:s").format(now);
     
      if (await _appDocDirFolder.exists()) {
        var result = await FlutterImageCompress.compressAndGetFile(
          croppedImage.absolute.path, '$directory/image.jpg',
          quality: 70,
          rotate: 0,
        );
        imageNew = result;
        FileUtils.printLog("Image compressed.");
      } else {
        final Directory _appDocDirNewFolder =
        await _appDocDirFolder.create(recursive: true);
        var result = await FlutterImageCompress.compressAndGetFile(
          croppedImage.absolute.path, '$directory/image.jpg',
          quality: 70,
          rotate: 0,
        );
        imageNew = result;
        
        FileUtils.printLog("Image compressed.");
      }
      
    if (await _appDocDirFolder.exists()) {
  
      final File newImage = await imageNew.copy('$directory/$imageFinal-$index-$counts-.jpg');
          
          print("image name index");
          //print(newImage);
    } else {
        print("image new image");
      final Directory _appDocDirNewFolder =
      await _appDocDirFolder.create(recursive: true);
      
      final File newImage = await imageNew.copy('$directory/$imageFinal-$index-$counts-.jpg');
    }
    
    var dir = Directory('/storage/emulated/0/CKross_Wheels_EMI/Images/$imageFinal/image.jpg');
    dir.deleteSync(recursive: true);
    FileUtils.printLog("Image saved to directory.");
    //LeadCreationImagePageState.refreshList(context);
    return true;
  }

 static deleteFolders(){
   bool isgetfolder = false;
   print("in delee");
     final dir = Directory('/storage/emulated/0/CKross_Wheels_EMI/Images');

    dir.deleteSync(recursive: true);
    print("Image deleted to directory.");
    isgetfolder = true;
    FileUtils.printLog("Image deleted to directory.");
    return isgetfolder ;
    
  }
  static getCount(path) async {
    print("In path exists");
    print(path);
    int count;
    if(await path.exists()){
      print("In path exists getcount");
      Directory appDocDirFolder = path;
       var imageList = appDocDirFolder.listSync().map((item) => item.path).where((item) => item.endsWith(".jpg")).toList(growable: false);
          print("First image added");
          print(imageList);
          if(imageList.length != 0){
            print("imageList if");
            var pathString = imageList.last;
            print("First image ");
            final intInStr = RegExp(r'\d+');
            print("First image added......");
            var numbers =  intInStr.allMatches(pathString.toString()).map((m) => m.group(0));
            print("111111");
            print(numbers.last);
            count = int.parse(numbers.last);
            print(count++);    //Dont delete this line
            return count++;}
//            }else{
//            print("imageList else");
//
//            return count ;
//          }

       
    }else{
      count = 0;
      return count;
    }

  }

  Map<String, List<String>> GetImages(String subFolderName, String formName)
        {
          print("In get image of pdf");
          print(formName);
           Map<String, List<String>> ImagesDictionary = new Map<String, List<String>>(); 
           try{
                  var dirFolder = Directory('/storage/emulated/0/CKross_Wheels_EMI/Images/$formName');
                  if(dirFolder.existsSync()){
                  var getpdf =   dirFolder
                      .listSync()
                      .map((item) => item.path)
                      .where((item) => item.endsWith(".pdf"))
                      .toList(growable: false);
                    if(getpdf.isNotEmpty){
                      print("getpdf");
                  print(getpdf.toString());
                  List<String> FilePath = new List<String>();
                  FilePath.add(getpdf[0]);
                  ImagesDictionary[formName]=FilePath;
                  print(ImagesDictionary);
                    }
                  
                  }
           }on Exception catch(e){
             print("Exception in GetImages"+e.toString());
           }
           return ImagesDictionary;
        }
}