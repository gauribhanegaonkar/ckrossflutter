

class AppConstants {
  static String noInternetConnection = 'No Active Internet Connection';
  static String somethingWentWrong =
      'Something went wrong. Please try again later.';
  static String customerNotFound = 'Customer Not Found';

            static String IS_LOGGED_IN = "isLogin";
   static String PRODUCT_NAME = "New TW";
         static String DATABASE_NAME = "Sumasoft";
         static String APP_TYPE = "M";
         static String SMS_RECEIVER_NAME = "TFCTOR";
         static String RESPONSE = "Response";
         static String CLIENT_NAME = "Sumasoft";
         static String LANGUAGE = "language";
         static String APP_VERSION = "app_version";
         static String TOKEN = "token";
         static String APP_LANG = "en";

         static String LOGGED_IN_USER_ROLE = "user_role";
         static String LOGGED_IN_USER = "logged_in_user";
         static String LOGGED_IN_USER_ID = "user_id";
         static String LOGGED_IN_USER_NAME = "user_name";

         static String RESOLVE_QUERIES_ID = "106";
         static String RESPONSE_VERSION_NO = "0";

         static String SEARCH_BTN_TXT = "Search";
         static String CLEAR_BTN_TXT = "Clear";

         static String IP_ADDRESS = "IP_ADDRESS";
         static String Ckross = "Ckross";
         static String OTP = "OTP";

         static String HOME_MENU = "home_menu";
         static String PENDING_KEY = "Pending";

         static String STATUS_REPORT = "Status Report";
         static String STATUS_REPORT_SE_KEY = "Status Report SE";

         static String CREATE_LEAD = "upload";
         static String UPLOAD_TEMPLATE_NAME = "LeadCreation";
         static String CREATE_LEAD_DOC_TITLE = "Upload Images";
         static String LEAD_CREATION_KEY = "Lead Creation";
         static String CURRENT_PRODUCT_LEAD_CREATION_ID = "lead_creation_id";

         static String RESOLVE_QUERIES = "resolveQueries";
         static String RE_UPLOAD_TITLE = "Re-Upload Details";
         static String RESOLVE_QUERIES_TEMPLATE_NAME = "QueryResolution";
         static String RESOLVE_QUERY_DOC_TITLE = "Resolve Query Images";
         static String QUERY_RESOLUTION_KEY = "Query Resolution";
         static String QUERY_RESOLUTION = "Query Resolution";
         static String CURRENT_PRODUCT_QUERY_RESOLUTION_ID = "query_resolution_id";

         static String PRE_DISBURSEMENT = "preDisbursement";
         static String PRE_DISBURSEMENT_TITLE = "Pre-Disbursement Details";
         static String PRE_DISBURSEMENT_TEMPLATE_NAME = "Pre-DisbursementDocumentUpload";
         static String PRE_DIS_DOC_TITLE = "Pre-Disbursement Images";
         static String PRE_DISBURSEMENT_KEY = "Pre-Disbursement Document Upload";
         static String PRE_DISBURSEMENT_REPORT = "Pre Disbursement Report";
         static String CURRENT_PRODUCT_PRE_DISBURSEMENT_ID = "pre_disbursement_id";

         static String POST_DISBURSEMENT = "postDisbursement";
         static String POST_DISBURSEMENT_TITLE = "Post-Disbursement Details";
         static String POST_DISBURSEMENT_TEMPLATE_NAME = "Post-DisbursementDocumentUpload"; 
         static String POST_DISBURSEMENT_KEY = "Post-Disbursement Document Upload";
         static String POST_DISBURSEMENT_REPORT = "Post Disbursement Report";
         static String CURRENT_PRODUCT_POST_DISBURSEMENT_ID = "post_disbursement_id";

         static String SERVICE_INSPECTION = "Service Inspection";
         static String SERVICE_INSPECTION_LEAD_TITLE = "Service Inspection Query Details";
         static String SERVICE_INSPECTION_TEMPLATE_NAME = "ServiceInspcetionVideoUpload";
         static String SERVICE_INSPECTION_KEY = "Service Inspection Video Upload";
         static String CURRENT_PRODUCT_SERVICE_INSPECTION_ID = "service_inspection_id";

         static String STATUS_UPDATE_LEAD = "StatusUpdateLead";
         static String STATUS_UPDATE_LEAD_TITLE = "Status Update Lead Details";
         static String STATUS_UPDATE_LEAD_TEMPLATE_NAME = "UpdateLead";
         static String STATUS_UPDATE_LEAD_DOC_TITLE = "Status Update Lead Images";
         static String CURRENT_PRODUCT_STATUS_UPDATE_LEAD_ID = "status_update_lead_id";

         static String PRE_DISBURSEMENT_ADVICE = "preDisbursementAdvice";
         static String PRE_DISBURSEMENT_ADVICE_TITLE = "Pre-Disbursement Details";
         static String PRE_DISBURSEMENT_ADVICE_TEMPLATE_NAME = "Pre-DisbursementAdviceUpload";
         static String PRE_DISBURSEMENT_ADVICE_KEY = "Pre-Disbursement Advice Upload";
         static String PRE_DISBURSEMENT_ADVICE_REPORT = "Pre Disbursement Report";
         static String CURRENT_PRODUCT_PRE_DISBURSEMENT_ADVICE_ID = "pre_disbursement_advice_id";

         static String PRE_DISBURSEMENT_RESOLVE_QUERIES = "preDisbursementQueryResolution";
         static String PRE_DISBURSEMENT_RESOLVE_QUERIES_TEMPLATE_NAME = "Pre-DisbursementQueryResolution";
         static String PRE_DISBURSEMENT_QUERY_RESOLUTION_KEY = "Pre-Disbursement Query Resolution";
         static String CURRENT_PRODUCT_PRE_DISBURSEMENT_QUERY_RESOLUTION_ID = "pre_disbursement_query_resolution_id";

         static String POST_DISBURSEMENT_RESOLVE_QUERIES = "postDisbursementQueryResolution";
         static String POST_DISBURSEMENT_RESOLVE_QUERIES_TEMPLATE_NAME = "Post-DisbursementQueryResolution";
         static String POST_DISBURSEMENT_QUERY_RESOLUTION_KEY = "Post-Disbursement Query Resolution";
         static String CURRENT_PRODUCT_POST_DISBURSEMENT_QUERY_RESOLUTION_ID = "post_disbursement_query_resolution_id";

         static String PRE_DISBURSEMENT_ADVICE_RESOLVE_QUERIES = "resolveQueries";
         static String PRE_DISBURSEMENT_ADVICE_RESOLVE_QUERIES_TEMPLATE_NAME = "QueryResolution";
         static String PRE_DISBURSEMENT_ADVICE_QUERY_RESOLUTION_KEY = "Pre-Disbursement Advice Query Resolution";
         static String CURRENT_PRODUCT_PRE_DISBURSEMENT_ADVICE_QUERY_RESOLUTION_ID = "query_resolution_id";

         static String CC_LEADS_CREATED = "CCLeads";
         static String CC_LEADS_CREATED_TEMPLATE_NAME = "CCLeadsApproved";
         static String CC_LEADS_CREATED_KEY = "CC Leads";
         static String CURRENT_PRODUCT_CC_LEADS_CREATED_ID = "cc_leads_created_id";

         static String CREDIT_REFER_QUEUE = "CreditReferQueue";
         static String CREDIT_REFER_QUEUE_REUPLOAD_TITLE = "Credit Refer Queue Details";
         static String CREDIT_REFER_QUEUE_TEMPLATE_NAME = "CreditReferQueue";
         static String CREDIT_REFER_QUEUE_KEY = "Credit Refer Queue";
         static String CURRENT_PRODUCT_CREDIT_REFER_QUEUE_ID = "credit_refer_queue_id";

         static String CURRENT_PRODUCT_ID = "product_id";
         static String CURRENT_PRODUCT_NAME = "product_name";
         static String CURRENT_PRODUCT_LOAD_HTML = "load_html";

         static int VIDEO_REQUEST_CODE = 100;
         static String VIDEO_SELECTED = "VideoSelected";
         static String VIDEO_CAPTURED = "VideoCaptured";
         static String VIDEO_COMPRESSED = "VideoCompressed";
         static String VIDEO_COMPRESSION_FAILED = "VideoCompressionFailed";

         static String IMAGE_CLICKED = "ImageClicked";
         static String DEALER_BRANCH = "DealerBranch";
         static String DEALER_STATE = "DealerState";

         static String IS_PRODUCT_LOADED_SUCESSFULLY = "IS_PRODUCT_LOADED_SUCESSFULLY";

         static String DATE_SELECTED = "DateSelected";

         static String ALERT = "Alert";
         static String NO_INTERNET_CONNECTION = "Please Check Your Internet Connection!";
         static String OK = "Ok";
         static String DOWNLOADING_APK = "Downloading Updated Apk...";
         static String PLEASE_WAIT_ALERT = "Please Wait...";
         static String UNABLE_TO_DOWNLOAD_APK = "Unable to download Apk file.";
         static String TRY_AGAIN = "Please try again later!";
         static String DIFFERENT_APK_VERSION_ERROR = "New Update is available. Please download the updated Apk.";
         static String UPDATE = "UPDATE";
         static String IS_APK_DOWNLOADED = "isApkDownloaded";
         static String SOMETHING_WENT_WRONG_TRY = "Something went wrong at Server side, Please Try after sometime.";
         static String SOMETHING_WENT_WRONG_REUPLOAD = "Something went wrong at Server side, Please Reupload after sometime.";
         static String NO_RESPONSE = "No Response from Server.\nThis can be due to low/bad internet connection or request timeout.";
         static String TOKEN_EXPIRED_RELOGIN = "Seems like Token has been Expired, You need to ReLogin.";

         static String DEFAULT_NOTIFICATION_CHANNED_ID = "FirebasePushNotificationChannel";
         static String DEFAULT_NOTIFICATION_CHANNED_NAME = "General";
         static String NOTIFICATION_TOKEN = "NotificationToken";
         static String IS_NOTIFICATION_TOKEN_UPDATED = "IS_NOTIFICATION_TOKEN_UPDATED";
         static String CURRENT_NOTIFICATION_TYPE = "CURRENT_NOTIFICATION_TYPE";

         static String BACKGROUND_SYNC_MESSAGE = "BACKGROUND_SYNC_MESSAGE";
         static String BACKGROUND_SYNC_PROGRESS = "BACKGROUND_SYNC_PROGRESS";
         static String IS_BACKGROUND_UPLOAD_IN_PROGRESS = "IS_BACKGROUND_UPLOAD_IN_PROGRESS";
         static String PENDING_CASES_COUNT = "PENDING_CASES_COUNT";

         static List<String> PDFFileList = new List<String>();
         static String IsSchemeSelected = "IsSchemeSelected";
         static bool Notes = false;
         static bool IsValidData = false;
         static String IS_INTERNET_AVAILABLE = "IS_INTERNET_AVAILABLE";

         static String PRODUCTJSON = "PRODUCTJSON";
         static String DEALERJSON = "DEALERJASON";
         static String SELECTEDTEXT = "SELECTEDTEXT";
         static String PAN_No = "PAN_No";
         static String app_Plain__DateOfBirth = "app_Plain__DateOfBirth";
         static String FirstName = "FirstName";
         static String LastName = "LastName";

        static String res__DocumentNumber = "res__DocumentNumber";
        static String  pincode = "pincode";
        static String HouseNumber = "HouseNumber";
   
             
}
