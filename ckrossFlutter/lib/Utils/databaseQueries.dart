import 'package:ckrossFlutter/models/ckrossCase.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
//﻿using System;
//using ckrosslosMobileXamarin.Persistence.DatabaseModels;

//{
     class DatabaseQueries
    {
        // table 
         static String ClientTemplateTable = "ClientTemplate";
         static String TABLE_TEMPLATE_JSON = "TemplateJson";
         static String TABLE_LOGIN_JSON_TEMPLATE = "LoginJsonTemplate";
         static String TABLE_DYNAMIC_FIELD_TABLE = "DynamicTableFields";
         static String TABLE_CREATED_CASE_TABLE = "CkrossCase_Table";
         static String TABLE_PRODUCT = "Product";
         static String TABLE_MAKE = "Make";
         static String TABLE_MODEL = "Model";
		 static String TABLE_DEALER_SO = "DealerSO";
         static String TABLE_MAKE_DEALER_DATA = "Make_Dealer_Data";
         static String TABLE_STATES = "States";
         static String TABLE_DEALER_LOCATION_VD = "Dealer_LocationVD";
         static String TABLE_MODEL_VD = "ModelVD";
         static String TABLE_YEAR_VD = "YearVD";
         static String TABLE_DEALER_VD = "DealerVD";
         static String TABLE_CITY = "City";
         static String TABLE_SUB_LOAN_TYPE = "SubLoanType";
         static String TABLE_VEHICLE_CATEGORY = "VehicleCategory";

        //ClientTemplate table Queries
        static  String CLIENT_NAME="client_name";
         static  String TEMPLATE_NAME="template_name";
         static  String TEMPLATE_VERSION="version";
         static  String IS_DEPRECATED_TEMPLATE="is_deprecated";
         static  String JSON_LANGUAGE = "language";

        //TemplateJson columns
        static  String TEMPLATE_JSON_ID="template_json_id";
        static  String TEMPLATE_JSON_FORM_NAME="form_name";
        static  String FK_CLIENT_TEMPLATE_ID="client_template_id";

        //DynamicTableFields columns 
        static  String FK_TEMPLATE_JSON_ID="template_json_id";

        //LoginJsonTemplate columns 
         static  String LOGIN_JSON_KEY = "jsonKey";
         static String LOGIN_JSON_VALUE = "jsonValue";
        //Case
         static String CASE_ID = "case_id";
         static String TEMP_CASE_ID = "temp_case_id";
         static String APPLICANT_JSON = "applicant_json";
         static String IS_SUBMIT = "is_submit";
         static String CASE_TYPE = "case_type";
         static String USER_ID = "user_id";
        // static String PRODUCT_ID = "product_id";

        //product column
         static String PRODUCT_ID = "product_id";
         static String PRODUCT_NAME = "product_name";
      
        //make column
         static String M_PRODUCT_ID = "product_id";
         static String MAKE_ID = "make_id";
         static String MAKE_NAME= "make_name";

        //make_dealer_data column
         static String MAKE_DEALER_DATA_ID = "make_dealer_data_id";
         static String MAKE_DEALER_DATA_NAME = "make_dealer_data_name";
         static String DEALER_TYPE = "Dealer_Type";
        //  static String DEALERSHIP_NAME = "Dealership_Name";
        
        //model column
         static String MODEL_ID = "model_id";
         static String MO_MAKE_ID = "make_id";
         static String MODEL_NAME = "model_name";

         static String ON_ROAD_PRICE = "on_road_price";
         static String DATE = "date";
         static String CURRENT_COUNTER = "currentCounter";
         static String GENERATED_MACHINE_IDENTIFIER = "generatedMachineIdentifier";
         static String MACHINE_IDENTIFIER = "machineIdentifier";
         static String GENERATED_PROCESS_IDENTIFIER = "generatedProcessIdentifier";
         static String TIME = "time";
         static String COUNTER = "counter";
         static String PROCESS_IDENTIFIER = "processIdentifier";
         static String TIME_STAMP = "timestamp";
         static String TIME_SECOND= "timeSecond";
        
		//dealerSo column
		 static String DEALER_ID = "dealer_id";
		 static String DEALERSHIP_NAME = "dealership_name";
		 static String DEALERSHIP_DATA= "dealership_data";

        //added by Rohit 24/12/2018
        //States Table
         static String STATE_ID = "state_id";
         static String STATE_NAME = "state_name";

        //Sub Loan Type Table
         static String SUB_LOAN_ID = "sub_loan_id";
         static String LOAN_TYPE = "loan_type";
         static String DENTING_CONDITION = "denting_dondition";

        //city column
         static String CITY_NAME = "city_name";
               
        static String IS_SUBMIT_CLICKEd = "is_submit";
        //Queries for ClientTemplate 
         static  String CreateSingleClientTemplateAccVerionNoQuery(String templateName, String nbfcName, String app_language, String version_no, String value)
        {
            String Query = "SELECT  * FROM " + ClientTemplateTable +
                " WHERE " + CLIENT_NAME + " = '" + nbfcName + "' AND " + TEMPLATE_NAME +
                " = '" + templateName + "' AND " + JSON_LANGUAGE + " = '" + app_language +
                "' AND " + TEMPLATE_VERSION + " = '" + version_no + "' AND " + IS_DEPRECATED_TEMPLATE + " = '" + value + "'";
         
            return Query;
        }

         static String CreateSingleTemplateJsonQuery(String client_template_id, String fieldFormName)
        {
            String Query = "SELECT  * FROM " + TABLE_TEMPLATE_JSON + 
                " WHERE " + FK_CLIENT_TEMPLATE_ID + " = '" + client_template_id + 
                "' AND " + TEMPLATE_JSON_FORM_NAME + " = '" + fieldFormName + "'";
            return Query;
        }

         static String CreateGetTemplateByClientTemplateId(id)
		{
            String Query = "SELECT  * FROM " + TABLE_TEMPLATE_JSON +
                " WHERE " + FK_CLIENT_TEMPLATE_ID + " = '" + id + "'";
            return Query;
		}
  
        // s

		 static String CreateSingleDynamicFieldTable(String template_json_id)
        {
            String Query = "SELECT  * FROM " + TABLE_DYNAMIC_FIELD_TABLE + " WHERE " 
                + FK_TEMPLATE_JSON_ID + " = '" + template_json_id + "'";
            return Query;
        }

         static String truncateTable(String tableName){
            String query = "DELETE FROM " + tableName;
            return query;
        }

         static String CreateGetSingleDynamicFieldTable(id)
        {
            String Query = "SELECT  * FROM " + TABLE_DYNAMIC_FIELD_TABLE + 
                " WHERE " + FK_TEMPLATE_JSON_ID + " = '" + id + "'";
            return Query;
        }

         static String CreateGetLoginJsontemplateImageJson(String LOGGED_IN_USER_ROLE, String app_language){
            String query = "SELECT  * FROM " + TABLE_LOGIN_JSON_TEMPLATE +
                " WHERE " + LOGIN_JSON_KEY + " = '"+LOGGED_IN_USER_ROLE+"'"  + " AND " +
                JSON_LANGUAGE + " = '"+app_language+"'";
            return query;
        }

//         static String CreateGetLoginJsontemplateImageJson(String jsonKey)
//         {
//           String Query = "SELECT * FROM " + TABLE_LOGIN_JSON_TEMPLATE + " WHERE " + LOGIN_JSON_KEY + " = '" + jsonKey + "'" ;
//           return Query;
//         }

        // //added by nilima on 14 June 2018
         static String DeleteLoginJsontemplateImageJson(jsonKey,language) 
        {
            String Query = "DELETE FROM " + TABLE_LOGIN_JSON_TEMPLATE +  " WHERE " + 
                 LOGIN_JSON_KEY + " = '" + jsonKey + "'" + " AND " +
                JSON_LANGUAGE + " = '" + language + "'";
            return Query;
        }
               
         static String GetLoginTemplateCountQuery(jsonKey,language)
        {
            String countQuery = "SELECT * FROM " + TABLE_LOGIN_JSON_TEMPLATE + " WHERE " + LOGIN_JSON_KEY + " = '" + jsonKey + "'" + " AND " + JSON_LANGUAGE + " = '" + language + "'";
            return countQuery;
        }

         static String GetCaseDependsOnTypeAndCaseID(caseType , caseId)
        {
            String countQuery = "SELECT COUNT(1) as count FROM " + TABLE_CREATED_CASE_TABLE + " WHERE " + CASE_TYPE + " = '" + caseType + "'" + " AND " + CASE_ID + " = '" + caseId + "'";
            return countQuery;
        }
       
         static String GetMaxCaseID()
        {
            String Query = "SELECT MAX( CAST ( " + CASE_ID + " as Int ) ) as LastId FROM " + TABLE_CREATED_CASE_TABLE;
            return Query;
        }

         static String GetMaxProductId()
        {
            String Query = "SELECT MAX(" + PRODUCT_ID + ") as LastId FROM " + TABLE_PRODUCT;
            return Query;
        }

         static String GetMaxMakeId()
        {
            String Query = "SELECT MAX(" + MAKE_ID + ") as LastId FROM " + TABLE_MAKE;
            return Query;
        }

         static String GetMaxModelId()
        {
            String Query = "SELECT MAX(" + MODEL_ID + ") as LastId FROM " + TABLE_MODEL;
            return Query;
        }

         static String DeleteCaseDependsOnId(String caseId, String userId)
        {
            String Query = "DELETE FROM " + TABLE_CREATED_CASE_TABLE + " WHERE " + CASE_ID + " = '" + caseId + "'"+ " AND " + USER_ID + " = '" + userId + "'";
            return Query;
        }

         static String GetSingleCase(String caseId, String userId)
        {
            String selectQuery = "SELECT  * FROM " + TABLE_CREATED_CASE_TABLE + " WHERE " + CASE_ID + " = '" + caseId + "'"+ " AND " + USER_ID + " = '" + userId + "'";
            return selectQuery;
        }
        //added by nilima 0n 15 June 2018
         static String GetSingleCaseIs_SubmitFalse(String caseId, String userId)
        {
            String selectQuery = "SELECT  * FROM " + TABLE_CREATED_CASE_TABLE + " WHERE " + CASE_ID + " = '" + caseId + "'" + " AND " + USER_ID + " = '" + userId + "'" 
                + " AND " + IS_SUBMIT + " = 'false' ";
            return selectQuery;
        }

         static String GetAllPendingCases(String userID, String productID)
        {
            String selectQuery = "SELECT  * FROM " + TABLE_CREATED_CASE_TABLE 
                + " WHERE " + IS_SUBMIT + " = 'false'" 
                + " AND " + USER_ID + " = '" + userID + "'" 
                + " AND " + PRODUCT_ID + " = '" + productID + "'";
            return selectQuery;
		}

         static String GetAllPendingCasesCount(String userID, String productID)
        {
            String selectQuery = "SELECT COUNT(1) as count FROM " + TABLE_CREATED_CASE_TABLE
                + " WHERE " + IS_SUBMIT + " = 'false'"
                + " AND " + USER_ID + " = '" + userID + "'"
                + " AND " + PRODUCT_ID + " = '" + productID + "'";
            return selectQuery;
        }

         static String GetFirstPendingCases(String userID, String productID)
        {
            String selectQuery = "SELECT * FROM " + TABLE_CREATED_CASE_TABLE
                + " WHERE " + IS_SUBMIT + " = 'false'"
                + " AND " + USER_ID + " = '" + userID + "'"
                + " AND " + PRODUCT_ID + " = '" + productID + "'" 
                + " LIMIT 1";
            return selectQuery;
        }

         static String UpdateCase( ckrossCase)
        {
          print(ckrossCase["temp_case_id"]);
          print(ckrossCase["is_submit"]);
          print(ckrossCase["user_id"]);
            String selectQuery = "UPDATE " + TABLE_CREATED_CASE_TABLE + " SET " + TEMP_CASE_ID + " = '" + ckrossCase["temp_case_id"] +  "'"+ " , "+IS_SUBMIT+ " = '" + ckrossCase["is_submit"] + "'" + " WHERE " + CASE_ID + " = '" + ckrossCase["case_id"] + "'"+ " AND " + USER_ID + " = '" + ckrossCase["user_id"] + "'";
            return selectQuery;
        }

        //  static String UpdatePendingCase(CkrossCase ckrossCase)
        // {
        //     String selectQuery = "UPDATE " + TABLE_CREATED_CASE_TABLE + " SET " + TEMP_CASE_ID + " = '" + ckrossCase.temp_case_id + "'" + " , " + IS_SUBMIT + " = '" + ckrossCase.is_submit + "'" + " , " + APPLICANT_JSON + " = '" + ckrossCase.applicant_json + "'" +" WHERE " + CASE_ID + " = '" + ckrossCase.case_id + "'" + " AND " + USER_ID + " = '" + ckrossCase.user_id + "'";
        //     return selectQuery;
        // }

        //  static String UpdateCaseDependsOnTypeAndCaseID(CkrossCase ckrossCase)
        // {
        //     String selectQuery = "UPDATE " + TABLE_CREATED_CASE_TABLE + " SET " + APPLICANT_JSON + " = '" + ckrossCase.applicant_json + "'" +  " WHERE " + CASE_ID + " = '" + ckrossCase.case_id + "'" +" , "+ CASE_TYPE + " = '" + ckrossCase.case_type + "'"+ " AND " + USER_ID + " = '" + ckrossCase.user_id + "'";
        //     return selectQuery;
        // }

         static String getproduct(String value)
        {
            String Query = "SELECT * FROM "+ TABLE_PRODUCT +
                " WHERE " + PRODUCT_NAME + " = '" + value + "'";
            return Query;
        }

         static String getmake(String value)
        {
            String Query = "SELECT * FROM " + TABLE_MAKE+
                " WHERE " + MAKE_NAME + " = '" + value + "'";
            return Query;
        }

         static String getmakebyproductid(String selectedpid)
        {
            String Query = "SELECT * FROM " + TABLE_MAKE +
                " WHERE " + M_PRODUCT_ID + " = '" + selectedpid + "'";
            return Query;
        }

         static String getmakebyproductidandjsonmakelist(String selectedpid)
        {
            String Query = "select m.make_id,m.make_name from Make m inner join Make_Dealer_Data md on m.make_name=md.make_dealer_data_name where m.product_id=" + selectedpid;
            return Query;
        }

         static String getmodelbymakeid(String selectedmkid)
        {
            String Query = "SELECT * FROM " + TABLE_MODEL+
                " WHERE " + MO_MAKE_ID + " = '" + selectedmkid + "'";
            return Query;
        }
      static String getmodelbymakeidVD(String selectedmkid)
      {
        String Query = "SELECT * FROM " + TABLE_MODEL_VD+
            " WHERE " + MO_MAKE_ID + " = '" + selectedmkid + "'";
        return Query;
      }

         static String getmodelbymodelname(String modelname)
        {
            String Query = "SELECT * FROM " + TABLE_MODEL +
                " WHERE " + MODEL_NAME + " = '" + modelname + "'";
            return Query;
        }

         static String TruncateGivenTable(String TableName)
        {
            String Query = "DELETE FROM " + TableName;
            return Query;
        }
       
         static String CheckProductExistInDB(String ProductName)
        {
            String Query = "SELECT  * FROM " + TABLE_PRODUCT + " WHERE " + PRODUCT_NAME + " = '" + ProductName + "'";
            return Query;
        }

         static String CheckMakeExistInDB(String MakeName)
        {
            String Query = "SELECT  * FROM " + TABLE_MAKE + " WHERE " + MAKE_NAME + " = '" + MakeName + "'";
            return Query;
        }

         static String CheckModelExistInDB(String ModelName)
        {
            String Query = "SELECT  * FROM " + TABLE_MODEL + " WHERE " + MODEL_NAME + " = '" + ModelName + "'";
            return Query;
        }

        //Queries for ClientTemplate 
         static String GetLastInsertedRowId()
        {
            String Query = "SELECT MAX(client_template_id) as LastId FROM ClientTemplate";
			return Query;
        }

        //Queries for ClientTemplate 
         static String GetLastInsertedRowIdMax()
        {
            String Query = "select seq from sqlite_sequence where name = " +"'"+DatabaseQueries.ClientTemplateTable+"'";
            return Query;
        }

         static String getAllTemplateJsonByClientTemplateId(String client_template_id)
        {
            String Query = "SELECT  * FROM " + TABLE_TEMPLATE_JSON + " WHERE " + FK_CLIENT_TEMPLATE_ID + " = '" + client_template_id + "'";
            return Query;
        }

		//  static String getDealershipDataByName(params String[] dealership_name)
    //     {
		// 	String Query = "SELECT  * FROM " + TABLE_DEALER_SO + " WHERE " +  DEALERSHIP_NAME + " = '" + dealership_name[0] + "'";
    //         return Query;
    //     }

        //Added By Rohit 26/12/2018
         static String GetDealershipDataByName(String dealership_name)
        {
            String Query = "SELECT  * FROM " + TABLE_DEALER_SO + " WHERE " + DEALERSHIP_NAME + " = '" + dealership_name + "'";
            return Query;
        }

        //  static String GetModelVDName(int makeid, String modelname)
        // {
        //     String Query = "SELECT * FROM " + TABLE_MODEL_VD + " WHERE " + MODEL_NAME + " = '" + modelname + "'" + " AND " + MAKE_ID + " = '" + makeid + "'";
        //     return Query;
        // }

         static String GetModelVDNameByMakeId(String makeid)
        {
            String Query = "SELECT * FROM " + TABLE_MODEL_VD + " WHERE " + MAKE_ID + " = '" + makeid + "'";
            return Query;
        }

         static String GetVehicleCategoryByModelId(String modelId)
        {
            String Query = "SELECT * FROM " + TABLE_VEHICLE_CATEGORY + " WHERE " + MODEL_ID + " = '" + modelId + "'";
            return Query;
        }

         static String GetYearVDByModelId(String modelId)
        {
            String Query = "SELECT * FROM " + TABLE_YEAR_VD + " WHERE " + MODEL_ID + " = '" + modelId + "'";
            return Query;
        }

         static String GetCityNames()
        {
            String Query = "SELECT * FROM " + TABLE_CITY;
            return Query;
        }

         static String GetSubLoanTypes(String loanType)
        {
            String Query = "SELECT * FROM " + TABLE_SUB_LOAN_TYPE + " WHERE " + LOAN_TYPE + " = '" + loanType + "'";
            return Query;
        }

//         static String CreateGetLoginJsontemplateMenuJson(String jsonKey, String appLang)
//        {
//            String Query = "SELECT  * FROM " + TABLE_LOGIN_JSON_TEMPLATE +
//                " WHERE " + LOGIN_JSON_KEY + " = '" + jsonKey + "'" + " AND " +
//                JSON_LANGUAGE + " = '" + appLang + "'";
//            return Query;
//        }
         static String CreateGetLoginJsontemplateMenuJson(String jsonKey)
         {
           String Query = "SELECT  * FROM " + TABLE_LOGIN_JSON_TEMPLATE + " WHERE " + LOGIN_JSON_KEY + " = '" + jsonKey + "'" ;
           return Query;
         }
          static String UpdateSaveCaseDependsOnTypeAndCaseID(CkrossCase ckrossCase)
        {
          print(ckrossCase.isSubmit);
          print(ckrossCase.caseId);
          print(ckrossCase.caseType);
          print(ckrossCase.userId);
            String selectQuery = "UPDATE " + TABLE_CREATED_CASE_TABLE + " SET " + "is_submit" + " = '" + ckrossCase.isSubmit + "'" + " WHERE " + CASE_ID + " = '" + ckrossCase.caseId + "'" + " AND " + CASE_TYPE + " = '" + ckrossCase.caseType + "'" + " AND " + USER_ID + " = '" + ckrossCase.userId + "'";
            print(selectQuery);
            print("selectQuery");
            return selectQuery;
        }

    }
