import 'dart:convert';
//import 'dart:js';
import 'dart:convert' as JSON;
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/YearVdDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/client_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/loginJson_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/templateJsonDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ResponseModels/DoOtpLoginResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/models/city.dart';
import 'package:ckrossFlutter/models/clientTemplate_data.dart';
import 'package:ckrossFlutter/models/dealerso_data.dart';
import 'package:ckrossFlutter/models/dyanamic_table_field.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/models/model.dart';
import 'package:ckrossFlutter/models/modelVd.dart';
import 'package:ckrossFlutter/models/product.dart';
import 'package:ckrossFlutter/models/states.dart';
import 'package:ckrossFlutter/models/subLoanType.dart';
import 'package:ckrossFlutter/models/templateJson.dart';
import 'package:ckrossFlutter/models/vehicleCategory.dart';
import 'package:ckrossFlutter/models/yearVD.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:collection/collection.dart';
//import 'package:js/js.dart';

class UITemplateOperations {
  static String m_jsonObjectApplicant = "";
  static String details_json = "";

  //static  UITemplateOperations m_jsonObjectApplicant = new UITemplateOperations();
  static ClientTemplate cKrossStoreClientTemplate;
  //  static ClientTemplate clientTemplate;
  static TemplateJson cKrossStoreTemplateJson = null;
  static DynamicTableFields cKrossStoreDynamicTableFields = null;
  static LoginJsonTemplate cKrossStoreLoginJsonTemplate;
  static Product cKrossStoreProduct = null;
  static Make cKrossStoreMake = null;
  static Model cKrossStoreModel = null;
  static DealerSO cKrossStoreDealerSO = null;
  // static Make_Dealer_Data cKrossStoreMakeDealerData = null;
  static States cKrossStoreStates = null;
  //static Dealer_LocationVD cKrossStoreDealer_LocationVD = null;
  static ModelVD cKrossStoreModelVD = null;
  static VehicleCategory cKrossStoreVehicleCategory = null;
  static YearVD cKrossStoreYearVD = null;
  static City cKrossStoreCity = null;
  static SubLoanType cKrossStoreSubLoanType = null;
  static String app_language, nbfcName, client_name;

  //static ILogger logger = DependencyService.Get<ILogManager>().GetLog();
  static String userRole;
  // var subProcessFieldsData=[];
  DatabaseHelper databaseHelper = DatabaseHelper();
  static DatabaseHelper db = DatabaseHelper();

  //Created by satoshi 13-Apr-2018
  static initDatabase() async {
    try {
      print("Start of InitDatabase");
      //  DatabaseHelper databaseHelper = DatabaseHelper();
      //var dbFuture =  DatabaseHelper();
      //var db =  DatabaseHelper();
      //dbFuture._initDatabase();
      // ClientTemplate cKrossStoreClientTemplate ;
      // cKrossStoreTemplateJson = new SQLiteTemplateJson(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreDynamicTableFields = new SQliteDynamicTableField(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreLoginJsonTemplate = new SQLiteLoginJsonTemplate(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreProduct = new SQLiteProduct(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreMake = new SQLiteMake(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreModel = new SQLiteModel(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreMakeDealerData = new SQLiteMake_Dealer_Data(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreStates = new SQLiteState(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreDealer_LocationVD = new SQLiteDealerLocationVD(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreModelVD = new SQLiteModelVD(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreVehicleCategory = new SQLiteVehicleCategory(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreYearVD = new SQLiteYearVD(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreCity = new SQLiteCity(DependencyService.Get<ISQLiteDb>());
      // cKrossStoreSubLoanType = new SQLiteSubLoanType(DependencyService.Get<ISQLiteDb>());

      print("End of InitDatabase");
    } on Exception catch (_) {
      print("Exception In InitDatabase() : ");
    }
  }

  static SaveUpdatedTemplateInDatabase(LoadHtmlFieldsResModel resmodel, jsonObjectApplicant) async {
    m_jsonObjectApplicant = jsonObjectApplicant;
    print("Welcome to SaveUpdatedTemplateInDatabase");
    print(m_jsonObjectApplicant);
    print(json.encode(resmodel.subProcessFieldsData));
    try {
      print("Start of SaveUpdatedTemplateInDatabase");
      //   var newMap = <String, dynamic>{};
      //  newMap.addAll(subProcessFieldsData[0]);
      // print(json.decode(subProFieldsData));
      // var newMap = json.decode(subProFieldsData);
      // var newMap2 = json.decode(tabData);
      // print(newMap[0]);
      // print(newMap2[0]);
      print("msgggggggg");
      List<TabDatum> tabDat = resmodel.tabData;
      //SubProcessFieldsDatum arr = new SubProcessFieldsDatum();
      List<SubProcessFieldsDatum> subProcessFieldsData =
          resmodel.subProcessFieldsData;

      // SubProcessFieldsDatum Data = subProcessFieldsData["subProcessFieldsData"];
      // print(subProcessFieldsData);
      // var newMap = json.encode(resmodel.subProcessFieldsData);
      // List newMap ;
      //newMap.addAll(resmodel.subProcessFieldsData);
      print("newMap");
      //List<SubProcessFieldsDatum> SubProcessFieldsData = newMap;
      //SubProcessFieldsData = newMap;
      //print(SubProcessFieldsData);
      //= map.toString();

      // print(resmodel.subProcessFieldsData);
      //SubProcessFieldsDatum SubProcessFieldsData ;

      // newMap.addAll(resmodel.subProcessFieldsData);
      // SubProcessFieldsData = newMap[""];
      // String tabDatas = tabData;
      //var subProFields = subProFieldsData;

      if (tabDat.length > 0) {
        for (int tabDataCnt = 0; tabDataCnt < tabDat.length; tabDataCnt++) {
          try {
            print("in try");

            String templateName =
            tabDat[tabDataCnt].process.replaceAll(" ", "");
            await InitializeAppData();
            String version_no = "0";

            TabDatum processToTabToSubProcess = tabDat[tabDataCnt];
            //check for template with client name,template name and version is exist in db
            String query =
            DatabaseQueries.CreateSingleClientTemplateAccVerionNoQuery(
                templateName, nbfcName, app_language, version_no, "false");
            print("query before execute");
            var listClientTemplate = await db.execute(query);
            print("list of client template");
            print(listClientTemplate);
            if (listClientTemplate.length > 0) {
              print("update template in database");
              // print(listClientTemplate[0]);
              try {
                print("Start of TruncateClientTemplateTable");
                String query =
                DatabaseQueries.truncateTable(DatabaseQueries.ClientTemplateTable);
                print(query);
                db.execution(query);
                print("End of TruncateClientTemplateTable");
              } on Exception catch (_) {
                print("Exception occured in TruncateTemplateJsonTable ");
              }

              try {
                print("Start of TruncateTABLE_TEMPLATE_JSON");
                String query =
                DatabaseQueries.truncateTable(DatabaseQueries.TABLE_TEMPLATE_JSON);
                print(query);
                db.execution(query);
                print("End of TruncateTABLE_TEMPLATE_JSON");
              } on Exception catch (_) {
                print("Exception occured in TruncateTABLE_TEMPLATE_JSON ");
              }

//
// var clientTemplates = listClientTemplate[0];
//              // clientTemplates =listClientTemplate[0];
//              // print(clientTemplates);
//              //  ClientTemplate clientTemplate =listClientTemplate[0];
//              //update template in database
//              await UpdateTemplateOfTable(clientTemplates,
//                  processToTabToSubProcess, subProcessFieldsData);

              List<SubProcessFieldsDatum> jsons = resmodel.subProcessFieldsData;
              print("jsons......");
              print(jsons);

              await InsertTemplateInTableAsync(processToTabToSubProcess, jsons);
            } else {
              print("insert new template");
              //  List<SubProcessFieldsDatum> SubProcessFields;
              //  SubProcessFields=resmodel.subProcessFieldsData;
              print("insert new template222");
              List<SubProcessFieldsDatum> jsons = resmodel.subProcessFieldsData;

              // var newMap  ;
              // SubProcessFieldsDatum SubProcessFieldsData = SubProcessFields ;
              // var add= newMap.addAll(SubProcessFields);
              //var added = json.encode(add);
              //  SubProcessFieldsDatum da = json.encode(SubProcessFields);
              //insert new template
              await InsertTemplateInTableAsync(processToTabToSubProcess, jsons);
            }
          } on Exception catch (e) {
            print("Exception in SaveUpdatedTemplateInDatabase $e");
          }
        }
      }
    } on Exception catch (e) {
      print("Exception in main SaveUpdatedTemplateInDatabase $e");
    }
  }

  static UpdateTemplateOfTable(UpdateclientTemplate,
      TabDatum processToTabToSubProcess, subProcessFieldsData) async {
    try {
      print("UpdateTemplateOfTable");
      // print(UpdateclientTemplate);
      // print(subProcessFieldsData);
      // print(processToTabToSubProcess);
      ClientTemplate clientTemplate = new ClientTemplate();
      print("1111");
      // var map = <String,dynamic>{};
      // map.addAll(clientTemplate.toMap());
      // print(clientTemplate.toMap());

      print("Start of UpdateTemplateOfTable");

      //String m_jsonObjectApplicant="";
      //var newMap= json.decode(subProcessFieldsData.toString());
      TemplateJson templateJson = null;
      DynamicTableFields dynamicFieldTable = new DynamicTableFields();
      List<String> arrayListFormNames =
          processToTabToSubProcess.tabs[0].subProcesses;

      print("arrayListFormNames" + arrayListFormNames.toString());
      //-------Start of template to be updated in client_template table ------///

      String form_array = arrayListFormNames.toString();
      String version_no = "0";

      String applicant_json = "";
      try {
        applicant_json = m_jsonObjectApplicant.toString();
      } on Exception catch (_) {
        print("Exception Occured In UpdateTemplateOfTable ");
      }
      // String details_json ="";
      print("jsonnnn");
      var clientnewMap = <String, dynamic>{};
      clientnewMap.addAll(UpdateclientTemplate);
      print("form");
      print(clientnewMap["form_name_array"]);
      //print(clientnewMap["version_no"]);
      // print(UpdateclientTemplate);

      // if(details.isNotEmpty){
      print("not null");
      // print(clientnewMap["details_json"]);
      clientTemplate.details_json = clientnewMap["details_json"];
      // }

      print("version" + clientnewMap["version"]);
      print(clientTemplate.version);
      //  clientTemplate.details_json= clientnewMap[details_json];
      // clientTemplate.applicant_json = clientnewMap["applicant_json"];
      clientTemplate.version = clientnewMap["version"];
      clientTemplate.form_name_array = clientnewMap["form_name_array"];
      //print("clientTemplate.client_template_id"+clientTemplate.client_template_id);
      if (clientnewMap["client_template_id"] != 0) {
        print("clientTemplate.client_template_id != 0");

        await db.updation("ClientTemplate", clientTemplate.toMap());
        db.search();
        print("client template search");
        //print("res"+res.toString());
        //await cKrossStoreClientTemplate. UPDATE(clientTemplate);
      }
      //-------End of template to be updated in client_template table ------///

      //-------Start of field to be updated in json_template,field_table ------///
      // SubProcessFieldDataResponse subProcessFieldDataResponse = gson.fromJson(subProcessFieldsData.trim(), SubProcessFieldDataResponse.class);
      print("!!!!");
      // print(subProcessFieldsData);
      List<SubProcessFieldsDatum> arrayListSubProcessToField =
          subProcessFieldsData;
      //  print(arrayListSubProcessToField.toJson());

      //var arrList = arrayListSubProcessToField;
      for (int formNameCnt = 0;
      formNameCnt < arrayListFormNames.length;
      formNameCnt++) {
        String formName = arrayListFormNames[formNameCnt];
        String templateName = clientnewMap["template_name"];
        // = UpdateclientTemplate;
        print("templateName.......");
        print(templateName);
        print("formName" + formName);
        print(UpdateclientTemplate.toString());
        // SubProcessFieldsDatum arrayListSubProcessToField=subProcessFieldsData;
        // print(arrayListSubProcessToField);
        for (int templateFieldCnt = 0;
        templateFieldCnt < arrayListSubProcessToField.length;
        templateFieldCnt++) {
          // print(subProcessFieldsData[0].subProcessKey);
          //   var MapSubProcessFieldsDatum = <String, dynamic>{};
          // MapSubProcessFieldsDatum.addAll(subProcessFieldsData[0]);
          // print(MapSubProcessFieldsDatum["subProcessKey"]);
          print("@@@@");
          // print(arrayListSubProcessToField);

          String fieldFormNameKey =
              subProcessFieldsData[templateFieldCnt].subProcessKey;
          String fieldFormName =
              subProcessFieldsData[templateFieldCnt].subProcessName;
          String fieldTemplateNameRemovedSpace =
          subProcessFieldsData[templateFieldCnt]
              .processName
              .replaceAll(" ", "");
          String fieldTemplateName = fieldTemplateNameRemovedSpace;

          print("fgfdhdhdfghgfdhfg...........");
          print("fieldFormNameKey.............");
          print(fieldFormNameKey);
          print("fieldTemplateName.............");
          print(fieldTemplateName);
          print("template name............");
          print(templateName);
          print("formName.................");
          print(formName);

          //check for equality
          if (formName == fieldFormNameKey &&
              fieldTemplateName.contains(templateName)) {
            print("get row to be updated from json_template table");
            //     //get row to be updated from json_template table
            String query = DatabaseQueries.CreateSingleTemplateJsonQuery(
                clientnewMap["client_template_id"].toString(), fieldFormName);
            List<TemplateJson> listTemplateJson = await db.execution(query);
            // var arr = new List();
            //  arr.addAll(listTemplateJson);
            //print("listTemplateJson"+listTemplateJson);

            var subProcessToField = subProcessFieldsData[templateFieldCnt];

            print("subProcessToField");

            print(subProcessToField);
            String templateFieldJson = subProcessToField.toString();
            String jsonTemplateId = null;
            print("listtemplateJson................");
            print(listTemplateJson);
            if (listTemplateJson != null) {
              print("update template json fields");
              templateJson = listTemplateJson[0];
              //update template json fields
              jsonTemplateId = await updateTemplateJsonFields(
                  templateJson, subProcessToField, templateFieldJson);
              print(jsonTemplateId);
              print("complete update..........");
            } else {
              print("insert template json fields");
              //insert template json fields
              print(templateFieldJson);
              jsonTemplateId = await InsertTemplateJsonFieldsAsync(
                  subProcessToField,
                  templateFieldJson,
                  clientTemplate.client_template_id.toString());

              print(jsonTemplateId);
              print("complete insert..............");
            }
            //     //check for table exist and enter all data in field_table
            print("check for table exist and enter all data in field_table");
            //var subProcessToFielddata=json.encode(subProcessToField);
            // print(subProcessToField.isTableExist);

            if (subProcessToField.isTableExist) {
              //get row to be updated from field_table
              if (templateJson != null) {
                query = DatabaseQueries.CreateSingleDynamicFieldTable(
                    templateJson.template_json_id.toString());
                List<DynamicTableFields> listDynamicFieldTable =
                await db.execution(query);
                print(listDynamicFieldTable);
                // cKrossStoreDynamicTableFields.ExecuteQueryAsync(query);
                if (listDynamicFieldTable.length > 0) {
                  print("subprocssssss updateDynamicFieldTable");
                  // dynamicFieldTable = listDynamicFieldTable[0];
                  //update dynamic table entry
//                  await updateDynamicFieldTable(
//                      dynamicFieldTable, subProcessToField);
                } else {
                  print("subprocssssss");
                  print(subProcessToField);
                  //insert dynamic table entry
                  await insertDynamicFieldTable(
                      subProcessToField, jsonTemplateId);
                }
              } else {
                //insert dynamic table entry
                await insertDynamicFieldTable(
                    subProcessToField, jsonTemplateId);
              }
            }
          } else {
            print("Not matches");
          }
        }
      }
      print("End of UpdateTemplateOfTable");
    } on Exception catch (_) {
      print("Exception Occured In UpdateTemplateOfTable ");
    }

    //-------End of field to be updated in json_template,field_table ------///
  }

//  static updateDynamicFieldTable(DynamicTableFields dynamicFieldTable,
//      SubProcessFieldsDatum subProcessToField) async {
//    try {
//      print("Start of updateDynamicFieldTable");
//      String tableName = subProcessToField.id.toString();
//      print("iddddddddd" + tableName.toString());
//
//      String tableHeaderArray = subProcessToField.tableHeaders.join(',');
//      String tableKeysArray = subProcessToField.tableFieldKeys.join(',');
//      String tableManditoryFields =
//          subProcessToField.mandatoryTableFields.join(',');
//      String tableManditoryHeaders =
//          subProcessToField.mandatoryTableHeaders.join(',');
//      String tableClearSubProcesFields =
//          subProcessToField.clearSubProcessFields.join(',');
//
//      dynamicFieldTable.table_name = tableName;
//      dynamicFieldTable.table_headers_array = tableHeaderArray;
//      dynamicFieldTable.table_keys_array = tableKeysArray;
//      dynamicFieldTable.mandatory_fields = tableManditoryFields;
//      dynamicFieldTable.mandatory_table_headers = tableManditoryHeaders;
//      dynamicFieldTable.clear_subprocess_fields = tableClearSubProcesFields;
//      db.updation(cKrossStoreDynamicTableFields, dynamicFieldTable);
//      // await cKrossStoreDynamicTableFields.UpdateDataAsync(dynamicFieldTable);
//      print("End of updateDynamicFieldTable");
//    } on Exception catch (_) {
//      print("Exception occured in updateDynamicFieldTable");
//    }
//  }

  static updateTemplateJsonFields(TemplateJson templateJson,
      SubProcessFieldsDatum subProcessToField, String templateFieldJson) async {
    String json_template_id = "";
    try {
      print("Start of updateTemplateJsonFields");
      String tableExist = subProcessToField.isTableExist.toString();
      String controllerName = subProcessToField.controllerName;
      String mandatoryFieldKeys = "";
      String mandatoryFieldLbls = "";
      String allFieldKeys = "";
      List<SubProcessField> arrayListSubProcessField =
          subProcessToField.subProcessFields;
      //get all keys and mandatory keys
      if (arrayListSubProcessField.length > 0) {
        int subProcessFieldCnt = 0;
        for (subProcessFieldCnt = 0;
        subProcessFieldCnt < arrayListSubProcessField.length - 1;
        subProcessFieldCnt++) {
          String _key = arrayListSubProcessField[subProcessFieldCnt].key;
          String _Lbl = arrayListSubProcessField[subProcessFieldCnt].lable;
          allFieldKeys = allFieldKeys + _key + ",";

          bool _isManditory = false;
          if (userRole.toLowerCase().startsWith("VD".toLowerCase())) {
            _isManditory =
            (arrayListSubProcessField[subProcessFieldCnt].isMandatory &&
                !arrayListSubProcessField[subProcessFieldCnt].isreadonly);
          } else {
            _isManditory =
                arrayListSubProcessField[subProcessFieldCnt].isMandatory;
          }

          if (_isManditory) {
            mandatoryFieldKeys = mandatoryFieldKeys + _key + ",";
            mandatoryFieldLbls = mandatoryFieldLbls + _Lbl + ",";
          }
        }

        String key = arrayListSubProcessField[subProcessFieldCnt].key;
        String Lbl = arrayListSubProcessField[subProcessFieldCnt].lable;
        allFieldKeys = allFieldKeys + key;

        bool isManditory = false;
        if (userRole.toLowerCase().startsWith("VD".toLowerCase())) {
          isManditory =
          (arrayListSubProcessField[subProcessFieldCnt].isMandatory &&
              !arrayListSubProcessField[subProcessFieldCnt].isreadonly);
        } else {
          isManditory =
              arrayListSubProcessField[subProcessFieldCnt].isMandatory;
        }

        int Keylength = mandatoryFieldKeys.length;
        int Lbllength = mandatoryFieldLbls.length;
        if (isManditory) {
          mandatoryFieldKeys = mandatoryFieldKeys + key;
          mandatoryFieldLbls = mandatoryFieldLbls + Lbl;
        } else {
          if (mandatoryFieldKeys.length > 0) {
            try {
              mandatoryFieldKeys =
                  mandatoryFieldKeys.substring(0, Keylength - 1);
              mandatoryFieldLbls =
                  mandatoryFieldLbls.substring(0, Lbllength - 1);
            } on Exception catch (_) {
              print("Exception in updateTemplateJsonFields ");
            }
          }
        }
      }

      templateJson.field_json = templateFieldJson;
      templateJson.istable_exists = tableExist;
      templateJson.controller_name = controllerName;
      templateJson.mandatory_field_key_array = mandatoryFieldKeys;
      templateJson.mandatory_field_lable_array = mandatoryFieldLbls;
      templateJson.other_field_key_array = allFieldKeys;
      db.updation(cKrossStoreTemplateJson, templateJson);
      //  await cKrossStoreTemplateJson.UpdateDataAsync(templateJson);
      json_template_id = templateJson.template_json_id.toString();
    } on Exception catch (_) {
      print("Exception in updateTemplateJsonFields ");
    }
    return json_template_id;
  }

  static InsertTemplateInTableAsync(TabDatum processToTabToSubProcess,
      List<SubProcessFieldsDatum> subProcessField) async {
    try {
      print("Start of InsertTemplateInTableAsync");
      ClientTemplate clientTemplates = new ClientTemplate();

      List<SubProcessFieldsDatum> subProcessFieldsData = subProcessField;
      //ClientTemplate clientTemplate ;

      List<String> arrayListFormNames =
          processToTabToSubProcess.tabs[0].subProcesses;
      //-------Start of template to be added in client_template table ------///
      String form_array = arrayListFormNames.toString();
      //string client_name = Application.Current.Properties[AppConstants.CLIENT_NAME].ToString();
      await InitializeAppData();
      //remove space if present
      String template_name_removed_space =
      processToTabToSubProcess.process.replaceAll(" ", "");
      String template_name = template_name_removed_space;
      //String applicant_json=  m_jsonObjectApplicant.toString();
      String details_json = "";

      clientTemplates.applicant_json = m_jsonObjectApplicant;
      clientTemplates.is_deprecated = "false";
      clientTemplates.client_name = client_name;
      clientTemplates.template_name = template_name;
      print("clientTemplates.template_name........" + clientTemplates.template_name);
      clientTemplates.details_json = details_json;
      //  print(clientTemplate.details_json);
      String version_no = "0";

      clientTemplates.version = version_no;
      clientTemplates.form_name_array = form_array;
      // clientTemplate.language = Application.Current.Properties[AppConstants.LANGUAGE].ToString();//commented by swapnil
      clientTemplates.language = app_language;
      //clientTemplate.client_template_id = 1;

      var client_template_id = db.insert("ClientTemplate", clientTemplates);

      String Query = DatabaseQueries.GetLastInsertedRowId();
      List rowList = await db.rawquery(Query);
      var newMap = <String, dynamic>{};
      newMap.addAll(rowList[0]);

      if (rowList.length > 0) {
        print("in if stmt");
        int id = newMap["LastId"];
        client_template_id = id;

        //-------End of template to be added in client_template table ------///

        //-------Start of field to be added in json_template,field_table ------///
        var data = json.encode(subProcessFieldsData);
        print("data");
        print(data);

        List<SubProcessFieldsDatum> arrayListSubProcessToField =
            subProcessFieldsData;
        // var array =[];
        //   array.addAll(arrayListSubProcessToField);
        // arrayListSubProcessToField=  subProcessFieldsData;
        // print(arrayListSubProcessToField);
        for (int formNameCnt = 0;
        formNameCnt < arrayListFormNames.length;
        formNameCnt++) {
          String formName = arrayListFormNames[formNameCnt];
          String templateName = clientTemplates.template_name;

          for (int templateFieldCnt = 0; templateFieldCnt < arrayListSubProcessToField.length; templateFieldCnt++) {
            // print(arrayListSubProcessToField.length);
            try {
              //           print("templateFieldCnt");
              //           print(arrayListSubProcessToField[0]);
              // print(arrayListSubProcessToField[templateFieldCnt]);
              // SubProcessFieldsDatum map= <String,dynamic>{};
              // map.addAll(arrayListSubProcessToField[0]);
              print(" arrayListSubProcessToField......");
              print( arrayListSubProcessToField);
              String fieldFormNameKey =
                  arrayListSubProcessToField[0].subProcessKey;
              String fieldFormName =
                  arrayListSubProcessToField[0].subProcessName;
              print(fieldFormNameKey);
              print(fieldFormName);
              //remove space from fieldFormName
              //             //remove space if present

              String fieldTemplateNameRemovedSpace =
              arrayListSubProcessToField[0].processName.replaceAll(" ", "");
              String fieldTemplateName = fieldTemplateNameRemovedSpace;

              if (formName == fieldFormNameKey &&
                  fieldTemplateName.contains(templateName)) {
                //                 //get row to be updated from json_template table
                print("get row ");

                SubProcessFieldsDatum subProcessToField =
                arrayListSubProcessToField[templateFieldCnt];
                print("next row");
                String templateFieldJson = jsonEncode(subProcessToField);
                print("templateFieldJson next.............");
                print(templateFieldJson);
                String jsonTemplateId = null;

                //                 //insert template json fields
                jsonTemplateId = await InsertTemplateJsonFieldsAsync(
                    subProcessToField,
                    templateFieldJson,
                    client_template_id.toString());
                // db.search();
                print(jsonTemplateId);
                print("complete first insert ...............");

                //                 //check for table exist and enter all data in field_table
                //                 if (subProcessToField.isTableExist)
                //                 {
                //                     //insert dynamic table entry
                //                     print("insert dynamic table entry");
                //                     //await insertDynamicFieldTable(subProcessToField, jsonTemplateId);
                //                 }
              }
            } on Exception catch (_) {
              print("e.GetBaseException();");
            }
          }
        }
      }
      print("End of InsertTemplateInTableAsync");
    } on Exception catch (_) {
      print("Exception occured in InsertTemplateInTableAsync ");
    }

    //-------End of field to be added in json_template,field_table ------///
  }
//         //original

// //         //created by santoshi-3-Apr-2018
  static insertDynamicFieldTable(
      subProcessToField, String jsonTemplateId) async {
    try {
      print("Start of insertDynamicFieldTable");
      DynamicTableFields dynamicFieldTable = new DynamicTableFields();
      String tableName = subProcessToField.tableId.toString();
      // String datasubProcessToField= json.encode(subProcessToField);
      print(tableName);
      print(subProcessToField.tableHeaders.runtimeType);
      subProcessToField.tableHeaders.join(',');
      String tableHeaderArray = subProcessToField.tableHeaders.join(',');

      String tableKeysArray = subProcessToField.tableFieldKeys.join(',');
      String tableManditoryFields =
      subProcessToField.mandatoryTableFields.join(',');
      String tableManditoryHeaders =
      subProcessToField.mandatoryTableHeaders.join(',');
      String tableClearSubProcesFields =
      subProcessToField.clearSubProcessFields.join(',');

      dynamicFieldTable.table_name = tableName;
      dynamicFieldTable.table_headers_array = tableHeaderArray;
      dynamicFieldTable.table_keys_array = tableKeysArray;
      dynamicFieldTable.mandatory_fields = tableManditoryFields;
      dynamicFieldTable.mandatory_table_headers = tableManditoryHeaders;
      dynamicFieldTable.clear_subprocess_fields = tableClearSubProcesFields;
      dynamicFieldTable.template_json_id = jsonTemplateId;
      await db.insertdynamicFieldTable("DynamicTableFields", dynamicFieldTable);
      //await cKrossStoreDynamicTableFields.AddDataAsync(dynamicFieldTable);
      print("End of insertDynamicFieldTable");
    } on Exception catch (_) {
      print("Exception occured in insertDynamicFieldTable ");
    }
  }

  static InsertTemplateJsonFieldsAsync(SubProcessFieldsDatum subProcessToField, String templateFieldJson, String client_template_id) async {
    String json_template_id = "";
    try {
      print("Start of InsertTemplateJsonFieldsAsync");
      print(templateFieldJson);

//      var productArray = JSON.jsonDecode(templateFieldJson);
//      print("productarray");
//      print(productArray);
//      for (int i = 0; i < productArray.length; i++) {
//        Map<String, dynamic> PObject = productArray[i];
//        print("pobject.......");
//        print(PObject);
//        if (PObject.containsKey("subProcessFields")) {
//          var productName = PObject["subProcessFields"];
//          print("productName " + productName);
//        }
//      }
      print(subProcessToField.subProcessFields);
      List<SubProcessField> arrayListSubProcessField =
          subProcessToField.subProcessFields;

      TemplateJson templateJson = new TemplateJson();
      String tableExist = subProcessToField.isTableExist.toString();
      String controllerName = subProcessToField.controllerName;
      int seqId = subProcessToField.seqId;
      print("seqId");
      print(seqId);
      String mandatoryFieldKeys = "";
      String mandatoryFieldLbls = "";
      String allFieldKeys = "";
      // List<SubProcessField> arrayListSubProcessField = subProcessToField.subProcessFields;
      //get all keys and mandatory keys
      if (arrayListSubProcessField.length > 0) {
        int subProcessFieldCnt = 0;
        for (subProcessFieldCnt = 0;
        subProcessFieldCnt < arrayListSubProcessField.length - 1;
        subProcessFieldCnt++) {
          String _key = arrayListSubProcessField[subProcessFieldCnt].key;
          String _Lbl = arrayListSubProcessField[subProcessFieldCnt].lable;
          allFieldKeys = allFieldKeys + _key + ",";
          print("allFieldKeys" + allFieldKeys);
          print("_Lbl" + _Lbl);

          bool _isManditory = false;
          if (userRole.toLowerCase().startsWith("VD".toLowerCase())) {
            _isManditory =
            (arrayListSubProcessField[subProcessFieldCnt].isMandatory &&
                !arrayListSubProcessField[subProcessFieldCnt].isreadonly);
          } else {
            _isManditory =
                arrayListSubProcessField[subProcessFieldCnt].isMandatory;
          }

          if (_isManditory) {
            mandatoryFieldKeys = mandatoryFieldKeys + _key + ",";
            mandatoryFieldLbls = mandatoryFieldLbls + _Lbl + ",";
          }
        }
        String key = arrayListSubProcessField[subProcessFieldCnt].key;
        String Lbl = arrayListSubProcessField[subProcessFieldCnt].lable;
        allFieldKeys = allFieldKeys + key;

        bool isManditory = false;
        if (userRole.toLowerCase().startsWith("VD".toLowerCase())) {
          isManditory =
          (arrayListSubProcessField[subProcessFieldCnt].isMandatory &
          !arrayListSubProcessField[subProcessFieldCnt].isreadonly);
        } else {
          isManditory =
              arrayListSubProcessField[subProcessFieldCnt].isMandatory;
        }

        int Keylength = mandatoryFieldKeys.length;
        int Lbllength = mandatoryFieldLbls.length;
        if (isManditory) {
          mandatoryFieldKeys = mandatoryFieldKeys + key;
          mandatoryFieldLbls = mandatoryFieldLbls + Lbl;
        } else {
          if (mandatoryFieldKeys.length > 0) {
            print("mandatoryFieldKeys.length > 0");
            print(mandatoryFieldKeys.substring(0, Keylength - 1));
            print(mandatoryFieldLbls =
                mandatoryFieldLbls.substring(0, Lbllength - 1));
            try {
              mandatoryFieldKeys =
                  mandatoryFieldKeys.substring(0, Keylength - 1);
              mandatoryFieldLbls =
                  mandatoryFieldLbls.substring(0, Lbllength - 1);
            } on Exception catch (_) {
              print(" e.GetBaseException();");
            }
          }
        }
      }
      templateJson.form_name = subProcessToField.subProcessName;
      templateJson.field_json = templateFieldJson;
      templateJson.client_template_id = client_template_id;
      templateJson.istable_exists = tableExist;
      templateJson.mandatory_field_lable_array = mandatoryFieldLbls;
      templateJson.controller_name = controllerName;
      templateJson.mandatory_field_key_array = mandatoryFieldKeys;
      templateJson.other_field_key_array = allFieldKeys;


      //db.add(cKrossStoreTemplateJson, templateJson);
      //await cKrossStoreTemplateJson.AddDataAsync(templateJson);
      json_template_id = templateJson.template_json_id.toString();
      print(" templateJson.field_json.........");
      print( templateJson.field_json);
      await TemplateJsonTable.insertTemplateJson(templateJson);
      print("End of InsertTemplateJsonFieldsAsync");
    } on Exception catch (_) {
      print("Exception occured in InsertTemplateJsonFieldsAsync ");
    }
    return json_template_id;
  }

  static SaveMenusInDatabase(String jsonkey, String jsonValue) async {
    try {
      //cKrossStoreLoginJsonTemplate = new SQLiteLoginJsonTemplate(DependencyService.Get<ISQLiteDb>());
      print("Start of SaveMenusInDatabase");
      print(jsonkey);
      print(jsonValue);
      //await InitDatabase();
      await InitializeAppData();
      LoginJsonTemplate menuTemplate = new LoginJsonTemplate();
      LoginJsonTemplateDB loginJsonTemplateDB = new LoginJsonTemplateDB();
      menuTemplate.jsonKey = jsonkey;
      menuTemplate.jsonValue = jsonValue;
      menuTemplate.language = app_language;

      String countQuery = DatabaseQueries.GetLoginTemplateCountQuery(
          menuTemplate.jsonKey, menuTemplate.language);
      print("countQuery" + countQuery);
      //  db.search();
      await db.rawquery(countQuery);
      List<LoginJsonTemplate> loginTemplateJsonList =
      new List<LoginJsonTemplate>();
      loginTemplateJsonList = await db.execution(countQuery);
      print("loginTemplateJsonList");
      print(loginTemplateJsonList);
      if (loginTemplateJsonList != null) {
        if (loginTemplateJsonList.length > 0) {
          String Query = DatabaseQueries.DeleteLoginJsontemplateImageJson(
              menuTemplate.jsonKey, menuTemplate.language);
          await db.execution(Query);
          // await db.insertLoginJsonTemplate("LoginJsonTemplate",menuTemplate);
          int id = await loginJsonTemplateDB.insertLoginJsonData(menuTemplate);
          print("menutemplate" + id.toString());
        }
        //added by nilima on 21 June 2018
        else {
          //int diImagesRowID = await db.insertLoginJsonTemplate("LoginJsonTemplate", menuTemplate);
          int id = await loginJsonTemplateDB.insertLoginJsonData(menuTemplate);
          print("menutemplate" + id.toString());
        }
      } else {
        // int diImagesRowID = await db.insertLoginJsonTemplate("LoginJsonTemplate", menuTemplate);
        int id = await loginJsonTemplateDB.insertLoginJsonData(menuTemplate);
        print("menutemplate" + id.toString());
      }
      print("End of SaveMenusInDatabase");
    } catch (e) {
      print("Exception ocuured in SaveMenusInDatabase $e");
    }
  }

  TruncateTemplateJsonTable() async {
    try {
      print("Start of TruncateTemplateJsonTable");
      String query =
      DatabaseQueries.truncateTable(DatabaseQueries.TABLE_TEMPLATE_JSON);
      print(query);
      db.execution(query);
      print("End of TruncateTemplateJsonTable");
    } on Exception catch (_) {
      print("Exception occured in TruncateTemplateJsonTable ");
    }
  }

  TruncateLoginJsonTemplateTable() async {
    try {
      print("Start of TruncateLoginJsonTemplateTable");
      String query = DatabaseQueries.truncateTable(
          DatabaseQueries.TABLE_LOGIN_JSON_TEMPLATE);
      db.execution(query);
      print("End of TruncateLoginJsonTemplateTable");
    } on Exception catch (e) {
      FileUtils.printLog(
          "Exception occured in TruncateLoginJsonTemplateTable +$e");
    }
  }

  TruncateProductTable() async {
    try {
      FileUtils.printLog("Start of TruncateProductTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_PRODUCT);
      await db.execution(query);
      FileUtils.printLog("End of TruncateProductTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateProductTable $e ");
    }
  }

  TruncateMakeTable() async {
    try {
      print("Start of TruncateMakeTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_MAKE);
      await db.execution(query);
      print("End of TruncateMakeTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateMakeTable $e");
    }
  }

  TruncateModelTable() async {
    try {
      FileUtils.printLog("Start of TruncateModelTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_MODEL);
      await db.execution(query);
      FileUtils.printLog("End of TruncateModelTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateModelTable $e");
    }
  }

  TruncateDealerSOTable() async {
    try {
      FileUtils.printLog("Start of TruncateDealerSOTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_DEALER_SO);
      await db.execution(query);
      FileUtils.printLog("End of TruncateDealerSOTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateDealerSOTable $e");
    }
  }

  TruncateMakeDealerDataTable() async {
    try {
      FileUtils.printLog("Start of TruncateMakeDealerDataTable");
      String query = DatabaseQueries.TruncateGivenTable(
          DatabaseQueries.TABLE_MAKE_DEALER_DATA);
      // cKrossStoreMakeDealerData = new SQLiteMake_Dealer_Data(DependencyService.Get<ISQLiteDb>());
      // await cKrossStoreMakeDealerData.ExecuteQueryAsync(query);
      db.execution(query);
      FileUtils.printLog("End of TruncateMakeDealerDataTable");
    } on Exception catch (e) {
      FileUtils.printLog(
          "Exception occured in TruncateMakeDealerDataTable $e ");
    }
  }

  TruncateStatesTable() async {
    try {
      FileUtils.printLog("Start of TruncateStatesTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_STATES);
      //cKrossStoreStates = new SQLiteState(DependencyService.Get<ISQLiteDb>());
      //await cKrossStoreStates.ExecuteQueryAsync(query);
      //var a = await cKrossStoreStates.ExecuteQueryAsync("Select * FROM " + DatabaseQueries.TABLE_STATES);
      await db.execution(query);
      FileUtils.printLog("End of TruncateStatesTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateStatesTable $e");
    }
  }

//         //Added by Netra on 26/12/2018
  TruncateDealerLocationVDTable() async {
    try {
      FileUtils.printLog("Start of TruncateDealerLocationVDTable");
      String query = DatabaseQueries.TruncateGivenTable(
          DatabaseQueries.TABLE_DEALER_LOCATION_VD);
      //await cKrossStoreDealer_LocationVD.ExecuteQueryAsync(query);
      db.execution(query);
      FileUtils.printLog("End of TruncateDealerLocationVDTable");
    } on Exception catch (e) {
      FileUtils.printLog(
          "Exception occured in TruncateDealerLocationVDTable $e");
    }
  }

  TruncateModelVDTable() async {
    try {
      FileUtils.printLog("Start of TruncateModelVDTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_MODEL_VD);
      //await cKrossStoreModelVD.ExecuteQueryAsync(query);
      db.execution(query);
      FileUtils.printLog("End of TruncateModelVDTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateModelVDTable $e");
    }
  }

  TruncateVehicleCategoryTable() async {
    try {
      FileUtils.printLog("Start of TruncateVehicleCategoryTable");
      String query = DatabaseQueries.TruncateGivenTable(
          DatabaseQueries.TABLE_VEHICLE_CATEGORY);
      //await cKrossStoreModelVD.ExecuteQueryAsync(query);
      db.execution(query);
      FileUtils.printLog("End of TruncateVehicleCategoryTable");
    } on Exception catch (e) {
      FileUtils.printLog(
          "Exception occured in TruncateVehicleCategoryTable $e ");
    }
  }

  TruncateYearVDTable() async {
    try {
      FileUtils.printLog("Start of TruncateYearVDTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_YEAR_VD);
      //await cKrossStoreYearVD.ExecuteQueryAsync(query);
      db.execution(query);
      FileUtils.printLog("End of TruncateYearVDTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateYearVDTable $e");
    }
  }

  TruncateCityTable() async {
    try {
      FileUtils.printLog("Start of TruncateCityTable");
      String query =
      DatabaseQueries.TruncateGivenTable(DatabaseQueries.TABLE_CITY);
      //await cKrossStoreCity.ExecuteQueryAsync(query);
      db.execution(query);
      FileUtils.printLog("End of TruncateCityTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateCityTable $e");
    }
  }

  TruncateSubLoanTypeTable() async {
    try {
      FileUtils.printLog("Start of TruncateSubLoanTypeTable");
      String query = DatabaseQueries.TruncateGivenTable(
          DatabaseQueries.TABLE_SUB_LOAN_TYPE);
      //CKrossStore<SubLoanType> temp = new SQLiteSubLoanType(DependencyService.Get<ISQLiteDb>());
      // await temp.ExecuteQueryAsync(query);
      db.execution(query);
      FileUtils.printLog("End of TruncateSubLoanTypeTable");
    } on Exception catch (e) {
      FileUtils.printLog("Exception occured in TruncateSubLoanTypeTable $e");
    }
  }

// //         //SO Delaer Data save code
  static AddDealerSODataAsync(jArray) async {
    try {
      print("Start of AddDealerSODataAsync");

      for (Object jArrayDelaership in jArray) {
        // Object jObjectDealership = jArrayDelaership;
        Map<String, dynamic> jObjectDealership = jArrayDelaership;
        // print("jObjectDealership"+jArrayDelaership.toString());
        String dealership_name =
        jObjectDealership["Dealership_Name"].toString();
        DealerSO dealerSO = new DealerSO();
        dealerSO.dealership_name = dealership_name;
        dealerSO.dealership_data = jObjectDealership.toString();

        db.insertDealerSO("DealerSO", dealerSO);
      }
      print("End of AddDealerSODataAsync");
    } catch (e) {
      print("Exception occured in AddDealerSODataAsync $e");
    }
  }

  static InitializeAppData() async {
    try {
      print("Start of InitializeAppData(");
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      if (tempLanguage == null || tempLanguage.isNotEmpty) {
        app_language = tempLanguage;
      } else {
        sp.setString(AppConstants.LANGUAGE, AppConstants.LANGUAGE);
        app_language = AppConstants.APP_LANG;
      }

      var tempUserRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      if (tempUserRole != null || tempUserRole.isNotEmpty) {
        userRole = tempUserRole;
      } else {
        // String query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ROLE);
        String query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ROLE, app_language);
        List<LoginJsonTemplate> listUserRole = await db.execution(query);
        if (listUserRole.length > 0) {
          userRole = listUserRole[0].jsonValue;
        }
      }

      var tempNbfcName = sp.getString(AppConstants.CLIENT_NAME);
      nbfcName = ((tempNbfcName != null) || (tempNbfcName.isNotEmpty))
          ? tempNbfcName
          : AppConstants.CLIENT_NAME;

      var tempClient_name = sp.getString(AppConstants.CLIENT_NAME);
      client_name = ((tempClient_name != null) || (tempClient_name.isNotEmpty))
          ? tempClient_name
          : AppConstants.DATABASE_NAME;

      print("End of InitializeAppData");
    } on Exception catch (e) {
      print("Exception occured in InitializeAppData $e");
    }
  }
}
