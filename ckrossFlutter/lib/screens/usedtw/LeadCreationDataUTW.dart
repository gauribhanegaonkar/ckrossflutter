import 'dart:convert' as JSON;
import 'dart:collection';
import 'dart:convert';
import 'dart:io' show HttpStatus, Platform;
import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/CKrossUI.dart';
import 'package:ckrossFlutter/Utils/Headers.dart';
import 'package:ckrossFlutter/Utils/TextField.dart';
import 'package:ckrossFlutter/Utils/TextField.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/Utils/dropdown.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/YearVdDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/ckross_casedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/client_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/dealer_SOdataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/dyanamic_table_fielddataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/loginJson_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/makeDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/modelDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/model_vdDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/productDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/subLoanTypeDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/templateJsonDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ResponseModels/DoOtpLoginResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/getUniqueColumnData.dart';
import 'package:ckrossFlutter/models/ckrossCase.dart';
import 'package:ckrossFlutter/models/clientTemplate_data.dart';
import 'package:ckrossFlutter/models/dealer_locationVD.dart';
import 'package:ckrossFlutter/models/dealerso_data.dart';
import 'package:ckrossFlutter/models/dyanamic_table_field.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/models/modelVd.dart';
import 'package:ckrossFlutter/models/templateJson.dart';
import 'package:ckrossFlutter/models/yearVD.dart';
import 'package:ckrossFlutter/providers/getModelbyMake_api_provider.dart';
import 'package:ckrossFlutter/providers/sublone_api_provider.dart';
import 'package:ckrossFlutter/providers/vehicalCategory_api_provider.dart';
import 'package:ckrossFlutter/providers/year_api_provider.dart';
import 'package:ckrossFlutter/screens/LeadCreationImage.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeadCreationDataUTW extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LeadCreationDataStateful());
  }
}

class LeadCreationDataStateful extends StatefulWidget {
  String _selection;
  LeadCreationDataStateful({Key key}) : super(key: key);

  @override
  _LeadCreationDataState createState() => new _LeadCreationDataState();
}

class _LeadCreationDataState extends State<LeadCreationDataStateful>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  DatabaseHelper db = DatabaseHelper();
  var list;
  final _formKey = GlobalKey<FormState>();
  TextEditingController controllers = new TextEditingController();
  // var textEditingControllers = <TextEditingController>[];
  Map<String, TextEditingController> textEditingControllers = {};
  Map<String, TextEditingController> textEditingControllerTextField = {};
  String allMandatoryKeys, client_template_id, templateName;
  String userId, appLang, userName, nbfcName, databaseName, userRole;
  String currentProductID;
  String applicantJson = null, detailsJson = null;
  String currentProductName;
  String controllerName, jsonTemplateId, allFieldsKeys;
  final _key = GlobalKey();
  List<Widget> children = [];
  List<Widget> labelchildren = [];
  var controllerList = [];
  var placeholderName;
  String case_id;

  var dealershipName = "",
      dealerName = "",
      dealerBranch = "",
      Location = "",
      Dealer_Code = "",
      SourcingChannel = "",
      dealer_State = "",
      dealerAddress = "",
      DealersBankDetails,
      DisbursementTO = "",
      DealerBankName = "",
      DealerBankAccountNo = "",
      DealerIFSCCode = "",
      DealerBranchName = "",
      DealerMICRCode = "",
      Type_Of_Account = "",
      dealerDistrictname = "",
      rto_by_whom = "",
      Insurance_by_whom = "";

  static String _selection;

  var activePlaceholderColor, placeholderColor;

  String text;
  var keyboard;
  bool isEnabled;
  bool isData = false;
  bool isValid = true;
  var product;
  var make;
  List makeList;
  List subLoanList;
  var model;
  var labeltext;
  String LeadID;
  int _count = 0;
  var res;
  var value;
  var placeholder;
  var textfiledtext;
  var validationMessage = "", validationpattern = "", maxlength;
  Map mapFormData = Map();
  SharedPreferences sp;
  List<String> arrayUI = [];
  var arr = [];
  String user;
  var returned;
  var textEditingControllerText;
  var editingcomplete;
  var keyboardDropdown;
  var validationPatternDropdown = "";
  var validationMessageDropdown = "";
  var maxlengthDropdown;

  bool isEnabledDropdown;
  String selectedmkid;
  String selectedmodelid;
  Map<String, dynamic> SubprocessedField;

  Map<String, dynamic> SubprocessedFieldModel;
  FocusNode _focus = new FocusNode();

  void initState() {
    super.initState();
    initData();
  }

  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    //children.clear();
  }

  var _chosenValue;
  Widget build(BuildContext context) {
    //_selection=widget._selection;
    return WillPopScope(
        onWillPop: () {
          Navigator.pushReplacementNamed(context, '/homeScreen');
          children.clear();
        },
        child: Scaffold(
            appBar: AppBar(
              title: Text("Create Lead utw"),
            ),
            body: Form(
                key: _formKey,
                child: Container(
                  margin:
                      EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 0),
                  child: SingleChildScrollView(
                      child: Column(
                    children: <Widget>[
                      Column(
                          key: _key,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: children),
                      Container(
                        margin: EdgeInsets.only(
                            left: 0, top: 20, right: 0, bottom: 0),
                        width: double.infinity,
                        child: RaisedButton(
                            color: Colors.lightBlue[900],
                            child: Text("NEXT",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18)),
                            onPressed: () {
//                              print("Successfullly");
//                              print(mapFormData);
                              //_formKey.currentState.save();
                              // saveFromData();
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                print("Successfullly");
                                print(mapFormData);
                              saveFromData();
                                Navigator.pushReplacementNamed(context, '/leadCreationImage');
                              }

                              //Textfield().onbuttonpress();
                            }),
                      ),
                    ],
                  )),
                ))));
  }

  initData() async {
    try {
      print("initData method");
      AppConstants.IsValidData = null;
      ClientTemplate cKrossStoreClientTemplate;
      TemplateJson cKrossStoreTemplateJson;
      DynamicTableFields cKrossStoreDynamicTableFields;
      LoginJsonTemplate cKrossStoreLoginJsonTemplate;

      await getTemplateFromDatabaseAsync();
      print("initData method...........");
      print(textEditingControllerTextField);
      // textEditingControllerTextField["dealer_State"].text = "Neha";
      await intiSetBranchAndState();
    } on Exception catch (_) {
      print("Exception in InitComponents");
    }
  }

  intiSetBranchAndState() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String dealerbranch = sp.getString(AppConstants.DEALER_BRANCH);
    print(dealerbranch);
    print("init dealer branch");



        textEditingControllerTextField["dealerBranch"].text = dealerbranch;
        textEditingControllerTextField["Branch"].text = dealerbranch;

      print("dealer_State........");
      textEditingControllerTextField["dealer_State"].text =
          sp.getString(AppConstants.DEALER_STATE);

  }

  getTemplateFromDatabaseAsync() async {
    try {
      print("getTemplateFromDatabaseAsync method");
      templateName = AppConstants.UPLOAD_TEMPLATE_NAME;
      await InitializeAppData();
      if (!mounted) {
        return;
      }
      String version_no = "0";
      String query1 =
          DatabaseQueries.CreateSingleClientTemplateAccVerionNoQuery(
              templateName, nbfcName, appLang, version_no, "false");
      List listClientTemplate =
          await clientTemplateDB.executeClientTemplateData(query1);

      var getFormNameArray = <String, dynamic>{};
      getFormNameArray.addAll(listClientTemplate[0]);
      var abc = getFormNameArray["form_name_array"].toString();
      abc = abc.substring(1, abc.length - 1);

      print(listClientTemplate);

      if (listClientTemplate.length > 0) {
        print("successssssssss");
        var map = <String, dynamic>{};
        map.addAll(listClientTemplate[0]);
        Map<String, dynamic> clientTemplate = map;
        applicantJson = clientTemplate['applicant_json'];
        detailsJson = clientTemplate['details_json'];
        client_template_id = clientTemplate['client_template_id'].toString();
        var formNameArray = abc.split(',');

        // String query2 = DatabaseQueries.CreateGetTemplateByformname(client_template_id);

        String query2 = DatabaseQueries.CreateGetTemplateByClientTemplateId(
            client_template_id);
        List templateJsonArrayList =
            await TemplateJsonTable.executeTemplatejsonData(query2);

        if (templateJsonArrayList.length > 0) {
          await createUIFromDbTemplate(templateJsonArrayList, formNameArray);
        }
      }
    } on Exception catch (_) {
      print("Exception in getTemplateFromDatabaseAsync");
    }
  }

  InitializeAppData() async {
    try {
      print("InitializeAppData method");
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      appLang = !(tempLanguage == null || tempLanguage.isEmpty)
          ? tempLanguage
          : AppConstants.APP_LANG;
      var tempUserId = sp.getString(AppConstants.LOGGED_IN_USER_ID);
      print(tempUserId);
      if (!(tempUserId == null || tempUserId.isEmpty)) {
        // print(tempUserId);
        userId = tempUserId;
      } else {
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ID, appLang);
        List listUser = await db.execution(Query);
        if (!mounted) {
          return;
        }
        print("listUser" + listUser.toString());
        if (listUser.length > 0) {
          userId = listUser[0].jsonValue;
        }
      }

      var tempnbfcName = sp.getString(AppConstants.CLIENT_NAME);
      nbfcName = !(tempnbfcName == null || tempnbfcName.isEmpty)
          ? tempnbfcName
          : AppConstants.CLIENT_NAME;
      var tempUserRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      if (!(tempUserRole == null || tempUserRole.isEmpty)) {
        userRole = tempUserRole;
      } else {
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ROLE, appLang);
        List<LoginJsonTemplate> listUserRole = await db.execution(Query);
        if (listUserRole.length > 0) {
          userRole = listUserRole[0].jsonValue;
        }
      }
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
      currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
      print("End of InitializeAppData ");
    } on Exception catch (_) {
      print("Exception In InitializeAppData");
    }
  }

  createUIFromDbTemplate(templateJsonArrayList, formNameArray) async {
    //CkrossUI ckrossUI = new CkrossUI(this);
    try {
      print("createUIFromDbTemplate");
      String dealer_data_json = null;
      String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
          "dealer_data", appLang);
      List listLoginJson =
          await LoginJsonTemplateDB.executeLoginJsonTemplateData(Query);
      //print(listLoginJson);
      if (listLoginJson.length > 0) {
        dealer_data_json = listLoginJson[0].jsonValue;
      }
      if (!(dealer_data_json == null || dealer_data_json.isEmpty)) {
        bool isSourcingActive = false;
        Map<String, String> dealerJsonObject = json.decode(dealer_data_json);
        //var dealerJsonObject = dealer_data_json;
        if (dealerJsonObject.containsKey("Is_Sourcing_Active")) {
          isSourcingActive = dealerJsonObject["Is_Sourcing_Active"] as bool;
        }

        if (!isSourcingActive) {
          showDialogs();
          //await Navigation.PopAsync();
        }
      }
      await InitializeAppData();
      print("after app initialize");
      // print(templateJsonArrayList);
      for (int j = 0; j < formNameArray.length; j++) {
        for (int i = 0; i < templateJsonArrayList.length; i++) {
          print("in for..........");
          //print(formNameArray[j]);
          var templateJson = <String, dynamic>{};

          templateJson.addAll(templateJsonArrayList[i]);
          print(templateJsonArrayList[i]);
          print(formNameArray[j].toString());
          print(templateJson["form_name"].toString());
          if (formNameArray[j].toString().replaceAll(" ", "") ==
              templateJson["form_name"].toString().replaceAll(" ", "")) {
            print("successfully in if stmt");

            String tableExist = templateJson["istable_exists"];
            String template_json_id =
                templateJson["template_json_id"].toString();

            print(templateJson["form_name"]);
            DynamicTableFields dynamicTableField = new DynamicTableFields();

            if (tableExist.toLowerCase() == "true".toLowerCase()) {
              String query = DatabaseQueries.CreateGetSingleDynamicFieldTable(
                  template_json_id);
              print("template_json_id" + template_json_id);
              List dynamicTableFieldList =
                  await DynamicTableFieldsDB.ExecuteQueryDynamicTableFields(
                      query);
              if (dynamicTableFieldList.length > 0) {
                dynamicTableField = dynamicTableFieldList[0];
                print("dynamicTableField");
                // print(dynamicTableField);
              }
            }
            controllerName = templateJson["controller_name"];
            jsonTemplateId = templateJson["template_json_id"].toString();
            String fieldJson = templateJson["field_json"];
            tableExist = templateJson["istable_exists"];
            allFieldsKeys = templateJson["other_field_key_array"];
            allMandatoryKeys = templateJson["mandatory_field_key_array"];
            String formName = templateJson["form_name"];

            CreateUIForHeader(formName);
            print("fieldjson......");
            print(fieldJson);
            for (int i = 0; i < fieldJson.length; i++) {}
            var subProcessToField = <String, dynamic>{};
            subProcessToField.addAll(json.decode(fieldJson));
            var subProcessFields = subProcessToField["subProcessFields"];

//            for(int i = 0; i< subProcessFields.length; i++){
//              var subProcessFields = subProcessToField["subProcessFields"];
//              print("subprocess fielsd........");
//              print(subProcessFields);
//            }
//            List<String> mobileapikey = subProcessFields["fieldName"];
//            print("mobile api .......");
//            print(mobileapikey);

            for (int subProcessFieldsCnt = 0;
                subProcessFieldsCnt < subProcessFields.length;
                subProcessFieldsCnt++) {
              print("applicantJson in lead creation");
              print(applicantJson);
              var view;

              view = await CreateHorizontalLayoutAndFields(
                  formName,
                  subProcessFields[subProcessFieldsCnt],
                  detailsJson,
                  templateName,
                  applicantJson,
                  dealer_data_json);

              if (view != null) {
                if (mounted) {
                  setState(() {
                    children.add(view);
                  });
                }
                //print(view);
              }
            }
          }
        }
      }
    } on Exception catch (_) {
      print("Exception In createUIFromDbTemplate");
    }
  }

  showDialogs() {
    return showDialog(
      // context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: new Text(
              "You can’t select the dealer as dealer is blocked from sourcing"),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  CreateUIForHeader(formName) async {
    print("CreateUIForHeader");
    if (mounted) {
      setState(() {
        children.add(Rows(formName));
      });
    }
  }

  Widget Rows(formName) {
    print("Rows");
    return Row(
      children: <Widget>[
        Headers.headers(formName),
        Container(
          height: 20,
        )
      ],
    );
  }

  CreateHorizontalLayoutAndFields(formName, subProcessField, detailsJson,
      templateName, applicantJson, dealerJson) async {
    print("applicantJsonString in CreateHorizontalLayoutAndFields");
    print(applicantJson);

    String keys = subProcessField["key"].toString();
    print("keys.........." + keys);
    arrayUI.add((keys));
    print("arrrrrrrrrrrrrrrrrrrr..............");
    print(arrayUI);

    var val;
    try {
      if (mounted) {
        val = await IdentifyViews(formName, subProcessField, detailsJson,
            templateName, applicantJson, dealerJson);
      }
      print("vall");
      print(val);
      print("view in CreateHorizontalLayoutAndFields");
    } catch (e) {
      print("Exception in CreateHorizontalLayoutAndFields() : ");
    }
    return val;
  }

  IdentifyViews(formName, subProcessFields, String detailsData,
      String templateName, applicantJsonString, String dealerJson) async {
    print("applicantJsonString in identitfyviews");
    print(applicantJsonString);

    BuildContext context;
    var labelreturn;
    //  var map = <String,dynamic>{};
    //  map.addAll(subProcessField)
    //
    // ;
    //  print(map);
    String FieldType = subProcessFields["fieldType"].toString();
    String value = subProcessFields["defaultValue"].toString();
    print("before val");

    //  print(subProcessFields);
    //  print(subProcessFields["defaultValue"]);
    //print(subProcessFields);
    print("fieldType");

    print(subProcessFields["fieldType"]);
    switch (subProcessFields["fieldType"]) {
      case "hidden":
        if (FieldType.toLowerCase() == "hidden") {
          createTextField(subProcessFields, applicantJsonString, dealerJson);

          //MyHomePage();

          var entry = subProcessFields["key"];
          if (!(entry == "isEnabled")) {
            entry = true;

            if (subProcessFields["key"] == "Dealer_Type" &&
                (subProcessFields["value"] == null ||
                    subProcessFields["value"] == " ")) {
              //         JObject jsonObjectApplicant = null;
              //         using (var reader = new JsonTextReader(new StringReader(applicantJsonString)))
              //         {
              //             reader.DateParseHandling = DateParseHandling.None;
              //             jsonObjectApplicant = JObject.Load(reader);
              //         }

              //         if (jsonObjectApplicant.ContainsKey("LoanType"))
              //         {
              //             String loanType = (string)jsonObjectApplicant.SelectToken("LoanType");
              //             entry.Text = loanType;
              //         }
              //         else
              //         {
              //             entry.Text = subProcessField.Value.ToString();
              //         }
            } else {
              // entry.Text = subProcessField.Value.ToString();
            }
            entry.IsEnabled = false;
          }
        }
        // parent.Children.Add(entry);
        break;
      case "label":
        //create lable first
        Text label = null;
        print("In case lable");
        print(subProcessFields["isMandatory"]);
        if (subProcessFields["isMandatory"]) {
          labelreturn = Row(
            children: <Widget>[
              Text(
                // textcolo : Colors.white,
                subProcessFields["lable"] + ":",
                style: TextStyle(
                  color: Colors.red,
                  fontFamily: 'Raleway',
                  fontSize: 18.0,
                ),
              ),
              Text(
                value,
                style: TextStyle(color: Colors.black),
              )
            ],
          );
        } else {
          labelreturn = Text(
            // textcolo : Colors.white,
            subProcessFields["key"] + ":",
            style: TextStyle(
              color: Colors.blue,
              fontFamily: 'Raleway',
              fontSize: 18.0,
            ),
          );
          // return label;
        }

        if (value == null || value.isEmpty)
        //|| templateName.Equals("QueryResolution") ||
        //           templateName.Equals("Pre-DisbursementDocumentUpload") ||
        //           templateName.Equals("Post-DisbursementDocumentUpload") ||
        //           templateName.Equals("Pre-DisbursementAdviceUpload") ||
        // templateName.Equals(AppConstants.STATUS_UPDATE_LEAD_TEMPLATE_NAME) ||
        //           templateName.Equals(AppConstants.SERVICE_INSPECTION_TEMPLATE_NAME) ||
        //           templateName.Equals(AppConstants.PRE_DISBURSEMENT_RESOLVE_QUERIES_TEMPLATE_NAME) ||
        //           templateName.Equals(AppConstants.POST_DISBURSEMENT_RESOLVE_QUERIES_TEMPLATE_NAME) ||
        //           templateName.Equals(AppConstants.CC_LEADS_CREATED_TEMPLATE_NAME) ||
        //           templateName.Equals(AppConstants.CREDIT_REFER_QUEUE_TEMPLATE_NAME)
        // )
        {
          try {
            //               JObject jsonObjectApplicant = new JObject(applicantJsonString);
            //               value = (string)jsonObjectApplicant.SelectToken(subProcessField.Key);
            print("before  aaaaaaaa");
            if (subProcessFields["key"] == "LEAD ID") {
              print("subProcessField lead id");
              print(subProcessFields);
              //                   JObject jsonObjectDetailsData = new JObject(detailsData);
              value = subProcessFields;
            }
          } catch (e) {
            Object jsonObjectApplicant = null;
            print("In catch lable");
            try {
              //                   JObject jsonObjectDetailsData = new JObject(detailsData);
              if (subProcessFields["key"] == "LEADID") {
                value = subProcessFields;
              } else {
                value = subProcessFields;
              }
            } catch (e1) {
              print("Exception in label");
              if (subProcessFields["key"] == "LEADID") {
                value = LeadID;
              }
            }
          }
        }

        // //create value of that label

        return labelreturn;
        break;

      case "textArea":
        try {
          // print("ismadnrertio"+subProcessFields["IsMandatory"].toString());
          if (subProcessFields["IsMandatory"] == true) {
            print("IN texarea");
            print(subProcessFields["lable"]);
            labelreturn = Text(
              subProcessFields["lable"],
              style: TextStyle(color: Colors.red, fontSize: 18),
            );
          } else {
            print("IN texarea else");
            print(subProcessFields["lable"]);
            //   labelreturn = Text(
            //    subProcessFields["lable"],
            //    style:TextStyle(
            //     color: Colors.blue,
            //     fontSize: 18
            //    ),
            //  );
            labelreturn = createTextField(
                subProcessFields, applicantJsonString, dealerJson);
          }
        } catch (e1) {
          print("Excetion in textarea switch case");
        }
        return labelreturn;
        break;

      case "text":
        if (subProcessFields["isDataList"]) {
          print("i ma text");

//          labelreturn = createTextField(
//              subProcessFields, applicantJsonString, dealerJson);

           if(subProcessFields["fieldName"] == 'Branch'){
            print("i ma branch");
            subProcessFields['key'] = 'Branch';

            print( subProcessFields);
            print("i ma branch");
            labelreturn = createTextField(
                subProcessFields, applicantJsonString, dealerJson);
            }else{
            print("i ma text else");
            labelreturn = createTextField(
              subProcessFields, applicantJsonString, dealerJson);}
        } else {
          print("i ma label");
          labelreturn = createTextField(
              subProcessFields, applicantJsonString, dealerJson);
          print("label return");
          print(labelreturn);
        }
        return labelreturn;
        break;
      case "dropDownList":
        labelreturn =
            createDropDown(formName, subProcessFields, applicantJsonString);
        return labelreturn;
        break;
    }
  }

  createDropDown(formname, subProcessField, applicantJsonString) {
    var placeholder;
    bool isMandatory = false;
    print("In dropdown");
    print(placeholder.toString().replaceAll(" ", "r") + "Controller");
    var textEditingController;
    var placeholderName;

    createDealerDependentField(dealershipName) async {
      print("In createDealerDependentField");
      String query = DatabaseQueries.GetDealershipDataByName(dealershipName);
      List dealerSOList = await DealerSODB.executeClientTemplateData(query);
      print("prints");

      var dealerSO = <String, dynamic>{};
      dealerSO.addAll(dealerSOList[0]);
      // print(dealerSO['dealership_data']);
      var list = dealerSO['dealership_data'].toString().split(',');

      if (dealerSOList.length > 0) {
        if (dealerSO['dealership_data'].toString().contains("isActive")) {
          var dependentFieldMob =
              subProcessField["dependentFieldMobList"].toString().split(',');
          for (int i = 0; i < dependentFieldMob.length; i++) {
            print("dependentFieldMob");
            // print(subProcessField["dependentFieldMobList"]);
            print(dependentFieldMob[i]);

            var ab = dependentFieldMob[i].toString();
            ab = ab.substring(1, ab.length);
            var MobList = ab.split(',');
            var abc = dealerSO["dealership_data"].toString();
            abc = abc.substring(1, abc.length - 1);
            var pqr = abc.split(',');
            print(pqr[0]);

            for (int j = 0; j < pqr.length; j++) {
              print("for print only");
              print(pqr[j]);
            }
            for (int j = 0; j < pqr.length; j++) {
              print(" in for  inner");
              print(pqr[0]);
              if (pqr[j].trim().contains("Branch_Name")) {
                print("dealerbranch1111");
                var xyz = pqr[j].replaceAll(":", ',').split(',');
                print(xyz[1]);
                 print("inif dealer Branch ");
//                setState(() {
//                  textEditingControllerTextField["dealerBranch"].text = xyz[1].trim();
//                });
              }

              if ((pqr[j]).contains(MobList[0].trim())) {
                print("In if moblist");
                print(MobList[0]);
                var xyz = pqr[j].replaceAll(":", ',').split(',');
                print(xyz[0].trim());

                print(MobList);

                if (xyz[0].trim() == "dealer_State") {
                  setState(() {
                    textEditingControllerTextField["dealer_State"].text =
                        xyz[1].trim();
                    textEditingControllerTextField["SourcingChannel"].text =
                        "Dealer";
                  });
                }
                if (xyz[0].trim() == "Dealer_Code" ||
                    xyz[0].trim() == "Dealer_Name") {
                  print("dealer name.......");
                  print(textEditingControllerTextField["dealer"]);
                  print(xyz[1]);
                  setState(() {
                    textEditingControllerTextField[xyz[0].trim()].text =
                        xyz[1].trim();
                  });
                }
                //createTextField("subProcessFields", "applicantJsonString", "dealerJson").setdata();
                //print(textEditingControllerTextField["Middle Name"].text);

              }
            }
          }
        }
      }
    }

    Future<void> _handleDynamicTitleSelected(var val, controllerKey) async {
      print("in dynamic handle selected");
      print(controllerKey);
      print(textEditingControllers[controllerKey].text);
      print("i am controller.........");
      setState(() {
        textEditingControllers[controllerKey].text = val;
        make = textEditingControllers[controllerKey].text;

        //   product=textEditingController.text;
        //  make=textEditingController.text;
        //  model=make=textEditingController.text;
      });

      if (controllerKey == 'LoanType' &&
          textEditingControllers["LoanType"].text != null) {
        print("textEditingControllers text.......sub");

        print(textEditingControllers["LoanType"].text);
        Dialogs.showSimpleDialog(context, "Please Wait Loading subloan.... ");
        var id = await SubloanUTData().GetSubLoanTypes(context,
            textEditingControllers["LoanType"].text.trim(), subProcessField);
        print(id);
        Dialogs.dismissSimpleDialog(context);
        print("trrueujdjdj");
      }
      if (controllerKey == "Make" &&
          textEditingControllers["Make"].text != null) {
        if (SubprocessedField["isMobileInputRequired"] == true) {
          print(" make if.........");
          print(textEditingControllers["Make"].text);
          // await doCheckIsValid( SubprocessedField );
        }
        Dialogs.showSimpleDialog(context, "Please Wait Loading Models.... ");
        await deleteModelsDataFromDb();
        var id = await getModelsBasedOnSelectedMake(
            textEditingControllers["Make"].text,
            SubprocessedField,
            selectedmkid);
        print("id..........");
        print(id);
        Dialogs.dismissSimpleDialog(context);
      }

      if (controllerKey == "Dealership_Name" &&
          textEditingControllers["Dealership_Name"].text != null) {
        print("at Dealership Name ");
        print(textEditingControllers["Dealership_Name"].text);
        createDealerDependentField(
            textEditingControllers["Dealership_Name"].text);
      }

      if (controllerKey == "Model_Variant" &&
          textEditingControllers["Model_Variant"].text != null) {
        Dialogs.showSimpleDialog(
            context, "Please Wait Loading Year and Vehicale Category.... ");
        await deleteYearDataFromDb();
        await GetVehicleCategoryBasedOnModel(
            textEditingControllers["Model_Variant"].text,
            selectedmodelid,
            SubprocessedFieldModel);
        await GetYearsBasedOnModel(textEditingControllers["Model_Variant"].text,
            selectedmodelid, SubprocessedFieldModel);
        Dialogs.dismissSimpleDialog(context);
//        String queryGetModel = DatabaseQueries.getmodelbymodelname(textEditingControllers["Vehicle Model"].text);
//        List modellistddl = await MakeDB.executeQuery(queryGetModel);
//        print("set ORP");
//        var map = <String,dynamic>{};
//        map.addAll(modellistddl[0]);
//        setState(() {
//          textEditingControllerTextField["On Road Price Rs."].text=map["on_road_price"];
//        });
//          MakeDB.executeQuery(queryGetModel).then((val){
// print("set ORP");
//        print(val);
//         });
      }
      Navigator.pop(context);
    }

    _createDynamicChildTitleItem(var text, controllerKey) {
      return InkWell(
        child: Container(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          child: Text(text.toString()),
        ),
        onTap: () => {
          print("call to handleselected"),
          _handleDynamicTitleSelected(text, controllerKey)
        },
      );
    }

    showDynamicDropdown(objectList, controllerKey, context) {
      print(objectList);
      return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Text(
                    "title",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Divider(color: Colors.black),
                Flexible(
                  child: ListView.separated(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: objectList.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return _createDynamicChildTitleItem(
                          objectList[index], controllerKey);
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(color: Colors.grey),
                  ),
                ),
              ],
            ),
          );
        },
      );
    }

    var subProcessFieldValue = subProcessField["value"];
    print("Start of CreateDropdown()");
    print(applicantJsonString);
    var controllerKey;
    InitializeAppDatas();
    String classId = subProcessField["key"];
    String textColor = Colors.black.toString();
    int fontSize = 18;
    List<String> _items = [""].toList();

    //  if (subProcessField.Key.ToLower().Equals("product"))
    //           {
    //               xfx_entry.StyleId = "Make";
    //           }
    //           else if (subProcessField.Key.ToLower().Equals("make"))
    //           {
    //               xfx_entry.StyleId = "Model_Variant";
    //           }
    //           else if (subProcessField.Key.ToLower().Equals("model_variant"))
    //           {
    //               xfx_entry.StyleId = "orp";
    //           }
    //           else
    //           {
    //               xfx_entry.StyleId = "";
    //           }
    if (Platform.isAndroid) {
      if (subProcessField["isMandatory"]) {
        isMandatory = subProcessField["isMandatory"];
        activePlaceholderColor = Colors.red;
        placeholderColor = Colors.red;
      } else {
        activePlaceholderColor = Colors.grey[500];
        placeholderColor = Colors.grey[500];
      }
    } else if (Platform.isIOS) {
      if (subProcessField["isMandatory"] ||
          subProcessField["key"] == "Main_DealerCode") {
        placeholderColor = Colors.red.toString();
        ;
        activePlaceholderColor = Colors.red.toString();
      } else {
        activePlaceholderColor = Colors.grey[500].toString();
        placeholderColor = Colors.grey[500].toString();
      }
    }
    print("checking");
    print(subProcessField["validation"]);
    if (subProcessField["validation"] == "Numeric" ||
        subProcessField["validation"] == "Numbers" ||
        subProcessField["validation"] == "Decimal") {
      print("Numeric validation");
      keyboard = TextInputType.number;
      print("keyboard" + keyboard);
    } else if (subProcessField["validation"] == "alphaNumeric") {
      print("alphaNumeric validation");
      keyboard = TextInputType.text;
      print("keyboard" + keyboard.toString());
    }
    if (subProcessField["validationPattern"] != null &&
        !(subProcessField["validationPattern"] == "")) {
      print(subProcessField["validationPattern"]);
      // xfx_entry.Behaviors.Add(new ValidationPatternBehavior(subProcessField.ValidationPattern, subProcessField.ValidationMessage, subProcessField.Maxlength));
    }
    // if (subProcessField["isreadonly"] == "true"||subProcessField["key"]=="Main_DealerCode")
    // {
    //   print("check enalbe");
    //      print(subProcessField["key"]);
    //     isEnabledDropdown = false;
    //     //stackLayout.IsEnabled = false;
    // }
    try {
      // JObject jsonObject = JObject.Parse(applicant);
      // String text = (string)jsonObject.SelectToken(subProcessField.Key);
      if (subProcessField != null && !(subProcessField == "")) {
        print("itttttt");
        text = subProcessField["key"];
        var user = <String, dynamic>{};
        user.addAll(subProcessField);

        placeholder = subProcessField["lable"]; //added by nilima 15 may 2018
        print("classid" + classId);
        print(subProcessField["key"]);
        controllerKey = subProcessField["key"];
        print(controllerKey);
        //Added by Rohit July 22, 2019
        if (classId == "Dealership_Name") {
          if (userRole.toLowerCase().startsWith("SO".toLowerCase()) &&
              currentProductID == "1") {
            print("dealershippppp name");
            print(subProcessField["Dealership_Name"]);
            print(subProcessField);

            SetDealerData(text, subProcessField);
          }
        }
      } else {
        placeholder = subProcessField["lable"];
        controllerKey = subProcessField["key"];
      }
    } catch (e) {
      print("Exception 1 in CreateDropDown() : ");

      text = "";
      placeholder = subProcessField["lable"];
      controllerKey = subProcessField["key"];
    }

    print("validation...");
    //  print(subProcessField);
    // if(subProcessField["key"]=="Main_DealerCode"){
    //   print("main dealer code disalbe");
    //   isEnabled = false;
    // }
    // isEnabled = false;
    //if(subProcessField["Main Dealer Code"])
    ondropdownSelect(textcontrollerName, context, placeholdervslue) async {
      print(controllerKey);
      print("ondropdownSelect method.....");
      print(placeholdervslue);
      print("selected value.......");
      print(textcontrollerName);
      print("textcontrollerName value.......");
      print(context);
      print("selected value.......");
      try {
        if (subProcessField["value"].toString().contains("[")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("[", "");
        }
        if (subProcessField["value"].toString().contains("]")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("]", "");
        }
        if (subProcessField["value"].toString().contains("\"")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("\"", "");
        }
        if (subProcessField["value"].toString().contains("\n")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("\n", "");
        }
        print("objectList before");

        List<String> objectList =
            subProcessField["value"].toString().split(',').toList();
        print(objectList.toString());
        print(objectList.length);
        if (objectList.length == 1 && objectList.contains("")) {
          objectList.remove("");
        }
        // objectList = objectList.Select(x => x.Trim()).ToArray().ToList();
        // SharedPreferences sp = await SharedPreferences.getInstance();
        //var userRole = Application.Current.Properties[AppConstants.LOGGED_IN_USER_ROLE];
        //var userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE)
        print("classId" + classId);
        if (classId == "Product") {
          try {
            // StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //arrayListpicker = new List<XfxEntry>();
            // FindAllddlorp(mainParentLayout, "Make");
            //XfxEntry orpXfxEntry3 = arrayListpicker[0];
            //orpXfxEntry3.Text = "";

            // arrayListpicker = new List<XfxEntry>();
            // FindAllddlorp(mainParentLayout, "Model_Variant");
            // XfxEntry orpXfxEntry = arrayListpicker[0];
            // orpXfxEntry.Text = "";

            // arrayListpicker = new List<XfxEntry>();
            // FindAllddlorp(mainParentLayout, "orp");
            // XfxEntry orpXfxEntry1 = arrayListpicker[0];
            // orpXfxEntry1.Text = "";
          } catch (e) {
            print("exception in branch id prduct");
          }
        }
        print("iserrole" + userRole);
        if (objectList.length == 0 &&
            !userRole.toString().toLowerCase().startsWith("VD".toLowerCase()))
        //if (objectList.Count == 0 && currentProductID.Equals("1"))
        {
          print("startsWithV");
          if (objectList.length == 0 && currentProductID == "2") {
            print("currentProductID==" + currentProductID.toString());
            print("in 2");
            print(subProcessField);

//
            if (controllerKey == "subLoanType") {
              print("i ma at subLoanType");
              if (textEditingControllers["LoanType"].text != null &&
                  textEditingControllers["LoanType"].text.length != 0) {
                String query;
                query = await DatabaseQueries.GetSubLoanTypes(
                    textEditingControllers["LoanType"].text.trim());
                print("query......");
                print(query);

                List data = await SubLoanTypeDB().showSubloan();
                print('data.................');
                print(data);

                SubLoanTypeDB.executeQuery(query).then((onValue) {
                  print(onValue);
                  print("onValue.......");
                  for (int i = 0; i < onValue.length; i++) {
                    var map = <String, dynamic>{};
                    map.addAll(onValue[i]);
                    print(map["sub_loan_type"]);
                    objectList.add(map["sub_loan_type"]);
                  }
                  print("placeholder value in gettting....");
                  subLoanList = objectList;
                  print(objectList);
                  print(placeholder);
                  print("placeholder value in gettting..................");
                  var dropdown =
                      showDynamicDropdown(objectList, controllerKey, context);
                  return dropdown;
                });
              } else {
                await Dialogs.showDialogs(context, "Please Select Loan Type");
              } //loantype if

            } //end of dealership if
            if (subProcessField["key"].toString().toLowerCase() ==
                "dealership_name") {
              print("In dealership name dropdown");
              if (textEditingControllers["LoanType"].text != null &&
                  textEditingControllers["LoanType"].text.length != 0) {
                List dealerList = await DealerSODB.getData();
                print(dealerList);
                List dealershipNameList = [];
                for (int i = 0; i < dealerList.length; i++) {
                  var map = <String, dynamic>{};
                  map.addAll(dealerList[i]);
                  String dealershipName = map["dealership_name"];
                  var abc = map["dealership_data"].toString();
                  abc = abc.substring(1, abc.length - 1);
                  var pqr = abc.split(',');

                  var dealerType = pqr[1].replaceAll(":", ',').split(',');
                  print("dealerType[1]");
                  print(dealerType[1]);

                  if (map["dealership_data"].contains("Dealer_Type")) {
                    var DealerType = dealerType[1];

                    if (DealerType.toString().toLowerCase().trim() ==
                        textEditingControllers["LoanType"]
                            .text
                            .toLowerCase()
                            .trim()) {
                      print("in if dealer");
                      print(dealershipName);
                      dealershipNameList.add(dealershipName);
                    }
                  }
                }
                print("after for loop");
                print(controllerKey);
                var dropdown = showDynamicDropdown(
                    dealershipNameList, controllerKey, context);
                return dropdown;
              } else {
                await Dialogs.showDialogs(context, "Please Select Loan Type");
              }
            }

            if (controllerKey == "Make") {
              List onValue = await MakeDB().showMake();
              print("makelis.....");
              print(onValue);

              for (int i = 0; i < onValue.length; i++) {
                var map = <String, dynamic>{};
                map.addAll(onValue[i]);
                print(map["make_name"]);
                objectList.add(map["make_name"]);
                selectedmkid = map["make_id"].toString();
                print("selectedmkid..." + selectedmkid);
              }
              print("placeholder value in gettting");
              makeList = objectList;
              print(placeholder);
              var dropdown =
                  showDynamicDropdown(objectList, controllerKey, context);

              print("selectedmkid..." + selectedmkid);
              SubprocessedField = subProcessField;
              print("SubprocessedField.......make......");
              print(SubprocessedField);
              return dropdown;
            }
            if (controllerKey == "Model_Variant") {
              print("in model");
              print(make);
              if (textEditingControllers["Make"].text != null &&
                  textEditingControllers["Make"].text.length != 0) {
                print("before calling function..........");
                print(textEditingControllers["Make"].text);
                print("model sublist...");
                print(subProcessField);

                List data = await ModelVdDB().showMake();

                print("data of model");
                print(data);

                String queryGetModel =
                    DatabaseQueries.getmodelbymakeidVD(selectedmkid);
                print("queryGetModel" + queryGetModel);

                MakeDB.executeQuery(queryGetModel).then((modellist) {
                  print("modellist");
                  print(modellist);
                  objectList = new List<String>();
                  for (int i = 0; i < modellist.length; i++) {
                    var mapModelName = <String, dynamic>{};
                    mapModelName.addAll(modellist[i]);
                    String modelname = mapModelName["model_name"];
                    selectedmodelid = mapModelName["model_id"].toString();
                    print("selectedmodelid..." + selectedmodelid);
                    if (!objectList.contains(mapModelName["model_name"])) {
                      objectList.add(modelname);
                    }
                    //objectList.Add(modelname);
                  }
                  model = objectList;
                  var dropdown =
                      showDynamicDropdown(objectList, controllerKey, context);
                  SubprocessedFieldModel = subProcessField;
                  print("SubprocessedField.......make......");
                  print(SubprocessedField);
                  return dropdown;
                });
              } else {
                print("pleae select make");
                Dialogs.showDialogs(context, "Please Select Make");
              }
            }
            if (controllerKey == 'year_of_manufacturing') {
              print("at model variant");
//
              if (textEditingControllers["Make"].text != null &&
                  textEditingControllers["Make"].text.length != 0) {
                print("before calling function model..........");
                if (textEditingControllers["Model_Variant"].text != null &&
                    textEditingControllers["Model_Variant"].text.length != 0) {
                  print("before calling function api model..........");
                  print(textEditingControllers["Model_Variant"].text);

//                  clearFieldData("amount");
//                  clearFieldData("AmountFinance");
//
                  List onValue = await YearVDDB().showYearVD();
                  print("year.....");
                  print(onValue);
                  for (int i = 0; i < onValue.length; i++) {
                    var map = <String, dynamic>{};
                    map.addAll(onValue[i]);
                    print(map["year"]);
                    objectList.add(map["year"]);
//                    selectedmkid = map["make_id"].toString();
//                    print("selectedmkid..."+selectedmkid);
                  }
                  print("placeholder value in gettting");
                  makeList = objectList;
                  print(placeholder);
                  var dropdown =
                      showDynamicDropdown(objectList, controllerKey, context);
                  return dropdown;
                } else {
                  print("pleae select model");
                  Dialogs.showDialogs(context, "Please Select Model_variant");
                }
              } else {
                print("pleae select make");
                Dialogs.showDialogs(context, "Please Select Make");
              }
            }

            //             //get data to display in popup list
            //             //check on Dealer_Type is LAV or UsedTW or P2P as told by Shweta on 04th July, 2019
            //             if (subProcessField.Key.ToLower().Equals("dealership_name"))
            //             {
            //                 //StackLayout mainParentLayout1 = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
            //                 //arrayListpicker = new List<XfxEntry>();
            //                 //FindAllddlorp(mainParentLayout1, "Dealership_Name");
            //                 //XfxEntry orpXfxEntry3 = arrayListpicker[0];

            //                 CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
            //                 List<DealerSO> dealerList = (List<DealerSO>)await cKrossStoreDealerSO.GetAllDataAsync();

            //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout, "LoanType");
            //                 XfxEntry xfxEntry = arrayListpicker[0];
            //                 string LoanType = xfxEntry.Text;

            //                 if (!String.IsNullOrEmpty(LoanType))
            //                 {
            //                     if (LoanType.ToLower().Equals("Used TW".ToLower()) || LoanType.ToLower().Equals("P2P".ToLower()))
            //                     {
            //                         LoanType = "Used TW";
            //                     }

            //                     for (int i = 0; i < dealerList.Count; i++)
            //                     {
            //                         string dealershipName = dealerList[i].dealership_name;

            //                         DealerSO dealerSO = dealerList[i];
            //                         JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
            //                         if (dealerObject.ContainsKey("Dealer_Type"))
            //                         {
            //                             string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
            //                             if (DealerType.ToLower().Equals(LoanType.ToLower()))
            //                             {
            //                                 objectList.Add(dealershipName);
            //                             }
            //                         }
            //                     }

            //                     if (objectList.Count == 0)
            //                     {
            //                         await _page.DisplayAlert("Alert", "No Dealers Found", "Ok");
            //                     }
            //                 }
            //                 else
            //                 {
            //                     await _page.DisplayAlert("Alert", "Please Select Loan Type", "Ok");
            //                 }
            //             }
            //             else if (subProcessField.Key.ToLower().Equals("dealer_State".ToLower()))
            //             {
            //                 //get from state database table
            //                 CKrossStore<States> cKrossStates = new SQLiteState(DependencyService.Get<ISQLiteDb>());
            //                 List<States> stateList = (List<States>)await cKrossStates.GetAllDataAsync();
            //                 //dataList = stateList;
            //                 for (int i = 0; i < stateList.Count; i++)
            //                 {
            //                     string stateName = stateList[i].state_name;
            //                     objectList.Add(stateName);
            //                 }
            //                 dataList = stateList;

            //                 if (currentProductID.Equals("2"))
            //                 {
            //                     if (stateList.Count == 0 && subProcessField.isMobileInputRequired)
            //                     {
            //                         await doCheckIsValid(parent, subProcessField);
            //                     }
            //                 }
            //                 else if (currentProductID.Equals("1"))
            //                 {
            //                     if (stateList.Count == 0)
            //                     {
            //                         await _page.DisplayAlert("Alert", "No Dealer States Found", "Ok");
            //                     }
            //                 }
            //             }
            //             else if (subProcessField.Key.ToLower().Equals("Make".ToLower()))
            //             {
            //                 //get from make database table
            //                 CKrossStore<Make> cKrossMake = new SQLiteMake(DependencyService.Get<ISQLiteDb>());
            //                 List<Make> makeList = (List<Make>)await cKrossMake.GetAllDataAsync();
            //                 for (int i = 0; i < makeList.Count; i++)
            //                 {
            //                     string makeName = makeList[i].make_name;
            //                     objectList.Add(makeName);
            //                 }
            //                 dataList = makeList;
            //             }
            //             else if (subProcessField.Key.ToLower().Equals("Model_Variant".ToLower()))
            //             {
            //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout, "Make");
            //                 var selectedMakeId = arrayListpicker[0].StyleId;

            //                 if (!selectedMakeId.ToLower().Equals("Make".ToLower()))
            //                 {
            //                     string query = DatabaseQueries.GetModelVDNameByMakeId(selectedMakeId);
            //                     CKrossStore<ModelVD> cKrossStoreModelVD = new SQLiteModelVD(DependencyService.Get<ISQLiteDb>());
            //                     List<ModelVD> modelVDList = await cKrossStoreModelVD.ExecuteQueryAsync(query);

            //                     for (int i = 0; i < modelVDList.Count; i++)
            //                     {
            //                         string modelName = modelVDList[i].model_name;
            //                         objectList.Add(modelName);
            //                     }
            //                     dataList = modelVDList;

            //                     if (modelVDList.Count == 0 && subProcessField.isMobileInputRequired)
            //                     {
            //                         doCheckIsValid(parent, subProcessField);
            //                     }
            //                 }
            //             }
            //             else if (subProcessField.Key.ToLower().Equals("year_of_manufacturing".ToLower()))
            //             {
            //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout, "Model_Variant");
            //                 var selectedModelId = arrayListpicker[0].StyleId;

            //                 if (!selectedModelId.ToLower().Equals("Model_Variant".ToLower()))
            //                 {
            //                     CKrossStore<YearVD> cKrossStoreYearVD = new SQLiteYearVD(DependencyService.Get<ISQLiteDb>());
            //                     string query = DatabaseQueries.GetYearVDByModelId(selectedModelId);
            //                     List<YearVD> yearVDList = await cKrossStoreYearVD.ExecuteQueryAsync(query);
            //                     for (int i = 0; i < yearVDList.Count; i++)
            //                     {
            //                         string year = yearVDList[i].year;
            //                         if (!objectList.Contains(year))
            //                         {
            //                             objectList.Add(year);
            //                         }
            //                         //objectList.Add(year);
            //                     }
            //                     dataList = yearVDList;

            //                     if (yearVDList.Count == 0 && subProcessField.isMobileInputRequired)
            //                     {
            //                         doCheckIsValid(parent, subProcessField);
            //                     }
            //                 }
            //             }
            //             else if (subProcessField.Key.ToLower().Equals("dealerBranch".ToLower()))
            //             {
            //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout, "dealerBranch");
            //                 CKrossStore<City> cKrossStorecity = new SQLiteCity(DependencyService.Get<ISQLiteDb>());
            //                 string SQLquery = DatabaseQueries.GetCityNames();
            //                 List<City> cityList = await cKrossStorecity.ExecuteQueryAsync(SQLquery);
            //                 for (int i = 0; i < cityList.Count; i++)
            //                 {
            //                     string year = cityList[i].city_name;
            //                     objectList.Add(year);
            //                 }
            //                 dataList = cityList;
            //             }
            //             //this else added on 22 Aug, 2019 -- addtion of MainDealer Dropdown
            //             else if (subProcessField.Key.ToLower().Equals("Main_DealershipName".ToLower()))
            //             {
            //                 CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
            //                 List<DealerSO> dealerList = (List<DealerSO>)await cKrossStoreDealerSO.GetAllDataAsync();

            //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout, "LoanType");
            //                 XfxEntry xfxEntry = arrayListpicker[0];
            //                 string LoanType = xfxEntry.Text;

            //                 if (!String.IsNullOrEmpty(LoanType))
            //                 {
            //                     if (LoanType.ToLower().Equals("P2P".ToLower()))
            //                     {
            //                         LoanType = "Used TW";
            //                     }

            //                     for (int i = 0; i < dealerList.Count; i++)
            //                     {
            //                         string dealershipName = dealerList[i].dealership_name;

            //                         DealerSO dealerSO = dealerList[i];
            //                         JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
            //                         if (dealerObject.ContainsKey("Dealer_Type"))
            //                         {
            //                             string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
            //                             if (DealerType.ToLower().Equals(LoanType.ToLower()))
            //                             {
            //                                 string DealerCategorization = dealerObject.SelectToken("Dealer_Categorisation").ToString();
            //                                 if (DealerCategorization.ToLower().Equals("Main Dealer".ToLower()))
            //                                 {
            //                                     objectList.Add(dealershipName);
            //                                 }
            //                             }
            //                         }
            //                     }

            //                     if (objectList.Count == 0)
            //                     {
            //                         await _page.DisplayAlert("Alert", "No Dealers Found", "Ok");
            //                     }
            //                 }
            //                 else
            //                 {
            //                     await _page.DisplayAlert("Alert", "Please Select Loan Type", "Ok");
            //                 }
            //             }
            //             //this else added on 29nd Aug, 2019 -- addtion of SubLoanType Dropdown
            //             else if (subProcessField.Key.ToLower().Equals("subLoanType".ToLower()))
            //             {
            //                 StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout, "LoanType");
            //                 XfxEntry xfxEntry = arrayListpicker[0];
            //                 string LoanType = xfxEntry.Text;

            //                 if (!String.IsNullOrEmpty(LoanType))
            //                 {
            //                     CKrossStore<SubLoanType> cKrossStore = new SQLiteSubLoanType(DependencyService.Get<ISQLiteDb>());
            //                     string query = DatabaseQueries.GetSubLoanTypes(LoanType);
            //                     List<SubLoanType> subLoanList = (List<SubLoanType>)await cKrossStore.GetAllDataAsync();

            //                     for (int i = 0; i < subLoanList.Count; i++)
            //                     {
            //                         string type = subLoanList[i].sub_loan_type;
            //                         objectList.Add(type);
            //                     }

            //                     if (objectList.Count == 0)
            //                     {
            // if (_page != null)
            // {
            // 	await _page.DisplayAlert("Alert", "No Sub Loan Type Found", "Ok");
            // }
            //                         else
            // {
            //     DependencyService.Get<IToastInterface>().ShortAlert("No Sub Loan Type Found");
            // }
            //                     }
            //                 }
            //                 else
            //                 {
            //                     await _page.DisplayAlert("Alert", "Please Select Loan Type", "Ok");
            //                 }
            //             }
          } else if (objectList.length == 0 && currentProductID == "1") {
            print("currentProductID111111111");
            //             CKrossStore<Product> cKrossStoreProduct = new SQLiteProduct(DependencyService.Get<ISQLiteDb>());
            //             CKrossStore<Make> cKrossStoreMake = new SQLiteMake(DependencyService.Get<ISQLiteDb>());
            //             CKrossStore<Model> cKrossStoreModel = new SQLiteModel(DependencyService.Get<ISQLiteDb>());
            //             StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //             arrayListpicker = new List<XfxEntry>();
            //             FindAllddlnew(mainParentLayout, subProcessField, xfx_entry.ClassId);
            //             if (arrayListpicker.Count > 0)
            //             {
            //                 XfxEntry parenXfxEntry = arrayListpicker[0];

            //  if (textEditingController.text.toString().trim()=="Product"){
            //  print("In product");
            //   String queryGetProduct = DatabaseQueries.getproduct("NewTW");
            //    Future<List> productlistddl ;
            //    ProductDataDB.executeQuery(queryGetProduct).then((val){
            //    print("productlistddl");
            //    print(productlistddl.toString());
            //    var dropdown=showDynamicDropdown(productlistddl,placeholder,context);
            //         return dropdown;
            //    });
            //  }
            print("holder");
            print(placeholdervslue);
            if (placeholdervslue == "Vehicle Make") {
              print("In Vehicle make");
              print(textEditingControllers["Product"].text.length);
              if (textEditingControllers["Product"].text != null &&
                  textEditingControllers["Product"].text.length != 0) {
                //                     {
                if (textEditingControllers["Dealership Name"].text != null &&
                    textEditingControllers["Dealership Name"].text.length !=
                        0) {
                  //                         if (productlistddl.Count > 0)
                  //                         {
                  //                             Product product = productlistddl[0];
                  //                             string selectedpid = product.product_id.ToString();
                  //                             // string queryGetMake = DatabaseQueries.getmakebyproductid(selectedpid);
                  String queryGetMake = DatabaseQueries.getmakebyproductid(
                      "1"); //added by nilima on 24 july 2018
                  print("queryGetMake");

                  Future<List> makelistddl;
                  MakeDB.executeQuery(queryGetMake).then((onValue) {
                    print(onValue);
                    // List makename;
                    for (int i = 0; i < onValue.length; i++) {
                      var map = <String, dynamic>{};
                      map.addAll(onValue[i]);
                      print(map["make_name"]);
                      objectList.add(map["make_name"]);
                    }
                    print("placeholder value in gettting");
                    makeList = objectList;
                    print(placeholder);
                    var dropdown =
                        showDynamicDropdown(objectList, placeholder, context);
                    return dropdown;
                  });

                  // //                             objectList = new List<string>();
                  //                              for (int i = 0; i < makelistddl.length; i++)
                  //                              {
                  //                                     List makename = ["New TW"];
                  //                                 string makename = makelistddl[i].make_name;
                  //                                 if (!objectList.Contains(makename))
                  //                                 {
                  //  return  makename
                  //   .map((user) => DropdownMenuItem<String>(
                  //         child: Text(user),
                  //         value: user,
                  //       ))
                  //   .toList();
                  //                                     objectList.Add(makename);
                  //     }
                  //                             }
                  //                         }
                  //                         else
                  //                         {
                  //                             await _page.DisplayAlert("Ckross", "Make List Empty", "Ok");
                }
              } else {
                Dialogs.showDialogs(context, "Please Select Product");
              }
            } else if (placeholdervslue == "Vehicle Model") {
              print("in model");
              print(make);
              if (textEditingControllers["Vehicle Make"].text != null &&
                  textEditingControllers["Vehicle Make"].text.length != 0) {
                // {
                String queryGetMakebyMakeName = DatabaseQueries.getmake(
                    textEditingControllers["Vehicle Make"].text);
                List onValue =
                    await MakeDB.executeQuery(queryGetMakebyMakeName);
                print(onValue);
                var map = <String, dynamic>{};
                print("In mappp");
                map.addAll(onValue[0]);
                print("after map");
                print(map["make_name"]);
                var make = map["make_name"];
                String selectedmkid = map["make_id"].toString();
                print("selectedmkid" + selectedmkid);
                String queryGetModel =
                    DatabaseQueries.getmodelbymakeid(selectedmkid);
                print("queryGetModel" + queryGetModel);
                MakeDB.executeQuery(queryGetModel).then((modellist) {
                  print("modellist");
                  print(modellist);
                  objectList = new List<String>();
                  for (int i = 0; i < modellist.length; i++) {
                    var mapModelName = <String, dynamic>{};
                    mapModelName.addAll(modellist[i]);
                    String modelname = mapModelName["model_name"];
                    String on_road_price = mapModelName["on_road_price"];

                    if (!objectList.contains(mapModelName["model_name"])) {
                      objectList.add(modelname);
                    }
                    //objectList.Add(modelname);
                  }
                  model = objectList;
                  var dropdown = showDynamicDropdown(
                      objectList, placeholdervslue, context);
                  return dropdown;
                });
              } else {
                print("pleae select make");
                Dialogs.showDialogs(context, "Please Select Make");
              }
            } else if (subProcessField["key"].toString().toLowerCase() ==
                "dealership_name") {
              print("In dealership name dropdown");
              // CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
              List dealerList = await DealerSODB.getData();
              List dealershipNameList = [];
              for (int i = 0; i < dealerList.length; i++) {
                var map = <String, dynamic>{};
                map.addAll(dealerList[i]);
                String dealershipName = map["dealership_name"];
                print("qqqqqqqqqqqqq");
                var abc = map["dealership_data"].toString();
                abc = abc.substring(1, abc.length - 1);
                var pqr = abc.split(',');

                var dealerType = pqr[1].replaceAll(":", ',').split(',');
                print(dealerType[1]);

                //  map2.addAll(json.decode(pqr));
                // print(map["dealership_data"]);
                // print(map2["Dealer_Type"]);
                //     //check on Dealer_Type is NewTW as told by Shweta on 04th July, 2019
                //     DealerSO dealerSO = dealerList[i];
                //     JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
                if (map["dealership_data"].contains("Dealer_Type")) {
                  print("keyyyyyyyyyyyyyyyy");
                  var DealerType = dealerType[1];
                  print(DealerType.toString().toLowerCase().trim());
                  print("New TW".toLowerCase().trim());
                  if (DealerType.toString().toLowerCase().trim() ==
                      "New TW".toLowerCase().trim()) {
                    print("in if dealer");
                    print(dealershipName);
                    dealershipNameList.add(dealershipName);
                  }
                }
              }
              print("after for loop");
              print(dealershipNameList);
              var dropdown =
                  showDynamicDropdown(dealershipNameList, placeholder, context);
              return dropdown;
            }

            //following all else if added for UTW for SO login by Rohit on 1st July, 2019
            //             else if (subProcessField.Key.ToLower().Equals("dealer_State".ToLower()))
            //             {
            //                 //get from state database table
            //                 CKrossStore<States> cKrossStates = new SQLiteState(DependencyService.Get<ISQLiteDb>());
            //                 List<States> stateList = (List<States>)await cKrossStates.GetAllDataAsync();
            //                 //dataList = stateList;
            //                 for (int i = 0; i < stateList.Count; i++)
            //                 {
            //                     string stateName = stateList[i].state_name;
            //                     objectList.Add(stateName);
            //                 }
            //                 dataList = stateList;

            //                 if (currentProductID.Equals("2"))
            //                 {
            //                     if (stateList.Count == 0 && subProcessField.isMobileInputRequired)
            //                     {
            //                         doCheckIsValid(parent, subProcessField);
            //                     }
            //                 }
            //                 else if (currentProductID.Equals("1"))
            //                 {
            //                     if (stateList.Count == 0)
            //                     {
            //                         _page.DisplayAlert("Alert", "No Dealer States Found", "Ok");
            //                     }
            //                 }
            //             }
            //             else if (subProcessField.Key.ToLower().Equals("year_of_manufacturing".ToLower()))
            //             {
            //                 StackLayout mainParentLayout1 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout1, "Model_Variant");
            //                 var selectedModelId = arrayListpicker[0].StyleId;

            //                 if (!selectedModelId.ToLower().Equals("Model_Variant".ToLower()))
            //                 {
            //                     CKrossStore<YearVD> cKrossStoreYearVD = new SQLiteYearVD(DependencyService.Get<ISQLiteDb>());
            //                     string query = DatabaseQueries.GetYearVDByModelId(selectedModelId);
            //                     List<YearVD> yearVDList = await cKrossStoreYearVD.ExecuteQueryAsync(query);
            //                     for (int i = 0; i < yearVDList.Count; i++)
            //                     {
            //                         string year = yearVDList[i].year;
            //                         if (!objectList.Contains(year))
            //                         {
            //                             objectList.Add(year);
            //                         }
            //                         //objectList.Add(year);
            //                     }
            //                     dataList = yearVDList;

            //                     if (yearVDList.Count == 0 && subProcessField.isMobileInputRequired)
            //                     {
            //                         await doCheckIsValid(parent, subProcessField);
            //                     }
            //                 }
            //             }
            //             else if (subProcessField.Key.ToLower().Equals("dealerBranch".ToLower()))
            //             {
            //                 StackLayout mainParentLayout2 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout2, "dealerBranch");
            //                 CKrossStore<City> cKrossStorecity = new SQLiteCity(DependencyService.Get<ISQLiteDb>());
            //                 string SQLquery = DatabaseQueries.GetCityNames();
            //                 List<City> cityList = await cKrossStorecity.ExecuteQueryAsync(SQLquery);
            //                 for (int i = 0; i < cityList.Count; i++)
            //                 {
            //                     string year = cityList[i].city_name;
            //                     objectList.Add(year);
            //                 }
            //                 dataList = cityList;
            //             }
            //             //this else added on 22nd Aug, 2019 -- addtion of MainDealer Dropdown
            //             else if (subProcessField.Key.ToLower().Equals("Main_DealershipName".ToLower()))
            //             {
            //                 if (subProcessField.dependentFieldMobList != null)
            //                 {
            //                     StackLayout mainParentLayout2 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //                     ClearDependantFields(mainParentLayout2, subProcessField.dependentFieldMobList);
            //                 }

            //                 CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
            //                 List<DealerSO> dealerList = (List<DealerSO>)await cKrossStoreDealerSO.GetAllDataAsync();
            //                 for (int i = 0; i < dealerList.Count; i++)
            //                 {
            //                     string dealershipName = dealerList[i].dealership_name;

            //                     DealerSO dealerSO = dealerList[i];
            //                     JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
            //                     if (dealerObject.ContainsKey("Dealer_Type"))
            //                     {
            //                         string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
            //                         if (DealerType.ToLower().Equals("New TW".ToLower()))
            //                         {
            //                             string DealerCategorization = dealerObject.SelectToken("Dealer_Categorisation").ToString();
            //                             //code as per shweta told for adding iaActive condition
            //                             if (dealerObject.ContainsKey("isActive"))
            //                             {
            //                                 bool isActive = (bool)dealerObject.SelectToken("isActive");
            //                                 if (isActive)
            //                                 {
            //                                     if (DealerCategorization.ToLower().Equals("Main Dealer".ToLower()))
            //                                     {
            //                                         objectList.Add(dealershipName);
            //                                     }
            //                                 }
            //                             }
            //                         }
            //                     }
            //                 }
            //             }
            //             //this else added on 29nd Aug, 2019 -- addtion of MainDealerCode Dropdown
            //             else if (subProcessField.Key.ToLower().Equals("Main_DealerCode".ToLower()))
            //             {
            //                 StackLayout mainParentLayout2 = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //                 arrayListpicker = new List<XfxEntry>();
            //                 FindAllddlorp(mainParentLayout2, "Main_DealershipName");

            //                 if (arrayListpicker.Count > 0)
            //                 {
            //                     XfxEntry parenXfxEntry = arrayListpicker[0];
            //                     string Main_DealershipName = parenXfxEntry.Text.Trim();

            //                     CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
            //                     string query = DatabaseQueries.GetDealershipDataByName(Main_DealershipName);
            //                     List<DealerSO> dealerList = await cKrossStoreDealerSO.ExecuteQueryAsync(query);

            //                     for (int i = 0; i < dealerList.Count; i++)
            //                     {
            //                         DealerSO dealerSO = dealerList[i];
            //                         JObject dealerObject = JObject.Parse(dealerSO.dealership_data);
            //                         if (dealerObject.ContainsKey("Dealer_Type"))
            //                         {
            //                             string DealerType = dealerObject.SelectToken("Dealer_Type").ToString();
            //                             if (DealerType.ToLower().Equals("New TW".ToLower()))
            //                             {
            //                                 string DealerCategorization = dealerObject.SelectToken("Dealer_Categorisation").ToString();
            //                                 if (DealerCategorization.ToLower().Equals("Main Dealer".ToLower()))
            //                                 {
            //                                     string DealerCode = dealerObject.SelectToken("Dealer_Code").ToString();
            //                                     objectList.Add(DealerCode);
            //                                 }
            //                             }
            //                         }
            //                     }
            //                 }
            //             }
          }
        }

        print("objectList@@@@@@@@@");
        print(objectList);
        if (objectList != null && objectList.length > 0) {
          print("dfgdfgdfgdfgdf");
          print(currentProductID);
          //if (userRole.toString().toLowerCase().startsWith("VD".toLowerCase()))
          if (currentProductID == "2") {
            if (isValid) {
              print("at drop down list show..................");
              showDynamicDropdown(objectList, controllerKey, context);

              //showPopup(objectList, xfx_entry, subProcessField.Lable, stackLayout, subProcessField, parent, dataList);
            }
            isValid = true;
          } else {
            if (userRole
                .toString()
                .toLowerCase()
                .startsWith("VD".toLowerCase())) {
              if (isValid) {
                print("in vd");
                // showPopup(objectList, xfx_entry, subProcessField.Lable, stackLayout, subProcessField, parent, dataList);
              }
              isValid = true;
            } else {
              print("1111111111222222");
              print(objectList);
              print("at drop down list show..................");
              showDynamicDropdown(objectList, controllerKey, context);
            }
            //     }
            //    }
            //    }
          }
        }
      } catch (e) {
        print(e);
      }
    }

    if (subProcessField["validation"] == "Numeric" ||
        subProcessField["validation"] == "Numbers" ||
        subProcessField["validation"] == "Decimal") {
      keyboardDropdown = TextInputType.number;
    } else if (subProcessField["validation"] == "alphaNumeric") {
      keyboardDropdown = TextInputType.text;
    }
    if (subProcessField["validationPattern"] != null &&
        !(subProcessField["validationPattern"] == " ")) {
      validationPatternDropdown = subProcessField["validationPattern"];
      validationMessageDropdown = subProcessField["validationMessage"];
      maxlengthDropdown = subProcessField["maxlength"];
      print("checking");
      print(subProcessField);
      print(validationPatternDropdown);
      print(validationMessageDropdown);
    }

    if (subProcessField["isreadonly"] == true ||
        subProcessField["key"] == "Main_DealerCode") {
      isEnabledDropdown = false;
    } else {
      isEnabledDropdown = true;
    }
    Map<String, TextEditingController> placeholderNamesarray = {};
    placeholderNamesarray.putIfAbsent(placeholder, () => placeholderName);
    textEditingController = new TextEditingController();
    textEditingControllers.putIfAbsent(
        controllerKey, () => textEditingController);
    print("searchhhh");
    //print(textEditingController);
    // return textFields.add( TextField(controller: textEditingController));
    //  });
    //textEditingController = new TextEditingController(text:placeholder);
    showitems(textcontrollerName, placeholdervslue) {
      print("shoeitempsss");
      //print(obj)

      var returnlist =
          ondropdownSelect(textcontrollerName, context, placeholdervslue);
      return returnlist;
    }

    void _handleTitleSelected(var val) {
      setState(() {
        textEditingController.text = val;
      });
      Navigator.pop(context);
    }

    Future<void> _showDialogTitleList(
        BuildContext context, String title, details) {
      print(details);
      double screenHeight = MediaQuery.of(context).size.height;
      double height =
          details.length * 53 > 600 ? screenHeight : details.length * 53.0;
      Widget _createChildTitleItem(var text) {
        return InkWell(
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Text(text.toString()),
          ),
          onTap: () => {_handleTitleSelected(text.toString())},
        );
      }

      return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Text(
                    title,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Divider(color: Colors.black),
                Container(
                  child: ListView.separated(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: details.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return _createChildTitleItem(details[index]);
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(color: Colors.grey),
                  ),
                ),
              ],
            ),
          );
        },
      );
    }

    showdropdown() {
      print("In showdropdeonnnn");
      labeltext = placeholder;
      var dropdown = Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: TextFormField(
          enabled: isEnabledDropdown,
          // focusNode: _focus,
          controller: textEditingController,
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: placeholderColor),
            ),
            disabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: placeholderColor),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: placeholderColor),
            ),
            //  border: OutlineInputBorder(),
            labelText: placeholder,
            labelStyle: TextStyle(color: placeholderColor),
            contentPadding: EdgeInsets.only(top: 0, left: 0, right: 0),
            suffixIcon: IconButton(
              icon: Icon(
                Icons.arrow_drop_down,
                color: Colors.orange,
              ),
            ),
          ),
          showCursor: false,
          readOnly: true,
          style: TextStyle(fontSize: 18),
          onSaved: (String val) {
            print("save drop down ");
            // mapFormData[placeholder] = val;
            var keys = placeholder.toString().replaceAll(" ", "_");
            mapFormData['"' + controllerKey + '"'] = '"' + val + '"';
            //mapFormData[]
            //  saveFromData();
            // print(jplaceholder + " "+val);
          },
          onTap: () => {
            print("on tap"),
            print(textEditingControllers),
            list = showitems(textEditingController.text, controllerKey),
            _showDialogTitleList(context, 'Select Title', list)
          },
          validator: (name) {
            if (isMandatory == true) {
              if (name == null || name == "") {
                print("In drop null validation");
                return "Please Select $placeholder ";
              } else {
                print("In else drop");
                return null;
              }
            }
          },
        ),
      );
      return dropdown;
    }

    var dropdown = showdropdown();
    print("444444444444");
    print(dropdown);
    return dropdown;
  }

  InitializeAppDatas() async {
    try {
      print(" In InitializeAppData");
      SharedPreferences sp = await SharedPreferences.getInstance();
      userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
    } catch (e) {
      print("Exception In InitializeAppData " + e.Message);
    }
  }

  SetDealerData(String dealerName, subProcessField) async {
    print("in SetDealerData method");
    try {
      //CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
      print(dealerName);
      String query = DatabaseQueries.GetDealershipDataByName(dealerName);
      print(query);
      List dealerSOList = await DealerSODB.executeClientTemplateData(query);

      print("LIsted DealerSODB");
      print(dealerSOList);

      //   if (dealerSOList.Count > 0)
      //   {
      //       DealerSO dealerSO = dealerSOList[0];
      //       JObject dealerObject = JObject.Parse(dealerSO.dealership_data);

      //       await UITemplateOperations.InitDatabase();
      //       await UITemplateOperations.TruncateMakeDealerDataTable();
      //       await SaveMakeFromDealerData(dealerObject);
      //  }
    } catch (e) {
      print("Exception in SetDealerData() : $e");
    }
  }

  select(String s) {
    // WidgetsBinding.instance.addPostFrameCallback((_) async {
    setState(() {
      _selection = s;
      // });
    });
    WidgetsBinding.instance.addObserver(this);
  }

  createTextField(subProcessFields, applicantJsonString, dealerJson) async {
    var validationpatternTextField;
    var placeholderTextField;
    var validationMessageTextField;
    var controllerKeyText;

    bool isMandatory = false;
    print(dealerJson);
    print("textfeidl Notes");
    print(subProcessFields);
    try {
      String key = subProcessFields["key"];
      if (key.toLowerCase() == "product") {
        //xfx_entry.StyleId = "Make";
      }
      if (subProcessFields["isMandatory"]) {
        print("subProcessFields in if createtextfiled ");
        //  print(subProcessFields["isMandatory"]);
        isMandatory = subProcessFields["isMandatory"];
        activePlaceholderColor = Colors.red;
        placeholderColor = Colors.red;
      } else {
        print("subProcessFields in else createtextfiled ");
        activePlaceholderColor = Colors.grey[500];
        placeholderColor = Colors.grey[500];
      }
      try {
        String text = null;

        if (applicantJsonString.containsKey(subProcessFields["Key"])) {
          textfiledtext = applicantJsonString;
        }
        if (!(dealerJson == null || dealerJson.isEmpty)) {
          if (dealerJson.containsKey(subProcessFields["key"])) {
            textfiledtext = subProcessFields["key"];
          } else {
            textfiledtext = "";
            placeholderTextField = subProcessFields["lable"];
            controllerKeyText = subProcessFields["key"];
          }
        }
        if (text != null && !(text == null || text.isEmpty)) {
          print("in not empty");
          print(text);
          print(subProcessFields["lable"]);
          textfiledtext = text;
          //to check ,by nilima
          placeholderTextField = subProcessFields["lable"];
          controllerKeyText = subProcessFields["key"];
        } else {
          print("is empty");
          print(subProcessFields["lable"]);
          textfiledtext = "";
          placeholderTextField = subProcessFields["lable"];
          controllerKeyText = subProcessFields["key"];
        }
      } catch (e) {
        print("Exception in createTextField : ");
        textfiledtext = "";
        placeholderTextField = subProcessFields["lable"];
        controllerKeyText = subProcessFields["key"];
      }

      if (subProcessFields["validation"] == "Numeric" ||
          subProcessFields["validation"] == "numbers" ||
          subProcessFields["validation"] == "decimal") {
        keyboard = TextInputType.number;
      } else if (subProcessFields["Validation"] == "alphaNumeric") {
        print("alphaNumeric");
        keyboard = TextInputType.text;
      }
      if (subProcessFields["validationPattern"] != null &&
          !(subProcessFields["validationPattern"] == null ||
              subProcessFields["validationPattern"] == "")) {
        validationpatternTextField = subProcessFields["validationPattern"];
        validationMessageTextField = subProcessFields["validationMessage"];
        maxlength = subProcessFields["maxlength"];
      }
      if (subProcessFields["isreadonly"] == true) {
        isEnabled = false;
      } else {
        isEnabled = true;
      }
      if (subProcessFields["key"].contains("SourcingChannel")) {
        print("sourcing channel...................................");
        InitializeAppData();

        // Vendor Officer : firstName lastName

        if (userRole.startsWith("VD".toLowerCase())) {
          textfiledtext = "Vendor Officer : " + user;
        } else {
          print(
              "sourcing channel................565665556656...................");
          textfiledtext = "Sourcing Channel";
          placeholderTextField = "Dealer";
          //  textEditingControllerTextField["Sourcing Channel"].text = "Dealer";
        }
      }
//      if(subProcessFields["key"].contains("dealerBranch"))  {
//        print("dealerBranch........yttttttt");
//
//        String dealerbranch = sp.getString(AppConstants.DEALER_BRANCH);
//        print(dealerbranch);
//          textfiledtext = "Dealer Branch";
//          placeholderTextField = "Goa";
//
//      }
//      if(subProcessFields["key"].contains("dealer_State"))  {
//        print("dealer_State........");
//        textfiledtext = "Dealer State";
//        placeholderTextField = sp.getString(AppConstants.DEALER_STATE);
//      }

    } on Exception catch (e) {
      print("Exception in createTextField : " + e.toString());
    }
    print("returnnn");
    print(validationpatternTextField);
    // This list of controllers can be used to set and get the text from/to the TextFields
    //var placeholderName;
    var textFields = <TextFormField>[];
    Map<String, TextEditingController> placeholderNamesarray = {};
    placeholderNamesarray.putIfAbsent(
        placeholderTextField, () => placeholderName);
    var textEditingController = new TextEditingController();
    textEditingControllerTextField.putIfAbsent(
        controllerKeyText, () => textEditingController);
    print("in textfield file");
    print(controllerKeyText);

    textFields.add(TextFormField(
      controller: textEditingController,
      enabled: isEnabled,
      //  focusNode: FocusNode(canRequestFocus: false),
      onSaved: (String value) {
        print(placeholderTextField);
        print(value);
        //mapFormData[placeholderTextField] = value;
        print("on save");
        print(mapFormData[placeholderTextField]);
        mapFormData['"' + controllerKeyText + '"'] = '"' + value + '"';
        print(controllerKeyText);
        print('"' + value + '"');
        // saveFromData();
        // print(placeholderTextField+" "+value);
      },
      onChanged: (String val) {
        print(controllerKeyText);
        print(textEditingControllers["Last Name"].text);
      },
      onEditingComplete: () {
        print("valuessss");
        print(textEditingControllers);
        print(textEditingControllers["Last Name"].text);
      },
      keyboardType: keyboard,
      decoration: new InputDecoration(
        hintText: textfiledtext,
        labelText: placeholderTextField,
        disabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: placeholderColor),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: placeholderColor),
        ),
        labelStyle: TextStyle(color: placeholderColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: placeholderColor),
        ),
      ),
      validator: (name) {
        Pattern pattern = validationpatternTextField;
        RegExp regex;
        if (pattern != null) {
          regex = new RegExp(pattern);
        }

        if (isMandatory == true) {
          if (name == null || name == "") {
            print("In text null validation");
            return "Please Enter $placeholderTextField ";
          } else {
            if (!(regex.hasMatch(name))) {
              return validationMessageTextField;
            } else {
              return null;
            }
          }
          //  }
        }
      },
    ));
    returned = Column(
      children: [
        Column(children: textFields),
      ],
    );
    print("returned");
    print(controllers);

    return returned;
  }

  InitializeAppDataTextField() async {
    try {
      print("InitializeAppDataTextField()");
      SharedPreferences sp = await SharedPreferences.getInstance();
      user = sp.getString(AppConstants.LOGGED_IN_USER);
      print(user);
      userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      print(userRole);
    } catch (e) {
      e.GetBaseException();
    }
  }

  onbuttonpress() {
    print("fghfghfghfghgfhf");

    //print(controller.text);
  }

  saveFromData() async {
    print("In form data save");
    try {
      bool isValid = true;
      if (isValid) {
        var allFieldsKeysArray = [];
        allFieldsKeysArray = allFieldsKeys.split(',');
        print(allFieldsKeysArray);

        try {
          print(mapFormData);
          var jsonObjectApplicant = mapFormData;
          createApplicant(jsonObjectApplicant);
          jsonObjectApplicant =
              await AddDealerDataInApplicant(jsonObjectApplicant);
          if (await ValidateFromApplicant(jsonObjectApplicant) == true) {
            String Query = DatabaseQueries.GetMaxCaseID();
            List maxId = await ckrossCaseDB.executeQuery(Query);
            var map = <String, dynamic>{};
            map.addAll(maxId[0]);
            int mobileTempCaseId;
            if (map["LastId"] != null) {
              mobileTempCaseId = map["LastId"] + 1;
              print("In last id");
              print(mobileTempCaseId);
            }
            await deleteCaseIfAlreadyExist(jsonObjectApplicant);
            var jsonObject =
                createSubmitCaseJson(jsonObjectApplicant, mobileTempCaseId);
            print("%%%%5%%");
            print(jsonObject);
            String fName = "", lName = "", customerName = "", SourceBy = "";
            try {
              if (jsonObjectApplicant.containsKey("First_Name")) {
                print("11122222");
                fName = jsonObjectApplicant["FirstName"].toString();
              }
              if (jsonObjectApplicant.containsKey("Last_Name")) {
                lName = jsonObjectApplicant["LastName"].toString();
              }
              if (jsonObjectApplicant.containsKey("Source_By")) {
                SourceBy = jsonObjectApplicant["Source_By"].toString();
              }
            } on Exception catch (ex) {
              print("Exception In HandleClick" + ex.toString());
            }
            customerName = fName + " " + lName;
            String case_id;
            // await delteteIfAlreadyExist(case_id);
            case_id = await SaveCaseInDb(jsonObject.toString(),
            AppConstants.CREATE_LEAD, mobileTempCaseId, customerName);
            print("caseiddddddd");
            SharedPreferences sp = await SharedPreferences.getInstance();
            sp.setString("Case_Id", case_id);
            print(case_id);
            await delteteIfAlreadyExist(case_id);
            Navigator.pushReplacementNamed(context, '/leadCreationImage');
          }
        } catch (e1) {
          //Dialogs.showDialogs(contrext, "NETWORK_ERROR");
          print("Exception In HandleClick" + e1.toString());
        }
      }
    } on Exception catch (e) {
      //Dialogs.showDialogs("","Network Error");
      print("Exception In HandleClick" + e.toString());
    }
  }

  createApplicant(jsonObjectApplicant) async {
    print("in create applicantt");
    print(jsonObjectApplicant);

    Map<String, dynamic> ApplicantJsonList = HashMap.from(jsonObjectApplicant);
    //print(ApplicantJsonList['"'+"Scheme Discussed"+'"']);
    // var  abc = jsonObjectApplicant.substring(1, jsonObjectApplicant.length-1 );
    //    var pqr = abc.split(',');
    if (ApplicantJsonList['"' + "SchemeDiscussed" + '"'] == '"' + "Yes" + '"') {
      print("Scheme Discusse present");
      String SchemeSelected = "true";
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString(AppConstants.IsSchemeSelected, SchemeSelected);
      sp.getString(AppConstants.IsSchemeSelected);
    }
  }

  AddDealerDataInApplicant(ApplicantJson) async {
    print("######");
    print(ApplicantJson);
    Map<String, dynamic> ApplicantJsonList = HashMap.from(ApplicantJson);
    print("In AddDealerDataInApplicant");
    List<String> dealerObject;
    String dealer_data_json = "";
    print(userRole);
    try {
      if (userRole.toLowerCase().startsWith("SO".toLowerCase())) {
        try {
          print(ApplicantJson);
          if (ApplicantJson.containsKey('"' + "Dealership_Name" + '"')) {
            String Dealership_Name =
            ApplicantJson['"' + "Dealership_Name" + '"'];
            print(Dealership_Name);
            Dealership_Name =
                Dealership_Name.substring(1, Dealership_Name.length - 1);
            print("*********");
            print(Dealership_Name);
            String query =
            DatabaseQueries.GetDealershipDataByName(Dealership_Name);
            print(query);
            List dealerSOList = await ckrossCaseDB.executeQuery(query);
            print("check query");
            print(dealerSOList);
            //"convert dealerSOList to  DealerDataSo ; "
            if (dealerSOList.length > 0) {
              var dealerSOMap = <String, dynamic>{};
              dealerSOMap.addAll(dealerSOList[0]);

              var abc = dealerSOMap["dealership_data"].toString();
              abc = abc.substring(1, abc.length - 1);
              var pqr = abc.split(',');

              dealerObject = pqr;
              print(dealerObject);

              //     for(int i = 0;i<pqr.length;i++){
              //           var xyz= pqr[i].replaceAll(":", ',').split(',');
              //           var xy0 = xyz[0].replaceAll('[', "");
              //           var xy1 = xyz[1].replaceAll(']', "");

              //     }

            }
          }
        } on Exception catch (e) {
          print("Exception In AddDealerDataInApplicant() : " + e.toString());
        }
      }
      // }
      //     else
      //     {
      //         try
      //         {
      //             string Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson("dealer_data", appLang);
      //             List<LoginJsonTemplate> listLoginJson = await cKrossStoreLoginJsonTemplate.ExecuteQueryAsync(Query);
      //             if (listLoginJson.Count > 0)
      //             {
      //                 dealer_data_json = listLoginJson[0].jsonValue;
      //                 dealerObject = JObject.Parse(dealer_data_json);
      //             }
      //         }
      //         catch (Exception e)
      //         {
      //             logger.Info("Exception In AddDealerDataInApplicant() : " + e.GetBaseException());
      //             e.GetBaseException();
      //         }
      //     }

      if (dealerObject != null) {
        print("mainnnnnnnn");
        print(ApplicantJson);
        print(dealerObject);
        var mapdealerObject = <String, dynamic>{};
        var mapDealerBankDetails = <String, dynamic>{};

        for (int i = 0; i < dealerObject.length; i++) {
          if (dealerObject[i].contains("Dealership_Name")) {
            print("1111in dealership name");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealershipName = xyz[1];
            //  print(dealershipName);
            if (!(ApplicantJson.containsKey("Dealership_Name"))) {
              ApplicantJsonList['"' + "Dealership_Name" + '"'] =
                  '"' + dealershipName + '"';
              print("In add ");
              print(ApplicantJsonList['"' + "Dealership_Name" + '"']);
            }
          }

          if (dealerObject[i].contains("Dealer_Name")) {
            print("In daeler name");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerName = xyz[1];
            if (!(ApplicantJson.containsKey("Dealer_Name"))) {
              ApplicantJsonList['"' + "Dealer_Name" + '"'] =
                  '"' + dealerName + '"';
            }
          }

          if (dealerObject[i].contains("Branch_Name")) {
            print("In daeler Branch");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerBranch = xyz[1];
            if (!(ApplicantJson.containsKey("Branch"))) {
              ApplicantJsonList['"' + "Branch" + '"'] =
                  '"' + dealerBranch + '"';
            }
          }
          if (dealerObject[i].contains("Location")) {
            print("In daeler Lcation");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            Location = xyz[1];
            print(Location);

            if (!(ApplicantJson.containsKey("Location"))) {
              ApplicantJsonList['"' + "Location" + '"'] = '"' + Location + '"';
            }
          }
          if (dealerObject[i].contains("Dealer_Code")) {
            print("In daeler Dealer_Code");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            Dealer_Code = xyz[1];
            print(Dealer_Code);
            if (!(ApplicantJson.containsKey("Dealer_Code"))) {
              ApplicantJsonList['"' + "Dealer_Code" + '"'] =
                  '"' + Dealer_Code + '"';
            }
          }
          if (dealerObject[i].contains("SourcingChannel")) {
            print("In daeler SourcingChannel");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            SourcingChannel = xyz[1];
            if (!(ApplicantJson.containsKey("SourcingChannel"))) {
              ApplicantJsonList['"' + "SourcingChannel" + '"'] =
                  '"' + SourcingChannel + '"';
            }
          }
          if (dealerObject[i].contains("dealer_State")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealer_State = xyz[1];
            if (!(ApplicantJson.containsKey("State"))) {
              ApplicantJsonList['"' + "State" + '"'] = '"' + dealer_State + '"';
            }
          }
          if (dealerObject[i].contains("Office_Address")) {
            print("in office address");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerAddress = xyz[1];
            if (!(ApplicantJson.containsKey("Office_Address"))) {
              ApplicantJsonList['"' + "Office_Address" + '"'] =
                  '"' + dealerAddress + '"';
            }
          }
          if (dealerObject[i].contains("DealersBankDetails")) {
            try {
              print("Dealerbankdetails");
              // print(dealerObject[i]);
              var str = dealerObject.toString();
              const start = "DealersBankDetails: [{";
              const end = "}";

              final startIndex = str.indexOf(start);
              final endIndex = str.indexOf(end, startIndex + start.length);
              var bankData = str.substring(startIndex + start.length, endIndex);
              var splitbankData = bankData.split(',');
              for (int k = 0; k < splitbankData.length; k++) {
                var splits = splitbankData[k].replaceFirst(":", ",").split(",");

                for (int j = 0; j < splits.length; j++) {
                  mapDealerBankDetails['"' + splits[0].trim() + '"'] =
                      '"' + splits[1].trim() + '"';
                }
              }

              if (!(ApplicantJson.containsKey("DealersBankDetailss"))) {
                ApplicantJsonList['"' + "DealersBankDetails" + '"'] =
                    mapDealerBankDetails.toString();
                print("IN bank deatilsss");
                print(ApplicantJsonList);
              }
            } on Exception catch (e) {
              print("Exception in DealersBankDetails" + e.toString());
            }
          }
          if (dealerObject[i].contains("Beneficiary_Name")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DisbursementTO = xyz[1];
            if (!(ApplicantJson.containsKey("DisbursementTO"))) {
              ApplicantJsonList['"' + "DisbursementTO" + '"'] =
                  '"' + DisbursementTO + '"';
            }
          }
          if (dealerObject[i].contains("Bank_Name")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerBankName = xyz[1];
            ;
            if (!(ApplicantJson.containsKey("DealerBankName"))) {
              ApplicantJsonList['"' + "DealerBankName" + '"'] =
                  '"' + DealerBankName + '"';
            }
          }
          if (dealerObject[i].contains("Bank_Account_Number")) {
            print("In bank account number");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerBankAccountNo = xyz[1];
            print(DealerBankAccountNo);
            if (!(ApplicantJson.containsKey("DealerBankAccountNo"))) {
              ApplicantJsonList['"' + "DealerBankAccountNo" + '"'] =
                  '"' + DealerBankAccountNo + '"';
            }
          }
          if (dealerObject[i].contains("IFSC_Code")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerIFSCCode = xyz[1];
            if (!(ApplicantJson.containsKey("DealerIFSCCode"))) {
              ApplicantJsonList['"' + "DealerIFSCCode" + '"'] =
                  '"' + DealerIFSCCode + '"';
            }
          }
          if (dealerObject[i].contains("Branch_Name")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerBranchName = xyz[1];
            if (!(ApplicantJson.containsKey("DealerBranchName"))) {
              ApplicantJsonList['"' + "DealerBranchName" + '"'] =
                  '"' + DealerBranchName + '"';
            }
          }
          if (dealerObject[i].contains("MICR_Code")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerMICRCode = xyz[1];
            if (!(ApplicantJson.containsKey("DealerMICRCode"))) {
              ApplicantJsonList['"' + "DealerMICRCode" + '"'] =
                  '"' + DealerMICRCode + '"';
            }
          }
          if (dealerObject[i].contains("Type_Of_Account")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            Type_Of_Account = xyz[1];
            if (!(ApplicantJson.containsKey("Type_Of_Account"))) {
              ApplicantJsonList['"' + "Type_Of_Account" + '"'] =
                  '"' + Type_Of_Account + '"';
            }
          }
          if (dealerObject[i].contains("districtname")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerDistrictname = xyz[1];
            if (!(ApplicantJson.containsKey("dealerDistrictname"))) {
              ApplicantJsonList['"' + "dealerDistrictname" + '"'] =
                  '"' + dealerDistrictname + '"';
            }
          }
          rto_by_whom = "Dealer";
          if (!(ApplicantJson.containsKey("rto_by_whom"))) {
            ApplicantJsonList['"' + "rto_by_whom" + '"'] =
                '"' + rto_by_whom + '"';
          }
          Insurance_by_whom = "Dealer";
          if (!(ApplicantJson.containsKey("Insurance_by_whom"))) {
            ApplicantJsonList['"' + "Insurance_by_whom" + '"'] =
                '"' + Insurance_by_whom + '"';
          }
        }
      }
    } on Exception catch (e) {
      print("Exception In AddDealerDataInApplicant() : " + e.toString());
    }
    print("ApplicantJsonList");
    print(ApplicantJsonList);
    return ApplicantJsonList;
  }

  ValidateFromApplicant(jsonObjectApplicant) async {
    print("In ValidateFromApplicant");
    String selectedLoanType = "";
    bool isMaditory = true;
    String query = DatabaseQueries.getAllTemplateJsonByClientTemplateId(
        client_template_id);
    List allMandatoryFieldsArrayList = await ckrossCaseDB.executeQuery(query);
    print(allMandatoryFieldsArrayList);
    for (int i = 0; i < allMandatoryFieldsArrayList.length; i++) {
      var templateJson = <String, dynamic>{};
      templateJson.addAll(allMandatoryFieldsArrayList[i]);
      //         TemplateJson templateJson = allMandatoryFieldsArrayList[i];
      String formName = templateJson["form_name"];
      String allMandatoryFields = templateJson["mandatory_field_key_array"];
      String allMandatoryFieldsLbl =
          templateJson["mandatory_field_lable_array"];

      if (!(allMandatoryFields == null || allMandatoryFields.isEmpty)) {
        print("In first if");
        var allMandatoryFieldsArray = allMandatoryFields.split(',');
        var allMandatoryFieldsLblArray = allMandatoryFieldsLbl.split(',');

        for (int j = 0; j < allMandatoryFieldsArray.length; j++) {
          try {
            print(allMandatoryFieldsArray[j]);
            String value = jsonObjectApplicant[allMandatoryFieldsArray[j]];
            print(value);
            // if (value==""|| value==null||  value==" ")
            // {
            //   print("In seconf if");
            //     isMaditory = false;
            //      Dialogs.showDialogs(context, allMandatoryFieldsLblArray[j] + " is Mandatory in " + formName);
            //     break;
            // }
            print("trtr");
            print(allMandatoryFieldsArray);
            if (userRole.toLowerCase().startsWith("VD".toLowerCase()) &&
                allMandatoryFieldsArray[j] == "LoanType" &&
                value == "UTW") {
              selectedLoanType = value;
            }
            print(selectedLoanType);
            if (!(selectedLoanType == "")) {
              var mapjsonobject = <String, dynamic>{};
              mapjsonobject.addAll(jsonObjectApplicant);
              String dealername = mapjsonobject["Dealership Name"].toString();
              String dealerbranch = mapjsonobject["Branch"].toString();
              String dealerCode = mapjsonobject["Dealer Code"].toString();
              print("<<<<<<<<<<<");
              print(dealername);
              print(dealerbranch);
              print(dealerCode);

              if (dealername == null || dealername == " " || dealername == "") {
                isMaditory = false;
                Dialogs.showDialogs(
                    context, "Dealer Name is Mandatory in " + formName);
                break;
              }

              if (dealerbranch == null ||
                  dealerbranch == " " ||
                  dealerbranch == "") {
                isMaditory = false;
                Dialogs.showDialogs(
                    context, "Branch is Mandatory in " + formName);
                break;
              }

              if (dealerCode == null || dealerCode == " " || dealerCode == "") {
                isMaditory = false;
                Dialogs.showDialogs(
                    context, "Dealer Code is Mandatory in " + formName);
                break;
              }
            }
          } on Exception catch (e) {
            print("Exception In ValidateFromApplicant" + e.toString());
          }
        }
        if (!isMaditory) {
          break;
        }
      }
    }
    return isMaditory;
  }

  deleteCaseIfAlreadyExist(jsonObjectApplicant) async {
    print("in deleteCaseIfAlreadyExist ");
    List allCase = await ckrossCaseDB.search();
    print(allCase);
    for (int i = 0; i < allCase.length; i++) {
      var map = <String, dynamic>{};
      map.addAll(allCase[i]);

      var applicantString = map["applicant_json"];

      try {
        print("In trying");
        var abc = applicantString;
        abc = abc.split(',');
        String firstname, lastname, mobile, product;
        for (int i = 0; i < abc.length; i++) {
          if (abc[i].contains("FirstName")) {
            print("in mapfirst name");
            var xyz = abc[i].replaceAll(":", ',').split(',');
            firstname = xyz[1];
          }
          if (abc[i].contains("LastName")) {
            var xyz = abc[i].replaceAll(":", ',').split(',');
            lastname = xyz[1];
          }
          if (abc[i].contains("Mobile")) {
            var xyz = abc[i].replaceAll(":", ',').split(',');
            mobile = xyz[1];
          }
          if (abc[i].contains("Product")) {
            var xyz = abc[i].replaceAll(":", ',').split(',');
            product = xyz[1];
          }
        }
        print("check values");
        print(firstname);
        // JObject jsonObject = JObject.Parse(applicantString);
        // JObject applicantExistingJsonObject = (JObject)jsonObject.SelectToken(controllerName);
        if ((jsonObjectApplicant["FirstName"].toString().trim() ==
            firstname.trim()) &&
            (jsonObjectApplicant["LastName"].toString().trim() ==
                lastname.trim()) &&
            (jsonObjectApplicant["Mobile"].toString().trim() ==
                mobile.trim()) &&
            (jsonObjectApplicant["Product"].toString().trim() ==
                product.trim())) {
          String delQuery = DatabaseQueries.DeleteCaseDependsOnId(
              map["case_id"].toString(), map["user_id"]);
          await ckrossCaseDB.executeQuery(delQuery);
          await delteteIfAlreadyExist(map["case_id"].toString());
          await ckrossCaseDB.search();
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => LeadCreationImage()),
//          );
        }
      } on Exception catch (e) {
        print("Exception In deleteCaseIfAlreadyExist" + e.toString());
      }
    }
  }

  createSubmitCaseJson(jsonObjectApplicant, int mobileTempCaseId) {
    print("In createSubmitCaseJson");
    var jsonObject = {};
    var map = <String, dynamic>{};
    map.addAll(HashMap.from(jsonObject));

    try {
      if (currentProductID == "2") {
        if (jsonObjectApplicant != null) {
          if (jsonObjectApplicant.containsKey("Dealer_Type")) {
            var temp = jsonObjectApplicant["Dealer_Type"];
            if (temp != null) {
              if (!jsonObjectApplicant.containsKey("Product")) {
                jsonObjectApplicant["Product"] = temp;
              }
            }
          }
        }
      }

      map[controllerName] = jsonObjectApplicant;

      //By Rohit 31/12/2018
      map['"' + "input_source" + '"'] = '"' + "M" + '"';
    } on Exception catch (e) {
      print("Exception In createSubmitCaseJson " + e.toString());
    }
    return map;
  }
  SaveCaseInDb(String applicant, String caseType, int mobileTempCaseId,
      String custName) async {
    String caseid;
    print("in SaveCaseInDb");
    try {
      CkrossCase aCase = new CkrossCase();
      aCase.caseId = mobileTempCaseId.toString();
      aCase.applicantJson = applicant;
      aCase.isSubmit = "false";
      aCase.tempCaseId = "";
      aCase.caseType = caseType;
      aCase.customerName = custName;
      aCase.userId = userId;
      aCase.imageLstFrmServerCnt = 0.toString();
      aCase.productId = currentProductID;
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString("CastType", caseType);
      int id = await ckrossCaseDB.insertCkrossCaseData(aCase);
      if (id != null) {
        String Query = DatabaseQueries.GetMaxCaseID();
        List maxId = await ckrossCaseDB.executeQuery(Query);
        var map = <String, dynamic>{};
        map.addAll(maxId[0]);
        case_id = map["LastId"].toString();
        caseid = case_id;
      }
    } on Exception catch (ex) {
      print("Exception In SaveCaseInDb " + ex.toString());
    }
    return caseid;
  }


  delteteIfAlreadyExist(case_id) async {
    try {
      await InitializeAppData();
      // DependencyService.Get<IImageOperations>().RemoveDirectory(userId + "-" + case_id);
    } on Exception catch (e) {
      print("Exception In delteteIfAlreadyExist" + e.toString());
    }
  }

  Future<bool> doCheckIsValid(Map<String, dynamic> subProcessField) async {
    print("subprocessfield.............");
    print(subProcessField);
    if (subProcessField["mobileInputAPIKey"] != null) {
      print("mobile.............");
      List<String> mobileInputAPIKeysList =
          getMobileInptAPIKeysList(subProcessField);
      isValid = await IsDependentFieldsValid(
          subProcessField['key'], mobileInputAPIKeysList);
    }
    return isValid;
  }

  Future<bool> IsDependentFieldsValid(
      String key, List<String> mobileInputAPIKeysList) async {
    print("in IsDependentFieldsValid ");
    print(key);
    print(mobileInputAPIKeysList);
    try {
      print("Start of IsDependentFieldsValid()");

      for (int i = 0; i < mobileInputAPIKeysList.length; i++) {
        print("mobileInputAPIKeysList[i]........");
        print(mobileInputAPIKeysList[i]);
        String val = getValueOfField(mobileInputAPIKeysList[i]);
        print("val...");
        print(val);

        if (val != null) {
          print("val not null if");
          if (mobileInputAPIKeysList[i].trim().toLowerCase() ==
                  (key.toLowerCase()) ||
              mobileInputAPIKeysList[i].trim().toLowerCase() ==
                  ("product_Id".toLowerCase())) {
            print("val not................. if");
            continue;
          }

          String msg = "Please Select " + mobileInputAPIKeysList[i].trim();
          await showdialog("Ckross", msg, "Ok");
          return false;
        }
      }
    } on Exception catch (e) {
      print("Exception in IsDependentFieldsValid() : " + e.toString());
      // e.GetBaseException();
    } finally {
      print("End of IsDependentFieldsValid()");
    }
    return true;
  }

  getValueOfField(String fieldName) {
    print("fieldname....");
    print(fieldName);
    for (int i = 0; i < arrayUI.length; i++) {
      print(arrayUI[i]);
      if (arrayUI[i] == fieldName) {
        print("in controller text ");
        String str;
        // print(textEditingControllers[arrrr[i]].text);
        print(textEditingControllers[arrayUI[i]].text);
        str = textEditingControllers[arrayUI[i]].text;

        return str != null ? str : " ";
      }
    }
    return "";
  }

  List<String> getMobileInptAPIKeysList(Map<String, dynamic> subProcessField) {
    print("getMobile.............");
    List<String> mobileInputAPIKeysList = new List<String>();

    String mobileInputAPIKeys = subProcessField["mobileInputAPIKey"].toString();

    print(mobileInputAPIKeys);
    if (mobileInputAPIKeys.toString().contains("[")) {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("[", "");
    }
    if (mobileInputAPIKeys.toString().contains("]")) {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("]", "");
    }
    if (mobileInputAPIKeys.toString().contains("\"")) {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("\"", "");
    }
    if (mobileInputAPIKeys.toString().contains("\n")) {
      mobileInputAPIKeys = mobileInputAPIKeys.toString().replaceAll("\n", "");
    }

    mobileInputAPIKeysList = mobileInputAPIKeys.toString().split(',').toList();
    if (mobileInputAPIKeysList.length == 1 &&
        mobileInputAPIKeysList.contains("")) {
      mobileInputAPIKeysList.remove("");
    }
    print("i am mobile input api key......");
    print(mobileInputAPIKeysList);
    return mobileInputAPIKeysList;
  }
  

  deleteYearDataFromDb() async {

    try {
      print("Start of TruncateYearTable");
      String query =
          DatabaseQueries.truncateTable(DatabaseQueries.TABLE_YEAR_VD);
      print(query);
      db.execution(query);
      print("End of TruncateYearTable");
    } on Exception catch (_) {
      print("Exception occured in TruncateYearTable ");
    }
  }

  deleteModelsDataFromDb() async {

    try {
      print("Start of TruncateModelsTable");
      String query =
      DatabaseQueries.truncateTable(DatabaseQueries.TABLE_MODEL_VD);
      print(query);
      db.execution(query);
      print("End of TruncateModelsTable");
    } on Exception catch (_) {
      print("Exception occured in TruncateModelsTable ");
    }
  }

  showdialog(String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  getModelsBasedOnSelectedMake(String selectedMake,
      Map<String, dynamic> subProcessField, String mkid) async {
    print(selectedMake);
    print(subProcessField);
    print(mkid);

    print("Start of getModelsBasedOnSelectedMake()");

    Map jObject = new Map();
    jObject["collectionName"] = "Price_Master_UTW";
    jObject["columnName"] = "Model_Variant";

    if (subProcessField["isMobileInputRequired"] == true) {
      if (subProcessField["mobileInputAPIKey"] != null) {
        print("in if.......");
        print(subProcessField["mobileInputAPIKey"]);
        List<String> mobileInputAPIKeysList =
        getMobileInptAPIKeysList(subProcessField);

        print("............");
        print(mobileInputAPIKeysList);
        List jArray =
        getDependentFieldJsonArray(mobileInputAPIKeysList, selectedMake);
        print("jarry..............");
        print(jArray);
        jObject["selectedDDKey"] = jArray;
        print("jobject................");
        print(jObject);
      }
      String requestJson = JSON.jsonEncode(jObject);
      print("requestjson.........");
      print(requestJson);

      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.GetUniqueColumnData(requestJson);
        try{
          if (response.statusCode == 200) {
            if ((response != null) && (response != "null") &&
                (response != "{}")) {
              try
              {
                GetUniqueColumnDataModel responseModel = getUniqueColumnDataModelFromJson(response.body);
                print(responseModel.details);
                if (responseModel.details.isNotEmpty) {
                  print("i am here ...........");
                  await SaveModelsBasedOnSelectedMake(responseModel.details, mkid);
                }else{
                  CustomSnackBar.showSnackBar(context, "Model not found.");
                }
              }
              on Exception catch (e)
              {

                print("Exception 1 in getModelsBasedOnSelectedMake() : "+  e.toString());
              }
              }
          }else if (response.statusCode == HttpStatus.forbidden ||
              response.statusCode.toString() == "800") {
            FileUtils.printLog("Response is : " + response.statusCode.toString());
            CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          } else if (response.statusCode == HttpStatus.unauthorized) {
            FileUtils.printLog("Response is : " + response.statusCode.toString());
            await showdialog(
                AppConstants.ALERT,  AppConstants.TOKEN_EXPIRED_RELOGIN, "OK");
            //AppUtils.LogOutUser(this);
          }// // end of statuscode
          else{
            await showdialog(AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
          }
        }on Exception catch(e){
          print("Exception 2 in getModelsBasedOnSelectedMake() : " + e.toString());
        }

      }// end of  ConnectivityResult  if
      else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    }// end of isMobileInputRequired

  }

  SaveModelsBasedOnSelectedMake(List<dynamic> details, String makeid) async {
    try {
      //CKrossStore<ModelVD> cKrossModelVD = new SQLiteModelVD(DependencyService.Get<ISQLiteDb>());
      print("savemodl....");
      print(details);
      print(makeid);

      ModelVdDB modelVdDB = new ModelVdDB();
      for (int i = 0; i < details.length; i++) {
        ModelVD modelVD = new ModelVD();
        modelVD.make_id = makeid;
        modelVD.model_name = details[i].toString();
        int id = await modelVdDB.insertModelVD(modelVD);
        print("id........");
        print(id);
      }
    } on Exception catch (e) {
      print("Exception in SaveModelsBasedOnSelectedMake() : " + e.toString());
      // e.GetBaseException();
    }
  }

  List getDependentFieldJsonArray(
      List<String> mobileInputAPIKeysList, String selected) {
    print("at getdependend fieldjson...........");
    print(mobileInputAPIKeysList);
    print(selected);
    print(arrayUI);
    List jArray = new List();
    List filedNames = [];
    for (int l = 0; l < mobileInputAPIKeysList.length; l++) {
      for (int k = 0; k < arrayUI[k].length; k++) {
        if (arrayUI[k].toLowerCase() ==
            (mobileInputAPIKeysList[l].trim().toLowerCase())) {
          filedNames.add(mobileInputAPIKeysList[l]);
        } else {
          filedNames.add(mobileInputAPIKeysList[l]);
        }
      }
    }
    print("filedNames");
    print(filedNames);
    print(mobileInputAPIKeysList);

    try {
      for (int i = 0; i < mobileInputAPIKeysList.length; i++) {
        Map jObject1 = new Map();
        jObject1["selectedKey"] = mobileInputAPIKeysList[i].trim();
        if (filedNames.length > 0) {
          for (int j = 0; j < filedNames[j].length; j++) {
            print("at for loop..........");
            print((mobileInputAPIKeysList[i].trim().toLowerCase()));
            print(filedNames[j].trim().toLowerCase());
            if (filedNames[j].toLowerCase() ==
                (mobileInputAPIKeysList[i].trim().toLowerCase())) {
              String temp = textEditingControllerTextField[filedNames[j]].text;
              if (temp.toLowerCase() == ("P2P".toLowerCase())) {
                temp = "Used TW";
              }
              jObject1[mobileInputAPIKeysList[i].trim()] = temp;
            } else {
              print("filedNames[j].toLowerCase()");
              print(mobileInputAPIKeysList[i].trim().toLowerCase());
              print(textEditingControllers["Make"].text);
              if (mobileInputAPIKeysList[i].trim().toLowerCase() == 'make' &&
                  textEditingControllers["Make"].text != null) {
                print("in if of Make");
                String temp = textEditingControllers["Make"].text;
                print(temp);
                jObject1[mobileInputAPIKeysList[i].trim()] = temp;
              } else {
                jObject1[mobileInputAPIKeysList[i].trim()] = selected;
              }
            }
          }
        } //end of if arrr

        if (mobileInputAPIKeysList[i].trim().toLowerCase() ==
            ("product_Id".toLowerCase())) {
          jObject1[mobileInputAPIKeysList[i].trim()] = 2;
        }

        jArray.add(jObject1);
      } //end of mobile for
    } //end of try
    on Exception catch (e) {
      print("Exception in getDependentFieldJsonArray() : " + e.toString());
    }
    return jArray;
  }

  ///end of function
  Future GetVehicleCategoryBasedOnModel(String selectedModel, String modelid,
      Map<String, dynamic> subProcessField) async {
    try {
      print("In GetSubLoanTypes");
      Map jObject = new Map();
      jObject["collectionName"] = "Price_Master_UTW";
      jObject["columnName"] = "VehicleCategory";

      print("in if");
      if (subProcessField.containsKey("mobileInputAPIKey") != null) {
        print("in if.......");
        List<String> mobileInputAPIKeysList =
            getMobileInptAPIKeysList(subProcessField);

        print("............");
        print(mobileInputAPIKeysList);
        List jArray =
            getDependentFieldJsonArray(mobileInputAPIKeysList, selectedModel);
        print("jarry..............");
        print(jArray);
        jObject["selectedDDKey"] = jArray;
        print("jobject................");
        print(jObject);
      }

      String requestJson = JSON.jsonEncode(jObject);

      print("requestJson at in class.............................");
      print(requestJson);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.GetUniqueColumnData(requestJson);
        if ((response != null) && (response != "null") && (response != "{}")) {
          print("response of date filter");
          print(response.body);
          //await handleGetReport(context, response);
          if (response.statusCode == 200) {
            if (response != null) {
              print("i am here ...........");

              GetUniqueColumnDataModel responseModel =
                  getUniqueColumnDataModelFromJson(response.body);
              print(responseModel.details);
              String vc = responseModel.details.toString().replaceAll("[", "").replaceAll("]","");
              textEditingControllerTextField["VehicleCategory"].text = vc;
              // await SaveYearOnModelType(responseModel.details);
            } else {
              await showdialog("Alert", "Data not found.", "OK");
            }
          } else {
            await showdialog("Alert", "Data not found.", "OK");
          }
        } else {
          //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          await showdialog(AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } on Exception catch (e) {
      print("Exception in GetSub-loan Data " + e.toString());
    }
  } //end of get sub loan data

  Future GetYearsBasedOnModel(String selectedModel, String selectedmodelid,
      Map<String, dynamic> subProcessField) async {
    try {
      print("In GetYearsBasedOnModel");
      Map jObject = new Map();
      jObject["collectionName"] = "Price_Master_UTW";
      jObject["columnName"] = "year_of_manufacturing";

      print("in if");
      if (subProcessField["mobileInputAPIKey"] != null) {
        print("in if.......");
        List<String> mobileInputAPIKeysList =
            getMobileInptAPIKeysList(subProcessField);

        print("............");
        print(mobileInputAPIKeysList);
        List jArray =
            getDependentFieldJsonArray(mobileInputAPIKeysList, selectedModel);
        print("jarry..............");
        print(jArray);
        jObject["selectedDDKey"] = jArray;
        print("jobject................");
        print(jObject);
      }

      String requestJson = JSON.jsonEncode(jObject);

      print("requestJson at in class.............................");
      print(requestJson);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.GetUniqueColumnData(requestJson);
        if ((response != null) && (response != "null") && (response != "{}")) {
          print("response of date filter");
          print(response.body);
          //await handleGetReport(context, response);
          if (response.statusCode == 200) {
            if (response != null) {
              print("i am here ...........");
              GetUniqueColumnDataModel responseModel =
                  getUniqueColumnDataModelFromJson(response.body);
              print(responseModel.details);
              await SaveYearOnModelType(responseModel.details, selectedmodelid);
            } else {
              await showdialog("Alert", "Data not found.", "OK");
            }
          } else {
            await showdialog("Alert", "Data not found.", "OK");
          }
        } else {
          //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          await showdialog(AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } on Exception catch (e) {
      print("Exception in GetSub-loan Data " + e.toString());
    }
  } //end of get sub loan data

  SaveYearOnModelType(List<dynamic> details, String selectedmodelid) async {
    try {
      //CKrossStore<ModelVD> cKrossModelVD = new SQLiteModelVD(DependencyService.Get<ISQLiteDb>());
      print("savemodl....");
      print(details);
      print(selectedmodelid);

      YearVDDB yearVDDB = YearVDDB();
      for (int i = 0; i < details.length; i++) {
        YearVD yearVD = new YearVD();

        yearVD.model_id = selectedmodelid;
        yearVD.year = details[i].toString();
        print("year....");
        int id = await yearVDDB.insertYearVD(yearVD);
        print("id........");
        print(id);
      }
    } on Exception catch (e) {
      print("Exception in SaveModelsBasedOnSelectedMake() : " + e.toString());
      // e.GetBaseException();
    }
  }

// clearFieldData(String fieldName)
//  {
//    arrayListEntryFields = new List<XfxEntry>();
//    FindEntryByKey(_parent, _subProcessField, fieldName);
//    if (arrayListEntryFields.Count > 0)
//    {
//      arrayListEntryFields[0].Text = "";
//    }
//  }

}
