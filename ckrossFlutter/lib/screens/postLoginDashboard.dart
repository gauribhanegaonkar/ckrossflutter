  
  
import 'package:ckrossFlutter/screens/permissionList.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';


class PostLoginDashboard extends StatefulWidget {
  PostLoginDashboard({Key key}) : super(key: key);

  @override
  _PostLoginDashboardState createState() => new _PostLoginDashboardState();
}
 class _PostLoginDashboardState extends State<PostLoginDashboard> with SingleTickerProviderStateMixin ,WidgetsBindingObserver{


   void initState() {
     super.initState();
     permissionAcessPhone();

   }

   Future permissionAcessPhone() async {
     List<PermissionGroup> plist = [];
     final PermissionHandler _permissionHandler = PermissionHandler();
     var permissionStatus = await _permissionHandler
         .checkPermissionStatus(PermissionGroup.camera);
     if (permissionStatus != PermissionStatus.granted) {
       plist.add(PermissionGroup.camera);
     }
     permissionStatus =
     await _permissionHandler.checkPermissionStatus(PermissionGroup.phone);
     if (permissionStatus != PermissionStatus.granted) {
       plist.add(PermissionGroup.phone);
     }
     permissionStatus =
     await _permissionHandler.checkPermissionStatus(PermissionGroup.microphone);
     if (permissionStatus != PermissionStatus.granted) {
       plist.add(PermissionGroup.microphone);
     }
     permissionStatus =
     await _permissionHandler.checkPermissionStatus(PermissionGroup.storage);
     if (permissionStatus != PermissionStatus.granted) {
       plist.add(PermissionGroup.storage);
     }
     permissionStatus =
     await _permissionHandler.checkPermissionStatus(PermissionGroup.sms);
     if (permissionStatus != PermissionStatus.granted) {
       plist.add(PermissionGroup.sms);
     }

     if (plist.length > 0) {
       PermissionService.hasPermission(plist);
     }
   }

   Widget build(BuildContext context) {
      return Scaffold(
      body: Center(
           child:Text("Welcome to App")
      )
      );
 }
 }