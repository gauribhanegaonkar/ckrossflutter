import 'dart:convert';
import 'dart:io';
import 'dart:convert' as JSON;
import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/UITemplateOperations.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/Utils/imageUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/makeDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/make_dealer_dataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/modelDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/productDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/RequestModels/LoadHTMLFieldsRequestModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/DoOtpLoginResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/GetMobileOTPResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/models/make.dart';
import 'package:ckrossFlutter/models/make_dealer_data.dart';
import 'package:ckrossFlutter/models/model.dart';
import 'package:ckrossFlutter/models/product.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart';
import 'package:dio/dio.dart' as dio;
import 'package:image_picker/image_picker.dart';
import 'package:open_file/open_file.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

class UserSignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: LoginPageStateful()
    );
  }
}
class LoginPageStateful extends StatefulWidget {
  LoginPageStateful({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}
class _LoginPageState extends State<LoginPageStateful> with SingleTickerProviderStateMixin ,WidgetsBindingObserver{

  static const platform = const MethodChannel('samples.flutter.io/files');
  List imgs;

  List<Widget> children = [];
   GetMobileOtpResModel model = new GetMobileOtpResModel();

    TextEditingController userIDController = new TextEditingController();
    TextEditingController appVersion = new TextEditingController();
     TextEditingController userID2Controller = new TextEditingController();

    TextEditingController otpController = new TextEditingController();
    String versionName;
    var newMap;
    var subcategory;
    var saveCurrentProductMap;
    var productArray;
    var responseObject;
    String productName = "";
    String make = "";
    String model1 = "" ;
    String model_variant = "";
    int onroadPrice ;
    String orp;
    int pid = 0 , mkid = 0, moid = 0 ;
    List<Map<String,dynamic>> jArrayDealerData;
    final dbHelper = DatabaseHelper.instance;
    var db1;

  static DatabaseHelper db = DatabaseHelper();
  @override
  void didChangeDependencies() {
 
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  void initState() {
    super.initState();
    checkLogin();
    getVersion();
  }

  checkLogin() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    if(sp.getBool(AppConstants.IS_LOGGED_IN) == true)
    {
      Navigator.pushReplacementNamed(context, '/homeScreen');
    }
  }
  getVersion () async {
    try{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
       appVersion.text = packageInfo.version;
    });
    children.add(signInWidget());
    } on Exception  catch (_) {
      children.add(signInWidget());
    }
  }
 @override
  Widget build(BuildContext context) {
      return Scaffold(
      body: Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
          children: children
        )
          ),
    );
  }

   signInWidget(){

return  MergeSemantics(
            child: Padding(
             padding: EdgeInsets.fromLTRB(20, 0.0, 20, 0),
              child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                         Padding(
                            padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                           child:Center(
                          child: Image.asset('images/wheels_emi_logo.png', height: 35)
                          ),
                         ),

                    Padding(
                        padding: EdgeInsets.fromLTRB(20, 10.0, 20.0, 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                 Icon(Icons.account_box,color: Colors.blueAccent,),
                                 Container(width:20),
                              Container(
                               width: 200,
                                child: TextField(
                                controller: userIDController,
                                decoration: InputDecoration(
                                labelStyle: new TextStyle(color: Colors.orange),
                                labelText: "User ID",
                                enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.orange),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.orange),
                                ),
                                //hintText: "example@mail.com",
                              ),
                             // autofocus: true,
                            ),
                            ),
                              ]
                            ),

                           Padding(
                             padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                            child:SizedBox(

                              width: double.infinity,
                               height: 40,
                              child: new RaisedButton(
                                color: Colors.blue[900],
                                child: Text(
                                  "SEND OTP",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Raleway',
                                    fontSize: 18.0,
                                  ),
                                ),
                                onPressed: () => {


                                  //  setState(() {
                                  //   preotp = false;
                                  // })
                                  sendOTP(context)
                                  },
                              ),
                            ),)
                            ,Text("(*OTP will be sent to your registered mobile no)", style: TextStyle(color:Colors.orange,fontSize: 12),)
                          ],
                        )),
                  ],
                ),
              ),
              Container(height: 10),
              Center(
                child: Text("App Version : "+appVersion.value.text)

                 ),
              Container(height: 10),
     Center(
     child: SizedBox(
     width: double.infinity,
     height: 40,
     child: new RaisedButton(
     color: Colors.blue[900],
     child: Text(
     "Video recording",
     style: TextStyle(
     color: Colors.white,
     fontFamily: 'Raleway',
     fontSize: 18.0,
     ),
     ),
     onPressed: () => {

     showDialog(
     context: context,
     builder: (context) {
     return Dialog(
     shape: RoundedRectangleBorder(
     borderRadius: BorderRadius.circular(10)),
     elevation: 10,
     child: Container(
     height: 100,
     width: 180,
     child: Column(
     children: <Widget>[
     SizedBox(
     width: double.infinity,
     child: FlatButton(
     child: Text('Select from gallary'),
     onPressed: () =>
     Navigator.pushReplacementNamed(context, '/videoFromGallery'),
     ),
     ),
     SizedBox(
     width: double.infinity,
     child: FlatButton(
     child: Text('Capture Video'),
     onPressed: () =>
     Navigator.pushReplacementNamed(context, '/videoRecording'),
     ),
     ),
     ],
     ),
     ),
     );

     //   ImageUtils.getAndSaveImage(),
     },
     ),

     FileUtils.saveLogFile(),
     //
     },
     ),
     ),
     ),


        ],
      ),
            ),
          );

    }
     
 
   sendOTP (BuildContext context) async {
     try{
    Dialogs.showSimpleDialog(context, "Please Wait");

    if(userIDController.text.length!=0){
       bool isValid= await getmobileotp() ;

        if(isValid!=false){
          setState((){
               children.removeAt(0);
                children.insert(0, OtpVerificationWidget());
                userID2Controller.text=userIDController.text;
                 });
        }
      // Dialogs.dismissSimpleDialog(context);
    }else{
     Dialogs.dismissSimpleDialog(context);

       showdialog("Please Enter User ID");

    }
     } on Exception  catch (_){
       Dialogs.dismissSimpleDialog(context);
     }

  }

  Widget OtpVerificationWidget(){
   return MergeSemantics(
            child: Padding(
             padding: EdgeInsets.fromLTRB(20, 0.0, 20, 0),
              child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                         Padding(
                            padding: EdgeInsets.fromLTRB(20, 10.0, 0, 0),
                           child:Center(
                          child: Image.asset('images/wheels_emi_logo.png', height: 35)
                          ),
                         ),
                   Padding(
                            padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                           child:Center(
                          child: Text("Waiting to autmatically detect OTP")
                          ),
                         ),
                    Padding(
                         padding: EdgeInsets.fromLTRB(20, 10.0, 0.0, 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                 Icon(Icons.account_box,color: Colors.blueAccent,),
                                 Container(width:20),
                              Container(
                               width: 200,
                                child: TextField(
                                controller: userID2Controller,
                                decoration: InputDecoration(
                                labelStyle: new TextStyle(color: Colors.orange),
                                labelText: "User ID",
                                enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.orange),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.orange),
                                ),
                                //hintText: "example@mail.com",
                              ),
                             // autofocus: true,
                            ),
                            ),
                              ]
                            ),

                            Padding(
                        padding: EdgeInsets.fromLTRB(0, 10.0, 0.0, 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                 Icon(Icons.lock,color: Colors.blueAccent,),
                                 Container(width:20),
                              Container(
                               width: 200,
                                child: TextField(
                                controller: otpController,
                                decoration: InputDecoration(
                                labelStyle: new TextStyle(color: Colors.orange),
                                labelText: "OTP",
                                enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.orange),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.orange),
                                ),
                                //hintText: "example@mail.com",
                              ),
                             // autofocus: true,
                            ),
                            ),
                              ]
                            ),

                          ])),

                           Padding(
                             padding: EdgeInsets.fromLTRB(0, 10.0, 20, 0),
                            child:SizedBox(

                              width: double.infinity,
                               height: 40,
                              child: new RaisedButton(
                                color: Colors.blue[900],
                                child: Text(
                                  "SIGN IN",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Raleway',
                                    fontSize: 18.0,
                                  ),
                                ),
                                onPressed: ()  {
                                  signIn();

                                 // sendOTP(context)
                                  },
                              ),
                            ),
                           ),
                            Padding(
                             padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                            child:Row(
                              children: <Widget>[
                                 InkWell(
                                  child: Text(
                                   "OTP not received?",
                                    style: TextStyle(fontSize: 15, color: Colors.blue[900]),
                                  ),
                                  onTap: () => {
                                    // onPressedSendOTP(
                                    //   context,
                                    //   mobileNumberController.value.text.toString(),
                                    // )
                                  },
                                ),
                                Container(width:70),
                                 InkWell(
                                  child: Text(
                                   "RESEND",
                                    style: TextStyle(fontSize: 15, color: Colors.blue[900]),
                                  ),
                                  onTap: () => {
                                    sendOTP(context)
                                    // onPressedSendOTP(
                                    //   context,
                                    //   mobileNumberController.value.text.toString(),
                                    // )
                                  },
                                ),
                            //   ],
                            // ),
                            // )
                          ],
                        )
                        ),
                  ],
                ),
              ),
        ],
      ),
            )])));


 }

 Future<bool> getmobileotp() async {
   bool valid = false;
    try {
      FocusScope.of(context).requestFocus(new FocusNode());
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.getMobileOTP(userIDController.text);
        if (response != null) {
         
          valid = await handleResponse(context, userIDController.text , response);
        } else {

          CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
        }
      } else {
        CustomSnackBar.showSnackBar(context,AppConstants.somethingWentWrong);
      }
    } on Exception  catch (_) {
      //Dialogs.dismissSimpleDialog(context);
      Dialogs.DialogWidget(context, "", "Customer Not Found");
      valid = false;
    }
     Dialogs.dismissSimpleDialog(context);
    return valid;

}

 Future handleResponse(
      BuildContext context, String userid, Response response) async {
    bool isSuccess = false;
    if (response.statusCode == 200) {
      GetMobileOtpResModel responseModel = getMobileOtpResModelFromJson(response.body);

      if (responseModel != null) {
        if (responseModel.success ==true && responseModel.redirect==true ) {
          isSuccess = true;
         CustomSnackBar.showSnackBar(context, responseModel.details);
          //getmobileotp(context, responseModel.customerdetail[0].agreementno);
        } else if ((responseModel.success == false||responseModel.success == null) && (responseModel.redirect == false ||responseModel.redirect==null)) {
          isSuccess=false;
          CustomSnackBar.showSnackBar(context, "Incorrect user ID. User does not exist at server");
         // await getmobileotp();
          return isSuccess;
        } else  {
         // Dialogs.dismissSimpleDialog(context);
          CustomSnackBar.showSnackBar(context, responseModel.details);
        }


      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } else {
      CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
    }
    return isSuccess;
  }

  Future<bool> _confirmOTP(
      BuildContext context, String userid, String otp) async {
        try{
    FocusScope.of(context).requestFocus(new FocusNode());
    bool valid = false;



    ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
    if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
     var response = await APIServices.doOTPLogin(userid, otp);
      if (response != null) {
      // print("resssss"+response.body);
        valid = await handleConfirmOTPResponse(context, userid, otp, response);
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } else {
      CustomSnackBar.showSnackBar(context, AppConstants.noInternetConnection);
    }

    // Dialogs.dismissSimpleDialog(context);
     return valid;
        } on Exception  catch (_) {
          Dialogs.dismissSimpleDialog(context);
          //print(Exception);
        }
  }

  Future<bool> handleConfirmOTPResponse(BuildContext context, String userid,
      String otp, Response response) async {
     bool isSuccess = false;
    if (response.statusCode == 200) {
      DoOtpLoginResModel responseModel =
          doOtpLoginResModelFromJson(response.body);
     if (responseModel != null) {
       if (responseModel.success) {


         isSuccess = true;
           await saveProcessIDsAsync(responseModel.menudetails);
           await saveCurrentProduct(responseModel.menudetails);


           SharedPreferences sp = await SharedPreferences.getInstance();
           sp.setString(AppConstants.LOGGED_IN_USER_ID, responseModel.loggedInUserId);
           sp.setString(AppConstants.LOGGED_IN_USER, responseModel.loggedInUser);
           sp.setString(AppConstants.TOKEN, responseModel.token);
           sp.setString(AppConstants.APP_VERSION, responseModel.appVersion);
           sp.setString(AppConstants.LOGGED_IN_USER_ROLE , responseModel.loggedInUserRole);
      
           String userRole = responseModel.loggedInUserRole;
           if ((userRole.toLowerCase().startsWith("SO".toLowerCase())) ||
                (userRole.toLowerCase().startsWith("SE".toLowerCase())) ||
                (userRole.toLowerCase().startsWith("VD".toLowerCase())))
            {
                await loadFieldData(responseModel , response.body);
            }
            else if (userRole.toLowerCase().startsWith("SIA".toLowerCase()))
            {

            }
            else
            {
                CustomSnackBar.showSnackBar(context, "User Role Is Not Valid");
            }

          return isSuccess;
        } else {
            //logger.Info("Response: Failed");
            Dialogs.dismissSimpleDialog(context);
            if (responseModel.details != null)
            {

              CustomSnackBar.showSnackBar(context, responseModel.details);
                //OtpEntry.Text = string.Empty; // added by nilima on 29 may 2018
            }
            else
            {
               CustomSnackBar.showSnackBar(context, "User data not available at Server.\nPlease try after sometime.");
                //OtpEntry.Text = string.Empty; // added by nilima on 29 may 2018
            }

         // CustomSnackBar.showSnackBar(context, responseModel.details);
          isSuccess=false;
          return isSuccess ;
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } else {
      CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
    }
    return isSuccess;
   }

 signIn() async {
  if(otpController.text.length==0){
      showdialog("Please Enter OTP");
  }else{
       Dialogs.showSimpleDialog(context, "Please Wait");
       bool valid = await _confirmOTP(context,userID2Controller.text,otpController.text);
       if(valid){
        // Navigator.pushReplacementNamed(context, '/PostLoginDashboard');
       }                                  
     }
 }
showdialog(String text){
    showDialog(
                context: context,
               // barrierDismissible: false,
                builder: (BuildContext context) {
                  return AlertDialog(
                    content: new Text(
                      text
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: new Text("OK"),
                        onPressed: () {
                         Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
  }

  loadFieldData(DoOtpLoginResModel responseModel , String responseContent) async{

    SharedPreferences sp = await SharedPreferences.getInstance();
    try{

        print("Start of loadFieldData()");
        String userName = "";
        var tempUserName = sp.get(AppConstants.LOGGED_IN_USER_NAME);
        userName = ! (tempUserName==null||tempUserName.isEmpty)? tempUserName : "";

        loadHtmlFields(responseModel,responseModel.productId.toString());
       
    }on Exception  catch (e) {
      print(e);
    }
  }


saveProcessIDsAsync(String menudetails) async {
          SharedPreferences sp = await SharedPreferences.getInstance();

 try{

   print("Start of SaveProcessIDsAsync()");
    newMap= json.decode(menudetails);
   
          var array = newMap["GroupToCategoryToSubCategory"];
          if (array != null && array.length != 0){
            for (int i = 0; i < array.length; i++)
                    {
                     newMap = <String, dynamic>{};
                     newMap.addAll(array[i]);
                     if(newMap["categoryName"]=="Leads"){

                       var subcatarray =newMap["SubCategory"]  ;
                       var subcat = <String, dynamic>{};
                       subcat.addAll(subcatarray[0]);
                       if(subcat["key"]=="Lead Creation"){
                         print("preffff");
                         String htmlPage = subcat["htmlPage"];
                         String processID = subcat["processID"];
                         String key = subcat["key"];
                         sp.setString(AppConstants.CURRENT_PRODUCT_LEAD_CREATION_ID,processID);
                         sp.setString(AppConstants.CURRENT_PRODUCT_LOAD_HTML, htmlPage);
                         sp.setString(AppConstants.LEAD_CREATION_KEY , key);
                       }
                       var subcat2 = <String, dynamic>{};
                       subcat2.addAll(subcatarray[1]);
                       if(subcat2["name"]=="List Stages"){
                        //  print("submenu");
                        //   print(subcat2["subMenu"]);
                          var namearray = subcat2["subMenu"];
                          for (int j = 0; j < namearray.length; j++){
                               var name = <String, dynamic>{};
                               name.addAll(namearray[j]);
                              //  print("name");
                              //  print(name["name"]);

                               if(name["name"]==AppConstants.QUERY_RESOLUTION_KEY){
                                     String processId = name["processID"];
                                  //   print("QUERY_RESOLUTION_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_QUERY_RESOLUTION_ID, processId);
                                     sp.setString(AppConstants.QUERY_RESOLUTION_KEY, name["name"]);

                               }
                               if(name["name"]==AppConstants.PRE_DISBURSEMENT_KEY){
                                     String processId = name["processID"];
                                //     print("PRE_DISBURSEMENT_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_PRE_DISBURSEMENT_ID, processId);
                                     sp.setString(AppConstants.PRE_DISBURSEMENT_KEY, name["name"]);

                               }
                               if(name["name"]==AppConstants.POST_DISBURSEMENT_KEY){
                                     String processId = name["processID"];
                                    //  print("POST_DISBURSEMENT_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_POST_DISBURSEMENT_ID, processId);
                                     sp.setString(AppConstants.POST_DISBURSEMENT_KEY, name["name"]);

                               }
                                if(name["name"]==AppConstants.SERVICE_INSPECTION_KEY){
                                     String processId = name["processID"];
                                    //  print("SERVICE_INSPECTION_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_SERVICE_INSPECTION_ID, processId);
                                     sp.setString(AppConstants.SERVICE_INSPECTION_KEY, name["name"]);

                               }
                                if(name["name"]==AppConstants.PRE_DISBURSEMENT_ADVICE_KEY){
                                     String processId = name["processID"];
                                    //  print("PRE_DISBURSEMENT_ADVICE_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_PRE_DISBURSEMENT_ADVICE_ID, processId);
                                     sp.setString(AppConstants.PRE_DISBURSEMENT_ADVICE_KEY, name["name"]);

                               }
                               if(name["name"]==AppConstants.POST_DISBURSEMENT_QUERY_RESOLUTION_KEY){
                                     String processId = name["processID"];
                                     print("POST_DISBURSEMENT_QUERY_RESOLUTION_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_POST_DISBURSEMENT_QUERY_RESOLUTION_ID, processId);
                                     sp.setString(AppConstants.POST_DISBURSEMENT_QUERY_RESOLUTION_KEY, name["name"]);

                               }
                               if(name["name"]==AppConstants.QUERY_RESOLUTION_KEY){
                                     String processId = name["processID"];
                                     print("QUERY_RESOLUTION_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_PRE_DISBURSEMENT_ADVICE_QUERY_RESOLUTION_ID, processId);
                                     sp.setString(AppConstants.PRE_DISBURSEMENT_ADVICE_QUERY_RESOLUTION_KEY, name["name"]);
                               }
                               if(name["name"]==AppConstants.CC_LEADS_CREATED_KEY){
                                     String processId = name["processID"];
                                     print("CC_LEADS_CREATED_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_CC_LEADS_CREATED_ID, processId);
                                     sp.setString(AppConstants.CC_LEADS_CREATED_KEY, name["name"]);
                               }
                               if(name["name"]==AppConstants.CREDIT_REFER_QUEUE_KEY){
                                     String processId = name["processID"];
                                     print("CREDIT_REFER_QUEUE_KEY"+processId);
                                     sp.setString(AppConstants.CURRENT_PRODUCT_CREDIT_REFER_QUEUE_ID, processId);
                                     sp.setString(AppConstants.CREDIT_REFER_QUEUE_KEY, name["name"]);
                               }
                          }

                       }


                     }
                    }
          }

 } on Exception  catch (e){
   print("Exception Message"+e.toString());
 }

}

saveCurrentProduct(String menudetails) async {
  try{
     SharedPreferences sp = await SharedPreferences.getInstance();

      print("Start of SaveCurrentProduct()");
      saveCurrentProductMap= json.decode(menudetails);
      var productArray = saveCurrentProductMap["GroupToCategoryToSubCategory"];

      if (productArray != null && productArray.length != 0){
        for (int i = 0; i < productArray.length; i++){
          saveCurrentProductMap = <String, dynamic>{};
          saveCurrentProductMap.addAll(productArray[i]);
          if(saveCurrentProductMap["categoryName"]=="Product"){
               int product_id = saveCurrentProductMap["product_Id"];
               var subCategoryArray =saveCurrentProductMap["SubCategory"];
              // print(saveCurrentProductMap["SubCategory"]);

               for (int j = 0; j < subCategoryArray.length; j++){
                  var subProduct = <String, dynamic>{};
                  subProduct.addAll(subCategoryArray[j]);
                  //  print("iddddddd");
                  //  print(subProduct["id"]);
                   if(product_id==subProduct["id"]){
                     sp.setString(AppConstants.CURRENT_PRODUCT_NAME,  subProduct["name"]);
                    //  print(subProduct["name"]);
                   }
               }
               //print("product ddd"+product_id.toString());

              // var subProductArray =saveCurrentProductMap["SubCategory"]  ;
              // var subProduct = <String, dynamic>{};
              // subProduct.addAll(subProductArray[0]);
          }
        }
      }

  }on Exception  catch (e){
   print("Exception Message"+e.toString());
 }
}

loadHtmlFields(DoOtpLoginResModel doOtpresponseModel,String product_Id) async {

   try {
      FocusScope.of(context).requestFocus(new FocusNode());
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.loadHtmlFields(product_Id);
        if (response != null&&!(identical(response,"null")) && !(response == "{}")) {
           await handleloadHtmlFieldsResponse(context,response,doOtpresponseModel);
        } else {

          CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
        }
      } else {
        CustomSnackBar.showSnackBar(context,AppConstants.somethingWentWrong);
      }
    } on Exception  catch (_) {
      //Dialogs.dismissSimpleDialog(context);
      Dialogs.DialogWidget(context, "", "Customer Not Found");
     // valid = false;
    }
     //Dialogs.dismissSimpleDialog(context);
}

 Future handleloadHtmlFieldsResponse(
      BuildContext context, Response response,DoOtpLoginResModel doOtpresponseModel) async {
      //  print("sdsfsdf"+response.body.toString());
    bool isSuccess = false;
    if (response.statusCode == 200) {
      LoadHtmlFieldsResModel responseModel = loadHtmlFieldsResModelFromJson(response.body);
      if (responseModel != null ) {
        if (responseModel.success ) {
           String userName = "";
           SharedPreferences sp = await SharedPreferences.getInstance();
        var tempUserName = sp.get(AppConstants.LOGGED_IN_USER_NAME);
        userName = ! (tempUserName==null||tempUserName.isEmpty)? tempUserName : "";
         // var map = json.decode(responseModel.toString());
         // print("resmodel"+responseModel.subProcessFieldsData.toString());
        //  SharedPreferences sp = await SharedPreferences.getInstance();
          sp.setString(AppConstants.CLIENT_NAME, AppConstants.DATABASE_NAME);
          sp.setString(AppConstants.LANGUAGE, AppConstants.APP_LANG);
          // Navigator.pushReplacementNamed(context, '/homeScreen');

          try{
              print("trucate login json table");
              await UITemplateOperations.initDatabase();
              await UITemplateOperations().TruncateTemplateJsonTable();
              await UITemplateOperations().TruncateLoginJsonTemplateTable();
              await UITemplateOperations().TruncateProductTable();
              await UITemplateOperations().TruncateMakeTable();
              await UITemplateOperations().TruncateModelTable();
              await UITemplateOperations().TruncateDealerSOTable();
              await UITemplateOperations().TruncateMakeDealerDataTable();
              await UITemplateOperations().TruncateStatesTable();
              await UITemplateOperations().TruncateDealerLocationVDTable();
              await UITemplateOperations().TruncateModelVDTable();
              await UITemplateOperations().TruncateVehicleCategoryTable();
              await UITemplateOperations().TruncateYearVDTable();
              
              await UITemplateOperations().TruncateCityTable();
                //var  jsonObjectApplicant = '';
                Object jsonObjectApplicant='' ;
                
               print("Start of insert template");
             
               await UITemplateOperations.SaveUpdatedTemplateInDatabase(responseModel, json.encode(jsonObjectApplicant));
                String jsonStringImages = "", jsonStatusUploadImages = "";
                String jsonPreUploadImages = "", jsonPostUploadImages = "", jsonVideosList = "";

                String jsonMenuDetails = doOtpresponseModel.menudetails;
                String jsonProductDetails = doOtpresponseModel.productToMakeToModelToOnPriceJsonArray;
                
               // var responseObject = json.encode(doOtpresponseModel);
                  Map<String, dynamic> responseObject = doOtpresponseModel.toJson() ;
                  Map<String, dynamic> loadhtmlresponseObject = responseModel.toJson() ;

               try
                            {
                               //Map<String,dynamic> jObject = json.decode(responseObject);
                               if (loadhtmlresponseObject.containsKey("details"))
                                {
                                  Map<String, dynamic> map2 =loadhtmlresponseObject["details"];                                  
                                   if (map2.containsKey("leadDocuments"))
                                    {
                                        jsonStringImages = map2["leadDocuments"].toString();
                                        print("jsonStringImages"+jsonStringImages);
                                    }
                                }

                                if (responseObject.containsKey("leadDocuments"))
                                {
                                  Map<String, dynamic> map2 =responseObject["leadDocuments"];
                                    //JObject jsonStringImagesObject = (JObject)responseObject.SelectToken("leadDocuments");
                                    jsonStringImages = map2.toString();
                                    print("leadDocuments in leads"+jsonStringImages);
                                }

                                if (responseObject.containsKey("Pre_Disb_DocumentList"))
                                {
                                    Map<String, dynamic> map =responseObject["Pre_Disb_DocumentList"];
                                    jsonPreUploadImages = map.toString();
                                  //  print("Pre_Disb_DocumentList"+jsonPreUploadImages);
                                }

                                if (responseObject.containsKey("Post_Disb_DocumentList"))
                                {
                                     Map<String, dynamic> map =responseObject["Post_Disb_DocumentList"];
                                    jsonPostUploadImages = map.toString();
                                  //  print("Post_Disb_DocumentList"+jsonPostUploadImages);
                                }

                                if (responseObject.containsKey("Service_Inspection_DocumentList"))
                                {
                                   Map<String, dynamic> map =responseObject["Service_Inspection_DocumentList"];
                                    jsonVideosList = map.toString();
                                    print("Service_Inspection_DocumentList"+jsonVideosList);
                                }

                                if (responseObject.containsKey("Update_Lead_DocumentList"))
                                {
                                  Map<String, dynamic> map =responseObject["Update_Lead_DocumentList"];
                                    jsonStatusUploadImages = map.toString();
                                    print("Update_Lead_DocumentList"+jsonStatusUploadImages);

                                }
                             }
                            catch ( e)
                            {
                                print("Exception In loadFieldData()$e " );
                                
                            }

                             print("save login menu table");
                            await UITemplateOperations.SaveMenusInDatabase("Menu Details", jsonMenuDetails);
                              sp.setString(AppConstants.PRODUCTJSON, jsonMenuDetails);
                            await UITemplateOperations.SaveMenusInDatabase("Images", jsonStringImages);
                            await UITemplateOperations.SaveMenusInDatabase("PreImages", jsonPreUploadImages);
                            await UITemplateOperations.SaveMenusInDatabase("PostImages", jsonPostUploadImages);
                            await UITemplateOperations.SaveMenusInDatabase("VideoNames", jsonVideosList);
                            await UITemplateOperations.SaveMenusInDatabase("StatusUploadImages", jsonStatusUploadImages);
                             
                           // var jArrayDealerData = null;
                            List<dynamic> jArrayDealerData = null;

                            try
                            {
                                if (responseObject.containsKey("dealer_data"))
                                {
                                    //save data in DealerSO table
                                  //  print("containsKey delaer"+responseObject.containsKey("dealer_data").toString());
                                    // Map<String, dynamic> map =responseObject["dealer_data"];
                                    List map =responseObject["dealer_data"];
                                    jArrayDealerData = map;
                                    await UITemplateOperations.SaveMenusInDatabase("dealer_data", jsonStatusUploadImages);
                                 //   print("jsonStatusUploadImages dealer_data"+jArrayDealerData.toString());
                                }
                            }
                            catch ( e)
                            {
                                
                                print("Exception In loadFieldData$e " );
                            }

                            try
                            {
                              print("In try of dealer_data_SO");
                                if (responseObject.containsKey("dealer_data_SO"))
                                {
                                    print("save data in DealerSO table");
                                    List map =responseObject["dealer_data_SO"];
                                    jArrayDealerData = map;
                                    //print("jArrayDealerDataSO"+map.toString());
                                    await UITemplateOperations.AddDealerSODataAsync(map);
                                }
                            }
                            catch ( e)
                            {
                                //print(e.GetBaseException().ToString());
                                print("Exception In loadFieldData $e" );
                            }
                            if (jsonProductDetails != null)
                            {
                              await SaveProductDataAsync(jsonProductDetails);
                            }
                            await SaveMakeFromDealerData(jArrayDealerData);


                            await UITemplateOperations.SaveMenusInDatabase(AppConstants.LOGGED_IN_USER_ID, doOtpresponseModel.loggedInUserId);
                            await UITemplateOperations.SaveMenusInDatabase(AppConstants.LOGGED_IN_USER_NAME, userName);
                            await UITemplateOperations.SaveMenusInDatabase(AppConstants.TOKEN, doOtpresponseModel.token);
                           await UITemplateOperations.SaveMenusInDatabase(AppConstants.LOGGED_IN_USER_ROLE, doOtpresponseModel.loggedInUserRole);

                            sp.setString(AppConstants.LOGGED_IN_USER_ID, doOtpresponseModel.loggedInUserId);
                            sp.setString(AppConstants.LOGGED_IN_USER_NAME, userIDController.text);
                            sp.setString(AppConstants.LOGGED_IN_USER, doOtpresponseModel.loggedInUser);
                            sp.setString(AppConstants.TOKEN, doOtpresponseModel.token);
                            //Preferences.Set(AppConstants.APP_VERSION, doOTPLoginResponseModel.app_Version);
                            sp.setString(AppConstants.LOGGED_IN_USER_ROLE, doOtpresponseModel.loggedInUserRole);
                            sp.setString(AppConstants.CURRENT_PRODUCT_ID, doOtpresponseModel.productId.toString());
                            if(doOtpresponseModel.productId.toString() == '1'){
                              print("at product id........");
                              sp.setString(AppConstants.SELECTEDTEXT, "New TW");
                            }
                            sp.setBool(AppConstants.IS_NOTIFICATION_TOKEN_UPDATED, false);
                            sp.setBool(AppConstants.IS_PRODUCT_LOADED_SUCESSFULLY, true);

                          //   db.search();
                          FileUtils.saveLogFile();
                          sp.setBool(AppConstants.IS_LOGGED_IN, true);
                           Navigator.pushReplacementNamed(context, '/homeScreen');
                           ImageUtils.deleteFolders();
          }on Exception catch (e) {
            print("Exception In main loadFieldData $e");
          }
          //getmobileotp(context, responseModel.customerdetail[0].agreementno);
        } else if (responseModel.success == false && responseModel.redirect == false ) {
          isSuccess=false;
          print("Incorrect");
          CustomSnackBar.showSnackBar(context, "Incorrect user ID. User does not exist at server");
         // await getmobileotp();
          return isSuccess;
        } else  {
          CustomSnackBar.showSnackBar(context, "responseModel.details");
        }


      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } else {
      CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
    }
    return isSuccess;
  }

  SaveMakeFromDealerData  (List<dynamic> jArrayDealerData) async{
    print("start save make data");
    print(jArrayDealerData);
    var makeDealerData = new MakeDealerDataDB();
    if (jArrayDealerData != null)
    {
      for (int x = 0; x < jArrayDealerData.length; x++)
      {
        Map<String,dynamic> jArrayDealerDataObject = jArrayDealerData[x];

        if (jArrayDealerDataObject.containsKey("Make"))
        {
          List<dynamic> jArrayMake = jArrayDealerDataObject["Make"];

          for (int y = 0; y < jArrayMake.length; y++)
          {
            MakeDalerData make_dealer_data = new MakeDalerData();
            make_dealer_data.dealership_name = jArrayDealerDataObject["Dealership_Name"];
            make_dealer_data.dealer_type = jArrayDealerDataObject["Dealer_Type"];
            make_dealer_data.make_dealer_data_name = jArrayMake[y].toString();
            int id = await makeDealerData.insertMakeDealerData(make_dealer_data);
           // await db.insertMakeDalerData("Make_Dealer_Data", make_dealer_data);
            // print("make delaer data .." + id.toString());
          }
        }
      }
    }
    print("end save make data");
  }

  SaveProductDataAsync(String jsonProductDetails) async{
    // SharedPreferences sp = await SharedPreferences.getInstance();
    print("productJson" + jsonProductDetails);


    var productDB = await ProductDataDB();
    var makeDB = await MakeDB();
    var modelDB = await ModelDB();
    productArray = JSON.jsonDecode(jsonProductDetails);
    //print("productArray");
    //print(productArray);
    try{
      for(int i = 0; i < productArray.length; i++ ){
        Map<String,dynamic> PObject = productArray[i];
       // print(PObject);
        if(PObject.containsKey("Product")){
          productName = PObject["Product"];
         // print("productName " + productName);
        }

        if(PObject.containsKey("Make")){
          make = PObject["Make"];
        //  print("Make " + make);
        }

        if(PObject.containsKey("Model")){
          model1 = PObject["Model"];
        //  print("Model " + model1);
        }

        if(PObject.containsKey("Model_Variant")){
          model_variant = PObject["Model_Variant"];
         // print("model_variant " + model_variant);
        }

        if(PObject.containsKey("orp")){
          onroadPrice = PObject["orp"];
         // print("onroadPrice " + onroadPrice.toString());
          orp = onroadPrice.toString();
        }
        if((productName.isNotEmpty && productName != null) && (make.isNotEmpty && make != null) && (orp.isNotEmpty && orp != null))
        {
          print("start table insertion");
          //db1 = await dbHelper.database;

          String queryProduct = DatabaseQueries.CheckProductExistInDB(productName);
          List<Map<String,dynamic>> productlist = await DatabaseHelper().execute(queryProduct);

          List<Map<String,dynamic>> data = await productDB.show();
          print("Product database..." + data.toString());

          if (productlist.length > 0)
          {
            print(productlist.length);
            print("if product");
            pid = productlist[0]['product_id'];
            print("pid");
            print(pid);


          }else{

            Product product = new Product();
            product.product_name = productName;
            int id = await productDB.insertProduct(product);
            print("insert data");
            print(id);
            if(id == 1){
              print("id == 1");
              String Query = DatabaseQueries.GetMaxProductId();
              List<Map<String,dynamic>> maxId = await DatabaseHelper().execute(Query);
              print("max id");
              print(maxId);
              pid = maxId[0]["LastId"];
              print(pid);
            }
          }//end product if

          String queryMake = DatabaseQueries.CheckMakeExistInDB(make);
          List<Map<String,dynamic>> makelist = await DatabaseHelper().execute(queryMake);
          List<Map<String,dynamic>> data1 = await makeDB.showMake();
          print("Make database..." + data1.toString());


          if(makelist.length > 0)
          {
            mkid = makelist[0]["make_id"];
            print("mkid" + mkid.toString());
          }else if (pid > 0){
            MakeModel make1 = new MakeModel();
            make1.product_id = pid.toString();
            make1.make_name = make;
            int id = await makeDB.insertMake(make1);
            print("insert make data" + id.toString());
            if (id == 1)
            {
              String QueryMake = DatabaseQueries.GetMaxMakeId();
              List<Map<String,dynamic>> maxId = await DatabaseHelper().execute(QueryMake);
              print("max id");
              mkid = maxId[0]["LastId"];
              print("mkid.." + mkid.toString());
            }
          }//end make if

          String queryModel = DatabaseQueries.CheckModelExistInDB(model1);
          List<Map<String,dynamic>> modellist = await DatabaseHelper().execute(queryModel);
          List<Map<String,dynamic>> data2 = await modelDB.showModel();
          print("Model database..." + data2.toString());

          if (modellist.length > 0)
          {
            moid = modellist[0]["model_id"];
          }
          else if (mkid > 0) {
            print("insert model");
            Model model1 = new Model();
            model1.make_id = mkid.toString();
            model1.model_name = model_variant;
            model1.on_road_price = orp;
            int id = await modelDB.insertModel(model1);
            if (id == 1)
            {
              String QueryModel = DatabaseQueries.GetMaxModelId();
              List<Map<String,dynamic>> maxId = await DatabaseHelper().execute(QueryModel);
              moid = maxId[0]["LastId"];
              print("moid.." + moid.toString());
            }
          }//end model if
        }//main if
      }//end for loop
    } on Exception catch (e) {
      print("Exception In saveProduct $e");
    }
    print("end savePeoductAsync");
  }//end saveProduct


}
