import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import 'package:flick_video_player/flick_video_player.dart';


class VideoApp extends StatefulWidget {
  VideoApp(this._controller, {Key key}) : super(key: key);

  VideoPlayerController _controller;
  @override
  _VideoAppState createState() => _VideoAppState(this._controller);
}

class _VideoAppState extends State<VideoApp> {
  _VideoAppState(this._controller, {Key key}) : super();
  VideoPlayerController _controller;

  FlickManager flickManager;

  @override
  void initState() {
    super.initState();
    flickManager = FlickManager(
      videoPlayerController:
      _controller,
    );
    _controller.play();

  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          // title: const Text('Camera example'),
          backgroundColor: Colors.black,

        ),
        body: Container(
        child: FlickVideoPlayer(
        flickManager: flickManager
        ),

        ),

      ),);

  }

  @override
  void dispose() {
    super.dispose();
   // _controller.dispose();
  }
}