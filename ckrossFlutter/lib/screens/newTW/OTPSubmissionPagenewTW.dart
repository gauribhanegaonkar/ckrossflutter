import 'dart:convert';
import 'dart:convert' as JSON;
import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ResponseModels/GetMobileOTPResModel.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/webUtils/apiConstants.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeadCreationOTPPageNewTW extends StatelessWidget {

  LeadCreationOTPPageNewTW(this.submitRequestJson, {Key key}) : super(key: key);
  String submitRequestJson;
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LeadCreationOTPPageNewTWStateful(this.submitRequestJson));
  }
}

class LeadCreationOTPPageNewTWStateful extends StatefulWidget {

  LeadCreationOTPPageNewTWStateful(this.submitRequestJson, {Key key}) : super();

  String submitRequestJson;
  @override
  _LeadCreationOTPPageNewTWStatefulState createState() =>
      _LeadCreationOTPPageNewTWStatefulState(this.submitRequestJson);
}

class _LeadCreationOTPPageNewTWStatefulState
    extends State<LeadCreationOTPPageNewTWStateful> {

  _LeadCreationOTPPageNewTWStatefulState(this.submitRequestJson, {Key key}) : super();

  String submitRequestJson;
  TextEditingController otpController = new TextEditingController();

  static String userId;
  static String appLang;
  static String userRole;
  static String currentProductID;
  static String currentLeadCreationProcessID;
  static String currentProductName;
  static String nbfcName;
  SharedPreferences sp;

  static DatabaseHelper db = DatabaseHelper();

  var leadId ;
  int count = 0;
  String otpText;
 // String submitRequestJson;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    InitializeAppData();
    setLeadId();
  }

  setLeadId() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    leadId = sp.getString('lead_id');
    print(leadId);
    print(" setLeadId()");
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    sp = await SharedPreferences.getInstance();
    setState(() {
      if (count == 0 ) {
        print("GENERATE OTP");
        otpText = 'GENERATE OTP';
      }else{
        print("RGENERATE OTP");
        otpText ='RGENERATE OTP';
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
          backgroundColor: Colors.blue, title: new Text('OTP Submission')),
      body: Container(
        child: Padding(
          padding: EdgeInsets.fromLTRB(20, 40, 20, 0),
          child: Column(
            children: [
              TextField(
                controller: otpController,
                decoration: InputDecoration(
                  labelText: 'Enter OTP',
                ),
              ),
              SizedBox(
                height: 30,
              ),
              MaterialButton(
                height: 40,
                minWidth: double.infinity,
                child: Text(otpText),
                color: Colors.blue,
                textColor: Colors.white,
                onPressed: () => {
                  if(count <= 4){
                  generateOTP(),
                }else{
                    print("not call generateOTP"),
        }
                },
              ),
              SizedBox(
                height: 15,
              ),
              MaterialButton(
                height: 40,
                minWidth: double.infinity,
                child: Text('VERIFY OTP'),
                color: Colors.blue,
                textColor: Colors.white,
                onPressed: () => {
                  if (otpController.text.toString().isNotEmpty)
                    {
                      verifyOTP(otpController.text.toString()),
                    }
                  else
                    {
                      CustomSnackBar.showSnackBar(
                          context, "Please Enter Valid OTP")
                    }
                },
              ),
              SizedBox(
                height: 15,
              ),
              MaterialButton(
                height: 40,
                minWidth: double.infinity,
                child: Text('SKIP'),
                color: Colors.blue,
                textColor: Colors.white,
                onPressed: () => {},
              ),
            ],
          ),
        ),
      ),
    );
  }

  generateOTP() async {
    print("at generateOTP() ");
    String url;
    try {
      String firstName = "", middleName = "", lastName = "", mobile = "";
      print("at generateOTP() ");
      if (currentProductID != null && !currentProductID.isEmpty && currentLeadCreationProcessID != null && !currentLeadCreationProcessID.isEmpty) {
        print("at generateOTP() in if ");
        url = APIConstants.OTP_BASE_URL +
            currentProductID +
            "/" +
            currentLeadCreationProcessID +
            "/" +
            leadId +
            APIConstants.MOBILE_OTP;

        var reqJSON;
       if(submitRequestJson != null){
         var jsonObject = JSON.jsonDecode(submitRequestJson);

         Map submitRequest = jsonObject;
         print("submitRequest");
         print(submitRequest);
         if (submitRequest.containsKey("Applicant"))
         {
           print("at Applicant ");
           var applicantString = submitRequest["Applicant"];
           print("............at .........");
           print(applicantString);
           Map applicant = applicantString;
           print("applicant");
           print(applicant);

           firstName = applicant.containsKey("FirstName") ? applicant["FirstName"].toString() : "";
           middleName = applicant.containsKey("MiddleName") ? applicant["MiddleName"].toString() : "";
           lastName = applicant.containsKey("LastName") ? applicant["LastName"].toString() : "";
           mobile = applicant.containsKey("Mobile") ? applicant["Mobile"].toString() : "";

           var applicantDataObject = new Map<String, dynamic>();
           applicantDataObject["FirstName"] =  firstName;
           applicantDataObject["MiddleName"] = middleName;
           applicantDataObject["LastName"] =  lastName;
           applicantDataObject["Mobile"] = mobile;

           Map applicantObject = new Map<String, dynamic>();
           applicantObject["Applicant"] = applicantDataObject;

           Map dataObject = new Map<String, dynamic>();
           dataObject["data"] = applicantObject;

           print("dataObject");
           print(dataObject);


           reqJSON = json.encode(dataObject);
           print("reqJSON");
           print(reqJSON);
           sp.setString('reqJson', reqJSON);
         }// end of applicant if

         FocusScope.of(context).requestFocus(new FocusNode());
         ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
         if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
           var response;

             print("in if");
             response = await APIServices.getGenerateOtp(url, reqJSON);
             if (response != null) {
             handleGenerateOTPResponse(context, response);
           } else {
             CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
           }
         } else {
           CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
         }


       }else{
         print("..............in else");
         FocusScope.of(context).requestFocus(new FocusNode());
         ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
         if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
           var response;

             print("in else");
             sp = await SharedPreferences.getInstance();
             var req = sp.getString('reqJson');
             print(req);
             response = await APIServices.getGenerateOtp(url, req);
           if (response != null) {
             handleGenerateOTPResponse(context, response);
           } else {
             CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
           }
         } else {
           CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
         }
       }// end of submitrequest if





      } else{
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    }
    on Exception catch (e) {
      //Dialogs.dismissSimpleDialog(context);
      print("exception" + e.toString());
     // print("Customer Not Found");
      //  Dialogs.DialogWidget(context, "", "Customer Not Found");
    }
   // Dialogs.dismissSimpleDialog(context);

  }

  static InitializeAppData() async {
    try {
      print("InitializeAppData method");
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      appLang = !(tempLanguage == null || tempLanguage.isEmpty)
          ? tempLanguage
          : AppConstants.APP_LANG;
      var tempUserId = sp.getString(AppConstants.LOGGED_IN_USER_ID);

      if (!(tempUserId == null || tempUserId.isEmpty)) {
        print(tempUserId);
        userId = tempUserId;
      } else {
        // String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ID);
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ID, appLang);
        List<LoginJsonTemplate> listUser = await db.execute(Query);
        print("listUser" + listUser.toString());
        if (listUser.length > 0) {
          userId = listUser[0].jsonValue;
        }
      }
      var tempnbfcName = sp.getString(AppConstants.CLIENT_NAME);
      nbfcName = !(tempnbfcName == null || tempnbfcName.isEmpty)
          ? tempnbfcName
          : AppConstants.CLIENT_NAME;
      var tempUserRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      if (!(tempUserRole == null || tempUserRole.isEmpty)) {
        userRole = tempUserRole;
        print("userRole" + userRole);
        // db.search();
      } else {
        // String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ROLE);
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ROLE, appLang);
        List<LoginJsonTemplate> listUserRole = await db.execution(Query);
        if (listUserRole.length > 0) {
          userRole = listUserRole[0].jsonValue;
        }
      }
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
      currentLeadCreationProcessID =
          sp.getString(AppConstants.CURRENT_PRODUCT_LEAD_CREATION_ID);
      currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
      print("End of InitializeAppData ");
    } on Exception catch (_) {
      print("Exception In InitializeAppData");
    }
  }

  handleGenerateOTPResponse(BuildContext context, response) {
    print("at handle response");
    if (response.statusCode == 200) {
      print("at handle response statusCode");
      GetMobileOtpResModel responseModel = getMobileOtpResModelFromJson(response.body);
      print("at handle response statusCode......");
      if (responseModel != null) {
        print("otp sent successfully not null");
        if (responseModel.success == true){
          print("otp sent successfully");
          CustomSnackBar.showSnackBar(context, "OTP has been sent successfully to the Customer.");
          count ++;
          print(count);
          print("count");
          Navigator.pushReplacementNamed(context, "/LeadCreationOTPPageNewTW");
          //getmobileotp(context, responseModel.customerdetail[0].agreementno);

        } else if ((responseModel.success == false || responseModel.success == null)) {
          print("Error occured while generating OTP ");
          CustomSnackBar.showSnackBar(context, "Error occured while generating OTP for Customer");
          // await getmobileotp();
        } else {
          // Dialogs.dismissSimpleDialog(context);
          print("at handle response responseModel.details");
          CustomSnackBar.showSnackBar(context,AppConstants.somethingWentWrong);
        }
      } else {
        print("at handle response AppConstants.somethingWentWrong");
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } else {
      print("at handle response AppConstants.somethingWentWrong..........");
      CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
    }
  }

  verifyOTP(String otp) async {
    String url;
    if (currentProductID != null &&
        !currentProductID.isEmpty &&
        currentLeadCreationProcessID != null &&
        !currentLeadCreationProcessID.isEmpty) {


      String url = APIConstants.REQUEST_CREATE_LEAD + "/" + APIConstants.UPDATE_DATA + "/" + currentProductID + "/" + leadId;

      Map reqJobject = new Map();


      reqJobject["data.Applicant.otp"] = otp;

      var requestJson = json.encode( reqJobject);
     // String requestJson = reqJobject.toString();


      FocusScope.of(context).requestFocus(new FocusNode());
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.getVerifyOTP(url, requestJson);
        if (response != null) {
           handleVerifyOTPResponse(context , response);
        } else {
          CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    }
  }

 void handleVerifyOTPResponse(BuildContext context, response) {
    print("at handleVerifyOTPResponse ");
   if (response.statusCode == 200) {
     GetMobileOtpResModel responseModel = getMobileOtpResModelFromJson(response.body);

     if (responseModel != null) {
       if (responseModel.success == true && responseModel.redirect == true) {
         CustomSnackBar.showSnackBar(
             context, "OTP stored successfully.");

         //getmobileotp(context, responseModel.customerdetail[0].agreementno);

       } else
       if ((responseModel.success == false || responseModel.success == null) &&
           (responseModel.redirect == false ||
               responseModel.redirect == null)) {
         CustomSnackBar.showSnackBar(
             context, "Error occured while generating OTP for Customer");
         // await getmobileotp();
       } else {
         // Dialogs.dismissSimpleDialog(context);
         CustomSnackBar.showSnackBar(context, responseModel.details);
       }
     } else {
       CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
     }
   } else {
     CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
   }
 }

}
