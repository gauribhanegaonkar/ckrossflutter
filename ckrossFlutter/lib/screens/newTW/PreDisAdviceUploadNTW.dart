import 'package:ckrossFlutter/models/statusReportDeatailsModel.dart';

import 'package:ckrossFlutter/providers/pre_dis_advice_upload_provider.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ckrossFlutter/CustomWidgets/custom_expansion_tile.dart'
as custom;

class PreDisAdvUploadNTW extends StatelessWidget {
  static const routeName = "/PreDisAdvUploadNTW";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pre-Disbursement Advice Upload'),
      ),
      body: FutureBuilder(
        future: Provider.of<PreDisAdvUplUTData>(context, listen: false)
            .getReportsFromAPI(context),
        builder: (ctx, snapshot) =>
        snapshot.connectionState == ConnectionState.waiting
            ? Center(
          child: Text("Please Wait.."),
        )
            : Consumer<PreDisAdvUplUTData>(
          child: const Text('No data available'),
          builder: (ctx, data, ch) => data.reportItems.length <= 0
              ? ch
              : new ListView.separated(
            itemCount: data.reportItems.length,
            separatorBuilder:
                (BuildContext context, int index) =>
                Divider(height: 25),
            itemBuilder: (context, i) {
              return new custom.ExpansionTile(
                //iconColor: Colors.white,
                trailing: RaisedButton(
                  color: Colors.white,
                  disabledColor: Colors.white,
                  child: Image.asset(
                    'images/imagesupload.png',
                    height: 30,
                  ),
                  elevation: 0,
                ),
                title: new Text(
                  data.reportItems[i].id.replaceAll(",", "\n"),
                  style: new TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w800,
                      height: 1.8,
                      wordSpacing: 1,
                      color: Colors.black),
                ),
                children: <Widget>[
                  new Column(
                      children: _buildExpandableContent(
                          data.reportItems[i])),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  _buildExpandableContent(NEWTWReport vehicle) {
    List list = [];
    List<Widget> columnContent = [];

    for (String content in vehicle.contents) {
      columnContent.add(
        new ListTile(
          title: new Text(
            content.replaceAll(",", "\n"),
            style: new TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w800,
                height: 1.8,
                wordSpacing: 1,
                color: Colors.blue),
          ),
        ),
      );
    }

    return columnContent;
  }
}
