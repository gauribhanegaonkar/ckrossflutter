import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:convert' as JSON;
import 'dart:io' as Io;
import 'dart:ffi';
import 'dart:typed_data';
import 'package:ckrossFlutter/models/ResponseModels/CreateLeadResModel.dart';
import 'package:ckrossFlutter/providers/LeadCreationProvider.dart';
import 'package:ckrossFlutter/screens/usedtw/LeadCreationDataUTW.dart';
import 'package:ckrossFlutter/webUtils/apiConstants.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:g_json/g_json.dart';

import 'dart:io';
import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/Utils/imageUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/ckross_casedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/loginJson_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ckrossCase.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/models/more_options.dart';
import 'package:ckrossFlutter/screens/LeadCreationData.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';

//import 'newTW/OTPSubmissionPagenewTW.dart';

class AdharUploadImage extends StatelessWidget {
  AdharUploadImage(this.imageName, {Key key}) : super(key: key);
  var imageName;
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: AdharUploadImageStateful(this.imageName));
  }
}

class AdharUploadImageStateful extends StatefulWidget {
  String _selection;
  AdharUploadImageStateful(this.imageName, {Key key}) : super();
  var imageName;
  @override
  AdharUploadImageState createState() => new AdharUploadImageState(this.imageName);
}

class AdharUploadImageState extends State<AdharUploadImageStateful> with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  AdharUploadImageState(this.imageName, {Key key}) : super();
  var imageName;
  var leadId ;
  var extension;
  String fromPange,
      caseID,
      tempCaseID,
      type,
      jsonObject,
      customerName,
      SourceBy;

  BuildContext context;
  String formUploadResponse = "";
  static String userId,
      appLang,
      userRole,
      currentProductID,
      currentProductName,
      currentLeadCreationProcessID,
      nbfcName;

  static var imageList;
  static DatabaseHelper db = DatabaseHelper();
  var imageListNew = new List<Map<String, String>>();
  var subImages;
  List<MoreOptions> choiceList = [];
  Map<int, dynamic> productNames = new Map<int, dynamic>();
  List<String> arr = [];
  List<String> imgs;
  SharedPreferences sp;
  static var userName;
  List<String> PdfList = new List<String>();
  LeadCreationImagePage(
      String fromPage,
      String caseID,
      String tempCaseID,
      String type,
      bool isInitialize,
      Object jsonObject,
      String customerName,
      String sourceBy) {
    try {
      this.fromPange = fromPage;
      this.caseID = caseID;
      this.tempCaseID = tempCaseID;
      this.type = type;
      this.jsonObject = jsonObject;
      this.customerName = customerName;
      this.SourceBy = sourceBy;
    } catch (e) {
      // e.GetBaseException();
      print("Exception In UploadImagePageNew");
    }
  }

  @override
  void initState() {
    GetListOfImages();
    AddToolbarMenusDynamically();
    setLeadId();
    super.initState();
  }

  void dispose() {
    super.dispose();
  }

  setLeadId() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    leadId = sp.getString('LEADID');
    print(leadId);
    print("setLeadId()");
  }

  @override
  Widget build(BuildContext context) {
    String newTW = 'New TW';

    // TODO: implement build
    return WillPopScope(
        onWillPop: () async {
          sp = await SharedPreferences.getInstance();
          final dir = Directory('/storage/emulated/0/CKross_Wheels_EMI/Images');
          if (dir.existsSync()) {
            dir.deleteSync(recursive: true);
          }
          if (sp.getString(AppConstants.CURRENT_PRODUCT_ID) == "1") {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LeadCreationData()),
            );
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LeadCreationDataUTW()),
            );
          }
        },
        child: Scaffold(
          appBar: new AppBar(
            backgroundColor: Colors.blue,
            title: new Text('CKROSS LOS'),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
                child: Text(newTW),
              ),
              Theme(
                  data: Theme.of(context).copyWith(
                    cardColor: Colors.black,
                    iconTheme: IconThemeData(color: Colors.white),
                  ),
                  child: PopupMenuButton<MoreOptions>(
                    onSelected: choiceAction,
                    itemBuilder: (BuildContext context) {
                      return choiceList.skip(0).map((MoreOptions choice) {
                        return PopupMenuItem<MoreOptions>(
                          value: choice,
                          child: Text(choice.name,
                              style: TextStyle(color: Colors.white)),
                        );
                      }).toList();
                    },
                  )),
            ],
          ),
          body: ShowListOfImages(context),
        ));
    ;
  }

  static InitializeAppData() async {
    try {
      print("InitializeAppData method");
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      appLang = !(tempLanguage == null || tempLanguage.isEmpty)
          ? tempLanguage
          : AppConstants.APP_LANG;
      var tempUserId = sp.getString(AppConstants.LOGGED_IN_USER_ID);

      if (!(tempUserId == null || tempUserId.isEmpty)) {
        print(tempUserId);
        userId = tempUserId;
      } else {
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ID, appLang);
        List<LoginJsonTemplate> listUser = await db.execute(Query);
        if (listUser.length > 0) {
          userId = listUser[0].jsonValue;
        }
      }
      var tempnbfcName = sp.getString(AppConstants.CLIENT_NAME);
      nbfcName = !(tempnbfcName == null || tempnbfcName.isEmpty)
          ? tempnbfcName
          : AppConstants.CLIENT_NAME;
      var tempUserRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      if (!(tempUserRole == null || tempUserRole.isEmpty)) {
        userRole = tempUserRole;
        print("userRole....." + userRole);
        // db.search();
      } else {
        print("else....");
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ROLE, appLang);
        print("Query");
        print(Query);
        List<LoginJsonTemplate> listUserRole = await db.execution(Query);
        if (listUserRole.length > 0) {
          userRole = listUserRole[0].jsonValue;
        }
      }
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
      currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
      currentLeadCreationProcessID = sp.getString(AppConstants.CURRENT_PRODUCT_LEAD_CREATION_ID);
      var tempUserName = sp.getString(AppConstants.LOGGED_IN_USER_NAME);
      if (!(tempUserName == null || tempUserName.isEmpty)) {
        userName = tempUserName;
      } else {
        print("else......................");
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_NAME, appLang);
        List listUserName = await ckrossCaseDB.executeQuery(Query);
        print("listUserName");
        print(listUserName);
        if (listUserName.length > 0) {
          var map = <String, dynamic>{};
          map.addAll(listUserName[0]);
          userName = map["jsonValue"];
          print(userName);
          print("userName");
        }
      }
      FileUtils.printLog("End of InitializeAppData ");
    } on Exception catch (_) {
      FileUtils.printLog("Exception In InitializeAppData");
    }
  }

  GetListOfImages() async {
    try {
      InitializeAppData();
      print("end of InitializeAppData()");
      String imageType = "Images";
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      appLang = !(tempLanguage == null || tempLanguage.isEmpty)
          ? tempLanguage
          : AppConstants.APP_LANG;
      String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(imageType, appLang);
      print("query");
      print(Query);
      List<Map<String, dynamic>> listLoginJson = await LoginJsonTemplateDB.executeLoginJsonTemplateImageData(Query);
      print("listLoginJson");
      print(listLoginJson);
      if (listLoginJson.length > 0) {
        var imagesJson = listLoginJson[0]['jsonValue'];
        print("imagesJson");
        if (imagesJson != null && !imagesJson.isEmpty) {
          try {
            String value = imagesJson.toString();
            value = value.toString().substring(1, value.length - 1); //remove curly brackets
            var keyValuePairs =
            value.split(","); //split the string to creat key-value pairs
            Map<String, String> list = new Map();

            for (int i = 0; i < keyValuePairs.length; i++) //iterate over the pairs
                {
              print("keyValuePairs");
              print(keyValuePairs[i]);
              var entry = keyValuePairs[i].split(":"); //split the pairs to get key and value
              list.putIfAbsent(entry[0].trim(), () => entry[1].trim()); //add them to the hashmap and trim whitespaces
            }

            List<String> keys = list.keys.toList();
            var imageList = new List<Map<String, String>>();
            List<Map<String, String>> arrayImages =
            new List<Map<String, String>>(list.length);

            for (int j = 0; j < keys.length; j++) {
              String keyValue = keys[j];
              var map = <String, dynamic>{};
              map.addAll(list);
              SharedPreferences sp = await SharedPreferences.getInstance();
              String valueString;
              if(imageName != null){
                valueString = imageName;
                sp.setString('valueString', valueString);
              }else{
                String value = sp.getString('valueString');
                valueString = value;
              }
              print("valueString");
              print(valueString);
              Map<String, String> dictImages = new Map<String, String>();
              print("keyValue");
              print(keyValue);
              if (keyValue.contains("__") && keyValue.contains('__ApplicantPANCard')) {
                var arrKeys = keyValue.split("__");
                int seq_index = int.parse(arrKeys[0]);
                int pos = seq_index - 1;

                dictImages.putIfAbsent('imageName', () => valueString);
                dictImages.putIfAbsent('imageName', () => valueString);


                dictImages.putIfAbsent('imageKey', () => keyValue);
                arrayImages[pos] = dictImages;
              } else {
                print("......");
              }
            }

            for (int i = 0; i < arrayImages.length; i++) {
              if (arrayImages[i] != null) {
                imageList.add(arrayImages[i]);
              }
              setState(() {
                imageListNew = imageList;
              });
            }
            //return ShowListOfImages(imageList,context);
          } catch (e) {
            FileUtils.printLog(
                "Exception occured In GetListOfImages" + e.GetBaseException());
          }
        } else {
          FileUtils.printLog(
              "In GetListOfImages, lead documents list is blank.");
          await DisplayAlert(AppConstants.ALERT,
              "lead documents list is blank.", AppConstants.OK, context);
        }
      } else {
        FileUtils.printLog("In GetListOfImages, lead documents list is blank.");
        await DisplayAlert(AppConstants.ALERT, "lead documents list is blank.",
            AppConstants.OK, context);
      }
    } catch (e) {
      // e.GetBaseException();
      FileUtils.printLog("Exception occured In GetListOfImages" + e.toString());
    }
  }

  Widget ShowListOfImages(BuildContext context) {
    String fileName;
    List<String> created;
    return Column(
        children: <Widget>[
    Expanded(
    child: SingleChildScrollView(
        child: ListView.builder(
        shrinkWrap: true, // 1st add
        physics: ClampingScrollPhysics(),
        padding: const EdgeInsets.all(8),
        itemCount: imageListNew.length,
        itemBuilder: (context, index) {
          fileName =
              imageListNew[index]['imageName'].replaceAll(" ", "");
          fileName = fileName.replaceAll("/", "_");
          return Card(
            child: Column(
              children: <Widget>[
                Container(
                  height: 70,
                  child: Row(
                    children: <Widget>[
                    Icon(
                    Icons.camera_alt,
                    size: 50,
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Text(
                      '${imageListNew[index]['imageName']}',
                      style: TextStyle(fontSize: 13),
                    ),
                  ),
                  SizedBox(width: 10),
                  IconButton(
                    icon: Icon(Icons.add_circle_outline),
                    color: Colors.cyan,
                    onPressed: () => {
                    showDialog(
                    context: context,
                    builder: (context) {
                      return Dialog(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(10)),
                        elevation: 10,
                        child: Container(
                          height: 100,
                          width: 180,
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                    child: Text(
                                        'Select from gallary'),
                                    onPressed: () async {
                                      bool result = await ImageUtils
                                          .getAndSaveImageFromGallary(
                                          index,
                                          imageListNew[
                                          index][
                                          'imageName']);
                                      if (result) {
                                        Navigator.pop(
                                            context, true);
                                        Navigator
                                            .pushReplacementNamed(
                                            context,
                                            '/PANUploadImage');
                                        print("done");
                                      }
                                    }),
                              ),
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                    child: Text(
                                        'Capture from camera'),
                                    onPressed: () async {
                                      bool result = await ImageUtils
                                          .getAndSaveImageFromCamera(
                                          index,
                                          imageListNew[
                                          index][
                                          'imageName']);
                                      if (result) {
                                        Navigator.pop(
                                            context, true);
                                        Navigator
                                            .pushReplacementNamed(
                                            context,
                                            '/PANUploadImage');
                                        print("done");
                                      }
                                    }),
                              ),
                            ],
                          ),
                        ),
                      );

                      //   ImageUtils.getAndSaveImage(),
                    },
                  ),
                  },
                ),
              ],
            ),
          ),
          new innerOneWidget(subDirImages: getImages(fileName)),
          ],
          ),
          );
        }),
    ),
    ),
    Align(
    alignment: FractionalOffset.bottomCenter,
    child: MaterialButton(
    minWidth: double.infinity,
    child: Text('Scan Document'),
    color: Colors.cyan,
    onPressed: () async => {

    for (int i = 0; i < imageListNew.length; i++)
    {
    created = await createPDF(imageListNew[i]['imageName'], context),
    },
    print("arr"),
    print(created),

    HandleClick(created,context)

    },
    ),
    )
    ],
    );
  }

  Future<void> DisplayAlert(
      String alert, String s, String ok, BuildContext context) async {
    // flutter defined function
    bool result = false;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  HandleClick(created,BuildContext context) async {
    print("in HandleClick");
    sp = await SharedPreferences.getInstance();
    print(created);
    FileUtils.printLog("In handleclick");
    try {
      String doc = "";
      String value = sp.getString('valueString');
      if(created.length == 2){
      if(value == ' 02-PAN'){
        doc = value.trim();
        await UploadToServer(context,created,doc);
      }else{
        doc = value;
        await UploadToServer(context,created,doc);
      }
      }else{
        CustomSnackBar.showSnackBar(context,"Please Upload 2 Images for " + doc);
      }

    } on Exception catch (e) {
      FileUtils.printLog("Exception in handel click" + e.toString());
    }
  }

  UploadToServer(BuildContext context,created,doc) async {
    try {
      print("In UploadServer");
      FileUtils.printLog("In UploadServer");

      // http://localhost:8080/LMS/ocrUpload/{productID}/{LEADID}/{docType}/{fileExtension}

      // http://115.124.101.73:8080/LMS/ocrUpload/1/837796/191/02-PAN/jpg
      String url = 'http://115.124.101.73:8080/LMS/ocrUpload'+
          "/" +
          currentProductID +
          "/" +
          leadId +
          "/" + '191' + "/" +
          doc + "/"
          + extension;

      print(url);
      await createPANAPI(context,url,created);

    } catch (e2) {
      FileUtils.printLog("Exception In UploadCaseToServer" + e2.toString());
    }
  }

  createPANAPI(BuildContext context, url,created) async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
    for(int i = 0; i < created.length; i++ ){
      Map reqData = new Map();
      reqData['image_base64'] = created[i];
      var reqJSON = json.encode(reqData);
      print("reqJSON");
      print(reqJSON);
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.adharUpload(reqJSON, url);
        try {
          if (response.statusCode == 200) {
            if ((response != null) && (response != "null") &&
                (response != "{}")) {
              try {
                var jsonObject = JSON.jsonDecode(response.body);
                Map data = jsonObject;
                if (data.containsKey('success') == true) {

                  String res__DocumentNumber = data['res__DocumentNumber'].toString()!= null ? data['res__DocumentNumber'].toString() : "";
                  String pincode = data['pincode']!= null ? data['pincode'].toString() : "";;
                  String HouseNumber = data["HouseNumber"]!= null ? data['HouseNumber'].toString() : "";;
                 // String app_Plain__DateOfBirth = data['app_Plain__DateOfBirth'].toString()!= null ? data['app_Plain__DateOfBirth'].toString() : "";;
                  sp = await SharedPreferences.getInstance();
                  String dataInfo;

                  sp.setString(AppConstants.res__DocumentNumber,res__DocumentNumber);
                  sp.setString(AppConstants.pincode, pincode);
                  sp.setString(AppConstants.HouseNumber,HouseNumber);
                //  sp.setString(AppConstants.app_Plain__DateOfBirth,app_Plain__DateOfBirth);


                  Timer(Duration(seconds: 10), () {
                    dataInfo = "Data Retrived : \n" + res__DocumentNumber + "\n"
                        + pincode + "\n"
                        + HouseNumber ;
                    showDialog(
                      context: context,
                      // barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Ckrossmmmmmmm'),
                          content: new Text(dataInfo),
                          actions: <Widget>[
                            FlatButton(
                              child: new Text('Ok'),
                              onPressed: () {
                                // Navigator.pushReplacementNamed(context, '/statusReport');
                                Navigator.pushReplacementNamed(
                                    context, '/LeadCreationDataSeqid2');
                              },
                            ),
                          ],
                        );
                      },
                    );
                  } );


                }




              }
              on Exception catch (e) {
                print("Exception 1 in cibicheck : " + e.toString());
              }
            }
          } else if (response.statusCode == HttpStatus.forbidden ||
              response.statusCode.toString() == "800") {
            FileUtils.printLog("Response is : " + response.statusCode.toString());
            CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          } else if (response.statusCode == HttpStatus.unauthorized) {
            FileUtils.printLog("Response is : " + response.statusCode.toString());
            CustomSnackBar.showSnackBar(
                context, AppConstants.TOKEN_EXPIRED_RELOGIN);
          } // // end of statuscode
          else {
            CustomSnackBar.showSnackBar(context, AppConstants.NO_RESPONSE);
            // await showdialog(AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
          }
        } on Exception catch (e) {
          print("Exception 1 in getLeadId : " + e.toString());
        }
      }
    }

  }

  deleteIfAlreadyExist(String case_id) async {
    print("In deleteIfAlreadyExist");
    try {
      await InitializeAppData();
      await ImageUtils.deleteFolders();
    } catch (e) {
      FileUtils.printLog("Exception In deleteIfAlreadyExist " + e.toString());
    }
  }

  UploadPdfFiles(pdfFiles) async {
    print("In UploadPdfFiles");
    FileUtils.printLog("In UploadPdfFiles");
    Uint32List imageUploaded;
    try {
      if (pdfFiles.length > 0) {
        imageUploaded = new Uint32List(pdfFiles.length);
        int uploadCnt = 0;
        String uploadIsCompleted = "false";
        for (int j = 0; j < pdfFiles.length; j++) {
          var fileNameArray = pdfFiles[j].split('/');
          var fileNamewithextension = fileNameArray[fileNameArray.length - 1];
          var tempfileName = fileNamewithextension.split('.');
          var fileName = tempfileName[0];
          var extension = tempfileName[1].toString().replaceFirst('.', "");

          String url = "";
          String query = "selectedImageType=" +
              fileName +
              "&LeadID=" +
              tempCaseID +
              "&extension=" +
              extension;
          url = APIConstants.REQUEST_UPLOAD_PDF +
              "/" +
              currentProductID +
              "?" +
              query;
          print("parameters");
          print(pdfFiles[j]);
          print(url);
          print(fileName);
          var responseData = await APIServices.UploadPdfFile(pdfFiles[j], url, fileName);
          print("responseData");
          print(responseData);
          print(responseData.statusCode);
          print("responseData end");
          if (responseData != null) {
//            var map = <String, dynamic>{};
//            map.addAll(responseData);
//            String isSuccess = map["success"].toString();
//            print("isSuccess");
//            print(isSuccess);
            if (responseData.statusCode == 200) {
              print(" imageUploaded[j] = 1");
              imageUploaded[j] = 1;
            } else {
              print(" imageUploaded[j] = 2");
              imageUploaded[j] = 2;
              uploadCnt--;
            }
          } else {
            print(" else imageUploaded[j] = 2");
            imageUploaded[j] = 2;
            uploadCnt--;
          }
        }
      }
    } catch (e) {
      FileUtils.printLog("Exception In UploadPdfFiles " + e.toString());
    }
    print("end of UploadPdfFiles");
    return imageUploaded;
  }

  CreateLeadRequestJson(List<String> PdfFileList) async {
    print("In CreateLeadRequestJson");
    var map4 = null;
    var jsonObject = null;
    Map<String, dynamic> data;
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      String Query = DatabaseQueries.GetSingleCase(sp.getString("Case_Id"),
          sp.getString(AppConstants.LOGGED_IN_USER_ID));
      await ckrossCaseDB.search();
      List ckrossSingleCase = await ckrossCaseDB.executeQuery(Query);
      if (ckrossSingleCase.length > 0) {
        var map = <String, dynamic>{};
        map.addAll(ckrossSingleCase[0]);
        var appjson = map["applicant_json"];
        var addQuotes = map["applicant_json"]
            .replaceFirst("Applicant", '"' + "Applicant" + '"');
        data = json.decode(addQuotes);
        if (data.containsKey("LEADID")) {
          data.remove("LEADID");
        }
        if (data.containsKey("input_source")) {
          data.remove("input_source");
          data["input_source"] = "M";
        } else {
          data["input_source"] = "M";
        }

        print("pdflisttt");
        print(PdfList);
        if (PdfList.length > 0) {
          data["documentsUpload"] = "true";
        } else {
          data["documentsUpload"] = "false";
        }
      }
    } catch (e) {
      print("Exception In CreateLeadRequestJson " + e.toString());
    }
    return json.encode(data);
  }

  UpdateImagesCntIncaseTable() async {
    try {
      print("In UpdateImagesCntIncaseTable");
      FileUtils.printLog("In UpdateImagesCntIncaseTable");
      SharedPreferences sp = await SharedPreferences.getInstance();

      String Query = DatabaseQueries.GetSingleCase(sp.getString("Case_Id"),
          sp.getString(AppConstants.LOGGED_IN_USER_ID));
      List ckrossSingleCase = await ckrossCaseDB.executeQuery(Query);

      if (ckrossSingleCase.length > 0) {
        var caseData = <String, dynamic>{};
        caseData.addAll(ckrossSingleCase[0]);
        caseData["imageLstFrmServerCnt"] = imageList.length;
        caseData["is_submit"] = "true";

        String UpdateCaseQuery = DatabaseQueries.UpdateCase(caseData);
        await ckrossCaseDB.executeQuery(UpdateCaseQuery);
        await ckrossCaseDB.search();
      }
    } catch (e) {
      FileUtils.printLog(
          "Exception in UpdateImagesCntIncaseTable " + e.toString());
    }
  }

  UpdateSaveCaseInDb(String caseType, String caseID) async {
    try {
      print("In UpdateSaveCaseInDb");
      FileUtils.printLog("In UpdateSaveCaseInDb ");

      SharedPreferences sp = await SharedPreferences.getInstance();

      String Query = DatabaseQueries.GetCaseDependsOnTypeAndCaseID(
          AppConstants.CREATE_LEAD, sp.getString("Case_Id"));
      print("Query");
      print(Query);
      CkrossCase aCase = new CkrossCase();
      aCase.caseId = sp.getString("Case_Id");
      aCase.tempCaseId = sp.getString("Case_Id");
      aCase.caseType = AppConstants.CREATE_LEAD;
      aCase.userId = sp.getString(AppConstants.LOGGED_IN_USER_ID);
      aCase.productId = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
      aCase.isSubmit = "true";

      List ListCount = await ckrossCaseDB.executeQuery(Query);

      if (ListCount[0].length > 0) {
        String QueryUpdate =
        DatabaseQueries.UpdateSaveCaseDependsOnTypeAndCaseID(aCase);
        List a = await ckrossCaseDB.executeQuery(Query);
      }
    } on Exception catch (e) {
      FileUtils.printLog("Exception In UpdateSaveCaseInDb() : " + e.toString());
    }
  }

  UpdateCaseCompleteStatus(BuildContext context,Uint32List imageUploaded, String submitRequestJson) async {
    print("In UpdateCaseCompleteStatus");
    FileUtils.printLog("In UpdateCaseCompleteStatus");
    try {
      int alluploadedCnt = 0;
      String url = "";
      print("imageUploaded");
      print(imageUploaded);
      if (imageUploaded != null) {
        for (int i = 0; i < imageUploaded.length; i++) {
          if (imageUploaded[i] == 1) {
            alluploadedCnt ++;
          } else if (imageUploaded[i] == 2) {
            break;
          }
        }
        print(" In post doUploadPDFEork " + ": Total pdf uploaded / outof: " + alluploadedCnt.toString() + "/" + imageUploaded.length.toString());

        FileUtils.printLog(" In post doUploadPDFEork " + ": Total pdf uploaded / outof: " + alluploadedCnt.toString() + "/" + imageUploaded.length.toString());

        if (alluploadedCnt == imageUploaded.length) {
          //  await InitializeAppData();
          print("alluploadedCnt");
          SharedPreferences sp = await SharedPreferences.getInstance();
          if (sp
              .getString(AppConstants.LOGGED_IN_USER_ROLE)
              .toLowerCase()
              .startsWith("SO".toLowerCase())) {
            url = APIConstants.REQUEST_UPDATE_LEAD +
                "/" +
                currentProductID +
                "/" +
                APIConstants.REQUEST_UPDATE_STATUS +
                "/" +
                APIConstants.UPLOAD +
                "/" +
                tempCaseID +
                "/" +
                currentLeadCreationProcessID;
          } else if (sp
              .getString(AppConstants.LOGGED_IN_USER_ROLE)
              .toLowerCase()
              .startsWith("VD".toLowerCase())) {
            url = APIConstants.REQUEST_UPDATE_LEAD +
                "/" +
                currentProductID +
                "/" +
                APIConstants.REQUEST_UPDATE_STATUS +
                "/" +
                APIConstants.UPLOAD +
                "/" +
                tempCaseID +
                "/" +
                currentLeadCreationProcessID;
          } else {
            url = APIConstants.REQUEST_UPDATE_LEAD +
                "/" +
                currentProductID +
                "/" +
                APIConstants.REQUEST_UPDATE_STATUS +
                "/" +
                APIConstants.UPLOAD +
                "/" +
                tempCaseID +
                "/" +
                currentLeadCreationProcessID;
          }

          print("URL : " + url);
          print("Request : " + submitRequestJson);
          FileUtils.printLog("URL : " + url);
          FileUtils.printLog("Request : " + submitRequestJson);
          var response = await APIServices.UpdateCompleteStatus(url);
          print("response UpdateCaseCompleteStatus");
          print(response);
          await HandleCompleteStatusResponse(context,response, submitRequestJson);
        } else {
          await Dialogs.showAlertDialog(
              context,
              "Unable to connect to server. \nForm is saved in local database now. \nYou can upload it through pending upload menu !",
              "Alert");
        }
      } else {
        await Dialogs.showAlertDialog(
            context,
            "Unable to connect to server......... \nForm is saved in local database now. \nYou can upload it through pending upload menu !",
            "Alert");
      }
    } catch (e) {
      FileUtils.printLog(
          "Exception In UpdateCaseCompleteStatus " + e.GetBaseException());
    }
  }

  HandleCompleteStatusResponse(BuildContext context,
      var response, String submitRequestJson) async {
    print("In HandleCompleteStatusResponse");
    FileUtils.printLog("In HandleCompleteStatusResponse");
    FileUtils.printLog("Response : " + response);
    print("Response : " + response);
    Map<String, dynamic> PObject;
    sp = await SharedPreferences.getInstance();
    try {
      if (response != null) {
        var jsonObject = JSON.jsonDecode(response);
        PObject = jsonObject;
        print('PObject.........');
        print(PObject);
        if ( PObject["success"] == true) {
          print("i am here response");
          print(sp.getString("Case_Id"));
          print(userId);
          String Query = DatabaseQueries.GetSingleCase(sp.getString("Case_Id"), userId);
          print("Query........");
          print(Query);
          List ckrossSingleCase = await ckrossCaseDB.executeQuery(Query);
          print("ckrossSingleCase");
          print(ckrossSingleCase);
          var map = <String, dynamic>{};
          map.addAll(ckrossSingleCase[0]);
          print("map");
          print(map);
          String applicant_json = map["applicant_json"].toString();
          print("applicant_json");
          print(applicant_json);
          String UpdateCaseQuery = DatabaseQueries.UpdateCase(map);
          print("UpdateCaseQuery");
          print(UpdateCaseQuery);
          await ckrossCaseDB.executeQuery(UpdateCaseQuery);
          //End of update status
          print("deleteIfAlreadyExist");
          await deleteIfAlreadyExist(sp.getString("Case_Id"));
          //remove this record from local database
          String delRecord = DatabaseQueries.DeleteCaseDependsOnId(sp.getString("Case_Id"), userId);
          print("delRecord");
          print(delRecord);
          await ckrossCaseDB.executeQuery(delRecord);
          print("end delete");
          await Dialogs.showdialog(context, formUploadResponse, "Alert",submitRequestJson);
          print("**********Jump to otp verification screen********");


        } else {
          await Dialogs.showAlertDialog(
              context,
              "Unable to connect to server. \nForm is saved in local database now. \nYou can upload it through pending upload menu !",
              "Alert");
        }
      } else {
        await Dialogs.showAlertDialog(
            context,
            "No response from server. This can be due to low/bad internet connection or request timeout",
            "Alert");
      }
    } catch (e) {
      print("Exception In HandleCompleteStatusResponse " + e.toString());
    }
  }

  List<String> getImages(String imageDirName) {
    // print("~~~~~~~~~~~");
    final String path =
        '/storage/emulated/0/CKross_Wheels_EMI/Images/$imageDirName';
    //  print(path);
    final myDir = new Directory(path);
    subImages = new List<String>();
    if (myDir.existsSync()) {
      //subImages = new List<String>();
      var images;
      images = myDir
          .listSync()
          .map((item) => item.path)
          .where((item) => item.endsWith(".jpg"))
          .toList(growable: false);
      ;

      subImages = images;
    } else {
      subImages.clear();
    }
    return subImages;
  }

  choiceAction(MoreOptions choice) async {
    print("choice");
    switch (choice.name) {
      case 'New TW':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);

        break;
      case 'Used TW':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);

        break;
      case 'Send Log':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);
        attachFileAndSendMail();
        break;
      case 'Log Out':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);

        break;
    }
  }

  void attachFileAndSendMail() async {
    List<File> file = await FileUtils.getFile();
    List<String> arr = [];
    for (int i = 0; i < file.length; i++) {
      print(file[i]);
      arr.add(file[i].path);
    }
    print(arr);
    Email email;

    email = Email(
      //   body: 'Email body',
      subject: 'Log File',
      //   recipients: ['example@example.com'],
      //   cc: ['cc@example.com'],
      //   bcc: ['bcc@example.com'],
      attachmentPaths: arr,

      isHTML: false,
    );

    await FlutterEmailSender.send(email);
  }

  AddToolbarMenusDynamically() async {
    FileUtils.printLog("Add tool bar");
    productNames = await GetProductNamesFromDb();
    try {
      setState(() {
        if (productNames.length > 0) {
          productNames.forEach((k, v) => choiceList.add(MoreOptions(k, v)));
          choiceList.add(new MoreOptions(2, "Send Log"));
          choiceList.add(new MoreOptions(3, "Log Out"));
        }
      });
    } on Exception catch (e) {
      FileUtils.printLog("Exception in AddToolbarMenusDynamically() $e ");
    }
  }

  Future GetProductNamesFromDb() async {
    Map<String, dynamic> productNames = new Map<String, dynamic>();

    String Query =
    DatabaseQueries.CreateGetLoginJsontemplateMenuJson("Menu Details");

    List<Map<String, dynamic>> ListMenu = await DatabaseHelper().execute(Query);

    if (ListMenu.length > 0) {
      String MenuDetailsJson = ListMenu[0]["jsonValue"];
      Map<String, dynamic> jsonObject = json.decode(MenuDetailsJson);
      List jArray = jsonObject['GroupToCategoryToSubCategory'];

      if (jArray != null && jArray.length != 0) {
        for (int i = 0; i < jArray.length; i++) {
          Map<String, dynamic> productJson = jArray[i];
          if (productJson["categoryName"].toString().contains("Product")) {
            List productArray = productJson["SubCategory"];
            for (int j = 0; j < productArray.length; j++) {
              Map<String, dynamic> productJson1 = productArray[j];

              String productID = productJson1["id"].toString();
              String productName = productJson1["name"];
              productNames.addAll({productID: productName});
            }
          }
        }
      }
    }
    return productNames;
  }

  createPDF(String folderName, BuildContext context) async {
    String base64Image;
    print("create pdf");
    String modifiedFolderName = folderName.replaceAll(" ", "");
    modifiedFolderName = modifiedFolderName.replaceAll("/", "_");

    //Create a PDF document.
    final String directory = '/storage/emulated/0/CKross_Wheels_EMI/Images/$modifiedFolderName';
    final Directory _appDocDirFolder = Directory('${directory}');
    if (await _appDocDirFolder.exists()) {
      imgs = new List();
      _getImages(_appDocDirFolder);
      if (imgs.length > 0) {
        final pdf = pw.Document();
        for (int i = 0; i < imgs.length; i++) {
          var fileNameArray = imgs[i].split('/');
          var fileNamewithextension = fileNameArray[fileNameArray.length - 1];
          var tempfileName = fileNamewithextension.split('.');
          var fileName = tempfileName[0];
          print("fileName");
          print(fileName);
          extension = tempfileName[1].toString().replaceFirst('.', "");
          print("extension");
          print(extension);


          var pieces = imgs[i].split("/");
          print("imgs[i]");
          print(imgs[i]);
//          List<int> imageBytes = File(imgs[i]).readAsBytesSync();
//          print(imageBytes);
//          base64Image = base64Encode(imageBytes);
          List<int> imageBytes = Io.File(imgs[i]).readAsBytesSync();
          print(imageBytes);
          base64Image = base64Encode(imageBytes);
          print("base64Image");
          print(base64Image);
          arr.add(base64Image);
        }


      }
    }
    return arr;
  }

  Future<void> _getImages(Directory appDocDirFolder) async {
    print("In get image............");
    print(appDocDirFolder);
    imageList = appDocDirFolder
        .listSync()
        .map((item) => item.path)
        .toList(growable: false);

    setState(() {
      imgs = imageList;
      FileUtils.printLog("Get all images from directory.");
    });
  }
}

class innerOneWidget extends StatelessWidget {
  innerOneWidget({
    Key key,
    @required this.subDirImages,
  }) : super(key: key);

  final List<String> subDirImages;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 150,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: subDirImages.length,
          itemBuilder: (context, index) => Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(1),
                  child: Image.file(
                    File(subDirImages[index]),
                    height: 100,
                    width: 80,
                    fit: BoxFit.fill,
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  color: Colors.red,
                  iconSize: 20,
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Delete image"),
                          content:
                          new Text("Are you sure to delete image?"),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("Cancel"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            new FlatButton(
                              child: new Text("Ok"),
                              onPressed: () {

                                print("path....");
                                print(subDirImages);
                                print(File(subDirImages[index]).path);
                                final dir = Directory(File(subDirImages[index]).path);
                                print(dir);
                                dir.deleteSync(recursive: true);
                                FileUtils.printLog(
                                    "Image saved to directory.");
                                Navigator.pop(context, true);
                                Navigator.pushReplacementNamed(
                                    context, '/PANUploadImage');
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          )),
    );

  }
  showPanInfo(BuildContext context, String text, String text1) {

  }

}
