import 'dart:async';

import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/models/statusReportDeatailsModel.dart';
import 'package:ckrossFlutter/webUtils/apiConstants.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as JSON;
import 'package:ckrossFlutter/CustomWidgets/custom_expansion_tile.dart'
as custom;


class UploadPendingNTW extends StatefulWidget {
  //StatusRepoetListView(List<String> datesSelected) : super();

  @override
  UploadPendingNTWState createState() => UploadPendingNTWState();
}

class UploadPendingNTWState extends State<UploadPendingNTW>{
  List<NEWTWReport> _listViewData = [];
  String currentProductID;
  SharedPreferences sp;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Upload Pending'),
      ),
      body: new ListView.separated(
        itemCount: _listViewData.length,
        separatorBuilder: (BuildContext context, int index) => Divider(height: 25),

        itemBuilder: (context, i) {
          return new custom.ExpansionTile(
            //iconColor: Colors.white,
            trailing: RaisedButton(
              color: Colors.white,
              disabledColor: Colors.white,
              child:  Image.asset('images/imagesupload.png', height: 30, ),
              elevation:0,
//                  onPressed: () => {
//                     Navigator.pushReplacementNamed(context, '/statusReportData'),
//
//            },
            ),
            title: new Text(_listViewData[i].id.replaceAll(",", "\n"),
              style: new TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w800,
                  height: 1.8,
                  wordSpacing: 1,
                  color: Colors.black),
            ),
            children: <Widget>[
              new Column(children: _buildExpandableContent(_listViewData[i])),
            ],
          );
        },
      ),
    );

  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // CreateExpandableReportUI();
  }

  @override
  void initState() {
    super.initState();

    setCurrentIdData();
  }

  setCurrentIdData() async {

    try {
      sp = await SharedPreferences.getInstance();
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
    } on Exception catch (e) {
      print("Exception In InitializeAppData " + e.toString());
    }

    await GetUploadData();
  }

  CreateExpandableReportUI(List reportHeadersArray,
      List<List<String>> reportDataArray, List reportHeadersUIArray) async {
    print(reportHeadersArray);
    print(reportDataArray);
    print(reportHeadersUIArray);
    Map<String, String> z = new Map<String, String>();
    List<String> mapData = new List<String>();
    try{
      setState(() {
        for (var i = 0; i < reportDataArray.length; i++) {
          var dataList = reportDataArray[i];

          for (var j = 0; j < dataList.length; j++) {
            print("datalist" + dataList.toString());
            for (var k = 0; k < reportHeadersArray.length; k++) {
              z[reportHeadersArray[k]] = dataList[j];
              j++;
            }
          }
          print("z..." + z.toString());
          mapData.add(z.toString());
          print("map data");
          print(mapData);
        }

        //  [{Lead ID: 782559, Customer First Name: Rizwan, Customer Last Name: ., Lead Stage: Conditional DO, Status: Processed, Edit: null, Source By: 18-WEMI, Dealer: VIPIN VAIH, Sales Officer: Abhishek Shrivastav, Last Update Time: 2020-09-14 16:15:13}, {Lead ID: 782765, Customer First Name: Brijendra Kumar, Customer Last Name: ., Lead Stage: Conditional DO, Status: Processed, Edit: null, Source By: 18-WEMI, Dealer: VIPIN VAIH, Sales Officer: Abhishek Shrivastav, Last Update Time: 2020-09-15 18:00:38}]

        mapData.forEach((k) => _listViewData.add(NEWTWReport(
            k.substring(k.indexOf("{"), k.indexOf("Edit")).replaceAll("{", " "),
            [
              k.substring(k.indexOf(" Source"), k.indexOf("}"))
            ])));

        // Dialogs.dismissSimpleDialog(context);
      });

    }on Exception catch(e){
      print("Exception in CreateExpandableReportUI " +e.toString());
    }

  }

  GetUploadData() async{
    print("start getStatusreport data");

    try {
      //Dialogs.showSimpleDialog(context, "Please Wait");
      String url = 'reports' + "/" + currentProductID  + "/" + 'Pending';
      print("requestJson");
      print(url);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      // Dialogs.showSimpleDialog(context, "Please Wait");
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response =
        await APIServices.GetReports(url);
        print("pending response");
        print(response.body);
        if ((response != null) && (response != "null") &&  (response != "{}")) {
          print("response of date filter");
          print(response.body);
          await handleGetReport(context, response);
        } else {
          //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
          await showdialog(
              AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
      //main if

    } on Exception catch (e) {
      print("Exception in GetStatusReportData " + e.toString());
    }

  }

  Future handleGetReport(BuildContext context, Response response) async {
    try {
      if (response.statusCode == 200) {
        var results = JSON.jsonDecode(response.body);
        if (results != null) {
          print("result");
          print(results);
          // JObject results = JObject.Parse(responseContent);
          List reportHeadersArray = results["reportHeaders"];
          print(reportHeadersArray);

          int editIndex = -1;
          for (int i = 0; i < reportHeadersArray.length; i++) {
            if (reportHeadersArray[i] == "Edit".toLowerCase()) {
              editIndex = i;
            }
          }
          if (editIndex > 0) {
            reportHeadersArray.remove(editIndex);
          }
          List reportDataArray = results["reportData"];
          List reportHeadersUIArray = results["reportHeadersUI"];
          print("arrays");
          print(reportDataArray);
          print(reportHeadersUIArray);
          if (reportDataArray != null) {
            if (reportDataArray.length > 0) {
              print("hi i am here report array");
              List<List<String>> arrayListDataValues =
              await GetValuesFromReportDataArrayJson(
                  reportDataArray, reportHeadersUIArray);
              print("array list ..." + arrayListDataValues.toString());
              await CreateExpandableReportUI(reportHeadersArray,
                  arrayListDataValues, reportHeadersUIArray);
             // Dialogs.dismissSimpleDialog(context);
            } else {
              await showdialog("Upload Pending", "No Data Available", "OK");
              // Navigation.RemovePage(this);
            }
          }
        } else {
          await showdialog(AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
          // Navigation.RemovePage(this);
        }
      } else if (response.statusCode == 401) {
        await showdialog(AppConstants.ALERT, AppConstants.TOKEN_EXPIRED_RELOGIN,
            AppConstants.OK);
        //AppUtils.LogOutUser(this);
      } else if (response.statusCode == 403 || response.statusCode == 800) {
        await showdialog(AppConstants.ALERT,
            AppConstants.SOMETHING_WENT_WRONG_TRY, AppConstants.OK);
        // Navigation.RemovePage(this);
      } else {
        await showdialog(AppConstants.ALERT,
            AppConstants.SOMETHING_WENT_WRONG_TRY, AppConstants.OK);
        // Navigation.RemovePage(this);
      }
    } on Exception catch (e) {
      print("Exception in GetStatusReportData " + e.toString());
    }
  }
  Future GetValuesFromReportDataArrayJson(
      List reportDataArray, List reportHeadersUIArray) async {
    List<List<String>> arrayListDataValues = new List<List<String>>();
    try {
      print("GetValuesFromReportDataArrayJson Adding elements in report cards");

      for (int i = 0; i < reportDataArray.length; i++) {

        try {
          List<String> values = new List<String>();
          for (int j = 0; j < reportHeadersUIArray.length; j++) {

            Map headersUIJsonObject = reportHeadersUIArray[j];

            String uiField = headersUIJsonObject["data"];

            print(reportDataArray);
            Map<String, dynamic> jsonObject = reportDataArray[i];
            print("jsonobject...." + jsonObject.toString());
            if (uiField != null && uiField.contains(".")) {

              try {
                List<String> fieldDataArray = uiField.split('.');
                String fieldDataVal = fieldDataArray[0];
                Map dataJsonObject = jsonObject[fieldDataVal];
                Map applicationJsonObject = dataJsonObject[fieldDataArray[
                (fieldDataArray.length) - (fieldDataArray.length - 1)]];
                String fieldFinalVal = applicationJsonObject[
                fieldDataArray[fieldDataArray.length - 1]];
                values.add(fieldFinalVal.toString());

              } on Exception catch (e) {

                print("GetValuesFromReportDataArrayJson " + e.toString());
                values.add("");

              }
            } else {
              try {

                String fieldFinalVal = jsonObject[uiField].toString();
                values.add(fieldFinalVal);

              } on Exception catch (e) {

                print("GetValuesFromReportDataArrayJson " + e.toString());
                values.add("");

              }
            }
          }
          arrayListDataValues.add(values);
        } on Exception catch (e) {
          print("GetValuesFromReportDataArrayJson " + e.toString());
        }
      } // end for loop

    } on Exception catch (e) {
      print("GetValuesFromReportDataArrayJson " + e.toString());
    }
    return arrayListDataValues;
  } //end of

  showdialog(String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/homeScreen');
              },
            ),
          ],
        );
      },
    );
  }
}
_buildExpandableContent(NEWTWReport vehicle) {
  List list = [];
  List<Widget> columnContent = [];

  for (String content in vehicle.contents) {
    columnContent.add(

      new ListTile(

        title: new Text(
          content.replaceAll(",","\n"),
          style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.w800,height: 1.8,wordSpacing: 1,color:Colors.blue),
        ),
      ),
    );
  }

  return columnContent;
}