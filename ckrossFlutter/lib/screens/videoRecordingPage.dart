import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/screens/selectGalleryVideo.dart';
//import 'package:adv_camera/adv_camera.dart'
//import 'package:camera_flash/flash_camera_controller.dart';
import 'package:ckrossFlutter/screens/videoAppController.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter_video_compress/flutter_video_compress.dart';
import 'package:gallery_saver/gallery_saver.dart';

//import 'package:torch_compat/torch_compat.dart';
//import 'package:flutter/services.dart';
//import 'package:flashlight/flashlight.dart';



class VideoExampleHome extends StatefulWidget {
  @override
  _CameraExampleHomeState createState() {
    return _CameraExampleHomeState();
  }
}

/// Returns a suitable camera icon for [direction].
IconData getCameraLensIcon(CameraLensDirection direction) {
  switch (direction) {
    case CameraLensDirection.back:
      return Icons.camera_rear;
    case CameraLensDirection.front:
      return Icons.camera_front;
    case CameraLensDirection.external:
      return Icons.camera;
  }
  throw ArgumentError('Unknown lens direction');
}

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class _CameraExampleHomeState extends State<VideoExampleHome>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  CameraController controller;
  AnimationController _controller;

  String imagePath;
  String videoPath;
  VideoPlayerController videoController;
  VoidCallback videoPlayerListener;
  bool _startRecording = false;
  bool enableAudio = true;
  int levelClock = 70;
  bool _visible = false;
  List<CameraDescription> cameras = [];
  int selectedCameraIdx;
  bool _hasFlashlight = false;
  bool _hasFlash = false;
  bool _isOn = false;
  var newFile;

  final _flutterVideoCompress = FlutterVideoCompress();


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    availableCameras().then((availableCameras) {
      cameras = availableCameras;

      if (cameras.length > 0) {
        setState(() {
          selectedCameraIdx = 0;
        });

        onNewCameraSelected(cameras[selectedCameraIdx]).then((void v) {});
      }
    }).catchError((err) {
      print('Error: $err.code\nError Message: $err.message');
    });


    startTimer();


    // initFlashlight();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    _controller.dispose();
    videoController.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }



  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future startTimer() {
    _controller = AnimationController(
        vsync: this,
        duration: Duration(
            seconds:
            levelClock) // gameData.levelClock is a user entered number elsewhere in the applciation
    );


  }



//  initFlashlight() async {
//    bool hasFlash = await Flashlight.hasFlashlight;
//    print("Device has flash ? $hasFlash");
//    setState(() {
//      _hasFlashlight = hasFlash;
//    });
//  }
//  Future _turnFlash() async {
//    _isOn ? Flashlight.lightOff() : Flashlight.lightOn();
//    var f = await Flashlight.hasFlashlight;
//    setState((){
//      _hasFlash = f;
//      _isOn = !_isOn;
//    });
//  }

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Loading',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
        ),
      );
    }
    final size = MediaQuery.of(context).size;
    final deviceRatio = size.width / size.height;
    final xScale = controller.value.aspectRatio / deviceRatio;
// Modify the yScale if you are in Landscape
    final yScale = 1.0;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        // title: const Text('Camera example'),
        backgroundColor: Colors.black,
        actions: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 100, 10),
            child: ButtonTheme(
              child: RaisedButton(
                color: Colors.grey,
                textColor: Colors.white,
                child: new Text("BACK"),
                onPressed: () {

                  Navigator.pop(context);
                  // Navigator.pushReplacementNamed(context, '/homeScreen');

                },
              ),
            ),
          ),
          Container(
            width: 50,
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: ButtonTheme(
              child: RaisedButton(
                color: Colors.grey,
                textColor: Colors.white,
                child: new Text("NEXT"),
                onPressed: () {
                  if (videoController == null) {
                    showdialog(
                        "Please record video to proceed OR Press BACK Button to go back");
                  } else {
                    print("video path...............");
                    print(videoController);
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (BuildContext context) => new VideoPlayerFromGallery(videoController,videoPath)));
                  }
                },
              ),
            ),
          ),
        ],
      ),
      body: Container(
        //aspectRatio: deviceRatio,

//        child:Transform(
//    alignment: Alignment.center,
//      transform: Matrix4.diagonal3Values(xScale, yScale, 1),
//      child: CameraPreview(controller),
//    ),

        child: Container(

          child: Stack(
            children: <Widget>[
              Center(
                child:Transform.scale(
                  scale: controller.value.aspectRatio/deviceRatio,
                  child: new AspectRatio(
                    aspectRatio: controller.value.aspectRatio,
                    child: new CameraPreview(controller),
                  ),
                )
              ),

              //  CameraPreview(controller),

              Visibility(
                visible: _visible,
                child: Countdown(
                  animation: StepTween(
                    begin: levelClock, // THIS IS A USER ENTERED NUMBER
                    end: 0,
                  ).animate(_controller),
                ),
              ),
//              Text(_hasFlashlight
//                  ? 'Your phone has a Flashlight.'
//                  : 'Your phone has no Flashlight.'),
              Container(
                alignment: Alignment.topRight,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
//                children: <Widget>[
//                  RaisedButton(
//                      child: Text('Turn on'),
//                      onPressed: () {
//
//                        TorchCompat.turnOn();
//                      }),
//                  RaisedButton(
//                      child: Text('Turn off'),
//                      onPressed: () {
//                        TorchCompat.turnOff();
//                      })
//                ],

                child: IconButton(
                  icon: Icon(Icons.flash_on),
                  iconSize: 40,
                  color: Colors.white,
                  splashColor: Colors.purple,
                  //  onPressed: () => _turnFlash(),,
                ),
//
              ),

              Container(
                width: double.infinity,
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 100.0,
                  padding: EdgeInsets.all(0.0),
                  // color: Color.fromRGBO(00, 00, 00, 0.7),
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(60, 0, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            // _cameraTogglesRowWidget(),

                            _thumbnailWidget(),
                          ],
                        ),
                      ),
                      Container(
                       // margin: const EdgeInsets.only(left: 60.0, right: 20.0),
                        width: double.infinity,
                        alignment: Alignment.center,
                        child: Material(
                          color: Colors.transparent,

                          child: FloatingActionButton(
                            heroTag: "btn1",
                            backgroundColor: Colors.transparent,
                            onPressed: () {
                              !_startRecording
                                  ? onVideoRecordButtonPressed()
                                  : onStopButtonPressed();
                              setState(() {
                                _startRecording = !_startRecording;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border:
                                Border.all(color: Colors.white, width: 4.0),
                                color: Colors.transparent,
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                  child: Text(
                                    !_startRecording ? 'Start' : 'Stop',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                    textAlign: TextAlign.center,
                                  )),
                            ),
                          ),
                        ),
                      ),
                      Container(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Display the preview from the camera (or a message if the preview is not available).



  /// Display the thumbnail of the captured image or video.
  Widget _thumbnailWidget() {
    return Expanded(
      child: Align(
        alignment: Alignment.centerRight,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            videoController == null && imagePath == null
                ? Container()
                : SizedBox(

              child: (videoController == null)
                  ? Text(imagePath)
                  : InkWell(
                child: Container(

                  child: Center(
                      child: new Stack(
                        children: <Widget>[
                          AspectRatio(
                              aspectRatio: videoController.value.size !=
                                  null
                                  ? videoController.value.aspectRatio
                                  : 1.0,
                              child: VideoPlayer(videoController)
                            // setState()
                          ),
                          Positioned(
                            top: 0,
                            right: 0.0,
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  videoController = null;
                                });
                              },
                              child: Align(
                                alignment: Alignment.topRight,
                                child: CircleAvatar(
                                  radius: 14.0,
                                  backgroundColor: Colors.white,
                                  child: Icon(Icons.close,
                                      color: Colors.red),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )),
                ),
                onTap: () {
                  print("videoController");
                  print(videoController);
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (BuildContext context) =>
                          new VideoApp(videoController)));
                },
              ),
              width: 70.0,
              height: 70.0,
            ),
          ],
        ),
      ),
    );
  }

  showdialog(String text) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

//
  /// Display the control bar with buttons to take pictures and record videos.



  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  Future<void> onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
     // enableAudio: enableAudio,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
          videoController?.dispose();
          videoController = null;
        });
        if (filePath != null) showInSnackBar('Picture saved to $filePath');
      }
    });
  }

  void onVideoRecordButtonPressed() async{
    startTimer();
    Timer(Duration(seconds: 70), () {
      if(_startRecording == true){
      print("in stop timer");
      onStopButtonPressed();}else{
        print("timer is set");
      }
    });

    setState(() {
      _visible = true;
      _controller.forward();
    });

    startVideoRecording().then((String filePath)  {
      if (mounted) setState(() {});

      if (filePath != null){

        showInSnackBar('Saving video to $filePath');
      }
    });

  }

  void onStopButtonPressed() {
    print("button stop timer");
    Dialogs.showSimpleDialog(context, "Please Wait File Saving in Folder");
    stopVideoRecording().then((_) async{
      if (mounted) setState(() {});
      print("file path....stop video" +videoPath);

      final a = await _flutterVideoCompress.getMediaInfo(videoPath);
      debugPrint("before compression..." + a.toJson().toString());
      print("info ...after saving... " + a.height.toString());
      print("info ...after saving... " + a.width.toString());

      final String dirPath = '/storage/emulated/0/CKross_Wheels_EMI/Video';
      await Directory(dirPath).create(recursive: true);
      final String filePath = '$dirPath/${timestamp()}.mp4';

      if(a.height > 640 || a.width > 360){

        print("in if");
        print(a.height);
        print(a.width);

        final info = await _flutterVideoCompress.startCompress(
          videoPath,
          quality: VideoQuality.MediumQuality, // default(VideoQuality.DefaultQuality)
          deleteOrigin: true, // default(false)
        );

//        final dir = Directory(videoPath);
//        dir.deleteSync(recursive: true);
        debugPrint("after compression" + info.toJson().toString());
         moveFile(info.file,filePath);
        setState(() {
          videoPath = filePath;
          print("videoPath");
        });


      }
      Dialogs.dismissSimpleDialog(context);
      showInSnackBar('Video recorded to: $videoPath');
//      setState(() {
//        videoPath == videoPath;
//      });
    });
    setState(() {
      print("stop controller timer");
      _visible = false;
      _controller.stop();
    });
  }

  Future<File> moveFile(File sourceFile, String newPath) async {
    print("call movie file");
    try {
      // prefer using rename as it is probably faster
      return await sourceFile.rename(newPath);
    } on FileSystemException catch (e) {
      // if rename fails, copy the source file and then delete it

        final newPathFile = await sourceFile.copy(newPath);
        await sourceFile.delete();

        return newPathFile;



    }
  }

  void onPauseButtonPressed() {
    pauseVideoRecording().then((_) {
      if (mounted) setState(() {});
      showInSnackBar('Video recording paused');
    });
  }

  void onResumeButtonPressed() {
    resumeVideoRecording().then((_) {
      if (mounted) setState(() {});
      showInSnackBar('Video recording resumed');
    });
  }

  Future<String> startVideoRecording() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }

    // final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '/storage/emulated/0/CKross_Wheels_EMI/Video';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.mp4';


//

    if (controller.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      videoPath = filePath;
      await controller.startVideoRecording(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  Future<void> stopVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.stopVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }

    await _startVideoPlayer();
  }

  Future<void> pauseVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.pauseVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  Future<void> resumeVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.resumeVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  Future<void> _startVideoPlayer() async {
    final VideoPlayerController vcontroller =
    VideoPlayerController.file(File(videoPath));
    videoPlayerListener = () {
      if (videoController != null && videoController.value.size != null) {
        // Refreshing the state to update video player with the correct ratio.
        if (mounted) setState(() {});
        videoController.removeListener(videoPlayerListener);
      }
    };
    vcontroller.addListener(videoPlayerListener);
    await vcontroller.setLooping(true);
    await vcontroller.initialize();
    final VideoPlayerController oldController = videoController;
    if (mounted) {
      setState(() {
        imagePath = null;
        videoController = vcontroller;
      });
    }
    await vcontroller.pause();
    await oldController?.dispose();
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }
}

class CameraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: VideoExampleHome(),
    );
  }
}

//Future<void> main() async {
//  // Fetch the available cameras before initializing the app.
//
//  runApp(CameraApp());
//}

class Countdown extends AnimatedWidget {
  Countdown({Key key, this.animation}) : super(key: key, listenable: animation);
  Animation<int> animation;

  @override
  build(BuildContext context) {
    Duration clockTimer = Duration(seconds: animation.value);

    String timerText =
        '${clockTimer.inMinutes.remainder(60).toString()}:${clockTimer.inSeconds.remainder(60).toString().padLeft(2, '0')}';

    print('animation.value  ${animation.value} ');
    print('inMinutes ${clockTimer.inMinutes.toString()}');
    print('inSeconds ${clockTimer.inSeconds.toString()}');
    print(
        'inSeconds.remainder ${clockTimer.inSeconds.remainder(60).toString()}');

    return Text(
      "$timerText",
      style: TextStyle(
        fontSize: 30,
        color: Colors.red,
      ),
    );
  }
}
