import 'dart:async';

import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/models/statusReportDeatailsModel.dart';
import 'package:ckrossFlutter/models/statusReportUsedModel.dart';
import 'package:ckrossFlutter/webUtils/apiConstants.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as JSON;
import 'package:ckrossFlutter/CustomWidgets/custom_expansion_tile.dart'
    as custom;

class StatusReportUsedTW extends StatefulWidget {
  StatusReportUsedTW(this.datesSelected, {Key key}) : super(key: key);
  List<String> datesSelected;
  @override
  ExpandViewState createState() => ExpandViewState(this.datesSelected);
}

class ExpandViewState extends State<StatusReportUsedTW> {
  //ExpandViewState(this.datesSelected, {Key key}) : super(key: key);
  ExpandViewState(this.datesSelected, {Key key}) : super();
  List<UsedTWReport> _listViewData = [];
  List<String> datesSelected;
  String currentProductID;
  SharedPreferences sp;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // CreateExpandableReportUI();
  }

  @override
  void initState() {
    super.initState();

    setCurrentId();
  }

  setCurrentId() async {
    try {
      sp = await SharedPreferences.getInstance();
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
    } on Exception catch (e) {
      print("Exception In InitializeAppData " + e.toString());
    }
    await GetStatusReportData();
  }

  CreateExpandableReportUI(List reportHeadersArray,
      List<List<String>> reportDataArray, List reportHeadersUIArray) async {
    print(reportHeadersArray);
    print(reportDataArray);
    print(reportHeadersUIArray);
    Map<String, String> z = new Map<String, String>();
    List<String> mapData = new List<String>();
    try {
      setState(() {
        for (var i = 0; i < reportDataArray.length; i++) {
          var dataList = reportDataArray[i];

          for (var j = 0; j < dataList.length; j++) {
            print("datalist" + dataList.toString());
            for (var k = 0; k < reportHeadersArray.length; k++) {
              z[reportHeadersArray[k]] = dataList[j];
              j++;
            }
          }
          print("z..." + z.toString());
          mapData.add(z.toString());
          print("map data");
          print(mapData);
        }

        mapData.forEach((k) => _listViewData.add(UsedTWReport(
                k.substring(k.indexOf("{"), k.indexOf("Source")).replaceAll("{", " ") +
                    k.substring(k.indexOf("Status"), k.indexOf("Retry")) +
                    //  k.substring(k.indexOf("In "), k.indexOf(" Dealer")) +
                    k.substring(k.indexOf("Hold"), k.indexOf("}")),
                [
                  k.substring(k.indexOf(" Source"), k.indexOf("Status")) +
                      k.substring(k.indexOf("Retry"), k.indexOf("Dealer")) +
                      k.substring(k.indexOf("Dealer"), k.indexOf(" Hold"))
                ])));

        // Dialogs.dismissSimpleDialog(context);
      });
    } on Exception catch (e) {
      print("Exception in CreateExpandableReportUI " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Status Report Used TW'),
      ),
      body: new ListView.separated(
        itemCount: _listViewData.length,
        separatorBuilder: (BuildContext context, int index) => Divider(height: 25),

        itemBuilder: (context, i) {
          return new custom.ExpansionTile(
          //iconColor: Colors.white,
              trailing: RaisedButton(
                color: Colors.white,
                disabledColor: Colors.white,
                  child:  Image.asset('images/imagesupload.png', height: 30, ),
                  elevation:0,
//                  onPressed: () => {
//                     Navigator.pushReplacementNamed(context, '/statusReportData'),
//
//            },
              ),
            title: new Text(_listViewData[i].id.replaceAll(",", "\n").replaceAll("null", ""),
                style: new TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w800,
                    height: 1.8,
                    wordSpacing: 1,
                    color: Colors.black),
              ),
            children: <Widget>[
              new Column(children: _buildExpandableContent(_listViewData[i])),
            ],
          );
        },
      ),
    );
  }

  GetStatusReportData() async {
    print("start getStatusreport data");
    print(currentProductID);
    try {
      Dialogs.showSimpleDialog(context, "Please Wait");
      if (currentProductID != null && currentProductID.isNotEmpty) {
        String url = 'reports' + "/" + currentProductID + "/" + AppConstants.STATUS_REPORT;

        Map reqJobject = new Map();
        reqJobject["redirectPage"] = "displayReport";
        reqJobject["isUserSpecific"] = true;
        reqJobject["isFilterSPecificOnly"] = true;

        Map criteriaobject = new Map();
        criteriaobject["LEAD_CREATION_DATE_TIME__StartDate"] = datesSelected[0];
        criteriaobject["LEAD_CREATION_DATE_TIME__EndDate"] = datesSelected[1];

        reqJobject["reportCriteria"] = criteriaobject;
        String requestJson = JSON.jsonEncode(reqJobject);

        print("requestJson");
        print(requestJson);
        ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
        // Dialogs.showSimpleDialog(context, "Please Wait");
        if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
          var response =
              await APIServices.GetReportsDateFilter(url, requestJson);
          if ((response != null) && (response != "null") &&  (response != "{}")) {
            print("response of date filter");
            print(response.body);
            await handleGetReport(context, response);
          } else {
            //CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
            await showdialog(
                AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
          }
        } else {
          CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
        }
      } //main if

    } on Exception catch (e) {
      print("Exception in GetStatusReportData " + e.toString());
    }
  } //end get Statusreportdata

  Future handleGetReport(BuildContext context, Response response) async {
    try {
      if (response.statusCode == 200) {
        var results = JSON.jsonDecode(response.body);
        if (results != null) {
          print("result");
          print(results);
          // JObject results = JObject.Parse(responseContent);
          List reportHeadersArray = results["reportHeaders"];
          print(reportHeadersArray);

          int editIndex = -1;
          for (int i = 0; i < reportHeadersArray.length; i++) {
            if (reportHeadersArray[i] == "Edit".toLowerCase()) {
              editIndex = i;
            }
          }
          if (editIndex > 0) {
            reportHeadersArray.remove(editIndex);
          }
          List reportDataArray = results["reportData"];
          List reportHeadersUIArray = results["reportHeadersUI"];
          print("arrays");
          print(reportDataArray);
          print(reportHeadersUIArray);
          if (reportDataArray != null) {
            if (reportDataArray.length > 0) {
              List<List<String>> arrayListDataValues =
                  await GetValuesFromReportDataArrayJson(
                      reportDataArray, reportHeadersUIArray);
              print("array list ..." + arrayListDataValues.toString());
              await CreateExpandableReportUI(reportHeadersArray,
                  arrayListDataValues, reportHeadersUIArray);
               Dialogs.dismissSimpleDialog(context);
            } else {
              await showdialog(
                  AppConstants.STATUS_REPORT, "No Data Available", "OK");
              // Navigation.RemovePage(this);
            }
          }
        } else {
          await showdialog(AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
          // Navigation.RemovePage(this);
        }
      } else if (response.statusCode == 401) {
        await showdialog(AppConstants.ALERT, AppConstants.TOKEN_EXPIRED_RELOGIN,
            AppConstants.OK);
        //AppUtils.LogOutUser(this);
      } else if (response.statusCode == 403 || response.statusCode == 800) {
        await showdialog(AppConstants.ALERT,
            AppConstants.SOMETHING_WENT_WRONG_TRY, AppConstants.OK);
        // Navigation.RemovePage(this);
      } else {
        await showdialog(AppConstants.ALERT,
            AppConstants.SOMETHING_WENT_WRONG_TRY, AppConstants.OK);
        // Navigation.RemovePage(this);
      }
    } on Exception catch (e) {
      print("Exception in GetStatusReportData " + e.toString());
    }
  }

  showdialog(String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {

                Navigator.pushReplacementNamed(context, '/statusReport');

              },
            ),
          ],
        );
      },
    );
  }

  Future GetValuesFromReportDataArrayJson(
      List reportDataArray, List reportHeadersUIArray) async {
    List<List<String>> arrayListDataValues = new List<List<String>>();
    try {
      print("GetValuesFromReportDataArrayJson Adding elements in report cards");

      for (int i = 0; i < reportDataArray.length; i++) {
        try {
          List<String> values = new List<String>();
          for (int j = 0; j < reportHeadersUIArray.length; j++) {
            Map headersUIJsonObject = reportHeadersUIArray[j];

            String uiField = headersUIJsonObject["data"];

            print(reportDataArray);
            Map<String, dynamic> jsonObject = reportDataArray[i];
            print("jsonobject...." + jsonObject.toString());
            if (uiField != null && uiField.contains(".")) {
              try {
                List<String> fieldDataArray = uiField.split('.');
                String fieldDataVal = fieldDataArray[0];
                Map dataJsonObject = jsonObject[fieldDataVal];
                Map applicationJsonObject = dataJsonObject[fieldDataArray[
                    (fieldDataArray.length) - (fieldDataArray.length - 1)]];
                String fieldFinalVal = applicationJsonObject[
                    fieldDataArray[fieldDataArray.length - 1]];
                values.add(fieldFinalVal.toString());
              } on Exception catch (e) {
                print("GetValuesFromReportDataArrayJson " + e.toString());
                values.add("");
              }
            } else {
              try {
                String fieldFinalVal = jsonObject[uiField].toString();
                values.add(fieldFinalVal);
              } on Exception catch (e) {
                print("GetValuesFromReportDataArrayJson " + e.toString());
                values.add("");
              }
            }
          }
          arrayListDataValues.add(values);
        } on Exception catch (e) {
          print("GetValuesFromReportDataArrayJson " + e.toString());
        }
      } // end for loop

    } on Exception catch (e) {
      print("GetValuesFromReportDataArrayJson " + e.toString());
    }
    return arrayListDataValues;
  } //end of GetValuesFromReportDataArrayJson
}

_buildExpandableContent(UsedTWReport vehicle) {
  List list = [];
  List<Widget> columnContent = [];

  for (String content in vehicle.contents) {
    columnContent.add(
      new ListTile(
        title: new Text(
          content.replaceAll(",", "\n").replaceAll("null", ""),
          style: new TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.w800,
              height: 1.8,
              wordSpacing: 1,
              color: Colors.blue),
        ),
      ),
    );
  }

  return columnContent;
}
