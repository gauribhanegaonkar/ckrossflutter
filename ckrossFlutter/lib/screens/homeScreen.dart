import 'dart:async';
import 'dart:convert' as JSON;
import 'dart:convert';
import 'dart:io';

import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/page_transition.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/UITemplateOperations.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/loginJson_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/makeDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/make_dealer_dataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/modelDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/productDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ResponseModels/DoOtpLoginResModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/models/make.dart';
import 'package:ckrossFlutter/models/make_dealer_data.dart';
import 'package:ckrossFlutter/models/menuModel.dart';
import 'package:ckrossFlutter/models/model.dart';
import 'package:ckrossFlutter/models/more_options.dart';
import 'package:ckrossFlutter/models/product.dart';
import 'package:ckrossFlutter/providers/make_api_data_provider.dart';
import 'package:ckrossFlutter/screens/LeadCreationData.dart';
import 'package:ckrossFlutter/screens/newTW/CCLeadsNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PostDisQueryResolutionNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PostDisbursementDocUploadLeadListNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PreDisAdviceUploadNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PreDisbursementDocUploadLeadListNTW.dart';
import 'package:ckrossFlutter/screens/permissionList.dart';
import 'package:ckrossFlutter/screens/statusReportDate.dart';
import 'package:ckrossFlutter/screens/usedtw/LeadCreationDataUTW.dart';
import 'package:ckrossFlutter/screens/usedtw/QueryResolutionUTW.dart';
import 'package:ckrossFlutter/screens/usedtw/cc_leads_UTW.dart';
import 'package:ckrossFlutter/screens/usedtw/creadit_refer_UTW.dart';
import 'package:ckrossFlutter/screens/usedtw/post_dis_query_res_UTW.dart';
import 'package:ckrossFlutter/screens/usedtw/post_disbursement_upload.dart';
import 'package:ckrossFlutter/screens/usedtw/pre_dis_query_resolution.dart';
import 'package:ckrossFlutter/screens/usedtw/pre_disbursement_upload.dart';
import 'package:ckrossFlutter/screens/usedtw/service_inspection_videoUpload.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import 'leadCreationPages/LeadCreationData_seq_id_4.dart';
import 'leadCreationPages/LeadCreation_seq_id_2.dart';
import 'leadCreationPages/LeadCreation_seq_id_3.dart';
import 'newTW/CreditReferQueueNTW.dart';
import 'newTW/PreDisQueryResolutionNTW.dart';
import 'newTW/Pre_approvalQueryResolutionNTW.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  var newMap;
  var subcategory;
  var saveCurrentProductMap;
  var productArray;
  var responseObject;
  List<MenuModel> menuItemsList = [];
  List<MoreOptions> choiceList = [];
  String productName = "";
  String make = "";
  String model1 = "";
  String model_variant = "";
  int onroadPrice;
  String orp;
  int pid = 0, mkid = 0, moid = 0;
  List<Map<String, dynamic>> jArrayDealerData;
  String newTW;
  DatabaseHelper databaseHelper = DatabaseHelper();
  static DatabaseHelper db = DatabaseHelper();
  Map<String, dynamic> AllDynamicMenus = new Map<String, dynamic>();
  Map<int, dynamic> productNames = new Map<int, dynamic>();

  static String token, appLang, userId, userRole, currentProductName;
  SharedPreferences sp = null;
  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    sp = await SharedPreferences.getInstance();
    setState(() {
      if (AppConstants.SELECTEDTEXT != null) {
        newTW = sp.getString(AppConstants.SELECTEDTEXT);
      }
    });

    //generateMenuList();
  }

  @override
  void initState() {
    super.initState();
    permissionAcessPhone();
    AddHomeMenusDynamically();
    AddToolbarMenusDynamically();

      GetDefaultDataAsync1();
     // data();

  }



  dealerJsondtaa() async {
    var loginData = await LoginJsonTemplateDB();
//    List<Map<String, dynamic>> data = await loginData.showLoginJsonTemplate();
//    print("LoginJsonTemplateDB database..." + data.toString());

    SharedPreferences sp = await SharedPreferences.getInstance();
    String menuJsonData = sp.getString(AppConstants.PRODUCTJSON);
    print("menudata data");
    print(menuJsonData);
  }
  GetDefaultDataAsync1()async
  {
    sp = await SharedPreferences.getInstance();
    if (sp.getString(AppConstants.CURRENT_PRODUCT_ID) == "2")
    {
     print("at GetDefaultDataAsync1()......... ");
     String value = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
     print("......" + value);

      await UITemplateOperations().TruncateMakeTable();
      //await UITemplateOperations.TruncateCityTable();

      await getMakes();
      //await getCitites();
    }
  }


  AddHomeMenusDynamically() async {
    AllDynamicMenus = await GetMenusFromDb();
    setState(() {
      print("all dynamic menus");
      print(AllDynamicMenus);

      if (AllDynamicMenus.length > 0) {
       /// print("before add list");
        AllDynamicMenus.forEach((k, v) => menuItemsList.add(MenuModel(
            k, k.toLowerCase().replaceAll(" ", "_").replaceAll("-", "_"), v)));
        menuItemsList
            .add(new MenuModel("Upload Pending", "upload_pending", "0"));
       // print(menuItemsList);
      }
    });
  }

  AddToolbarMenusDynamically() async {
   // print("Add tool bar");
    productNames = await GetProductNamesFromDb();
    try {
      setState(() {
       // print("tool bar");
        print(productNames);
        if (productNames.length > 0) {
          productNames.forEach((k, v) => choiceList.add(MoreOptions(k, v)));
          choiceList.add(new MoreOptions(2, "Send Log"));
          choiceList.add(new MoreOptions(3, "Log Out"));
         // print(choiceList);
        }
      });
    } on Exception catch (e) {
      print("Exception in AddToolbarMenusDynamically() $e ");
    }
  }

  choiceAction(MoreOptions choice) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    //print("choice");
    setState(() {
      switch (choice.name) {
        case 'New TW':
          Fluttertoast.showToast(
              msg: "This is Toast message ${choice.name}",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.grey,
              textColor: Colors.black,
              fontSize: 16.0);
         // print("page call");

          break;
        case 'Used TW':
          Fluttertoast.showToast(
              msg: "This is Toast message ${choice.name}",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.grey,
              textColor: Colors.black,
              fontSize: 16.0);
          print("page call");

          break;
        case 'Send Log':
          Fluttertoast.showToast(
              msg: "This is Toast message ${choice.name}",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.grey,
              textColor: Colors.black,
              fontSize: 16.0);
          attachFileAndSendMail();
          break;
        case 'Log Out':
          Fluttertoast.showToast(
              msg: "This is Toast message ${choice.name}",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.grey,
              textColor: Colors.black,
              fontSize: 16.0);
          sp.setBool(AppConstants.IS_LOGGED_IN, false);
          Navigator.pushReplacementNamed(context, '/');

          break;
      }
    });

    if(choice.id == 1 || choice.id == 2){
      Dialogs.showSimpleDialog(context, "Please Wait");
      var result = await ProductSwitch(choice.id);

      Timer(Duration(seconds: 10), () {
        Navigator.pop(context);
        // print(result);
        Dialogs.dismissSimpleDialog(context);
        // pass your animation time  in seconds and then just navigate
        sp.setString(AppConstants.CURRENT_PRODUCT_ID, choice.id.toString());
        sp.setString(AppConstants.SELECTEDTEXT, choice.name);
        sp.setString(AppConstants.PAN_No, "");
        Navigator.pushReplacementNamed(context, '/homeScreen');
      });
    }

  }

  void attachFileAndSendMail() async {
    List<File> file = await FileUtils.getFile();
    List<String> arr = [];
    for(int i =0; i < file.length; i++){
      print(file[i]);
      arr.add(file[i].path);

    }
    print(arr);
    Email email;

         email = Email(
          //   body: 'Email body',
          subject: 'Log File',
          //   recipients: ['example@example.com'],
          //   cc: ['cc@example.com'],
          //   bcc: ['bcc@example.com'],
          attachmentPaths: arr,

          isHTML: false,
        );

    await FlutterEmailSender.send(email);

    // await androidMethodChannel.invokeListMethod('sahreFile',{'filepatth':file.path});
    }

  Future permissionAcessPhone() async {
    List<PermissionGroup> plist = [];
    final PermissionHandler _permissionHandler = PermissionHandler();
    var permissionStatus =
        await _permissionHandler.checkPermissionStatus(PermissionGroup.camera);
    if (permissionStatus != PermissionStatus.granted) {
      plist.add(PermissionGroup.camera);
    }
    permissionStatus =
        await _permissionHandler.checkPermissionStatus(PermissionGroup.phone);
    if (permissionStatus != PermissionStatus.granted) {
      plist.add(PermissionGroup.phone);
    }
    permissionStatus = await _permissionHandler
        .checkPermissionStatus(PermissionGroup.microphone);
    if (permissionStatus != PermissionStatus.granted) {
      plist.add(PermissionGroup.microphone);
    }
    permissionStatus =
        await _permissionHandler.checkPermissionStatus(PermissionGroup.storage);
    if (permissionStatus != PermissionStatus.granted) {
      plist.add(PermissionGroup.storage);
    }
    permissionStatus =
        await _permissionHandler.checkPermissionStatus(PermissionGroup.sms);
    if (permissionStatus != PermissionStatus.granted) {
      plist.add(PermissionGroup.sms);
    }

    if (plist.length > 0) {
      PermissionService.hasPermission(plist);
    }
  }

  Widget _createListItem({MenuModel menu}) {
    return GridTile(
      child: Card(
        elevation: 2,
        margin: EdgeInsets.fromLTRB(6, 6, 6, 6),
        child: InkWell(
          // splashColor: Colors.,
          onTap: () => onMenuItemClicked(menu),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("images/" + menu.image + ".png", height: 50),
              Container(height: 15),
              Center(
                child: Text(
                  menu.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onMenuItemClickedShowMsg(BuildContext context, MenuModel menu) {
    print("msg");
    Fluttertoast.showToast(
        msg: "This is Toast message ${menu.name}",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.grey,
        textColor: Colors.black,
        fontSize: 16.0);
  }

  onMenuItemClicked(MenuModel menu) async {
    final mainMenu = sp.getString(AppConstants.SELECTEDTEXT);
    print("menu name...." +menu.name.toString());

    switch (menu.name) {
      case 'Lead Creation':

        mainMenu == 'New TW'
            ? Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 400),
            child: LeadCreationData(),
            //child: LeadCreationDataSeqid2(),
           // child: LeadCreationDataSeqid3(),
           // child: LeadCreationDataSeqid4(),
          ),
        )
            : Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 400),
            child:LeadCreationDataUTW(),
          ),
        );
        break;
      case 'CC Leads':
        mainMenu == 'New TW'
            ? Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 400),
                  child: CCLeadsNTW(),
                ),
              )
            : Navigator.push(
                context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 400),
              child: CCLeadsUTW(),
            ),
        );
        Fluttertoast.showToast(
            msg: "This is Toast message ${menu.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);
        break;
      case 'Query Resolution':
         mainMenu == 'New TW'
            ? Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 400),
                  child: QueryResolutionNTW(),
                ),
              )
            : Navigator.push(
                context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 400),
              child: QueryResolutionUTW(),
            ),
        );
        break;
      case 'Pre-Disbursement Document Upload':
        mainMenu == 'New TW'
            ? Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 400),
                  child: PreDisbursmentDocUploadNTW(),
                ),
              )
            : Navigator.push(
                context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 400),
              child: PreDisbursmentDocUploadUsedTW(),
            ),
        );
        break;
      case 'Pre-Disbursement Query Resolution':
       mainMenu == 'New TW'
            ? Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 400),
                  child: PreDisbursmentQueryResNTW(),
                ),
              )
            : Navigator.push(
                context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 400),
              child: PreDisbursmentQueryResUTW(),
            ),
        );
        break;
      case 'Post-Disbursement Document Upload':
        mainMenu == 'New TW'
            ? Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 400),
            child: PostDisbursmentDocUploadNTW(),
          ),
        )
            : Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 400),
            child: PostDisbursmentDocUploadUTW(),
          ),
        );
        break;
      case 'Post-Disbursement Query Resolution':
       mainMenu == 'New TW'
            ? Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 400),
                  child: PostDisbursmentQueryResNTW(),
                ),
              )
            : Navigator.push(
                context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 400),
              child: PostDisbursmentQueryResUTW(),
            ),
        );
        Fluttertoast.showToast(
            msg: "This is Toast message ${menu.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);
        break;
      case 'Pre-Disbursement Advice Upload':
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 400),
            child:  PreDisAdvUploadNTW(),
          ),
        );

        break;

      case 'Service Inspection Video Upload':
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 400),
            child:  ServiceInspVideoUTW(),
          ),
        );
        break;
      case 'Credit Refer Queue':
         mainMenu == 'New TW'
            ? Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 400),
                  child: CreditReferNTW(),
                ),
              )
            : Navigator.push(
                context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 400),
              child: CreditReferUTW(),
            ),
        );
        Fluttertoast.showToast(
            msg: "This is Toast message ${menu.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);
        break;
      case 'Status Report SE':
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 400),
            child: StatusReportDate(),
          ),
        );

        break;
      case 'Upload Pending':
//        Navigator.push(
//          context,
//          PageTransition(
//            type: PageTransitionType.rightToLeft,
//            duration: Duration(milliseconds: 400),
//            child:  new Text("success",
//                textAlign: TextAlign.center,
//                style: TextStyle(fontSize: 15)),
//          ),
//        );
        Fluttertoast.showToast(
            msg: "This is Toast message ${menu.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);
        break;
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        backgroundColor: Colors.blue,
        title: const Text('CKROSS LOS'),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
            child: Text(newTW != null ? newTW : 'New TW'),
          ),
          Theme(
              data: Theme.of(context).copyWith(
                cardColor: Colors.black,
                iconTheme: IconThemeData(color: Colors.white),
              ),
              child: PopupMenuButton<MoreOptions>(
                onSelected: choiceAction,
                itemBuilder: (BuildContext context) {
                  return choiceList.map((MoreOptions choice) {
                    return PopupMenuItem<MoreOptions>(
                      value: choice,
                      child: Text(choice.name,
                          style: TextStyle(color: Colors.white)),
                    );
                  }).toList();
                },
              )),
        ],
        automaticallyImplyLeading: false,
      ),
      body: new SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AnimationLimiter(
              child: GridView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: menuItemsList.length,
                padding: EdgeInsets.fromLTRB(10, 0, 10, 40),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1.2,
                ),
                itemBuilder: (context, index) {
                  return AnimationConfiguration.staggeredGrid(
                    position: index,
                    columnCount: 2,
                    duration: const Duration(milliseconds: 975),
                    child: ScaleAnimation(
                      //scale: 0.5,
                      child: FadeInAnimation(
                        child: _createListItem(menu: menuItemsList[index]),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future GetProductNamesFromDb() async {
    Map<int, dynamic> productNames = new Map<int, dynamic>();
    SharedPreferences sp = await SharedPreferences.getInstance();
    String Query =
        DatabaseQueries.CreateGetLoginJsontemplateMenuJson("Menu Details");

    List<Map<String, dynamic>> ListMenu = await DatabaseHelper().execute(Query);
    if (ListMenu.length > 0) {
      String MenuDetailsJson = ListMenu[0]["jsonValue"];
      Map<String, dynamic> jsonObject = json.decode(MenuDetailsJson);
      List jArray = jsonObject['GroupToCategoryToSubCategory'];

      if (jArray != null && jArray.length != 0) {
        for (int i = 0; i < jArray.length; i++) {
          Map<String, dynamic> productJson = jArray[i];
          if (productJson["categoryName"].toString().contains("Product")) {
            List productArray = productJson["SubCategory"];
            for (int j = 0; j < productArray.length; j++) {
              Map<String, dynamic> productJson1 = productArray[j];
              print("productjson1");
              print(productJson1);

              int productID = productJson1["id"];
              String productName = productJson1["name"];
              productNames.addAll({productID: productName});
            }
          }
        }
      }
    }
    return productNames;
  }

  Future GetMenusFromDb() async {
    print("start get menu");
    db.search3();
    Map<String, dynamic> menus = new Map<String, dynamic>();

    try {
      //cKrossStoreLoginJsonTemplate = new SQLiteLoginJsonTemplate(DependencyService.Get<ISQLiteDb>());
      String Query =
          DatabaseQueries.CreateGetLoginJsontemplateMenuJson("Menu Details");
      print("query");
      print(Query);
      List<Map<String, dynamic>> ListMenu =
          await DatabaseHelper().execute(Query);
      print("ListMenu");
      print(ListMenu);
      print("end list menu");
      if (ListMenu.length > 0) {
        String MenuDetailsJson = ListMenu[0]["jsonValue"];
        print("menudetails" + MenuDetailsJson);
        Map<String, dynamic> jsonObject = json.decode(MenuDetailsJson);
        List jArray = jsonObject['GroupToCategoryToSubCategory'];
        print(jArray);
        print("jsonObject");
        print(jsonObject);
        if (jArray != null && jArray.length != 0) {
          for (int i = 0; i < jArray.length; i++) {
            Map<String, dynamic> json = jArray[i];
            print("json");
            print(json);
            if (json["categoryName"].toString().contains("Reports")) {
              List reportsArray = json["SubCategory"];
              print("report array");
              print(reportsArray);

              for (int z = 0; z < reportsArray.length; z++) {
                Map<String, dynamic> jObject = reportsArray[z];
                String key = (jObject["name"].toString() != null)
                    ? jObject["name"].toString()
                    : "";
                print(key);
                //string value = !String.IsNullOrEmpty(jObject.SelectToken("processID").ToString()) ? jObject.SelectToken("processID").ToString() : "";
                String value = "";
                menus[key] = value;
                print("menus");
                print(menus);
              } //end reportArray
            } // end category if

            if (json["categoryName"].toString().contains("Leads")) {
              List productArray = json["SubCategory"];
              print("product array");
              print(productArray);

              Map<String, dynamic> leadObject = productArray[0];
              print(leadObject);
              Map<String, dynamic> jObject = leadObject;
              print(jObject);

              String key = (jObject["key"].toString() != null)
                  ? jObject["key"].toString()
                  : "";
              String value = (jObject["processID"].toString() != null)
                  ? jObject["processID"].toString()
                  : "";
              print("keys..." + key);
              print("values..." + value);
              menus[key] = value;
              print("menus..." + menus.toString());
              Map<String, dynamic> subMenuObject = productArray[1];
              if (subMenuObject["name"].toString().contains("List Stages")) {
                List subMenuArray = subMenuObject["subMenu"];
                for (int j = 0; j < subMenuArray.length; j++) {
                  Map<String, dynamic> subMenuJson1 = subMenuArray[j];

                  Map<String, dynamic> jObject1 = subMenuJson1;
                  String key1 = ((jObject1["name"].toString()) != null)
                      ? jObject1["name"].toString()
                      : "";
                  String value1 = (jObject1["processID"].toString() != null)
                      ? jObject1["processID"].toString()
                      : "";
                  print("keys1..." + key1);
                  print("values1..." + value1);
                  menus[key1] = value1;
                  print("menus1....." + menus.toString());
                }
              } //end list stages if
            } //end category leads
          } //end for loop

        } //jArray if

      } //main if
    } on Exception catch (e) {
      print("Exception In getMenusFromDb $e");
    }
    return menus;
  }

  ProductSwitch(int product_id) async {

    print("In ProductSwitch()");
    Map jObject = new Map();
    jObject["input_source"] = "M";
    jObject["product_Id"] = product_id;

    var requestContent = JSON.jsonEncode(jObject);
    print("requestContent");
    print(requestContent);

    try {
      bool valid = false;
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.productChange(requestContent);
        if (response != null) {
          print("resssss" + response.body);
          valid = await handleResponse(context, response);
        } else {
          CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.noInternetConnection);
      }
      return valid;
    } on Exception catch (_) {
      print(Exception);
    }
  }

  Future<bool> handleResponse(BuildContext context, Response response) async {
    print("i am at handle response");
    print(response.body);
 
    bool isSuccess = false;
    if (response.statusCode == 200) {
      var responseContent = await response.body.toString();

      print("response content in handle response");
      print(responseContent);
      DoOtpLoginResModel responseModel = doOtpLoginResModelFromJson(responseContent);
      // DoOtpLoginResModel responseModel = doOtpLoginResModelFromJson(response.body);
      if (responseModel != null) {
        if (responseModel.success) {
          print("response model not null");
          print(responseModel);

          isSuccess = true;
          await saveProcessIDsAsync(responseModel.menudetails);
          await saveCurrentProduct(responseModel.menudetails);


          SharedPreferences sp = await SharedPreferences.getInstance();
          sp.setString(AppConstants.DEALER_BRANCH, "");
          sp.setString(AppConstants.DEALER_STATE, "");
          sp.setString(
              AppConstants.LOGGED_IN_USER_ID, responseModel.loggedInUserId);
          sp.setString(AppConstants.LOGGED_IN_USER, responseModel.loggedInUser);
          sp.setString(AppConstants.TOKEN, responseModel.token);
          sp.setString(AppConstants.APP_VERSION, responseModel.appVersion);
          sp.setString(
              AppConstants.LOGGED_IN_USER_ROLE, responseModel.loggedInUserRole);
          print("token");
          await saveDealerData(response.body);
          print(sp.getString(AppConstants.TOKEN));
           userRole = responseModel.loggedInUserRole;
          if ((userRole.toLowerCase().startsWith("SO".toLowerCase())) ||
              (userRole.toLowerCase().startsWith("SE".toLowerCase())) ||
              (userRole.toLowerCase().startsWith("VD".toLowerCase()))) {
            await loadFieldData(responseModel, response.body);
          } else if (userRole.toLowerCase().startsWith("SIA".toLowerCase())) {
          } else {
            CustomSnackBar.showSnackBar(context, "User Role Is Not Valid");
            //await Navigation.PushAsync(new Login());
          }

          return isSuccess;
        } else {
          //logger.Info("Response: Failed");

          if (responseModel.details != null) {
            CustomSnackBar.showSnackBar(context, responseModel.details);
            //OtpEntry.Text = string.Empty; // added by nilima on 29 may 2018
          } else {
            CustomSnackBar.showSnackBar(context,"User data not available at Server.\nPlease try after sometime.");
            //OtpEntry.Text = string.Empty; // added by nilima on 29 may 2018
          }

          // CustomSnackBar.showSnackBar(context, responseModel.details);
          isSuccess = false;
          return isSuccess;
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } else {
      CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
    }
    return isSuccess;
  }

  saveDealerData(String dealerDataSo){
    print("dealer data so.......");
    print(dealerDataSo);

    Map<String,dynamic> map = json.decode(dealerDataSo);
    var array = map["dealer_data_SO"];
    print("array");
    for (int i = 0; i < array.length; i++) {
      var dealerSO = <String, dynamic>{};
      dealerSO.addAll(array[i]);
      if (dealerSO.containsKey("dealerBranch"))
      {
        print("in dealer branche");
        String DealerBranch = dealerSO["dealerBranch"];
        print(DealerBranch);
        sp.setString(AppConstants.DEALER_BRANCH, DealerBranch);
      }

      if (dealerSO.containsKey("dealer_State"))
      {
        print("in dealerstate");
        String DealerState = dealerSO["dealer_State"];
        print(DealerState);
        sp.setString(AppConstants.DEALER_STATE, DealerState);
      }

    }
    print(array);

  }

  saveProcessIDsAsync(String menudetails) async {
    SharedPreferences sp = await SharedPreferences.getInstance();

    try {
      print("Start of SaveProcessIDsAsync()");
      newMap = json.decode(menudetails);
      print("222222");

      print(newMap[0]);
      print(newMap["GroupToCategoryToSubCategory"]);
      var array = newMap["GroupToCategoryToSubCategory"];
      // List lst = newMap["GroupToCategoryToSubCategory"];
      if (array != null && array.length != 0) {
        for (int i = 0; i < array.length; i++) {
          newMap = <String, dynamic>{};
          newMap.addAll(array[i]);
          // print(newMap["categoryName"]);
          if (newMap["categoryName"] == "Leads") {
            var subcatarray = newMap["SubCategory"];
            var subcat = <String, dynamic>{};
            subcat.addAll(subcatarray[0]);
            if (subcat["key"] == "Lead Creation") {
              print("preffff");
              String htmlPage = subcat["htmlPage"];
              String processID = subcat["processID"];
              String key = subcat["key"];
              print("processid...........");
              print(processID);
              sp.setString(
                  AppConstants.CURRENT_PRODUCT_LEAD_CREATION_ID, processID);

              sp.setString(AppConstants.CURRENT_PRODUCT_LOAD_HTML, htmlPage);
              sp.setString(AppConstants.LEAD_CREATION_KEY, key);
            }
            var subcat2 = <String, dynamic>{};
            subcat2.addAll(subcatarray[1]);
            if (subcat2["name"] == "List Stages") {
              print("submenu");
              print(subcat2["subMenu"]);
              var namearray = subcat2["subMenu"];
              for (int j = 0; j < namearray.length; j++) {
                var name = <String, dynamic>{};
                name.addAll(namearray[j]);
                print("name");
                print(name["name"]);

                if (name["name"] == AppConstants.QUERY_RESOLUTION_KEY) {
                  String processId = name["processID"];
                  print("QUERY_RESOLUTION_KEY" + processId);
                  sp.setString(AppConstants.CURRENT_PRODUCT_QUERY_RESOLUTION_ID,
                      processId);
                  sp.setString(AppConstants.QUERY_RESOLUTION_KEY, name["name"]);
                }
                if (name["name"] == AppConstants.PRE_DISBURSEMENT_KEY) {
                  String processId = name["processID"];
                  print("PRE_DISBURSEMENT_KEY" + processId);
                  sp.setString(AppConstants.CURRENT_PRODUCT_PRE_DISBURSEMENT_ID,
                      processId);
                  sp.setString(AppConstants.PRE_DISBURSEMENT_KEY, name["name"]);
                }
                if (name["name"] == AppConstants.POST_DISBURSEMENT_KEY) {
                  String processId = name["processID"];
                  print("POST_DISBURSEMENT_KEY" + processId);
                  sp.setString(
                      AppConstants.CURRENT_PRODUCT_POST_DISBURSEMENT_ID,
                      processId);
                  sp.setString(
                      AppConstants.POST_DISBURSEMENT_KEY, name["name"]);
                }
                if (name["name"] == AppConstants.SERVICE_INSPECTION_KEY) {
                  String processId = name["processID"];
                  print("SERVICE_INSPECTION_KEY" + processId);
                  sp.setString(
                      AppConstants.CURRENT_PRODUCT_SERVICE_INSPECTION_ID,
                      processId);
                  sp.setString(
                      AppConstants.SERVICE_INSPECTION_KEY, name["name"]);
                }
                if (name["name"] == AppConstants.PRE_DISBURSEMENT_ADVICE_KEY) {
                  String processId = name["processID"];
                  print("PRE_DISBURSEMENT_ADVICE_KEY" + processId);
                  sp.setString(
                      AppConstants.CURRENT_PRODUCT_PRE_DISBURSEMENT_ADVICE_ID,
                      processId);
                  sp.setString(
                      AppConstants.PRE_DISBURSEMENT_ADVICE_KEY, name["name"]);
                }
                if (name["name"] ==
                    AppConstants.POST_DISBURSEMENT_QUERY_RESOLUTION_KEY) {
                  String processId = name["processID"];
                  print("POST_DISBURSEMENT_QUERY_RESOLUTION_KEY" + processId);
                  sp.setString(
                      AppConstants
                          .CURRENT_PRODUCT_POST_DISBURSEMENT_QUERY_RESOLUTION_ID,
                      processId);
                  sp.setString(
                      AppConstants.POST_DISBURSEMENT_QUERY_RESOLUTION_KEY,
                      name["name"]);
                }
                if (name["name"] == AppConstants.QUERY_RESOLUTION_KEY) {
                  String processId = name["processID"];
                  print("QUERY_RESOLUTION_KEY" + processId);
                  sp.setString(
                      AppConstants
                          .CURRENT_PRODUCT_PRE_DISBURSEMENT_ADVICE_QUERY_RESOLUTION_ID,
                      processId);
                  sp.setString(
                      AppConstants.PRE_DISBURSEMENT_ADVICE_QUERY_RESOLUTION_KEY,
                      name["name"]);
                }
                if (name["name"] == AppConstants.CC_LEADS_CREATED_KEY) {
                  String processId = name["processID"];
                  print("CC_LEADS_CREATED_KEY" + processId);
                  sp.setString(AppConstants.CURRENT_PRODUCT_CC_LEADS_CREATED_ID,
                      processId);
                  sp.setString(AppConstants.CC_LEADS_CREATED_KEY, name["name"]);
                }
                if (name["name"] == AppConstants.CREDIT_REFER_QUEUE_KEY) {
                  String processId = name["processID"];
                  print("CREDIT_REFER_QUEUE_KEY" + processId);
                  sp.setString(
                      AppConstants.CURRENT_PRODUCT_CREDIT_REFER_QUEUE_ID,
                      processId);
                  sp.setString(
                      AppConstants.CREDIT_REFER_QUEUE_KEY, name["name"]);
                }
              }
            }
          }
        }
      }
    } on Exception catch (e) {
      print("Exception Message" + e.toString());
    }
  }

  saveCurrentProduct(String menudetails) async {
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();

      print("Start of SaveCurrentProduct()");
      saveCurrentProductMap = json.decode(menudetails);
      var productArray = saveCurrentProductMap["GroupToCategoryToSubCategory"];

      if (productArray != null && productArray.length != 0) {
        for (int i = 0; i < productArray.length; i++) {
          saveCurrentProductMap = <String, dynamic>{};
          saveCurrentProductMap.addAll(productArray[i]);
          if (saveCurrentProductMap["categoryName"] == "Product") {
            int product_id = saveCurrentProductMap["product_Id"];
            var subCategoryArray = saveCurrentProductMap["SubCategory"];
            print(saveCurrentProductMap["SubCategory"]);

            for (int j = 0; j < subCategoryArray.length; j++) {
              var subProduct = <String, dynamic>{};
              subProduct.addAll(subCategoryArray[j]);
              print("iddddddd");
              print(subProduct["id"]);
              if (product_id == subProduct["id"]) {
                sp.setString(
                    AppConstants.CURRENT_PRODUCT_NAME, subProduct["name"]);
                print(subProduct["name"]);
              }
            }
            //print("product ddd"+product_id.toString());

            // var subProductArray =saveCurrentProductMap["SubCategory"]  ;
            // var subProduct = <String, dynamic>{};
            // subProduct.addAll(subProductArray[0]);
          }
        }
      }
    } on Exception catch (e) {
      print("Exception Message" + e.toString());
    }
  }

  loadFieldData(
      DoOtpLoginResModel responseModel, String responseContent) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    try {
      print("Start of loadFieldData()");
      String userName = "";
      var tempUserName = sp.get(AppConstants.LOGGED_IN_USER_NAME);
      userName =
          !(tempUserName == null || tempUserName.isEmpty) ? tempUserName : "";

      loadHtmlFields(responseModel, responseModel.productId.toString());

      // htmlRequestModel.processID = sp.get(AppConstants.CURRENT_PRODUCT_LEAD_CREATION_ID);
      //htmlRequestModel.landing_page = AppPreferences.GetValue(AppConstants.CURRENT_PRODUCT_LOAD_HTML, ""),
      // htmlRequestModel.product_Id = responseModel.productId.toString();

    } on Exception catch (e) {
      print(e);
    }
  }

  loadHtmlFields(
      DoOtpLoginResModel doOtpresponseModel, String product_Id) async {
    try {
      // FocusScope.of(context).requestFocus(new FocusNode());
      print("product id.........");
      print(product_Id);
      ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
      if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
        var response = await APIServices.loadHtmlFields(product_Id);
        if (response != null &&
            !(identical(response, "null")) &&
            !(response == "{}")) {
          print("1111111" + response.body.toString());
          await handleloadHtmlFieldsResponse(
              context, response, doOtpresponseModel,product_Id);
        } else {
          CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } on Exception catch (_) {
      //Dialogs.dismissSimpleDialog(context);
      Dialogs.DialogWidget(context, "", "Customer Not Found");
      // valid = false;
    }
    //Dialogs.dismissSimpleDialog(context);
  }

  Future handleloadHtmlFieldsResponse(BuildContext context, Response response,
      DoOtpLoginResModel doOtpresponseModel,String product_Id) async {
    print("sdsfsdf" + response.body.toString());
    bool isSuccess = false;
    if (response.statusCode == 200) {
      LoadHtmlFieldsResModel responseModel =
          loadHtmlFieldsResModelFromJson(response.body);
      if (responseModel != null) {
        if (responseModel.success) {
          String userName = "";
          SharedPreferences sp = await SharedPreferences.getInstance();
          var tempUserName = sp.get(AppConstants.LOGGED_IN_USER_NAME);
          userName = !(tempUserName == null || tempUserName.isEmpty)
              ? tempUserName
              : "";
          // var map = json.decode(responseModel.toString());
          print("resmodel" + responseModel.subProcessFieldsData.toString());
          //  SharedPreferences sp = await SharedPreferences.getInstance();
          sp.setString(AppConstants.CLIENT_NAME, AppConstants.DATABASE_NAME);
          sp.setString(AppConstants.LANGUAGE, AppConstants.APP_LANG);
          // Navigator.pushReplacementNamed(context, '/homeScreen');

          try {
            print("trucate login json table");
            await UITemplateOperations.initDatabase();
            await UITemplateOperations().TruncateTemplateJsonTable();
            await UITemplateOperations().TruncateLoginJsonTemplateTable();
            await UITemplateOperations().TruncateProductTable();
            await UITemplateOperations().TruncateMakeTable();
            await UITemplateOperations().TruncateModelTable();
            await UITemplateOperations().TruncateDealerSOTable();
            await UITemplateOperations().TruncateMakeDealerDataTable();
            await UITemplateOperations().TruncateStatesTable();
            await UITemplateOperations().TruncateDealerLocationVDTable();
            await UITemplateOperations().TruncateModelVDTable();
            await UITemplateOperations().TruncateVehicleCategoryTable();
            await UITemplateOperations().TruncateYearVDTable();

            await UITemplateOperations().TruncateCityTable();
            //var  jsonObjectApplicant = '';
            Object jsonObjectApplicant = '';

            print("Start of insert template");

            await UITemplateOperations.SaveUpdatedTemplateInDatabase(responseModel, json.encode(jsonObjectApplicant));
            String jsonStringImages = "", jsonStringImagesUTW = "", jsonStatusUploadImages = "";
            String jsonPreUploadImages = "",
                jsonPostUploadImages = "",
                jsonVideosList = "";

            String jsonMenuDetails = doOtpresponseModel.menudetails;
            String jsonProductDetails =
                doOtpresponseModel.productToMakeToModelToOnPriceJsonArray;

            // var responseObject = json.encode(doOtpresponseModel);
            Map<String, dynamic> responseObject = doOtpresponseModel.toJson();
            Map<String, dynamic> loadhtmlresponseObject =
                responseModel.toJson();

            try {
              //Map<String,dynamic> jObject = json.decode(responseObject);
              if (loadhtmlresponseObject.containsKey("details")) {
                Map<String, dynamic> map2 = loadhtmlresponseObject["details"];
                if (map2.containsKey("leadDocuments")) {
                  jsonStringImagesUTW = map2["leadDocuments"].toString();
                  print("jsonStringImages" + jsonStringImagesUTW);
                }
              }

              if (responseObject.containsKey("leadDocuments")) {
                Map<String, dynamic> map2 = responseObject["leadDocuments"];
                //JObject jsonStringImagesObject = (JObject)responseObject.SelectToken("leadDocuments");
                jsonStringImages = map2.toString();
                print("leadDocuments in leads" + jsonStringImages);
              }

              if (responseObject.containsKey("Pre_Disb_DocumentList")) {
                Map<String, dynamic> map =
                    responseObject["Pre_Disb_DocumentList"];
                jsonPreUploadImages = map.toString();
                print("Pre_Disb_DocumentList" + jsonPreUploadImages);
              }

              if (responseObject.containsKey("Post_Disb_DocumentList")) {
                Map<String, dynamic> map =
                    responseObject["Post_Disb_DocumentList"];
                jsonPostUploadImages = map.toString();
                print("Post_Disb_DocumentList" + jsonPostUploadImages);
              }

              if (responseObject
                  .containsKey("Service_Inspection_DocumentList")) {
                Map<String, dynamic> map =
                    responseObject["Service_Inspection_DocumentList"];
                jsonVideosList = map.toString();
                print("Service_Inspection_DocumentList" + jsonVideosList);
              }

              if (responseObject.containsKey("Update_Lead_DocumentList")) {
                Map<String, dynamic> map =
                    responseObject["Update_Lead_DocumentList"];
                jsonStatusUploadImages = map.toString();
                print("Update_Lead_DocumentList" + jsonStatusUploadImages);
              }
            } catch (e) {
              print("Exception In loadFieldData()$e ");
            }

            print("save login menu table");
            await UITemplateOperations.SaveMenusInDatabase(
                "Menu Details", jsonMenuDetails);
            sp.setString(AppConstants.PRODUCTJSON, jsonMenuDetails);

            print("product id............." + product_Id);
            if(product_Id == '2'){
              print("images............");

              print(jsonStringImagesUTW);
              await UITemplateOperations.SaveMenusInDatabase(
                  "Images", jsonStringImagesUTW);
            }else{
              print("images");
              print(jsonStringImages);
              await UITemplateOperations.SaveMenusInDatabase(
                  "Images", jsonStringImages);
            }
            await UITemplateOperations.SaveMenusInDatabase(
                "PreImages", jsonPreUploadImages);
            await UITemplateOperations.SaveMenusInDatabase(
                "PostImages", jsonPostUploadImages);
            await UITemplateOperations.SaveMenusInDatabase(
                "VideoNames", jsonVideosList);
            await UITemplateOperations.SaveMenusInDatabase(
            "StatusUploadImages", jsonStatusUploadImages);

            // var jArrayDealerData = null;
            List<dynamic> jArrayDealerData = null;

            try {
              if (responseObject.containsKey("dealer_data")) {
                //save data in DealerSO table
                print("containsKey delaer" + responseObject.containsKey("dealer_data").toString());
                // Map<String, dynamic> map =responseObject["dealer_data"];
                List map = responseObject["dealer_data"];
                jArrayDealerData = map;
                await UITemplateOperations.SaveMenusInDatabase(
                    "dealer_data", jsonStatusUploadImages);
                print("jsonStatusUploadImages dealer_data" +
                    jArrayDealerData.toString());
              }
            } catch (e) {
              print("Exception In loadFieldData$e ");
            }

            try {
              print("In try of dealer_data_SO");
              if (responseObject.containsKey("dealer_data_SO")) {

                print("save data in DealerSO table");
                List map = responseObject["dealer_data_SO"];
                jArrayDealerData = map;
                print("jArrayDealerDataSO" + map.toString());
                if (jArrayDealerData.length > 0)
                {
                  Map dealerSO = jArrayDealerData[1];
                  if (dealerSO.containsKey("dealerBranch"))
                  {
                    print("in dealer branche");
                    String DealerBranch = dealerSO["dealerBranch"];
                    print(DealerBranch);
                    //sp.setString(AppConstants.DEALER_BRANCH, DealerBranch);
                  }

                  if (dealerSO.containsKey("dealer_State"))
                  {
                    print("in dealerstate");
                    String DealerState = dealerSO["dealer_State"];
                    print(DealerState);
                   // sp.setString(AppConstants.DEALER_STATE, DealerState);
                  }
                }
                await UITemplateOperations.AddDealerSODataAsync(map);
              }
            } catch (e) {
              //print(e.GetBaseException().ToString());
              print("Exception In loadFieldData $e");
            }
            if (doOtpresponseModel.productId == 1) {
              await SaveProductDataAsync(jsonProductDetails);
            }
            await SaveMakeFromDealerData(jArrayDealerData);

            await UITemplateOperations.SaveMenusInDatabase(
                AppConstants.LOGGED_IN_USER_ID,
                doOtpresponseModel.loggedInUserId);
            await UITemplateOperations.SaveMenusInDatabase(
                AppConstants.LOGGED_IN_USER_NAME, userName);
            await UITemplateOperations.SaveMenusInDatabase(
                AppConstants.TOKEN, doOtpresponseModel.token);
            await UITemplateOperations.SaveMenusInDatabase(
                AppConstants.LOGGED_IN_USER_ROLE,
                doOtpresponseModel.loggedInUserRole);

            sp.setString(AppConstants.LOGGED_IN_USER_ID,
                doOtpresponseModel.loggedInUserId);
            //  sp.setString(AppConstants.LOGGED_IN_USER_NAME, userIDController.text);
            sp.setString(
                AppConstants.LOGGED_IN_USER, doOtpresponseModel.loggedInUser);
            sp.setString(AppConstants.TOKEN, doOtpresponseModel.token);
            //Preferences.Set(AppConstants.APP_VERSION, doOTPLoginResponseModel.app_Version);
            sp.setString(AppConstants.LOGGED_IN_USER_ROLE,
                doOtpresponseModel.loggedInUserRole);
            sp.setString(AppConstants.CURRENT_PRODUCT_ID,
                doOtpresponseModel.productId.toString());
            sp.setBool(AppConstants.IS_NOTIFICATION_TOKEN_UPDATED, false);
            sp.setBool(AppConstants.IS_PRODUCT_LOADED_SUCESSFULLY, true);

            //   db.search();

           // Navigator.pushReplacementNamed(context, '/homeScreen');
          } on Exception catch (e) {
            print("Exception In main loadFieldData $e");
          }
          //getmobileotp(context, responseModel.customerdetail[0].agreementno);
        } else if (responseModel.success == false &&
            responseModel.redirect == false) {
          isSuccess = false;
          print("Incorrect");
          CustomSnackBar.showSnackBar(
              context, "Incorrect user ID. User does not exist at server");
          // await getmobileotp();
          return isSuccess;
        } else {
          CustomSnackBar.showSnackBar(context, "responseModel.details");
        }
      } else {
        CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
      }
    } else {
      CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
    }
    return isSuccess;
  }

  SaveMakeFromDealerData(List<dynamic> jArrayDealerData) async {
    print("start save make data");
    print(jArrayDealerData);
    var makeDealerData = new MakeDealerDataDB();
    if (jArrayDealerData != null) {
      for (int x = 0; x < jArrayDealerData.length; x++) {
        Map<String, dynamic> jArrayDealerDataObject = jArrayDealerData[x];

        if (jArrayDealerDataObject.containsKey("Make")) {
          List<dynamic> jArrayMake = jArrayDealerDataObject["Make"];

          for (int y = 0; y < jArrayMake.length; y++) {
            MakeDalerData make_dealer_data = new MakeDalerData();
            make_dealer_data.dealership_name =
                jArrayDealerDataObject["Dealership_Name"];
            make_dealer_data.dealer_type =
                jArrayDealerDataObject["Dealer_Type"];
            make_dealer_data.make_dealer_data_name = jArrayMake[y].toString();
            int id =
                await makeDealerData.insertMakeDealerData(make_dealer_data);
            // await db.insertMakeDalerData("Make_Dealer_Data", make_dealer_data);
            // print("make delaer data .." + id.toString());
          }
        }
      }
    }
    print("end save make data");
  }

  SaveProductDataAsync(String jsonProductDetails) async {
    // SharedPreferences sp = await SharedPreferences.getInstance();
    print("productJson" + jsonProductDetails);

    var productDB = await ProductDataDB();
    var makeDB = await MakeDB();
    var modelDB = await ModelDB();
    productArray = JSON.jsonDecode(jsonProductDetails);
    print("productArray");
    print(productArray);
    try {
      for (int i = 0; i < productArray.length; i++) {
        Map<String, dynamic> PObject = productArray[i];
        print(PObject);
        if (PObject.containsKey("Product")) {
          productName = PObject["Product"];
          print("productName " + productName);
        }

        if (PObject.containsKey("Make")) {
          make = PObject["Make"];
          print("Make " + make);
        }

        if (PObject.containsKey("Model")) {
          model1 = PObject["Model"];
          print("Model " + model1);
        }

        if (PObject.containsKey("Model_Variant")) {
          model_variant = PObject["Model_Variant"];
          print("model_variant " + model_variant);
        }

        if (PObject.containsKey("orp")) {
          onroadPrice = PObject["orp"];
          print("onroadPrice " + onroadPrice.toString());
          orp = onroadPrice.toString();
        }
        if ((productName.isNotEmpty && productName != null) &&
            (make.isNotEmpty && make != null) &&
            (orp.isNotEmpty && orp != null)) {
          print("start table insertion");
          //db1 = await dbHelper.database;

          String queryProduct =
              DatabaseQueries.CheckProductExistInDB(productName);
          List<Map<String, dynamic>> productlist =
              await DatabaseHelper().execute(queryProduct);

          print("productList");
          print(productlist);
          List<Map<String, dynamic>> data = await productDB.show();
          print("Product database..." + data.toString());

          if (productlist.length > 0) {
            print(productlist.length);
            print("if product");
            pid = productlist[0]['product_id'];
            print("pid");
            print(pid);
          } else {
            Product product = new Product();
            product.product_name = productName;
            int id = await productDB.insertProduct(product);
            print("insert data");
            print(id);
            if (id == 1) {
              print("id == 1");
              String Query = DatabaseQueries.GetMaxProductId();
              List<Map<String, dynamic>> maxId =
                  await DatabaseHelper().execute(Query);
              print("max id");
              print(maxId);
              pid = maxId[0]["LastId"];
              print(pid);
            }
          } //end product if

          String queryMake = DatabaseQueries.CheckMakeExistInDB(make);
          List<Map<String, dynamic>> makelist =
              await DatabaseHelper().execute(queryMake);
          List<Map<String, dynamic>> data1 = await makeDB.showMake();
          print("Make database..." + data1.toString());

          if (makelist.length > 0) {
            mkid = makelist[0]["make_id"];
            print("mkid" + mkid.toString());
          } else if (pid > 0) {
            MakeModel make1 = new MakeModel();
            make1.product_id = pid.toString();
            make1.make_name = make;
            int id = await makeDB.insertMake(make1);
            print("insert make data" + id.toString());
            if (id == 1) {
              String QueryMake = DatabaseQueries.GetMaxMakeId();
              List<Map<String, dynamic>> maxId =
                  await DatabaseHelper().execute(QueryMake);
              print("max id");
              mkid = maxId[0]["LastId"];
              print("mkid.." + mkid.toString());
            }
          } //end make if

          String queryModel = DatabaseQueries.CheckModelExistInDB(model1);
          List<Map<String, dynamic>> modellist =
              await DatabaseHelper().execute(queryModel);
          List<Map<String, dynamic>> data2 = await modelDB.showModel();
          print("Model database..." + data2.toString());

          if (modellist.length > 0) {
            moid = modellist[0]["model_id"];
          } else if (mkid > 0) {
            print("insert model");
            Model model1 = new Model();
            model1.make_id = mkid.toString();
            model1.model_name = model_variant;
            model1.on_road_price = orp;
            int id = await modelDB.insertModel(model1);
            if (id == 1) {
              String QueryModel = DatabaseQueries.GetMaxModelId();
              List<Map<String, dynamic>> maxId =
                  await DatabaseHelper().execute(QueryModel);
              moid = maxId[0]["LastId"];
              print("moid.." + moid.toString());
            }
          } //end model if
        } //main if
      } //end for loop
    } on Exception catch (e) {
      print("Exception In saveProduct $e");
    }
    print("end savePeoductAsync");
  } //end saveProduct

  getMakes() async{
    makeUTData().GetMake(context);
  }





}
