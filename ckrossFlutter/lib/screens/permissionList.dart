import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionService {
  final PermissionHandler _permissionHandler = PermissionHandler();

  Future<bool> _requestPermission() async {
    var result = await _permissionHandler.requestPermissions([PermissionGroup.camera,PermissionGroup.phone,PermissionGroup.storage,PermissionGroup.sms,PermissionGroup.microphone]);
    print("result");
    print(result);
    if (result == PermissionStatus.granted) {

      print("request permission");

      return true;
    }
    print("false request permission");

    return false;
  }

  Future<bool> requestPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission();
    print(" permission");
    if (!granted) {
      onPermissionDenied();

    }
    return granted;
  }

  static Future<bool> hasPermission(List<PermissionGroup> permissionList) async {
    print("check permission");
    final PermissionHandler _permissionHandler = PermissionHandler();
    Map<PermissionGroup, PermissionStatus> permission = await PermissionHandler().requestPermissions(permissionList);
    return false;
  }




}
