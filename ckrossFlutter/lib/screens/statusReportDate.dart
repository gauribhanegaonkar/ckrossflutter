import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/screens/StatusReportUsedTW.dart';
import 'package:ckrossFlutter/screens/statusReportDetails.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StatusReportDate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: () {
        Navigator.pushReplacementNamed(context, '/homeScreen');
      },
      child: Scaffold(
        appBar: new AppBar(
            backgroundColor: Colors.blue,
            title: new Text('Status Report Date Filter')),
        body: StatusReport(),
      ),
    );
  }
}

class StatusReport extends StatefulWidget {
  @override
  StatusReportState createState() => StatusReportState();
}

class StatusReportState extends State<StatusReport> {
  final _formKey = GlobalKey<FormState>();

  String currentProductID;
  SharedPreferences sp;
  TextEditingController dobControllerfrom = new TextEditingController();
  TextEditingController dobControllerto = new TextEditingController();
  DateTime selectedDate;
  String fromDate;
  String endDate;

  bool isChecked = false,
      isfromdate = true,
      istodate = true,
      isfromvalid = false,
      istovalid = false;

  DateTime selectedFromDate;
  DateTime selectedToDate;
  DateTime pickedFrom;
  DateTime pickedTo;

  @override
  void initState() {
    super.initState();

    initFillDate();
    setCurrentId();
  }

  initFillDate() {
    selectedFromDate = DateTime.now();
    var formatter = new DateFormat('dd-MM-yyyy');
    String fromDate = formatter.format(selectedFromDate);
    dobControllerfrom.text = fromDate;

    selectedToDate = DateTime.now();
    var formatter1 = new DateFormat('dd-MM-yyyy');
    String endDate = formatter1.format(selectedToDate);
    dobControllerto.text = endDate;
  }

  setCurrentId() async {
    try {
      sp = await SharedPreferences.getInstance();
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
    }on Exception catch(e){
      print("Exception In InitializeAppData " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
//    return Form(
//
//      key: _formKey,
//      child: SingleChildScrollView(
//        child: Column(
//
//          children: <Widget>[
//            CustomerProfileWidget()
//          ],
//        ),
//      ),
//    );
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[CustomerProfileWidget()],
      )),
    );
  }

  Widget CustomerProfileWidget() {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10),
          height: 310,
          child: Container(
            // elevation: 3,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.orange,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 0, 10, 10),
                  child: RichText(
                    text: TextSpan(
                      text: 'Start Date',
                      style: TextStyle(color: Colors.orange, fontSize: 15),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 0, 15, 5),
                  child: TextFormField(
                    controller: dobControllerfrom,
                    // onTap: () => _selectFromDate(context),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.orange,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.orange,
                        ),
                      ),
                      errorText: isfromvalid
                          ? validatefrom(dobControllerfrom.text)
                          : null,
                      //labelText: getTranslated(context, 'DATE_OF_BIRTH'),
                      suffixIcon: IconButton(
                        icon: Icon(Icons.date_range),
                        color: Colors.orange,
                        onPressed: () => _selectFromDate(context),
                        padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                      ),
                    ),
                    showCursor: false,
                    readOnly: true,
                    style: TextStyle(fontSize: 18),
                    validator: (value) {
                      if (value.isEmpty || value.length == 0) {
                        return 'Please enter Date of Birth';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 10, 10, 10),
                  child: RichText(
                    text: TextSpan(
                      text: 'End Date',
                      style: TextStyle(color: Colors.orange, fontSize: 15),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 0, 15, 5),
                  child: TextFormField(
                    controller: dobControllerto,
                    // onTap: () => _selectToDate(context),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.orange,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.orange,
                        ),
                      ),
                      errorText:
                          istovalid ? validateto(dobControllerto.text) : null,
                      //labelText: getTranslated(context, 'DATE_OF_BIRTH'),
                      suffixIcon: IconButton(
                        icon: Icon(Icons.date_range),
                        color: Colors.orange,
                        onPressed: () => _selectToDate(context),
                        padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                      ),
                    ),
                    showCursor: false,
                    readOnly: true,
                    style: TextStyle(fontSize: 18),
                    validator: (value) {
                      if (value.isEmpty || value.length == 0) {
                        return 'Please enter Date of Birth';
                      } else if (pickedTo.isAfter(pickedFrom)) {
                        return 'End Date should be >= Start Date';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      width: 300,
                      height: 40,
                      child: RaisedButton(
                        color: Colors.blue[900],
                        child: Text(
                          'Submit',
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                        onPressed: () {
                          Handle_Submit_Clicked();
                          // Navigator.pushReplacementNamed(context, '/statusReportDetail');
                        },
                      ),
                    ),
                  ],
                ),
                //Container(height: 100)
              ],
            ),
          ),
        )
      ],
    );
  }

  String validatefrom(String value) {
    if (value.isEmpty) {
      return "Please select date";
    }
    return null;
  }

  String validateto(String value) {
    if (value.isEmpty) {
      return "Please select date";
    } else if (pickedTo.isBefore(pickedFrom)) {
      return 'End Date should be >= Start Date';
    }
    return null;
  }

  Future<Null> _selectFromDate(BuildContext context) async {
    pickedFrom = await showDatePicker(
        context: context,
        initialDate:
            selectedFromDate == null ? DateTime.now() : selectedFromDate,
        firstDate: DateTime(1950),
        lastDate: DateTime.now());
    if (pickedFrom != null) {
      setState(() {
        selectedFromDate = pickedFrom;
        var formatter = new DateFormat('dd-MM-yyyy');
        String fromDate = formatter.format(pickedFrom);
        dobControllerfrom.text = fromDate;
        isfromvalid = true;
      });
    }
  }

  Future<Null> _selectToDate(BuildContext context) async {
    pickedTo = await showDatePicker(
        context: context,
        initialDate: selectedToDate == null ? DateTime.now() : selectedToDate,
        firstDate: DateTime(1950),
        lastDate: DateTime.now());
    if (pickedTo != null) {
      setState(() {
        selectedToDate = pickedTo;
        var formatter = new DateFormat('dd-MM-yyyy');
        String formattedDate = formatter.format(pickedTo);
        dobControllerto.text = formattedDate;
        istovalid = true;
      });
    }
  }

  Handle_Submit_Clicked() async {
//    if (String.IsNullOrEmpty(dobControllerfrom) || String.IsNullOrEmpty(dobControllerto)) {
//      StartDateEntry.ErrorText = "*";
//      EndDateEntry.ErrorText = "*";
//      return;
//    }

    DateTime tempStartDate = selectedFromDate;
    DateTime tempEndDate = selectedToDate;
    print(tempStartDate);
    print(tempEndDate);
    if (tempStartDate.isBefore(tempEndDate)) {
      DateTime tempStartDate1 = selectedFromDate.toUtc();
      DateTime tempEndDate1 = selectedToDate.toUtc();
      print("utc");
      print(tempStartDate1);
      print(tempEndDate1);

      var formatter = new DateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
      String formattedStartDate = formatter.format(tempStartDate1);
      String formattedEndDate = formatter.format(tempEndDate1);
      String utcStartDate = formattedStartDate;
      String utcEndDate = formattedEndDate;

      print("formatted date...");
      print(utcStartDate);
      print(utcEndDate);

      List<String> temp = new List<String>();
      temp.add(utcStartDate);
      temp.add(utcEndDate);

      if (currentProductID == "1") {
        print(currentProductID);
       // Navigator.pushReplacementNamed(context, '/statusReportDetail');
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (BuildContext context) => new StatusRepoetListView(temp)));
      } else if (currentProductID == "2")
      {
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (BuildContext context) => new StatusReportUsedTW(temp)));

      }

    } else {
      showdialog("End Date should be >= Start Date");
    }//end main if
  } //end of handle submit

  showdialog(String text) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
