import 'dart:convert';
import 'dart:io';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/Utils/imageUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/loginJson_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/models/more_options.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';

class LeadCreationImagePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: LeadCreationImagePageStateFul()
    );
  }



}
class LeadCreationImagePageStateFul extends StatefulWidget {
  LeadCreationImagePageStateFul({Key key}) : super(key: key);
  @override
  LeadCreationImagePageState createState() => LeadCreationImagePageState();
}

  class LeadCreationImagePageState extends State<LeadCreationImagePageStateFul> with SingleTickerProviderStateMixin ,WidgetsBindingObserver{

  String fromPange;
  String caseID;
  String tempCaseID;
  String type;
  String jsonObject;
  String customerName;
  String SourceBy;
  BuildContext context;

  static String userId;
  static String appLang;
  static String userRole;
  static String currentProductID;
  static String currentProductName;
  static String nbfcName;

  static DatabaseHelper db = DatabaseHelper();
  var imageListNew = new List<Map<String,String>>();
  var subImages ;
  List<MoreOptions> choiceList = [];
  Map<int, dynamic> productNames = new Map<int, dynamic>();
  List<String> imgs;



  LeadCreationImagePage(String fromPage, String caseID, String tempCaseID, String type, bool isInitialize,
      Object jsonObject, String customerName, String sourceBy)
  {
    try
    {
     // logger = DependencyService.Get<ILogManager>().GetLog();
      this.fromPange = fromPage;
      this.caseID = caseID;
      this.tempCaseID = tempCaseID;
      this.type = type;
      this.jsonObject = jsonObject;
      this.customerName = customerName;
      this.SourceBy = sourceBy;

     // cKrossStoreLoginJsonTemplate = new SQLiteLoginJsonTemplate(DependencyService.Get<ISQLiteDb>());

     // InitializeComponent();

    //  setTitle();
    }
    catch (e)
    {
   // e.GetBaseException();
    print("Exception In UploadImagePageNew");
    }
  }

@override
  void initState() {
    // TODO: implement initState
  GetListOfImages();
  AddToolbarMenusDynamically();

  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String newTW = 'New TW';
    // TODO: implement build
    return  Scaffold(
        appBar: new AppBar(
        backgroundColor: Colors.blue,
        title: new Text('CKROSS LOS'),
    actions: <Widget>[
    Padding(
    padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
    child: Text(newTW),
    ),
    Theme(
    data: Theme.of(context).copyWith(
    cardColor: Colors.black,
    iconTheme: IconThemeData(color: Colors.white),
    ),
    child: PopupMenuButton<MoreOptions>(
    onSelected: choiceAction,
    itemBuilder: (BuildContext context) {
    return choiceList.skip(0).map((MoreOptions choice) {
    return PopupMenuItem<MoreOptions>(
    value: choice,
    child: Text(choice.name, style: TextStyle(color: Colors.white)),
    );
    }).toList();
    },
    )),
    ],
    ),
    body:ShowListOfImages(context),
    );;

  }

  static InitializeAppData() async {
    try{

      print("InitializeAppData method");
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      appLang = !(tempLanguage==null || tempLanguage.isEmpty) ?tempLanguage:AppConstants.APP_LANG;
      var tempUserId = sp.getString(AppConstants.LOGGED_IN_USER_ID);

      if(!(tempUserId==null||tempUserId.isEmpty))
      {
        print(tempUserId);
        userId = tempUserId;
      }
      else{
       // String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ID);
         String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ID, appLang);
        List<LoginJsonTemplate> listUser = await db.execute(Query);
        print("listUser"+listUser.toString());
        if (listUser.length > 0)
        {
          userId = listUser[0].jsonValue;
        }
      }
      var tempnbfcName = sp.getString(AppConstants.CLIENT_NAME);
      nbfcName = !(tempnbfcName==null||tempnbfcName.isEmpty) ? tempnbfcName : AppConstants.CLIENT_NAME;
      var tempUserRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      if (!(tempUserRole==null||tempUserRole.isEmpty))
      {
        userRole = tempUserRole;
        print("userRole"+userRole);
        // db.search();
      }
      else
      {
       // String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ROLE);
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(AppConstants.LOGGED_IN_USER_ROLE, appLang);
        List<LoginJsonTemplate> listUserRole = await db.execution(Query);
        if (listUserRole.length > 0)
        {
          userRole = listUserRole[0].jsonValue;
        }
      }
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
      currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
      print("End of InitializeAppData ");
    }on Exception catch (_){
      print("Exception In InitializeAppData");
    }
  }


  GetListOfImages()
   async {
    try
    {
      InitializeAppData();
    //  FileUtils.printLog("In GetListOfImages, Type is " + type);
     // String imagesJson = null;
      String imageType = "Images";
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      appLang = !(tempLanguage==null || tempLanguage.isEmpty) ?tempLanguage:AppConstants.APP_LANG;
     // String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(imageType);
      String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(imageType,appLang);
      List<Map<String,dynamic>> listLoginJson = await LoginJsonTemplateDB.executeLoginJsonTemplateImageData(Query);

    //  List<LoginJsonTemplate> listLoginJson = await cKrossStoreLoginJsonTemplate.ExecuteQueryAsync(Query);
      if (listLoginJson.length > 0)
      {

        var imagesJson = listLoginJson[0]['jsonValue'];
        print(imagesJson);
        if (imagesJson != null && !imagesJson.isEmpty )
        {
          try
          {

            String value = imagesJson.toString();
            value = value.toString().substring(1, value.length-1);           //remove curly brackets
            var keyValuePairs = value.split(",");              //split the string to creat key-value pairs
            Map<String,String> list = new Map();

            for(int i = 0; i < keyValuePairs.length; i++)         //iterate over the pairs
                {
              var entry = keyValuePairs[i].split(":");                   //split the pairs to get key and value
              list.putIfAbsent(entry[0].trim(),()=> entry[1].trim() );          //add them to the hashmap and trim whitespaces
            }

            List<String> keys =list.keys.toList();
            var imageList = new List<Map<String,String>>();
            List<Map<String,String>> arrayImages = new List<Map<String,String>>(list.length);

            for (int j = 0; j < keys.length; j++)
            {
              String keyValue = keys[j];
              var map = <String,dynamic>{};
              map.addAll(list);
              String valueString = map[keyValue].toString();
              Map<String, String> dictImages = new Map<String, String>();
              if (keyValue.contains("__"))
              {
                var arrKeys = keyValue.split("__");
                int seq_index = int.parse(arrKeys[0]);
                int pos = seq_index - 1;


                dictImages.putIfAbsent('imageName', () => valueString);
                dictImages.putIfAbsent('imageKey', () => keyValue);
                arrayImages[pos] = dictImages;

              }
              else
              {
                  dictImages.putIfAbsent('imageName', () => valueString);
                  dictImages.putIfAbsent('imageKey', () => keyValue);
                  arrayImages[j] = dictImages;

              }
            }

            for (int i = 0; i < arrayImages.length; i++)
            {
              if (arrayImages[i] != null)
              {
                imageList.add(arrayImages[i]);
              }
              setState(() {
                imageListNew = imageList;
              });
            }
            //return ShowListOfImages(imageList,context);
          }
    catch (e)
    {
   // e.GetBaseException();
    FileUtils.printLog("Exception occured In GetListOfImages" + e.GetBaseException());
    }
    }
    else
    {
    FileUtils.printLog("In GetListOfImages, lead ...........documents list is blank.");
    await DisplayAlert(AppConstants.ALERT, "lead documents list is blank.", AppConstants.OK,context);
    }
    }
    else
    {
    FileUtils.printLog("In GetListOfImages, lead documents list is blank.");
    await DisplayAlert(AppConstants.ALERT, "lead documents list is blank.", AppConstants.OK,context);
    }
    }
    catch (e)
    {
   // e.GetBaseException();
    FileUtils.printLog("Exception occured In GetListOfImages" + e.toString());
    }
  }

  Widget ShowListOfImages(BuildContext context) {
   String fileName;
   return Column(
     children: <Widget>[
  Expanded(child:
   SingleChildScrollView(
   child:ListView.builder(
       shrinkWrap: true, // 1st add
       physics: ClampingScrollPhysics(),
       padding: const EdgeInsets.all(8),
       itemCount: imageListNew.length,
       itemBuilder: (context,   index) {
         fileName = imageListNew[index]['imageName'];
         return Card(
           child:Column(
             children: <Widget>[
               Container(
                 height: 70,
                 child: Row(
                   children: <Widget>[
                     Icon(Icons.camera_alt,size: 50,),
                     SizedBox(width: 20),
                     Expanded(
                       child: Text('${imageListNew[index]['imageName']}',style: TextStyle(fontSize: 13),),
                     ),
                     SizedBox(width: 10),
                     IconButton(icon:Icon(Icons.add_circle_outline),
                       color: Colors.cyan,
                       onPressed: () =>
                       {

                         showDialog(
                           context: context,
                           builder: (context) {
                             return Dialog(
                               shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.circular(10)),
                               elevation: 10,
                               child:Container(
                                 height: 100,
                                 width: 180,
                                 child:Column(
                                   children: <Widget>[
                                     SizedBox(
                                       width: double.infinity,
                                       child: FlatButton(
                                           child: Text('Select from gallary'),
                                           onPressed:() async {

                                             bool result = await ImageUtils.getAndSaveImageFromGallary(index,imageListNew[index]['imageName']);
                                             if(result) {
                                               Navigator.pop(context,true);
                                               Navigator.pushReplacementNamed(context,'/leadCreationImagePage');
                                               print("done");
                                             }
                                           }
                                       ),
                                     ),
                                     SizedBox(
                                       width: double.infinity,
                                       child: FlatButton(
                                           child: Text('Capture from camera'),
                                           onPressed: () async {
                                             bool result = await ImageUtils.getAndSaveImageFromCamera(index,imageListNew[index]['imageName']);
                                             if(result) {
                                               Navigator.pop(context,true);
                                               Navigator.pushReplacementNamed(context,'/leadCreationImagePage');
                                               print("done");

                                             }
                                           }
                                       ),
                                     ),
                                   ],
                                 ),
                               ),
                             );

                             //   ImageUtils.getAndSaveImage(),
                           },
                         ),

                       },),
                   ],
                 ),

               ),
               new innerOneWidget(subDirImages:getImages(fileName)),

             ],
           ),
         );

       }
   ),
    ),
  ),
        Align(

           alignment: FractionalOffset.bottomCenter,
           child: MaterialButton(
             minWidth: double.infinity,
             child: Text('Submit'),
             color: Colors.cyan,
             onPressed: () => {
               for(int i = 0; i<imageListNew.length; i++){
                  createPDF(imageListNew[i]['imageName'], context),

               }

             },

           ),

       )

     ],
   );

  }

  Future<void> DisplayAlert(String alert, String s, String ok, BuildContext context) async{
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );

  }

    List<String> getImages(String imageDirName){
     final String path = '/storage/emulated/0/CKross_Wheels_EMI/Images/$imageDirName';

     final myDir = new Directory(path);
    subImages = new List<String>();
     if(myDir.existsSync()) {
       //subImages = new List<String>();
       var images;
       images = myDir
           .listSync()
           .map((item) => item.path)
           .where((item) => item.endsWith(".jpg"))
           .toList(growable: false);;

       subImages = images;

     }else{
       subImages.clear();
     }
     return subImages;
   }

  choiceAction(MoreOptions choice) async{
    print("choice");
    switch (choice.name) {
      case 'New TW':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);

        break;
      case 'Used TW':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);

        break;
      case 'Send Log':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);
        attachFileAndSendMail();
        break;
      case 'Log Out':
        Fluttertoast.showToast(
            msg: "This is Toast message ${choice.name}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0);

        break;
    }
  }

  void attachFileAndSendMail() async {
    List<File> file = await FileUtils.getFile();
    List<String> arr = [];
    for(int i =0; i < file.length; i++){
      print(file[i]);
      arr.add(file[i].path);

    }
    print(arr);
    Email email;

    email = Email(
      //   body: 'Email body',
      subject: 'Log File',
      //   recipients: ['example@example.com'],
      //   cc: ['cc@example.com'],
      //   bcc: ['bcc@example.com'],
      attachmentPaths: arr,

      isHTML: false,
    );

    await FlutterEmailSender.send(email);

    // await androidMethodChannel.invokeListMethod('sahreFile',{'filepatth':file.path});
  }

  AddToolbarMenusDynamically() async
  {
    print("Add tool bar");
    productNames = await GetProductNamesFromDb();
    try {
      setState(() {
        print("tool bar");
        print(productNames);
        if (productNames.length > 0) {
          productNames.forEach((k, v) => choiceList.add(MoreOptions(k, v)));
          choiceList.add(new MoreOptions(2, "Send Log"));
          choiceList.add(new MoreOptions(3, "Log Out"));
          print(choiceList);
        }
      });
    } on Exception catch (e) {
      print("Exception in AddToolbarMenusDynamically() $e ");
    }
  }

  Future GetProductNamesFromDb() async {
    Map<String, dynamic> productNames = new Map<String, dynamic>();

    String Query =
    DatabaseQueries.CreateGetLoginJsontemplateMenuJson("Menu Details");

    List<Map<String, dynamic>> ListMenu = await DatabaseHelper().execute(Query);
  
    if (ListMenu.length > 0) {
      String MenuDetailsJson = ListMenu[0]["jsonValue"];
      Map<String, dynamic> jsonObject = json.decode(MenuDetailsJson);
      List jArray = jsonObject['GroupToCategoryToSubCategory'];

      if (jArray != null && jArray.length != 0) {
        for (int i = 0; i < jArray.length; i++) {
          Map<String, dynamic> productJson = jArray[i];
          if (productJson["categoryName"].toString().contains("Product")) {
            List productArray = productJson["SubCategory"];
            for (int j = 0; j < productArray.length; j++) {
              Map<String,dynamic> productJson1 = productArray[j];
              print("productjson1");
              print(productJson1);

              String productID = productJson1["id"].toString();
              String productName = productJson1["name"];
              productNames.addAll({productID:productName});

              print(productNames);
            }
          }
        }
      }
    }
    return productNames;
  }

  createPDF(String folderName, BuildContext context) async {

      //Create a PDF document.
      final String directory = '/storage/emulated/0/CKross_Wheels_EMI/Images/$folderName';
      final Directory _appDocDirFolder = Directory('${directory}');
      if(await _appDocDirFolder.exists()){
      imgs = new List();
      _getImages(_appDocDirFolder);
      if(imgs.length > 0) {
        final pdf = pw.Document();
        for (int i = 0; i < imgs.length; i++) {
          var pieces = imgs[i].split("/");
          final image = PdfImage.file(
            pdf.document,
            bytes: File(imgs[i]).readAsBytesSync(),
          );
          pdf.addPage(pw.Page(
              pageFormat: PdfPageFormat.a4,
              build: (pw.Context context) {
                return pw.Column(
                    children: [
                      pw.Text(pieces[pieces.length - 1],
                        textAlign: pw.TextAlign.left,
                          style: pw.TextStyle(fontSize: 21)
                      ),
                      pw.Container(width: 600,
                        height: 800,
                        child: pw.Image(image, width: 700, height: 700),),

                    ]
                ); // Center
              })); // Page

        }

        if (await _appDocDirFolder.exists()) {
          String path = _appDocDirFolder.path;
          File file = File('$path/$folderName.pdf');
          await file.writeAsBytes(pdf.save());
          CustomSnackBar.showSnackBar(context, "PDF created.");
          FileUtils.printLog("PDF of all images has been created.");
          //OpenFile.open('$path/Output.pdf');
          //  document.dispose();
        } else {
          return null;
        }
      }
      }
  }

  Future<void> _getImages(Directory appDocDirFolder) async {
     var imageList = appDocDirFolder
          .listSync()
          .map((item) => item.path)
          .where((item) => item.endsWith(".jpg"))
          .toList(growable: false);

      setState(() {
        imgs = imageList;
        FileUtils.printLog("Get all images from directory.");
      });
    }
  }

 class innerOneWidget extends StatelessWidget {
   innerOneWidget({
    Key key,
    @required this.subDirImages,
  }) : super(key: key);


  final List<String> subDirImages;
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return   Container(
      height: 150,
      child:
      ListView.builder( scrollDirection: Axis.horizontal,
          itemCount: subDirImages.length,
        itemBuilder: (context, index) =>Container(child: Column(
          children: <Widget>[
          Padding(padding: EdgeInsets.all(1),
            child:
            Image.file(File(subDirImages[index]), height: 100, width: 80,fit: BoxFit.fill,),),
        IconButton(icon:Icon(Icons.delete),
          color: Colors.red,
        iconSize: 20,
        onPressed: (){

          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text("Delete image"),
                content: new Text("Are you sure to delete image?"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Cancel"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  new FlatButton(
                    child: new Text("Ok"),
                    onPressed: () {
                      final dir = Directory(File(subDirImages[index]).path);
                      dir.deleteSync(recursive: true);
                      FileUtils.printLog("Image saved to directory.");
                      Navigator.pop(context,true);
                      Navigator.pushReplacementNamed(context,'/leadCreationImagePage');
                    },
                  ),
                ],
              );
            },
          );

        },) ],),)
    ),
    );

  }
}
