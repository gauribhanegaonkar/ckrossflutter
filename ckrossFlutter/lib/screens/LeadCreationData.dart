import 'dart:collection';
import 'dart:convert';
import 'dart:convert' as JSON;
import 'dart:io' show HttpStatus, Platform;
import 'package:ckrossFlutter/CustomWidgets/dialogs.dart';
import 'package:ckrossFlutter/CustomWidgets/snackBar.dart';
import 'package:ckrossFlutter/Utils/Headers.dart';
import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/databaseQueries.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/database/all_tables_queries/ckross_casedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/client_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/dealer_SOdataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/dyanamic_table_fielddataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/loginJson_TemplatedataDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/makeDB.dart';
import 'package:ckrossFlutter/database/all_tables_queries/templateJsonDB.dart';
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/ResponseModels/getUniqueColumnData.dart';
import 'package:ckrossFlutter/models/ckrossCase.dart';
import 'package:ckrossFlutter/models/dyanamic_table_field.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/screens/homeScreen.dart';
import 'package:ckrossFlutter/screens/newTW/panImageUpload.dart';
import 'package:ckrossFlutter/webUtils/apiServices.dart';
import 'package:ckrossFlutter/webUtils/networkUtils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'leadCreationPages/LeadCreation_seq_id_2.dart';

class LeadCreationData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LeadCreationDataStateful());
  }
}

class LeadCreationDataStateful extends StatefulWidget {
  String _selection;
  LeadCreationDataStateful({Key key}) : super(key: key);

  @override
  _LeadCreationDataState createState() => new _LeadCreationDataState();
}

class _LeadCreationDataState extends State<LeadCreationDataStateful>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  DatabaseHelper db = DatabaseHelper();
  var list;
  final _formKey = GlobalKey<FormState>();
  TextEditingController controllers = new TextEditingController();
  // var textEditingControllers = <TextEditingController>[];
  Map<String, TextEditingController> textEditingControllers = {};
  Map<String, TextEditingController> textEditingControllerTextField = {};
  String case_id,
      allMandatoryKeys,
      client_template_id,
      templateName,
      currentProductName,
      userId,
      appLang,
      userName,
      nbfcName,
      databaseName,
      userRole,
      currentProductID,
      controllerName,
      jsonTemplateId,
      allFieldsKeys;
  String applicantJson = null, detailsJson = null;
  final _key = GlobalKey();
  List<Widget> children = [];
  List<Widget> labelchildren = [];
  var controllerList = [];
  var placeholderName;
  var dealershipName = "",
      dealerName = "",
      dealerBranch = "",
      Location = "",
      Dealer_Code = "",
      SourcingChannel = "",
      dealer_State = "",
      dealerAddress = "",
      DealersBankDetails,
      DisbursementTO = "",
      DealerBankName = "",
      DealerBankAccountNo = "",
      DealerIFSCCode = "",
      DealerBranchName = "",
      DealerMICRCode = "",
      Type_Of_Account = "",
      dealerDistrictname = "",
      rto_by_whom = "",
      Insurance_by_whom = "";

  static String _selection, text, LeadID, user;
  var validationMessageDropdown = "",
      validationPatternDropdown = "",
      textEditingControllerText,
      activePlaceholderColor,
      placeholderColor,
      textEditingControllerLabel,
      placeholder,
      textfiledtext,
      validationMessage = "",
      validationpattern = "",
      maxlength;
  var keyboard, value;
  SharedPreferences sp;
  bool isEnabled, isData = false, isValid = true, isEnabledDropdown;
  var product, make, model;
  var seq_id;
  List makeList;
  String LeadId;
  var labeltext,
      res,
      returned,
      editingcomplete,
      keyboardDropdown,
      maxlengthDropdown,
      arr = [];
  int _count = 0;
  Map mapFormData = Map();
  TextEditingController dobControllerfrom = new TextEditingController();

  DateTime selectedDate;
  String fromDate;

  DateTime selectedFromDate;
  DateTime selectedToDate;
  DateTime pickedFrom;

  bool isChecked = false,
      isfromdate = true,
      istodate = true,
      isfromvalid = false,
      istovalid = false;

  FocusNode _focus = new FocusNode();

  void initState() {
    super.initState();

    initData();
  }

  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    //children.clear();
  }



  var _chosenValue;
  Widget build(BuildContext context) {
    //_selection=widget._selection;
    return WillPopScope(
        onWillPop: () {
          Navigator.pushReplacementNamed(context, '/homeScreen');
          children.clear();
          //ImageUtils.deleteFolders();
        },
        child: Scaffold(
            appBar: AppBar(
              title: Text("Create Lead"),
            ),
            body: Form(
                key: _formKey,
                child: Container(
                  margin:
                      EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 0),
                  child: SingleChildScrollView(
                      child: Column(
                    children: <Widget>[
                      Column(
                          key: _key,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: children),
                      Container(
                        margin: EdgeInsets.only(
                            left: 0, top: 20, right: 0, bottom: 0),
                        width: double.infinity,
                        child: RaisedButton(
                            color: Colors.lightBlue[900],
                            child: Text("NEXT",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18)),
                            onPressed: () {

                             // if (_formKey.currentState.validate()) {
                               // print("validate");
                                _formKey.currentState.save();
                                print("Successfullly");
                                print(mapFormData);
                                saveFromData();
//                                Navigator.push(
//                                    context,
//                                    MaterialPageRoute(
//                                      builder: (context) => LeadCreationDataSeqid2(mapFormData),
//                                    ));
                           //   }

                            }),
                      ),
                    ],
                  )),
                ))));
  }

  initData() async {
    try {
      print("initData method");

      AppConstants.IsValidData = null;
      LeadId = await getLead();

      await getTemplateFromDatabaseAsync();

    } on Exception catch (_) {
      print("Exception in InitComponents");
    }
  }
  intiSetBranchAndState() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    setState(() {

        if(sp.getString(AppConstants.PAN_No) != ""){
        textEditingControllerTextField["PAN_No"].text = sp.getString(AppConstants.PAN_No);
        textEditingControllerTextField["FirstName"].text = sp.getString(AppConstants.FirstName);
        textEditingControllerTextField["LastName"].text = sp.getString(AppConstants.LastName);
        dobControllerfrom.text = sp.getString(AppConstants.app_Plain__DateOfBirth);

        }
        else{
          print("nulllllllll");
        }

    });
  }

  getTemplateFromDatabaseAsync() async {
    try {
      print("getTemplateFromDatabaseAsync method");
      templateName = AppConstants.UPLOAD_TEMPLATE_NAME;
      await InitializeAppData();
      if (!mounted) {
        return;
      }
      String version_no = "0";
      String query1 = DatabaseQueries.CreateSingleClientTemplateAccVerionNoQuery(templateName, nbfcName, appLang, version_no, "false");
      List listClientTemplate = await clientTemplateDB.executeClientTemplateData(query1);

      var getFormNameArray = <String, dynamic>{};
      getFormNameArray.addAll(listClientTemplate[0]);
      var abc = getFormNameArray["form_name_array"].toString();
      abc = abc.substring(1, abc.length - 1);

      if (listClientTemplate.length > 0) {
        var map = <String, dynamic>{};
        map.addAll(listClientTemplate[0]);
        Map<String, dynamic> clientTemplate = map;
        applicantJson = clientTemplate['applicant_json'];
        detailsJson = clientTemplate['details_json'];
        client_template_id = clientTemplate['client_template_id'].toString();
        var formNameArray = abc.split(',');

        // String query2 = DatabaseQueries.CreateGetTemplateByformname(client_template_id);

        String query2 = DatabaseQueries.CreateGetTemplateByClientTemplateId(
            client_template_id);
        List templateJsonArrayList =
            await TemplateJsonTable.executeTemplatejsonData(query2);

        if (templateJsonArrayList.length > 0) {
          await createUIFromDbTemplate(templateJsonArrayList, formNameArray);
        }
      }
    } on Exception catch (_) {
      print("Exception in getTemplateFromDatabaseAsync");
    }
  }

  InitializeAppData() async {
    try {
      print("InitializeAppData method");
      SharedPreferences sp = await SharedPreferences.getInstance();
      var tempLanguage = sp.getString(AppConstants.LANGUAGE);
      appLang = !(tempLanguage == null || tempLanguage.isEmpty)
          ? tempLanguage
          : AppConstants.APP_LANG;
      var tempUserId = sp.getString(AppConstants.LOGGED_IN_USER_ID);

      if (!(tempUserId == null || tempUserId.isEmpty)) {
        userId = tempUserId;
      } else {
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ID, appLang);
        List listUser = await db.execution(Query);
        if (!mounted) {
          return;
        }
        if (listUser.length > 0) {
          userId = listUser[0].jsonValue;
        }
      }

      var tempnbfcName = sp.getString(AppConstants.CLIENT_NAME);
      nbfcName = !(tempnbfcName == null || tempnbfcName.isEmpty)
          ? tempnbfcName
          : AppConstants.CLIENT_NAME;
      var tempUserRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      if (!(tempUserRole == null || tempUserRole.isEmpty)) {
        userRole = tempUserRole;
      } else {
        String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
            AppConstants.LOGGED_IN_USER_ROLE, appLang);
        List<LoginJsonTemplate> listUserRole = await db.execution(Query);
        if (listUserRole.length > 0) {
          userRole = listUserRole[0].jsonValue;
        }
      }
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
      currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
      print("End of InitializeAppData ");
    } on Exception catch (_) {
      print("Exception In InitializeAppData");
    }
  }

  createUIFromDbTemplate(templateJsonArrayList, formNameArray) async {
    try {
      print("createUIFromDbTemplate");
      String dealer_data_json = null;
      String Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson(
          "dealer_data", appLang);
      List listLoginJson =
          await LoginJsonTemplateDB.executeLoginJsonTemplateData(Query);
      if (listLoginJson.length > 0) {
        dealer_data_json = listLoginJson[0].jsonValue;
      }
      if (!(dealer_data_json == null || dealer_data_json.isEmpty)) {
        bool isSourcingActive = false;
        Map<String, String> dealerJsonObject = json.decode(dealer_data_json);
        if (dealerJsonObject.containsKey("Is_Sourcing_Active")) {
          isSourcingActive = dealerJsonObject["Is_Sourcing_Active"] as bool;
        }

        if (!isSourcingActive) {
          showDialogs();
          //await Navigation.PopAsync();
        }
      }
      await InitializeAppData();
      for (int j = 0; j < formNameArray.length; j++) {
        for (int i = 0; i < templateJsonArrayList.length; i++) {
          var templateJson = <String, dynamic>{};

          templateJson.addAll(templateJsonArrayList[i]);
          if (formNameArray[j].toString().replaceAll(" ", "") ==
              templateJson["form_name"].toString().replaceAll(" ", "")) {
            String tableExist = templateJson["istable_exists"];
            String template_json_id = templateJson["template_json_id"].toString();

            DynamicTableFields dynamicTableField = new DynamicTableFields();

            if (tableExist.toLowerCase() == "true".toLowerCase()) {
              String query = DatabaseQueries.CreateGetSingleDynamicFieldTable(
                  template_json_id);
              List dynamicTableFieldList =
                  await DynamicTableFieldsDB.ExecuteQueryDynamicTableFields(
                      query);
              if (dynamicTableFieldList.length > 0) {
                dynamicTableField = dynamicTableFieldList[0];
              }
            }
            controllerName = templateJson["controller_name"];
            jsonTemplateId = templateJson["template_json_id"].toString();
            String fieldJson = templateJson["field_json"];
            tableExist = templateJson["istable_exists"];
            allFieldsKeys = templateJson["other_field_key_array"];
            allMandatoryKeys = templateJson["mandatory_field_key_array"];
            String formName = templateJson["form_name"];
            // CreateUIForHeader(formName);
            var subProcessToField = <String, dynamic>{};
            subProcessToField.addAll(json.decode(fieldJson));
            var subProcessFields = subProcessToField["subProcessFields"];
             seq_id = subProcessToField["seq_id"];
            print("seq_id");
            print(seq_id);
            CreateUIForHeader(formName);
            if(seq_id == 1){
              for (int subProcessFieldsCnt = 0; subProcessFieldsCnt < subProcessFields.length; subProcessFieldsCnt++) {
                print("applicantJson in lead creation");
                var view;
                view = await CreateHorizontalLayoutAndFields(
                    formName,
                    subProcessFields[subProcessFieldsCnt],
                    detailsJson,
                    templateName,
                    applicantJson,
                    dealer_data_json);

                if (view != null) {
                  if (mounted) {
                    setState(() {
                      children.add(view);
                    });
                  }
                }
              }
            }

          }
        }
      }
    } on Exception catch (_) {
      print("Exception In createUIFromDbTemplate");
    }
  }

  showDialogs() {
    return showDialog(
      // context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: new Text(
              "You can’t select the dealer as dealer is blocked from sourcing"),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  CreateUIForHeader(formName) async {
    print("CreateUIForHeader");
    if(seq_id == 1){
      if (mounted) {
        setState(() {
          children.add(Rows(formName));
        });
      }
    }

  }

  Widget Rows(formName) {
    print("Rows");
    return Row(
      children: <Widget>[
        Headers.headers(formName),
        Container(
          height: 20,
        )
      ],
    );
  }

  CreateHorizontalLayoutAndFields(formName, subProcessField, detailsJson,
      templateName, applicantJson, dealerJson) async {
    print("applicantJsonString in CreateHorizontalLayoutAndFields");
    print(applicantJson);
    var val;
    try {
      if (mounted) {
        val = await IdentifyViews(context,formName, subProcessField, detailsJson,
            templateName, applicantJson, dealerJson);
      }
      print("vall");
      print(val);
      print("view in CreateHorizontalLayoutAndFields");
    } catch (e) {
      print("Exception in CreateHorizontalLayoutAndFields() : ");
    }
    return val;
  }

  IdentifyViews(context,formName, subProcessFields, String detailsData,
      String templateName, applicantJsonString, String dealerJson) async {
    print("applicantJsonString in identitfyviews");
    print(applicantJsonString);

    var labelreturn;
    //  var map = <String,dynamic>{};
    //  map.addAll(subProcessField);
    //  print(map);
    String FieldType = subProcessFields["fieldType"].toString();
    String value = subProcessFields["defaultValue"].toString();
    print("FieldType");
    print(FieldType);
    print(subProcessFields["fieldType"]);
    print(subProcessFields["fieldName"]);
    if(subProcessFields["fieldName"] == "Upload PAN Document"){
      return RaisedButton(
        color: Colors.lightBlue[900],
        child: Text("Upload PAN Document",
            style: TextStyle(
                color: Colors.white, fontSize: 18)),
        onPressed: () {
          if(textEditingControllers['IsPANAvailable'].text == 'Yes'){
            navigation('02-PAN');
          }else{
            Dialogs.showDialogs(context, "Please Select PAN");
          }
        },

      );
    }
    if(subProcessFields["fieldName"] == "Date Of Birth"){
      return Center(
          child: Padding(
            padding: const EdgeInsets.all(0.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextFormField(
                    controller: dobControllerfrom,
                  //  onTap: () => _selectFromDate(context),
                    showCursor: false,
                    readOnly: true,
                    style: TextStyle(fontSize: 18),
                    validator: (value) {
                      if (value.isEmpty || value.length == 0) {
                        return 'Please enter Date of Birth';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(0),
                      labelText: 'Date Of Birth',
                      disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: placeholderColor),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: placeholderColor),
                      ),
                      labelStyle: TextStyle(color: placeholderColor),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: placeholderColor),
                      ),
                      suffixIcon:IconButton(icon: Icon(Icons.date_range, color: Colors.orange,), onPressed: () => _selectFromDate(context),),


                    ),
                  ),
                ),
                Container(
                  child:  IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      dobControllerfrom.clear();
                    },
                  ), )
              ],
            ),
          ),

      );

    }//end of date of birth




    switch (subProcessFields["fieldType"]) {

      case "hidden":
        if (FieldType.toLowerCase() == "hidden") {
          createTextField(subProcessFields, applicantJsonString, dealerJson);

          var entry = subProcessFields["key"];
          if (!(entry == "isEnabled")) {
            entry = true;

            if (subProcessFields["key"] == "Dealer_Type" &&
                (subProcessFields["value"] == null ||
                    subProcessFields["value"] == " ")) {
              //         JObject jsonObjectApplicant = null;
              //         using (var reader = new JsonTextReader(new StringReader(applicantJsonString)))
              //         {
              //             reader.DateParseHandling = DateParseHandling.None;
              //             jsonObjectApplicant = JObject.Load(reader);
              //         }

              //         if (jsonObjectApplicant.ContainsKey("LoanType"))
              //         {
              //             String loanType = (string)jsonObjectApplicant.SelectToken("LoanType");
              //             entry.Text = loanType;
              //         }
              //         else
              //         {
              //             entry.Text = subProcessField.Value.ToString();
              //         }
            } else {
              // entry.Text = subProcessField.Value.ToString();
            }
            entry.IsEnabled = false;
          }
        }
        // parent.Children.Add(entry);
        break;
      case "label":

        Text label = null;
        print("In case lable");
        print(subProcessFields["isMandatory"]);
        if (subProcessFields["isMandatory"]) {
          labelreturn = Row(
            children: <Widget>[
              Text(
                subProcessFields["lable"] + ":",
                style: TextStyle(
                  color: Colors.red,
                  fontFamily: 'Raleway',
                  fontSize: 18.0,
                ),
              ),
              Text(value = LeadId)
            ],
          );
        } else {
          labelreturn = Text(
            subProcessFields["key"] + ":",
            style: TextStyle(
              color: Colors.blue,
              fontFamily: 'Raleway',
              fontSize: 18.0,
            ),
          );
          // return label;
        }

        if (value == null || value.isEmpty)

        {
          try {
            print("before  aaaaaaaa");
            if (subProcessFields["key"] == "LEAD ID") {
              print("subProcessField lead id");
              print(subProcessFields);
              value = subProcessFields;
            }
          } catch (e) {
            Object jsonObjectApplicant = null;
            print("In catch lable");
            try {
              if (subProcessFields["key"] == "LEADID") {
                value = subProcessFields;
              } else {
                value = subProcessFields;
              }
            } catch (e1) {
              print("Exception in label");
              if (subProcessFields["key"] == "LEADID") {
                value = LeadID;
              }
            }
          }
        }

        // //create value of that label

        return labelreturn;
        break;

      case "textArea":
        try {
          if (subProcessFields["IsMandatory"] == true) {
            print("IN texarea");
            print(subProcessFields["lable"]);
            labelreturn = Text(
              subProcessFields["lable"],
              style: TextStyle(color: Colors.red, fontSize: 18),
            );
          } else {
            print("IN texarea else");
            print(subProcessFields["lable"]);
            //   labelreturn = Text(
            //    subProcessFields["lable"],
            //    style:TextStyle(
            //     color: Colors.blue,
            //     fontSize: 18
            //    ),
            //  );
            labelreturn = createTextField(
                subProcessFields, applicantJsonString, dealerJson);
          }
        } catch (e1) {
          print("Excetion in textarea switch case");
        }
        return labelreturn;
        break;

      case "text":
        if (subProcessFields["isDataList"]) {
          print("i ma text");
          labelreturn = createTextField(subProcessFields, applicantJsonString, dealerJson);
          print("label return");
          print(labelreturn);
          intiSetBranchAndState();
        } else {
          labelreturn = createTextField(subProcessFields, applicantJsonString, dealerJson);
          print("label return");
          print(labelreturn);
          intiSetBranchAndState();
        }
        return labelreturn;
        break;
      case "dropDownList":
        labelreturn =
            createDropDown(formName, subProcessFields, applicantJsonString);
        return labelreturn;
        break;


    }
  }
  navigation(var imageUploadName){
    print("at navigation");
    print(imageUploadName);
    try{
     // Navigator.pushReplacementNamed(context, '/homeScreen');
      Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>  PANUploadImage(imageUploadName)),
    );
    }on Exception catch(e){
      print("exception in navigation" + e.toString());
    }

    }

  createDropDown(formname, subProcessField, applicantJsonString) {
    var placeholder;

    bool isMandatory = false;
    print("In dropdown");
    print(placeholder.toString().replaceAll(" ", "r") + "Controller");
    var textEditingController;
    var placeholderName;

    createDealerDependentField(dealershipName) async {
      print("In createDealerDependentField");
      String query = DatabaseQueries.GetDealershipDataByName(dealershipName);
      List dealerSOList = await DealerSODB.executeClientTemplateData(query);
      print("prints");
      var dealerSO = <String, dynamic>{};
      dealerSO.addAll(dealerSOList[0]);
      // print(dealerSO['dealership_data']);
      var list = dealerSO['dealership_data'].toString().split(',');

      if (dealerSOList.length > 0) {
        if (dealerSO['dealership_data'].toString().contains("isActive")) {
          var dependentFieldMob =
              subProcessField["dependentFieldMobList"].toString().split(',');
          for (int i = 0; i < dependentFieldMob.length; i++) {
            print("dependentFieldMob");
            // print(subProcessField["dependentFieldMobList"]);
            print(dependentFieldMob[i]);

            var ab = dependentFieldMob[i].toString();
            ab = ab.substring(1, ab.length);
            var MobList = ab.split(',');
            var abc = dealerSO["dealership_data"].toString();
            abc = abc.substring(1, abc.length - 1);
            var pqr = abc.split(',');
            print(pqr[0]);

            for (int j = 0; j < pqr.length; j++) {
              print("for print only");
              print(pqr[j]);
            }
            for (int j = 0; j < pqr.length; j++) {
              print(" in for  inner");
              print(pqr[0]);
              if (pqr[j].trim().contains("Branch_Name")) {
                print("dealerbranch1111");
                var xyz = pqr[j].replaceAll(":", ',').split(',');
                print(xyz[1]);
                // print("inif dealer Branch ");
                setState(() {
                  textEditingControllerTextField["dealerBranch"].text =
                      xyz[1].trim();
                });
              }

              if ((pqr[j]).contains(MobList[0].trim())) {
                print("In if moblist");
                print(MobList[0]);
                var xyz = pqr[j].replaceAll(":", ',').split(',');
                print(xyz[0].trim());

                print(MobList);

                if (xyz[0].trim() == "dealer_State") {
                  setState(() {
                    textEditingControllerTextField["dealer_State"].text =
                        xyz[1].trim();
                    textEditingControllerTextField["SourcingChannel"].text =
                        "Dealer";
                  });
                }
                if (xyz[0].trim() == "Dealer_Code" ||
                    xyz[0].trim() == "Dealer_Name") {
                  //   print("dfgdfgdfgdfgdfgdgd");
                  //   print(MobList[0]);
                  // print(textEditingControllerTextField["dealer"]);
                  setState(() {
                    textEditingControllerTextField[xyz[0].trim()].text =
                        xyz[1].trim();
                  });
                }
                //createTextField("subProcessFields", "applicantJsonString", "dealerJson").setdata();
                //print(textEditingControllerTextField["Middle Name"].text);

              }
            }
          }
        }
      }
    }

    Future<void> _handleDynamicTitleSelected(BuildContext context,var val, controllerKey) async {
      print("in dynamic handle selected");
      print(controllerKey);
      setState(() {
        textEditingControllers[controllerKey].text = val;
        make = textEditingControllers[controllerKey].text;
        });



      if (controllerKey == "Dealership_Name" &&
          textEditingControllers["Dealership_Name"].text != null) {
        print("at push ");
        createDealerDependentField(
            textEditingControllers["Dealership_Name"].text);

      }
      if (controllerKey == "Model_Variant" &&
          textEditingControllers["Model_Variant"].text != null) {
        String queryGetModel = DatabaseQueries.getmodelbymodelname(
            textEditingControllers["Model_Variant"].text);
        List modellistddl = await MakeDB.executeQuery(queryGetModel);
        print("set ORP");
        var map = <String, dynamic>{};
        map.addAll(modellistddl[0]);
        setState(() {
          textEditingControllerTextField["orp"].text = map["on_road_price"];
        });
      }
      Navigator.pop(context);
    }

    _createDynamicChildTitleItem(var text, controllerKey) {
      return InkWell(
        child: Container(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          child: Text(text.toString()),
        ),
        onTap: () => {
          print("call to handleselected"),
          _handleDynamicTitleSelected(context,text, controllerKey)
        },
      );
    }

    showDynamicDropdown(objectList, controllerKey, context) {
      print("showDynamicDropdown.........");
      print(objectList);
      return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Text(
                    "title",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Divider(color: Colors.black),
                Flexible(
                  child: ListView.separated(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: objectList.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return _createDynamicChildTitleItem(
                          objectList[index], controllerKey);
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(color: Colors.grey),
                  ),
                ),

              ],
            ),
          );
        },
      );
    }

    var subProcessFieldValue = subProcessField["value"];
    print("Start of CreateDropdown()");
    print(applicantJsonString);
    var controllerKey;
    InitializeAppDatas();
    String classId = subProcessField["key"];
    String textColor = Colors.black.toString();
    int fontSize = 18;
    List<String> _items = [""].toList();

    if (Platform.isAndroid) {
      if (subProcessField["isMandatory"]) {
        isMandatory = subProcessField["isMandatory"];
        activePlaceholderColor = Colors.red;
        placeholderColor = Colors.red;
      } else {
        activePlaceholderColor = Colors.grey[500];
        placeholderColor = Colors.grey[500];
      }
    } else if (Platform.isIOS) {
      if (subProcessField["isMandatory"] ||
          subProcessField["key"] == "Main_DealerCode") {
        placeholderColor = Colors.red.toString();
        ;
        activePlaceholderColor = Colors.red.toString();
      } else {
        activePlaceholderColor = Colors.grey[500].toString();
        placeholderColor = Colors.grey[500].toString();
      }
    }
    print("checking");
    print(subProcessField["validation"]);
    if (subProcessField["validation"] == "Numeric" ||
        subProcessField["validation"] == "Numbers" ||
        subProcessField["validation"] == "Decimal") {
      print("Numeric validation");
      keyboard = TextInputType.number;
      print("keyboard" + keyboard);
    } else if (subProcessField["validation"] == "alphaNumeric") {
      print("alphaNumeric validation");
      keyboard = TextInputType.text;
      print("keyboard" + keyboard.toString());
    }
    if (subProcessField["validationPattern"] != null &&
        !(subProcessField["validationPattern"] == "")) {
      print(subProcessField["validationPattern"]);
    }

    try {
      if (subProcessField != null && !(subProcessField == "")) {
        print("itttttt");
        text = subProcessField["key"];
        var user = <String, dynamic>{};
        user.addAll(subProcessField);

        placeholder = subProcessField["lable"];
        controllerKey = subProcessField["key"];
        print(controllerKey);
        //  print(subProcessField["key"]);
        if (classId == "Dealership_Name") {
          if (userRole.toLowerCase().startsWith("SO".toLowerCase()) &&
              currentProductID == "1") {
            print("dealershippppp name");
            print(subProcessField["Dealership_Name"]);
            print(subProcessField);

            SetDealerData(text, subProcessField);
          }
        }
      } else {
        placeholder = subProcessField["lable"];
        controllerKey = subProcessField["key"];
      }
    } catch (e) {
      print("Exception 1 in CreateDropDown() : ");

      text = "";
      placeholder = subProcessField["lable"];
      controllerKey = subProcessField["key"];
    }

    print("validation...");
   // onChange
    ondropdownSelect(textcontrollerName, context, placeholdervslue) async {
      print("ondropdownSelect method");
      print(controllerKey);
      try {
        if (subProcessField["value"].toString().contains("[")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("[", "");
        }
        if (subProcessField["value"].toString().contains("]")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("]", "");
        }
        if (subProcessField["value"].toString().contains("\"")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("\"", "");
        }
        if (subProcessField["value"].toString().contains("\n")) {
          subProcessField["value"] =
              subProcessField["value"].toString().replaceAll("\n", "");
        }
        print("objectList before");

        List<String> objectList =
            subProcessField["value"].toString().split(',').toList();
        print(objectList.toString());
        print(objectList.length);
//        if(controllerKey == 'IsPANAvailable'){
//          print(" PANUploadImage");
////          if(textEditingControllers['IsPANAvailable'].text == 'Yes'){
////            print("navigate");
////            Navigator.push(
////              context,
////              MaterialPageRoute(builder: (context) => HomeScreen()),
////            );
//            // Navigator.pushReplacementNamed(context, '/homeScreen');
//          }

        //}
        if (objectList.length == 1 && objectList.contains("")) {
          objectList.remove("");
        }
        // objectList = objectList.Select(x => x.Trim()).ToArray().ToList();
        // SharedPreferences sp = await SharedPreferences.getInstance();
        //var userRole = Application.Current.Properties[AppConstants.LOGGED_IN_USER_ROLE];
        //var userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE)
        print("classId " + classId);
        if (classId == "Product") {
          try {
            // StackLayout mainParentLayout = (Xamarin.Forms.StackLayout)parent.Parent.Parent;
            //arrayListpicker = new List<XfxEntry>();
            // FindAllddlorp(mainParentLayout, "Make");
            //XfxEntry orpXfxEntry3 = arrayListpicker[0];
            //orpXfxEntry3.Text = "";

            // arrayListpicker = new List<XfxEntry>();
            // Fi
            // ndAllddlorp(mainParentLayout, "Model_Variant");
            // XfxEntry orpXfxEntry = arrayListpicker[0];
            // orpXfxEntry.Text = "";

            // arrayListpicker = new List<XfxEntry>();
            // FindAllddlorp(mainParentLayout, "orp");
            // XfxEntry orpXfxEntry1 = arrayListpicker[0];
            // orpXfxEntry1.Text = "";
          } catch (e) {
            print("exception in branch id prduct");
          }
        }
        print("iserrole" + userRole);
        if (objectList.length == 0 &&
            !userRole.toString().toLowerCase().startsWith("VD".toLowerCase()))
        //if (objectList.Count == 0 && currentProductID.Equals("1"))
        {
          print("startsWithV");

          if (objectList.length == 0 && currentProductID == "1") {
            print("currentProductID111111111");
            print(controllerKey);

            if (controllerKey == "Make") {
              print("In Vehicle make");
              print(textEditingControllers["Product"].text.length);
              if (textEditingControllers["Product"].text != null &&
                  textEditingControllers["Product"].text.length != 0) {
                //                     {
                if (textEditingControllers["Dealership_Name"].text != null &&
                    textEditingControllers["Dealership_Name"].text.length !=
                        0) {


                  List onValue = await MakeDB().showMake();
                  print("makelis.....");
                  print(onValue);

                  for (int i = 0; i < onValue.length; i++) {
                    var map = <String, dynamic>{};
                    map.addAll(onValue[i]);
                    print(map["make_name"]);
                    objectList.add(map["make_name"]);
                  }
                  print("placeholder value in gettting");
                  makeList = objectList;
                  print(controllerKey);
                  var dropdown =
                  showDynamicDropdown(objectList, controllerKey, context);
                  return dropdown;
//                  String queryGetMake = DatabaseQueries.getmakebyproductid(
////                      "52"); //added by nilima on 24 july 2018
////                  print("queryGetMake.......................");
////
////
////                  Future<List> makelistddl;
////                  MakeDB.executeQuery(queryGetMake).then((onValue) {
////                    print(onValue);
////                    // List makename;
////                    for (int i = 0; i < onValue.length; i++) {
////                      var map = <String, dynamic>{};
////                      map.addAll(onValue[i]);
////                      print(map["make_name"]);
////                      objectList.add(map["make_name"]);
////                    }
////                    print("placeholder value in gettting");
////                    makeList = objectList;
////                    print(controllerKey);
////                    var dropdown =
////                        showDynamicDropdown(objectList, controllerKey, context);
////                    return dropdown;
////                  });
                }
              } else {
                Dialogs.showDialogs(context, "Please Select Product");
              }
            } else if (controllerKey == "Model_Variant") {
              print("in model");
              print(make);
              if (textEditingControllers["Make"].text != null &&
                  textEditingControllers["Make"].text.length != 0) {
                // {
                String queryGetMakebyMakeName = DatabaseQueries.getmake(
                    textEditingControllers["Make"].text);
                List onValue =
                    await MakeDB.executeQuery(queryGetMakebyMakeName);
                print(onValue);
                var map = <String, dynamic>{};
                print("In mappp");
                map.addAll(onValue[0]);
                print("after map");
                print(map["make_name"]);
                var make = map["make_name"];
                String selectedmkid = map["make_id"].toString();
                print("selectedmkid" + selectedmkid);
                String queryGetModel =
                    DatabaseQueries.getmodelbymakeid(selectedmkid);
                print("queryGetModel" + queryGetModel);
                MakeDB.executeQuery(queryGetModel).then((modellist) {
                  print("modellist");
                  print(modellist);
                  objectList = new List<String>();
                  for (int i = 0; i < modellist.length; i++) {
                    var mapModelName = <String, dynamic>{};
                    mapModelName.addAll(modellist[i]);
                    String modelname = mapModelName["model_name"];
                    String on_road_price = mapModelName["on_road_price"];

                    if (!objectList.contains(mapModelName["model_name"])) {
                      objectList.add(modelname);
                    }
                  }
                  model = objectList;
                  var dropdown =
                      showDynamicDropdown(objectList, controllerKey, context);
                  return dropdown;
                });
              } else {
                print("pleae select make");
                Dialogs.showDialogs(context, "Please Select Make");
              }
            } else if (subProcessField["key"].toString().toLowerCase() ==
                "dealership_name") {
              print("In dealership name dropdown");
              // CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
              List dealerList = await DealerSODB.getData();
              List dealershipNameList = [];
              for (int i = 0; i < dealerList.length; i++) {
                var map = <String, dynamic>{};
                map.addAll(dealerList[i]);
                String dealershipName = map["dealership_name"];
                var abc = map["dealership_data"].toString();
                abc = abc.substring(1, abc.length - 1);
                var pqr = abc.split(',');

                var dealerType = pqr[1].replaceAll(":", ',').split(',');
                print(dealerType[1]);

                if (map["dealership_data"].contains("Dealer_Type")) {
                  var DealerType = dealerType[1];

                  if (DealerType.toString().toLowerCase().trim() ==
                      "New TW".toLowerCase().trim()) {
                    print("in if dealer");
                    print(dealershipName);
                    dealershipNameList.add(dealershipName);
                  }
                }
              }
              print("after for loop");
              print(controllerKey);
              var dropdown = showDynamicDropdown(
                  dealershipNameList, controllerKey, context);
              return dropdown;
            }
          }
        }

        if (objectList != null && objectList.length > 0) {
          print(currentProductID);

          //if (userRole.toString().toLowerCase().startsWith("VD".toLowerCase()))
          if (currentProductID == "2") {
            if (isValid) {
              // showPopup(objectList, xfx_entry, subProcessField.Lable, stackLayout, subProcessField, parent, dataList);
            }
            isValid = true;
          } else {
            if (userRole
                .toString()
                .toLowerCase()
                .startsWith("VD".toLowerCase())) {
              if (isValid) {
                print("in vd");
                // showPopup(objectList, xfx_entry, subProcessField.Lable, stackLayout, subProcessField, parent, dataList);
              }
              isValid = true;
            } else {
              print(objectList);

              showDynamicDropdown(objectList, controllerKey, context);
            }
          }
        }
      } catch (e) {
        print(e);
      }
    }

    if (subProcessField["validation"] == "Numeric" ||
        subProcessField["validation"] == "Numbers" ||
        subProcessField["validation"] == "Decimal") {
      keyboardDropdown = TextInputType.number;
    } else if (subProcessField["validation"] == "alphaNumeric") {
      keyboardDropdown = TextInputType.text;
    }
    if (subProcessField["validationPattern"] != null &&
        !(subProcessField["validationPattern"] == " ")) {
      validationPatternDropdown = subProcessField["validationPattern"];
      validationMessageDropdown = subProcessField["validationMessage"];
      maxlengthDropdown = subProcessField["maxlength"];
      print("checking");
      print(subProcessField);
      print(validationPatternDropdown);
      print(validationMessageDropdown);
    }

    if (subProcessField["isreadonly"] == true ||
        subProcessField["key"] == "Main_DealerCode") {
      isEnabledDropdown = false;
    } else {
      isEnabledDropdown = true;
    }
    Map<String, TextEditingController> placeholderNamesarray = {};
    placeholderNamesarray.putIfAbsent(placeholder, () => placeholderName);
    textEditingController = new TextEditingController();
    textEditingControllers.putIfAbsent(
        controllerKey, () => textEditingController);
    print("show drop down controller");
    print(controllerKey);
    showitems(textcontrollerName, placeholdervslue) {
      print("in show item");
      print(placeholdervslue);
      var returnlist = ondropdownSelect(textcontrollerName, context, placeholdervslue);
      return returnlist;
    }

    void _handleTitleSelected(var val) {
      setState(() {
        textEditingController.text = val;
      });
      Navigator.pop(context);
    }

    Future<void> _showDialogTitleList(
        BuildContext context, String title, details) {
      print(details);
      double screenHeight = MediaQuery.of(context).size.height;
      double height =
          details.length * 53 > 600 ? screenHeight : details.length * 53.0;
      Widget _createChildTitleItem(var text) {
        return InkWell(
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Text(text.toString()),
          ),
          onTap: () => {_handleTitleSelected(text.toString())},
        );
      }

      return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Text(
                    title,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Divider(color: Colors.black),
                Flexible(
                  child: ListView.separated(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: details.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return _createChildTitleItem(details[index]);
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(color: Colors.grey),
                  ),
                ),
              ],
            ),
          );
        },
      );
    }

    showdropdown() {
      print("In showdropdeonnnn");
      // print(textEditingControllers);
      labeltext = placeholder;
      var dropdown = Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: TextFormField(
          enabled: isEnabledDropdown,
          // focusNode: _focus,
          controller: textEditingController,
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: placeholderColor),
            ),
            disabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: placeholderColor),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: placeholderColor),
            ),
            //  border: OutlineInputBorder(),
            labelText: placeholder,
            labelStyle: TextStyle(color: placeholderColor),
            contentPadding: EdgeInsets.only(top: 0, left: 0, right: 0),
            suffixIcon: IconButton(
              icon: Icon(
                Icons.arrow_drop_down,
                color: Colors.orange,
              ),
            ),
          ),
          showCursor: false,
          readOnly: true,
          style: TextStyle(fontSize: 18),

//          onChanged:(val){
//            print("onchanged drop down ");
//            navigation();
//          } ,
          onSaved: (val) {
            print("save drop down ");
            var keys = placeholder.toString().replaceAll(" ", "_");
            mapFormData['"' + controllerKey + '"'] = '"' + val + '"';
            //  mapFormData.putIfAbsent(placeholder, val);
            //mapFormData[]
            //  saveFromData();
            print(controllerKey);
            print('"' + val + '"');
          },
          onTap: () => {
            print("on tap"),
            print(textEditingControllers),
            list = showitems(textEditingController.text, controllerKey),
            _showDialogTitleList(context, 'Select Title', list)
          },
          validator: (name) {
            print('validator for dropdown');
            print(name);
            if (isMandatory == true) {

              print(textEditingControllers);
              if (name == null || name == "") {
                print("In drop null validation");
                return "Please Select $placeholder ";
              } else {
                print("In else drop");
                return null;
              }
            }
          },
        ),
      );
      return dropdown;
    }

    var dropdown = showdropdown();
    print("444444444444");
    print(dropdown);
    return dropdown;
  }

  InitializeAppDatas() async {
    try {
      print(" In InitializeAppData");
      SharedPreferences sp = await SharedPreferences.getInstance();
      userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
      currentProductName = sp.getString(AppConstants.CURRENT_PRODUCT_NAME);
      currentProductID = sp.getString(AppConstants.CURRENT_PRODUCT_ID);
    } catch (e) {
      print("Exception In InitializeAppData " + e.Message);
    }
  }

  SetDealerData(String dealerName, subProcessField) async {
    print("in SetDealerData method");
    try {
      //CKrossStore<DealerSO> cKrossStoreDealerSO = new SQLiteDealerSO(DependencyService.Get<ISQLiteDb>());
      print(dealerName);
      String query = DatabaseQueries.GetDealershipDataByName(dealerName);
      print(query);
      List dealerSOList = await DealerSODB.executeClientTemplateData(query);

      print("LIsted DealerSODB");
      print(dealerSOList);
      //   if (dealerSOList.Count > 0)
      //   {
      //       DealerSO dealerSO = dealerSOList[0];
      //       JObject dealerObject = JObject.Parse(dealerSO.dealership_data);

      //       await UITemplateOperations.InitDatabase();
      //       await UITemplateOperations.TruncateMakeDealerDataTable();
      //       await SaveMakeFromDealerData(dealerObject);
      //  }
    } catch (e) {
      print("Exception in SetDealerData() : $e");
    }
  }

  select(String s) {
    // WidgetsBinding.instance.addPostFrameCallback((_) async {
    setState(() {
      _selection = s;
      // });
    });
    WidgetsBinding.instance.addObserver(this);
  }

  createTextField(subProcessFields, applicantJsonString, dealerJson) {
    var placeholderTextField;
    var validationpatternTextField;
    var validationMessageTextField;
    var controllerKeyText;
    bool isMandatory = false;
    print(dealerJson);
    print("textfeidl Notes...........");
    print(subProcessFields);
    try {
      String key = subProcessFields["key"];
      if (key.toLowerCase() == "product") {
        //xfx_entry.StyleId = "Make";
      }
      if (subProcessFields["isMandatory"]) {
        print("subProcessFields in if createtextfiled ");
        //  print(subProcessFields["isMandatory"]);
        isMandatory = subProcessFields["isMandatory"];
        activePlaceholderColor = Colors.red;
        placeholderColor = Colors.red;
      } else {
        print("subProcessFields in else createtextfiled ");
        activePlaceholderColor = Colors.grey[500];
        placeholderColor = Colors.grey[500];
      }
      try {
        String text = null;

        if (applicantJsonString.containsKey(subProcessFields["Key"])) {
          textfiledtext = applicantJsonString;
        }
        if (!(dealerJson == null || dealerJson.isEmpty)) {
          if (dealerJson.containsKey(subProcessFields["key"])) {
            textfiledtext = subProcessFields["key"];
          } else {
            textfiledtext = "";
            placeholderTextField = subProcessFields["lable"];
            controllerKeyText = subProcessFields["key"];
          }
        }
        if (text != null && !(text == null || text.isEmpty)) {
          print("in not empty");
          print(text);
          print(subProcessFields["lable"]);
          textfiledtext = text;
          //to check ,by nilima
          placeholderTextField = subProcessFields["lable"];
          controllerKeyText = subProcessFields["key"];
        } else {
          print("is empty");
          print(subProcessFields["lable"]);
          textfiledtext = "";
          placeholderTextField = subProcessFields["lable"];
          controllerKeyText = subProcessFields["key"];
        }
      } catch (e) {
        print("Exception in createTextField : ");
        print(subProcessFields["key"]);
        textfiledtext = "";
        placeholderTextField = subProcessFields["lable"];
        controllerKeyText = subProcessFields["key"];
      }

      if (subProcessFields["validation"] == "Numeric" ||
          subProcessFields["validation"] == "numbers" ||
          subProcessFields["validation"] == "decimal") {
        keyboard = TextInputType.number;
      } else if (subProcessFields["Validation"] == "alphaNumeric") {
        print("alphaNumeric");
        keyboard = TextInputType.text;
      }
      if (subProcessFields["validationPattern"] != null &&
          !(subProcessFields["validationPattern"] == null ||
              subProcessFields["validationPattern"] == "")) {
        validationpatternTextField = subProcessFields["validationPattern"];
        validationMessageTextField = subProcessFields["validationMessage"];
        maxlength = subProcessFields["maxlength"];
      }
      if (subProcessFields["isreadonly"] == true) {
        isEnabled = false;
      } else {
        isEnabled = true;
      }
      if (subProcessFields["key"].contains("SourcingChannel")) {
        InitializeAppData();

        // Vendor Officer : firstName lastName
        if (userRole.startsWith("VD".toLowerCase())) {
          textfiledtext = "Vendor Officer : " + user;
        } else {
          textfiledtext = "Dealer";
        }
      }
    } catch (e) {
      print("Exception in createTextField : ");
    }
    print("returnnn");
    print(validationpatternTextField);
    // This list of controllers can be used to set and get the text from/to the TextFields
    //var placeholderName;
    var textFields = <TextFormField>[];
    Map<String, TextEditingController> placeholderNamesarray = {};
    placeholderNamesarray.putIfAbsent(placeholderTextField, () => placeholderName);
    var textEditingController = new TextEditingController();
    textEditingControllerTextField.putIfAbsent(controllerKeyText, () => textEditingController);
    print("in textfield file");
    print(controllerKeyText);
    textFields.add(TextFormField(controller: textEditingController, enabled: isEnabled,
      onSaved: (String value) {
        print(placeholderTextField);
        print("in textfiled cjeck ");
        var keys = placeholderTextField.toString().replaceAll(" ", "_");
        mapFormData['"' + controllerKeyText + '"'] = '"' + value + '"';
        print(controllerKeyText);
        print('"' + value + '"');
        },
      onEditingComplete: () {
        print("valuessss");
      },
      keyboardType: keyboard,
      decoration: new InputDecoration(
        hintText: textfiledtext,
        labelText: placeholderTextField,
        disabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: placeholderColor),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: placeholderColor),
        ),
        labelStyle: TextStyle(color: placeholderColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: placeholderColor),
        ),
      ),
      validator: (name) {
        print("textfield name");
        print(name);
        Pattern pattern = validationpatternTextField;
        RegExp regex;
        if (pattern != null) {
          regex = new RegExp(pattern);
        }

        if (isMandatory == true) {
          if (name == null || name == "") {
            print("In text null validation");
            return "Please Enter $placeholderTextField ";
          } else {
            if (!(regex.hasMatch(name))) {
              return validationMessageTextField;
            } else {
              return null;
            }
          }
          //  }
        }
      },
    ));
    //  intiSetBranchAndState(controllerKeyText);
    print('textFields');
    print(textFields);
    returned = Column(
      children: [
        Column(children: textFields),
      ],
    );

    return returned;
  }

  InitializeAppDataTextField() async {
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      user = sp.getString(AppConstants.LOGGED_IN_USER);
      userRole = sp.getString(AppConstants.LOGGED_IN_USER_ROLE);
    } catch (e) {
      e.GetBaseException();
    }
  }

  saveFromData() async {
    print("In form data save");
    try {
      bool isValid = true;
      if (isValid) {
        var allFieldsKeysArray = [];
        allFieldsKeysArray = allFieldsKeys.split(',');
        // print(allFieldsKeysArray);

        try {
          print(mapFormData);

          var tempObj = JSON.jsonEncode(mapFormData);
          sp.setString("mapFromData", tempObj);
          var jsonObjectApplicant = mapFormData;
          createApplicant(jsonObjectApplicant);
          jsonObjectApplicant =
              await AddDealerDataInApplicant(jsonObjectApplicant);
          print("&&&&&&");
          print(jsonObjectApplicant);
          if (await ValidateFromApplicant(jsonObjectApplicant) == true) {
            String Query = DatabaseQueries.GetMaxCaseID();
            List maxId = await ckrossCaseDB.executeQuery(Query);
            var map = <String, dynamic>{};
            map.addAll(maxId[0]);
            int mobileTempCaseId;
            if (map["LastId"] != null) {
              mobileTempCaseId = map["LastId"] + 1;
              print("In last id");
              print(mobileTempCaseId);
            }
            await deleteCaseIfAlreadyExist(jsonObjectApplicant);
            var jsonObject = createSubmitCaseJson(jsonObjectApplicant, mobileTempCaseId);
            print("%%%%5%%");
            print(jsonObject);
            String fName = "", lName = "", customerName = "", SourceBy = "";
            try {
              if (jsonObjectApplicant.containsKey("First_Name")) {
                print("11122222");
                fName = jsonObjectApplicant["FirstName"].toString();
              }
              if (jsonObjectApplicant.containsKey("Last_Name")) {
                lName = jsonObjectApplicant["LastName"].toString();
              }
              if (jsonObjectApplicant.containsKey("Source_By")) {
                SourceBy = jsonObjectApplicant["Source_By"].toString();
              }
            } on Exception catch (ex) {
              print("Exception In HandleClick" + ex.toString());
            }
            customerName = fName + " " + lName;
            String case_id;
            // await delteteIfAlreadyExist(case_id);
            case_id = await SaveCaseInDb(jsonObject.toString(), AppConstants.CREATE_LEAD, mobileTempCaseId, customerName);
            print("caseiddddddd");
            SharedPreferences sp = await SharedPreferences.getInstance();
            sp.setString("Case_Id", case_id);
            print(case_id);

            await delteteIfAlreadyExist(case_id);
           /// Navigator.pushReplacementNamed(context, '/LeadCreationDataSeqid2');
             Navigator.push(
             context,
             MaterialPageRoute(
               builder: (context) => LeadCreationDataSeqid2(mapFormData),
             ));

          }
        } catch (e1) {
          //Dialogs.showDialogs(contrext, "NETWORK_ERROR");
          print("Exception In HandleClick" + e1.toString());
        }
      }
    } on Exception catch (e) {
      //Dialogs.showDialogs("","Network Error");
      print("Exception In HandleClick" + e.toString());
    }
  }

  createApplicant(jsonObjectApplicant) async {
    print("in create applicantt");
    print(jsonObjectApplicant);

    Map<String, dynamic> ApplicantJsonList = HashMap.from(jsonObjectApplicant);
    //print(ApplicantJsonList['"'+"Scheme Discussed"+'"']);
    // var  abc = jsonObjectApplicant.substring(1, jsonObjectApplicant.length-1 );
    //    var pqr = abc.split(',');
    if (ApplicantJsonList['"' + "SchemeDiscussed" + '"'] == '"' + "Yes" + '"') {
      print("Scheme Discusse present");
      String SchemeSelected = "true";
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString(AppConstants.IsSchemeSelected, SchemeSelected);
      sp.getString(AppConstants.IsSchemeSelected);
    }
  }

  AddDealerDataInApplicant(ApplicantJson) async {
    print("######");
    print(ApplicantJson);
    Map<String, dynamic> ApplicantJsonList = HashMap.from(ApplicantJson);
    print("In AddDealerDataInApplicant");
    List<String> dealerObject;
    String dealer_data_json = "";
    print(userRole);
    try {
      if (userRole.toLowerCase().startsWith("SO".toLowerCase())) {
        try {
          print(ApplicantJson);
          if (ApplicantJson.containsKey('"' + "Dealership_Name" + '"')) {
            String Dealership_Name =
                ApplicantJson['"' + "Dealership_Name" + '"'];
            print(Dealership_Name);
            Dealership_Name =
                Dealership_Name.substring(1, Dealership_Name.length - 1);
            print("*********");
            print(Dealership_Name);
            String query =
                DatabaseQueries.GetDealershipDataByName(Dealership_Name);
            print(query);
            List dealerSOList = await ckrossCaseDB.executeQuery(query);
            print("check query");
            print(dealerSOList);
            //"convert dealerSOList to  DealerDataSo ; "
            if (dealerSOList.length > 0) {
              var dealerSOMap = <String, dynamic>{};
              dealerSOMap.addAll(dealerSOList[0]);

              var abc = dealerSOMap["dealership_data"].toString();
              abc = abc.substring(1, abc.length - 1);
              var pqr = abc.split(',');

              dealerObject = pqr;
              print(dealerObject);

              //     for(int i = 0;i<pqr.length;i++){
              //           var xyz= pqr[i].replaceAll(":", ',').split(',');
              //           var xy0 = xyz[0].replaceAll('[', "");
              //           var xy1 = xyz[1].replaceAll(']', "");

              //     }

            }
          }
        } on Exception catch (e) {
          print("Exception In AddDealerDataInApplicant() : " + e.toString());
        }
      }
      // }
      //     else
      //     {
      //         try
      //         {
      //             string Query = DatabaseQueries.CreateGetLoginJsontemplateImageJson("dealer_data", appLang);
      //             List<LoginJsonTemplate> listLoginJson = await cKrossStoreLoginJsonTemplate.ExecuteQueryAsync(Query);
      //             if (listLoginJson.Count > 0)
      //             {
      //                 dealer_data_json = listLoginJson[0].jsonValue;
      //                 dealerObject = JObject.Parse(dealer_data_json);
      //             }
      //         }
      //         catch (Exception e)
      //         {
      //             logger.Info("Exception In AddDealerDataInApplicant() : " + e.GetBaseException());
      //             e.GetBaseException();
      //         }
      //     }

      if (dealerObject != null) {
        print("mainnnnnnnn");
        print(ApplicantJson);
        print(dealerObject);
        var mapdealerObject = <String, dynamic>{};
        var mapDealerBankDetails = <String, dynamic>{};

        for (int i = 0; i < dealerObject.length; i++) {
          if (dealerObject[i].contains("Dealership_Name")) {
            print("1111in dealership name");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealershipName = xyz[1];
            //  print(dealershipName);
            if (!(ApplicantJson.containsKey("Dealership_Name"))) {
              ApplicantJsonList['"' + "Dealership_Name" + '"'] =
                  '"' + dealershipName + '"';
              print("In add ");
              print(ApplicantJsonList['"' + "Dealership_Name" + '"']);
            }
          }

          if (dealerObject[i].contains("Dealer_Name")) {
            print("In daeler name");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerName = xyz[1];
            if (!(ApplicantJson.containsKey("Dealer_Name"))) {
              ApplicantJsonList['"' + "Dealer_Name" + '"'] =
                  '"' + dealerName + '"';
            }
          }

          if (dealerObject[i].contains("Branch_Name")) {
            print("In daeler Branch");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerBranch = xyz[1];
            if (!(ApplicantJson.containsKey("Branch"))) {
              ApplicantJsonList['"' + "Branch" + '"'] =
                  '"' + dealerBranch + '"';
            }
          }
          if (dealerObject[i].contains("Location")) {
            print("In daeler Lcation");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            Location = xyz[1];
            print(Location);

            if (!(ApplicantJson.containsKey("Location"))) {
              ApplicantJsonList['"' + "Location" + '"'] = '"' + Location + '"';
            }
          }
          if (dealerObject[i].contains("Dealer_Code")) {
            print("In daeler Dealer_Code");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            Dealer_Code = xyz[1];
            print(Dealer_Code);
            if (!(ApplicantJson.containsKey("Dealer_Code"))) {
              ApplicantJsonList['"' + "Dealer_Code" + '"'] =
                  '"' + Dealer_Code + '"';
            }
          }
          if (dealerObject[i].contains("SourcingChannel")) {
            print("In daeler SourcingChannel");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            SourcingChannel = xyz[1];
            if (!(ApplicantJson.containsKey("SourcingChannel"))) {
              ApplicantJsonList['"' + "SourcingChannel" + '"'] =
                  '"' + SourcingChannel + '"';
            }
          }
          if (dealerObject[i].contains("dealer_State")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealer_State = xyz[1];
            if (!(ApplicantJson.containsKey("State"))) {
              ApplicantJsonList['"' + "State" + '"'] = '"' + dealer_State + '"';
            }
          }
          if (dealerObject[i].contains("Office_Address")) {
            print("in office address");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerAddress = xyz[1];
            if (!(ApplicantJson.containsKey("Office_Address"))) {
              ApplicantJsonList['"' + "Office_Address" + '"'] =
                  '"' + dealerAddress + '"';
            }
          }
          if (dealerObject[i].contains("DealersBankDetails")) {
            try {
              print("Dealerbankdetails");
              // print(dealerObject[i]);
              var str = dealerObject.toString();
              const start = "DealersBankDetails: [{";
              const end = "}";

              final startIndex = str.indexOf(start);
              final endIndex = str.indexOf(end, startIndex + start.length);
              var bankData = str.substring(startIndex + start.length, endIndex);
              var splitbankData = bankData.split(',');
              for (int k = 0; k < splitbankData.length; k++) {
                var splits = splitbankData[k].replaceFirst(":", ",").split(",");

                for (int j = 0; j < splits.length; j++) {
                  mapDealerBankDetails['"' + splits[0].trim() + '"'] =
                      '"' + splits[1].trim() + '"';
                }
              }

              if (!(ApplicantJson.containsKey("DealersBankDetailss"))) {
                ApplicantJsonList['"' + "DealersBankDetails" + '"'] =
                    mapDealerBankDetails.toString();
                print("IN bank deatilsss");
                print(ApplicantJsonList);
              }
            } on Exception catch (e) {
              print("Exception in DealersBankDetails" + e.toString());
            }
          }
          if (dealerObject[i].contains("Beneficiary_Name")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DisbursementTO = xyz[1];
            if (!(ApplicantJson.containsKey("DisbursementTO"))) {
              ApplicantJsonList['"' + "DisbursementTO" + '"'] =
                  '"' + DisbursementTO + '"';
            }
          }
          if (dealerObject[i].contains("Bank_Name")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerBankName = xyz[1];
            ;
            if (!(ApplicantJson.containsKey("DealerBankName"))) {
              ApplicantJsonList['"' + "DealerBankName" + '"'] =
                  '"' + DealerBankName + '"';
            }
          }
          if (dealerObject[i].contains("Bank_Account_Number")) {
            print("In bank account number");
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerBankAccountNo = xyz[1];
            print(DealerBankAccountNo);
            if (!(ApplicantJson.containsKey("DealerBankAccountNo"))) {
              ApplicantJsonList['"' + "DealerBankAccountNo" + '"'] =
                  '"' + DealerBankAccountNo + '"';
            }
          }
          if (dealerObject[i].contains("IFSC_Code")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerIFSCCode = xyz[1];
            if (!(ApplicantJson.containsKey("DealerIFSCCode"))) {
              ApplicantJsonList['"' + "DealerIFSCCode" + '"'] =
                  '"' + DealerIFSCCode + '"';
            }
          }
          if (dealerObject[i].contains("Branch_Name")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerBranchName = xyz[1];
            if (!(ApplicantJson.containsKey("DealerBranchName"))) {
              ApplicantJsonList['"' + "DealerBranchName" + '"'] =
                  '"' + DealerBranchName + '"';
            }
          }
          if (dealerObject[i].contains("MICR_Code")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            DealerMICRCode = xyz[1];
            if (!(ApplicantJson.containsKey("DealerMICRCode"))) {
              ApplicantJsonList['"' + "DealerMICRCode" + '"'] =
                  '"' + DealerMICRCode + '"';
            }
          }
          if (dealerObject[i].contains("Type_Of_Account")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            Type_Of_Account = xyz[1];
            if (!(ApplicantJson.containsKey("Type_Of_Account"))) {
              ApplicantJsonList['"' + "Type_Of_Account" + '"'] =
                  '"' + Type_Of_Account + '"';
            }
          }
          if (dealerObject[i].contains("districtname")) {
            var xyz = dealerObject[i].replaceAll(":", ',').split(',');
            dealerDistrictname = xyz[1];
            if (!(ApplicantJson.containsKey("dealerDistrictname"))) {
              ApplicantJsonList['"' + "dealerDistrictname" + '"'] =
                  '"' + dealerDistrictname + '"';
            }
          }
          rto_by_whom = "Dealer";
          if (!(ApplicantJson.containsKey("rto_by_whom"))) {
            ApplicantJsonList['"' + "rto_by_whom" + '"'] =
                '"' + rto_by_whom + '"';
          }
          Insurance_by_whom = "Dealer";
          if (!(ApplicantJson.containsKey("Insurance_by_whom"))) {
            ApplicantJsonList['"' + "Insurance_by_whom" + '"'] =
                '"' + Insurance_by_whom + '"';
          }
        }
      }
    } on Exception catch (e) {
      print("Exception In AddDealerDataInApplicant() : " + e.toString());
    }
    print("ApplicantJsonList");
    print(ApplicantJsonList);
    return ApplicantJsonList;
  }

  ValidateFromApplicant(jsonObjectApplicant) async {
    print("In ValidateFromApplicant");
    String selectedLoanType = "";
    bool isMaditory = true;
    String query = DatabaseQueries.getAllTemplateJsonByClientTemplateId(
        client_template_id);
    List allMandatoryFieldsArrayList = await ckrossCaseDB.executeQuery(query);
    print(allMandatoryFieldsArrayList);
    for (int i = 0; i < allMandatoryFieldsArrayList.length; i++) {
      var templateJson = <String, dynamic>{};
      templateJson.addAll(allMandatoryFieldsArrayList[i]);
      //         TemplateJson templateJson = allMandatoryFieldsArrayList[i];
      String formName = templateJson["form_name"];
      String allMandatoryFields = templateJson["mandatory_field_key_array"];
      String allMandatoryFieldsLbl =
          templateJson["mandatory_field_lable_array"];

      if (!(allMandatoryFields == null || allMandatoryFields.isEmpty)) {
        print("In first if");
        var allMandatoryFieldsArray = allMandatoryFields.split(',');
        var allMandatoryFieldsLblArray = allMandatoryFieldsLbl.split(',');

        for (int j = 0; j < allMandatoryFieldsArray.length; j++) {
          try {
            print(allMandatoryFieldsArray[j]);
            String value = jsonObjectApplicant[allMandatoryFieldsArray[j]];
            print(value);
            // if (value==""|| value==null||  value==" ")
            // {
            //   print("In seconf if");
            //     isMaditory = false;
            //      Dialogs.showDialogs(context, allMandatoryFieldsLblArray[j] + " is Mandatory in " + formName);
            //     break;
            // }
            print("trtr");
            print(allMandatoryFieldsArray);
            if (userRole.toLowerCase().startsWith("VD".toLowerCase()) &&
                allMandatoryFieldsArray[j] == "LoanType" &&
                value == "UTW") {
              selectedLoanType = value;
            }
            print(selectedLoanType);
            if (!(selectedLoanType == "")) {
              var mapjsonobject = <String, dynamic>{};
              mapjsonobject.addAll(jsonObjectApplicant);
              String dealername = mapjsonobject["Dealership Name"].toString();
              String dealerbranch = mapjsonobject["Branch"].toString();
              String dealerCode = mapjsonobject["Dealer_Code"].toString();

              if (dealername == null || dealername == " " || dealername == "") {
                isMaditory = false;
                Dialogs.showDialogs(
                    context, "Dealer Name is Mandatory in " + formName);
                break;
              }

              if (dealerbranch == null ||
                  dealerbranch == " " ||
                  dealerbranch == "") {
                isMaditory = false;
                Dialogs.showDialogs(
                    context, "Branch is Mandatory in " + formName);
                break;
              }

              if (dealerCode == null || dealerCode == " " || dealerCode == "") {
                isMaditory = false;
                Dialogs.showDialogs(
                    context, "Dealer Code is Mandatory in " + formName);
                break;
              }
            }
          } on Exception catch (e) {
            print("Exception In ValidateFromApplicant" + e.toString());
          }
        }
        if (!isMaditory) {
          break;
        }
      }
    }
    return isMaditory;
  }

  deleteCaseIfAlreadyExist(jsonObjectApplicant) async {
    print("in deleteCaseIfAlreadyExist ");
    List allCase = await ckrossCaseDB.search();
    print(allCase);
    for (int i = 0; i < allCase.length; i++) {
      var map = <String, dynamic>{};
      map.addAll(allCase[i]);

      var applicantString = map["applicant_json"];

      try {
        var abc = applicantString;
        abc = abc.split(',');
        String firstname, lastname, mobile, product;
        for (int i = 0; i < abc.length; i++) {
          if (abc[i].contains("FirstName")) {
            var xyz = abc[i].replaceAll(":", ',').split(',');
            firstname = xyz[1];
          }
          if (abc[i].contains("LastName")) {
            var xyz = abc[i].replaceAll(":", ',').split(',');
            lastname = xyz[1];
          }
          if (abc[i].contains("Mobile")) {
            var xyz = abc[i].replaceAll(":", ',').split(',');
            mobile = xyz[1];
          }
          if (abc[i].contains("Product")) {
            var xyz = abc[i].replaceAll(":", ',').split(',');
            product = xyz[1];
          }
        }

        if ((jsonObjectApplicant["FirstName"].toString().trim() ==
                firstname.trim()) &&
            (jsonObjectApplicant["LastName"].toString().trim() ==
                lastname.trim()) &&
            (jsonObjectApplicant["Mobile"].toString().trim() ==
                mobile.trim()) &&
            (jsonObjectApplicant["Product"].toString().trim() ==
                product.trim())) {
          String delQuery = DatabaseQueries.DeleteCaseDependsOnId(
              map["case_id"].toString(), map["user_id"]);
          await ckrossCaseDB.executeQuery(delQuery);
          await delteteIfAlreadyExist(map["case_id"].toString());
          await ckrossCaseDB.search();
        }
      } on Exception catch (e) {
        print("Exception In deleteCaseIfAlreadyExist" + e.toString());
      }
    }
  }

  createSubmitCaseJson(jsonObjectApplicant, int mobileTempCaseId) {
    print("In createSubmitCaseJson");
    var jsonObject = {};
    var map = <String, dynamic>{};
    map.addAll(HashMap.from(jsonObject));

    try {
      if (currentProductID == "2") {
        if (jsonObjectApplicant != null) {
          if (jsonObjectApplicant.containsKey("Dealer_Type")) {
            var temp = jsonObjectApplicant["Dealer_Type"];
            if (temp != null) {
              if (!jsonObjectApplicant.containsKey("Product")) {
                jsonObjectApplicant["Product"] = temp;
              }
            }
          }
        }
      }

      map[controllerName] = jsonObjectApplicant;

      //By Rohit 31/12/2018
      map['"' + "input_source" + '"'] = '"' + "M" + '"';
    } on Exception catch (e) {
      print("Exception In createSubmitCaseJson " + e.toString());
    }
    return map;
  }

  SaveCaseInDb(String applicant, String caseType, int mobileTempCaseId,
      String custName) async {
    String caseid;
    print("in SaveCaseInDb");
    try {
      CkrossCase aCase = new CkrossCase();
      aCase.caseId = mobileTempCaseId.toString();
      aCase.applicantJson = applicant;
      aCase.isSubmit = "false";
      aCase.tempCaseId = "";
      aCase.caseType = caseType;
      aCase.customerName = custName;
      aCase.userId = userId;
      aCase.imageLstFrmServerCnt = 0.toString();
      aCase.productId = currentProductID;
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString("CastType", caseType);
      int id = await ckrossCaseDB.insertCkrossCaseData(aCase);
      if (id != null) {
        String Query = DatabaseQueries.GetMaxCaseID();
        List maxId = await ckrossCaseDB.executeQuery(Query);
        var map = <String, dynamic>{};
        map.addAll(maxId[0]);
        case_id = map["LastId"].toString();
        caseid = case_id;
      }
    } on Exception catch (ex) {
      print("Exception In SaveCaseInDb " + ex.toString());
    }
    return caseid;
  }

  delteteIfAlreadyExist(case_id) async {
    try {
      await InitializeAppData();
    } on Exception catch (e) {
      print("Exception In delteteIfAlreadyExist" + e.toString());
    }
  }

  getLead() async {
    ConnectivityResult cr = await NetworkUtils.checkInternetConnectivity();
    String leadId;
    sp = await SharedPreferences.getInstance();
    if (cr == ConnectivityResult.mobile || cr == ConnectivityResult.wifi) {
      var response = await APIServices.GetLeadID();
      try {
        if (response.statusCode == 200) {
          if ((response != null) && (response != "null") &&
              (response != "{}")) {
            try {
              var jsonObject = JSON.jsonDecode(response.body);
              Map data = jsonObject;
              if (data.containsKey('success') == true) {
                leadId = data['LEADID'].toString();
                print('leadId');
                print(leadId);

                sp.setString('LEADID',leadId);
              }
            }
            on Exception catch (e) {
              print("Exception 1 in getLeadId : " + e.toString());
            }
          }
        } else if (response.statusCode == HttpStatus.forbidden ||
            response.statusCode.toString() == "800") {
          FileUtils.printLog("Response is : " + response.statusCode.toString());
          CustomSnackBar.showSnackBar(context, AppConstants.somethingWentWrong);
        } else if (response.statusCode == HttpStatus.unauthorized) {
          FileUtils.printLog("Response is : " + response.statusCode.toString());
          await showdialog(AppConstants.ALERT, AppConstants.TOKEN_EXPIRED_RELOGIN,
              AppConstants.OK);
        } // // end of statuscode
        else {
          CustomSnackBar.showSnackBar(context, AppConstants.NO_RESPONSE);
          // await showdialog(AppConstants.ALERT, AppConstants.NO_RESPONSE, "OK");
        }
      } on Exception catch (e) {
        print("Exception 1 in getLeadId : " + e.toString());
      }
    }
    return leadId;
  }
  Future<Null> _selectFromDate(BuildContext context) async {
    pickedFrom = await showDatePicker(
        context: context,
        initialDate:
        selectedFromDate == null ? DateTime.now() : selectedFromDate,
        firstDate: DateTime(1950),
        lastDate: DateTime.now());
    if (pickedFrom != null) {
      setState(() {
        selectedFromDate = pickedFrom;
        var formatter = new DateFormat('dd-MM-yyyy');
        String fromDate = formatter.format(pickedFrom);
        dobControllerfrom.text = fromDate;
        isfromvalid = true;
      });
    }
  }

  showdialog(String title, String text, String text1) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text(text1),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/');
              },
            ),
          ],
        );
      },
    );
  }
}
