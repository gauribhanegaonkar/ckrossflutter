import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';
import 'package:flick_video_player/flick_video_player.dart';


class VideoPlayerFromGallery extends StatefulWidget {
  VideoPlayerFromGallery(this._controller, this.videoPath,{Key key}) : super(key: key);

  VideoPlayerController _controller;
  String videoPath;

  @override
  _VideoAppState createState() => _VideoAppState(this._controller,this.videoPath);
}

class _VideoAppState extends State<VideoPlayerFromGallery> {
  _VideoAppState(this._controller,this.videoPath ,{Key key}) : super();
  VideoPlayerController _controller;
  String videoPath;

  Future<void> _initializeVideoPlayerFuture;
//
  File videoFile;
  FlickManager flickManager;
  // String path;

  @override
  void initState() {
    super.initState();
    if(_controller != null && videoPath != null){
    fickvideo(_controller);
    }else{
      print("hi i am null");
    }

  }

  fickvideo( VideoPlayerController _controller){
    flickManager = FlickManager(
      videoPlayerController:
      _controller,
    );
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    // _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_controller.toString()),
      ),
      // Use a FutureBuilder to display a loading spinner while waiting for the
      // VideoPlayerController to finish initializing.
      body: Column(
        children: <Widget>[
          SizedBox(
            child: InkWell(
              child: Visibility(
                visible: _controller != null,

                child: FutureBuilder(
                  future: _initializeVideoPlayerFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done ||
                        _controller != null) {
                      // If the VideoPlayerController has finished initialization, use
                      // the data it provides to limit the aspect ratio of the video.

                      return AspectRatio(
                        aspectRatio: _controller.value.aspectRatio,
                        // Use the VideoPlayer widget to display the video.

                        child: VideoPlayer(_controller),
                      );
                    } else {
                      // If the VideoPlayerController is still initializing, show a
                      // loading spinner.
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                ),

              ),
              onTap: () {
                showdialog(_controller);
              },
            ),
            width: 80,
            height: 80,
          ),
        Visibility(
          visible: _controller != null,
          child:RaisedButton(
            child: Text("Delete"),
            onPressed: () {
              setState(() {
                _controller = null;
                final dir = Directory(videoPath);
                dir.deleteSync(recursive: true);

              });


            },
          ),),
          RaisedButton(
            child: Text("Video"),
            onPressed: () {
              getVideo();
            },
          ),
        ],
      ),
// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  showdialog(VideoPlayerController _controller) {
    showDialog(
     // contentPadding: EdgeInsets.all(0.0),
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(

          content: Stack(

            children: <Widget>[

             Center(

               child:FlickVideoPlayer(

                   flickManager: flickManager
               ),
             ),
             Positioned(
               top: 0,
               right: 0.0,
               child: GestureDetector(
                 onTap: () {
                   Navigator.pop(context);
                   _controller.pause();
                 },
                 child: Align(
                   alignment: Alignment.topRight,
                   child: CircleAvatar(
                     radius: 14.0,
                     backgroundColor: Colors.white,
                     child: Icon(Icons.close,
                         color: Colors.red),
                   ),
                 ),
               ),
             ),

            ],
          ),
        );
      },
    );
  }

  Future getVideo() async {
    String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
    final String dirPath = '/storage/emulated/0/CKross_Wheels_EMI/Video';
    await Directory(dirPath).create(recursive: true);

    Future<File> _videoFile =
        ImagePicker.pickVideo(source: ImageSource.gallery);
    _videoFile.then((file) async {
      setState(() {
        videoPath = '$dirPath/${timestamp()}.mp4';
        videoFile = file;

        _controller = VideoPlayerController.file(videoFile);
        flickManager = FlickManager(
          videoPlayerController:
          _controller,
        );

        print("videofile");
        print(videoFile);
        moveFile(videoFile, videoPath);

        _initializeVideoPlayerFuture = _controller.initialize();

        // Use the controller to loop the video.
        _controller.setLooping(true);
      });
    });
  }

  Future<File> moveFile(File sourceFile, String newPath) async {
    try {
      // prefer using rename as it is probably faster
      return await sourceFile.rename(newPath);
    } on FileSystemException catch (e) {
      // if rename fails, copy the source file and then delete it
      final newFile = await sourceFile.copy(newPath);
      //await sourceFile.delete();
      return newFile;
    }
  }
}
