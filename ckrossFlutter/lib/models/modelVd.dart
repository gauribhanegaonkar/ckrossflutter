class ModelVD{

  String model_id;
  String make_id;
  String model_name;

  ModelVD();

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

   // map['model_id'] = model_id;
    map['make_id'] = make_id;
    map['model_name'] = model_name;


    return map;
  }

  // Extract a Category object from a Map object
  ModelVD.fromMapObject(Map<String, dynamic> map) {

    this.model_id = map['model_id'];
    this.make_id = map['make_id'];
    this.model_name = map['model_name'];

  }
}