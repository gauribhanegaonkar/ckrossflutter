class SubCategory{

  String sub_category_id = 'sub_category_id';
  String category_Id = 'category_id';
  String subcategory_name = 'subcategory_name';
  String subcategory_sequence_no = 'subcategory_sequence_no';
  String menuType = 'menuType';

  SubCategory(this.sub_category_id, this.category_Id, this.subcategory_name, this.subcategory_sequence_no, this.menuType);

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

   // map['sub_category_id'] = sub_category_id;
    map['category_Id'] = category_Id;
    map['subcategory_name'] = subcategory_name;
    map['subcategory_sequence_no'] = subcategory_sequence_no;
    map['menuType'] = menuType;


    return map;
  }

  // Extract a Category object from a Map object
  SubCategory.fromMapObject(Map<String, dynamic> map) {

    this.sub_category_id = map['sub_category_id'];
    this.category_Id = map['category_Id'];
    this.subcategory_name = map['subcategory_name'];
    this.subcategory_sequence_no = map['subcategory_sequence_no'];
    this.menuType = map['menuType'];

  }
}