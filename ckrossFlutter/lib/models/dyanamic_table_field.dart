class DynamicTableFields{

    String dynamic_field_table_id;
    String table_name;
    String template_name;
    String table_headers_array;
    String table_keys_array;
    String mandatory_fields;
    String mandatory_table_headers;
    String template_json_id;
    String clear_subprocess_fields;

    // DynamicTableFields(this.dynamic_field_table_id,this.table_name,this.template_name,this.table_headers_array,this.table_keys_array,this.mandatory_fields,
    //     this.mandatory_table_headers,this.template_json_id,this.clear_subprocess_fields);
DynamicTableFields();
    Map<String,dynamic> toMap(){
      var map = Map<String, dynamic>();

     // map['dynamic_field_table_id'] = dynamic_field_table_id;
      map['table_name'] = table_name;
      map['template_name'] = template_name;
      map['table_headers_array'] = table_headers_array;
      map['table_keys_array'] = table_keys_array;
      map['mandatory_fields'] = mandatory_fields;
      map['mandatory_table_headers'] = mandatory_table_headers;
      map['template_json_id'] = template_json_id;
      map['clear_subprocess_fields'] = clear_subprocess_fields;


      return map;
    }

    DynamicTableFields.fromMapObject(Map<String,dynamic>map){

      this.dynamic_field_table_id = map['dynamic_field_table_id'];
      this.table_name = map['table_name'];
      this.template_name = map['template_name'];
      this.table_headers_array = map['table_headers_array'];
      this.table_keys_array = map['table_keys_array'] ;
      this.mandatory_fields = map['mandatory_fields'];
      this.mandatory_table_headers = map['mandatory_table_headers'];
      this.template_json_id = map['template_json_id'];
      this.clear_subprocess_fields = map['clear_subprocess_fields'];

    }
}