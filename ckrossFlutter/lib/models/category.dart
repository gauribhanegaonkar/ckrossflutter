class Category{

  String categoryId;
  String categoryName;
  String loginJsonTemplateId;

  Category(this.categoryId,this.categoryName,this.loginJsonTemplateId);

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

    //map['category_id'] = categoryId;
    map['category_name'] = categoryName;
    map['login_json_template_id'] = loginJsonTemplateId;

    return map;
  }

  // Extract a Category object from a Map object
  Category.fromMapObject(Map<String, dynamic> map) {

    this.categoryId = map['category_id'];
    this.categoryName = map['category_name'];
    this.loginJsonTemplateId = map['login_json_template_id'];

  }
}