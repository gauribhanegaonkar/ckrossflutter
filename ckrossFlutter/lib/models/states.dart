class States{

  String state_id ;
  String state_name  ;


  States(this.state_id,this.state_name);

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

   // map['state_id'] = state_id;
    map['state_name'] = state_name;

    return map;
  }

  // Extract a Category object from a Map object
  States.fromMapObject(Map<String, dynamic> map) {

    this.state_id = map['state_id'];
    this.state_name = map['state_name'];


  }
}