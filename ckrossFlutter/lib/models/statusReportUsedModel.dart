
class UsedTWReport {
  String  id;
  List<String> contents = new List<String>();
  UsedTWReport(this.id,this.contents);


  @override
  String toString() {
    return '{ ${this.id},${this.contents}}';
  }
}