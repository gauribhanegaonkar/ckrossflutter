class MakeModel{

  String make_id ;
  String product_id  ;
  String make_name;

  //Make(this.make_id,this.product_id,this.make_name);
  MakeModel();
  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

   // map['state_id'] = state_id;
    map['make_id'] = make_id;
    map['product_id'] = product_id;
    map['make_name'] = make_name;

    return map;
  }

  // Extract a Category object from a Map object
  MakeModel.fromMapObject(Map<String, dynamic> map) {

    this.make_id = map['make_id'];
    this.product_id = map['product_id'];
    this.make_name = map['make_name'];



  }
}