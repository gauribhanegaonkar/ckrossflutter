class YearVD{

  String year_id;
  String model_id;
  String year;

  YearVD();

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

   // map['year_id'] = year_id;
    map['model_id'] = model_id;
    map['year'] = year;


    return map;
  }

  // Extract a Category object from a Map object
  YearVD.fromMapObject(Map<String, dynamic> map) {

    this.year_id = map['year_id'];
    this.model_id = map['model_id'];
    this.year = map['year'];

  }
}