// To parse this JSON data, do
//
//     final doOtpLoginResModel = doOtpLoginResModelFromJson(jsonString);

import 'dart:convert';

LoadHTMLFieldsRequestModel LoadHTMLFieldsRequestModelFromJson(String str) => LoadHTMLFieldsRequestModel.fromJson(json.decode(str));

String LoadHTMLFieldsRequestModelToJson(LoadHTMLFieldsRequestModel data) => json.encode(data.toJson());

class LoadHTMLFieldsRequestModel {
    LoadHTMLFieldsRequestModel({
        this.processID,
        this.landing_page,
        this.product_Id,
       
    });

    String processID;
    String landing_page;
    String product_Id;
    

    factory LoadHTMLFieldsRequestModel.fromJson(Map<String, dynamic> json) => LoadHTMLFieldsRequestModel(
        processID: json["processID"] == null ? null : json["processID"],
        landing_page: json["landing_page"] == null ? null : json["landing_page"],
         product_Id: json["product_Id"] == null ? null : json["product_Id"],
        
    );

    Map<String, dynamic> toJson() => {
        "processID": processID == null ? null : processID,
        "landing_page": landing_page == null ? null : landing_page,
         "product_Id": product_Id == null ? null : product_Id,
        
    };
}
