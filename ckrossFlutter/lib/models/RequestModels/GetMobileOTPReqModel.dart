// To parse this JSON data, do
//
//     final doOtpLoginResModel = doOtpLoginResModelFromJson(jsonString);

import 'dart:convert';

GetOTPRequestmodel GetOTPRequestmodelFromJson(String str) => GetOTPRequestmodel.fromJson(json.decode(str));

String GetOTPRequestmodelToJson(GetOTPRequestmodel data) => json.encode(data.toJson());

class GetOTPRequestmodel {
    GetOTPRequestmodel({
        this.username,
        this.password,
        this.database,
        this.client,
        this.appType,
        this.productName,
    });

    String username;
    String password;
    String database;
    String client;
    String appType;
    String productName;

    factory GetOTPRequestmodel.fromJson(Map<String, dynamic> json) => GetOTPRequestmodel(
        username: json["username"] == null ? null : json["username"],
       // password: json["password"] == null ? null : json["password"],
         database: json["database"] == null ? null : json["database"],
        // client: json["client"] == null ? null : json["client"],
        // appType: json["appType"] == null ? null : json["appType"],
        // productName: json["product_Name"] == null ? null : json["product_Name"],
    );

    Map<String, dynamic> toJson() => {
        "username": username == null ? null : username,
        //"password": password == null ? null : password,
         "database": database == null ? null : database,
        // "client": client == null ? null : client,
        // "appType": appType == null ? null : appType,
        // "product_Name": productName == null ? null : productName,
    };
}
