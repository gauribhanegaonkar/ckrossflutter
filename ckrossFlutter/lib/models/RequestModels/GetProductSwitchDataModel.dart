import 'dart:convert';

GetProductSwitchmodel GetProductSwitchmodelFromJson(String str) => GetProductSwitchmodel.fromJson(json.decode(str));

String GetProductSwitchmodelToJson(GetProductSwitchmodel data) => json.encode(data.toJson());


class GetProductSwitchmodel{
  GetProductSwitchmodel({
    this.input_source,
    this.product_Id,
  //  this.database

});
    String input_source;
    String product_Id;
    //String database;

  factory GetProductSwitchmodel.fromJson(Map<String, dynamic> json) => GetProductSwitchmodel(
    input_source: json["input_source"] == null ? null : json["input_source"],
    // password: json["password"] == null ? null : json["password"],
    product_Id: json["product_Id"] == null ? null : json["product_Id"],
    //database: json["database"] == null ? null : json["database"],
    // client: json["client"] == null ? null : json["client"],
    // appType: json["appType"] == null ? null : json["appType"],
    // productName: json["product_Name"] == null ? null : json["product_Name"],
  );

  Map<String, dynamic> toJson() => {
    "input_source": input_source == null ? null : input_source,
    //"password": password == null ? null : password,
    "product_Id": product_Id == null ? null : product_Id,
    //"database": database == null ? null : database,
    // "client": client == null ? null : client,
    // "appType": appType == null ? null : appType,
    // "product_Name": productName == null ? null : productName,
  };
}