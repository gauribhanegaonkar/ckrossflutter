class TemplateJson{

  String template_json_id;
  String form_name;
  String client_template_id;
  String istable_exists;
  String controller_name;
  String mandatory_field_key_array;
  String mandatory_field_lable_array;
  String field_json;
  String other_field_key_array;


  // TemplateJson(this.template_json_id, this.form_name, this.client_template_id, this.istable_exists, this.controller_name, this.mandatory_field_key_array, this.mandatory_field_lable_array,
  //     this.field_json, this.other_field_key_array);
  TemplateJson();

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

  //  map['template_json_id'] = template_json_id;
    map['form_name'] = form_name;
    map['client_template_id'] = client_template_id;
    map['istable_exists'] = istable_exists;
    map['controller_name'] = controller_name;
    map['mandatory_field_key_array'] = mandatory_field_key_array;
    map['mandatory_field_lable_array'] = mandatory_field_lable_array;
    map['field_json'] = field_json;
    map['other_field_key_array'] = other_field_key_array;


    return map;
  }

  // Extract a Category object from a Map object
  TemplateJson.fromMapObject(Map<String, dynamic> map) {

    this.template_json_id = map['template_json_id'];
    this.form_name = map['form_name'];
    this.client_template_id = map['client_template_id'];
    this.istable_exists = map['istable_exists'];
    this.controller_name = map['controller_name'];
    this.mandatory_field_key_array = map['mandatory_field_key_array'];
    this.mandatory_field_lable_array = map['mandatory_field_lable_array'];
    this.field_json = map['field_json'];
    this.other_field_key_array = map['other_field_key_array'];

  }
}