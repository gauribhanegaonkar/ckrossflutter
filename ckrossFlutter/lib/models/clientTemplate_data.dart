class ClientTemplate{
  String client_template_id;
  String client_name;
  String template_name ;
  String form_name_array ;
  String version ;
  String language ;
  String is_deprecated ;
  String applicant_json;
  String details_json;

  // ClientTemplate(this.client_template_id,this.client_name,this.template_name,this.form_name_array,this.version,this.language,
  //     this.is_deprecated,this.applicant_json,this.details_json);

ClientTemplate();
  Map<String,dynamic> toMap(){
    var map = Map<String, dynamic>();

    //map['client_template_id'] = client_template_id;
    map['client_name'] = client_name;
    map['template_name'] = template_name;
    map['form_name_array'] = form_name_array;
    map['version'] = version;
    map['language'] = language;
    map['is_deprecated'] = is_deprecated;
    map[' applicant_json'] =  applicant_json;
    map['details_json'] = details_json;

    return map;
}

 ClientTemplate.fromMapObject(Map<String,dynamic>map){

    this.client_template_id = map['client_template_id'];
    this.client_name = map['client_name'];
    this.template_name = map['template_name'];
    this.form_name_array = map['form_name_array'];
    this.version = map['version'] ;
    this.language = map['language'];
    this.is_deprecated = map['is_deprecated'];
    this.applicant_json = map['applicant_json'];
    this.details_json = map['details_json'];

  }

}