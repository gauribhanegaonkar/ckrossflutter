class CkrossCase{

  String caseRowId ;
  String caseId ;
  String caseType ;
  String applicantJson ;
  String isSubmit ;
  String tempCaseId ;
  String customerName ;
  String imageLstFrmServerCnt  ;
  String userId ;
  String productId ;

  // CkrossCase(this.caseRowId,this.caseId,this.caseType,this.applicantJson,this.isSubmit,this.tempCaseId,this.customerName
  //     ,this.imageLstFrmServerCnt,this.userId,this.productId);
CkrossCase();

  Map<String,dynamic> toMap(){
    var map = Map<String, dynamic>();

    //map['case_row_id'] = caseRowId;
    map['case_id'] = caseId;
    map['case_type'] = caseType;
    map['applicant_json'] = applicantJson;
    map['is_submit'] = isSubmit;
    map['temp_case_id'] = tempCaseId;
    map['customer_name'] = customerName;
    map['imageLstFrmServerCnt'] = imageLstFrmServerCnt;
    map['user_id'] = userId;
    map['product_id'] = productId;

    return map;
  }

  CkrossCase.fromMapObject(Map<String,dynamic>map){
    this.caseRowId = map['case_row_id'];
    this.caseId = map['case_id'];
    this.caseType = map['case_type'];
    this.applicantJson = map['applicant_json'];
    this.isSubmit = map['is_submit'] ;
    this.tempCaseId = map['temp_case_id'];
    this.customerName = map['customer_name'];
    this.imageLstFrmServerCnt = map['imageLstFrmServerCnt'];
    this.userId = map['user_id'];
    this.productId = map['product_id'];
  }
}