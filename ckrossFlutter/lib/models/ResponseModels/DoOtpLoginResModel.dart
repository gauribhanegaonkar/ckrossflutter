// To parse this JSON data, do
//
//     final doOtpLoginResModel = doOtpLoginResModelFromJson(jsonString);

import 'dart:convert';

DoOtpLoginResModel doOtpLoginResModelFromJson(String str) => DoOtpLoginResModel.fromJson(json.decode(str));

String doOtpLoginResModelToJson(DoOtpLoginResModel data) => json.encode(data.toJson());

class DoOtpLoginResModel {
    DoOtpLoginResModel({
        this.menudetails,
        this.redirect,
        this.details,
        this.languageAssigned,
        this.redirectUrl,
        this.sourcedBy,
        this.processesAssigned,
        this.loggedInUserRole,
        this.isProductAsSourceBy,
        this.dealerDataSo,
        this.postDisbDocumentList,
        this.userAuthentication,
        this.appVersion,
        this.loggedInUser,
        this.token,
        this.productToMakeToModelToOnPriceJsonArray,
        this.userBranch,
        this.serviceInspectionDocumentList,
        this.preDisbDocumentList,
        this.updateLeadDocumentList,
        this.success,
        this.productId,
        this.leadDocuments,
        this.loggedInUserId,
    });

    String menudetails;
    String details;
    bool redirect;
    String languageAssigned;
    String redirectUrl;
    String sourcedBy;
    String processesAssigned;
    String loggedInUserRole;
    String isProductAsSourceBy;
    List<DealerDataSo> dealerDataSo;
    PostDisbDocumentList postDisbDocumentList;
    bool userAuthentication;
    String appVersion;
    String loggedInUser;
    String token;
    String productToMakeToModelToOnPriceJsonArray;
    UserBranch userBranch;
    ServiceInspectionDocumentList serviceInspectionDocumentList;
    PreDisbDocumentList preDisbDocumentList;
    UpdateLeadDocumentList updateLeadDocumentList;
    bool success;
    int productId;
    LeadDocuments leadDocuments;
    String loggedInUserId;

    factory DoOtpLoginResModel.fromJson(Map<String, dynamic> json) => DoOtpLoginResModel(
        menudetails: json["menudetails"] == null ? null : json["menudetails"],
        redirect: json["redirect"] == null ? null : json["redirect"],
        details: json["details"] == null ? null : json["details"],
        languageAssigned: json["languageAssigned"] == null ? null : json["languageAssigned"],
        redirectUrl: json["redirectURL"] == null ? null : json["redirectURL"],
        sourcedBy: json["sourcedBy"] == null ? null : json["sourcedBy"],
        processesAssigned: json["processesAssigned"] == null ? null : json["processesAssigned"],
        loggedInUserRole: json["LOGGED_IN_USER_ROLE"] == null ? null : json["LOGGED_IN_USER_ROLE"],
        isProductAsSourceBy: json["isProductAsSourceBy"] == null ? null : json["isProductAsSourceBy"],
        dealerDataSo: json["dealer_data_SO"] == null ? null : List<DealerDataSo>.from(json["dealer_data_SO"].map((x) => DealerDataSo.fromJson(x))),
        postDisbDocumentList: json["Post_Disb_DocumentList"] == null ? null : PostDisbDocumentList.fromJson(json["Post_Disb_DocumentList"]),
        userAuthentication: json["UserAuthentication"] == null ? null : json["UserAuthentication"],
        appVersion: json["app_Version"] == null ? null : json["app_Version"],
        loggedInUser: json["LOGGED_IN_USER"] == null ? null : json["LOGGED_IN_USER"],
        token: json["token"] == null ? null : json["token"],
        productToMakeToModelToOnPriceJsonArray: json["productToMakeToModelToOnPriceJsonArray"] == null ? null : json["productToMakeToModelToOnPriceJsonArray"],
        userBranch: json["userBranch"] == null ? null : userBranchValues.map[json["userBranch"]],
        serviceInspectionDocumentList: json["Service_Inspection_DocumentList"] == null ? null : ServiceInspectionDocumentList.fromJson(json["Service_Inspection_DocumentList"]),
        preDisbDocumentList: json["Pre_Disb_DocumentList"] == null ? null : PreDisbDocumentList.fromJson(json["Pre_Disb_DocumentList"]),
        updateLeadDocumentList: json["Update_Lead_DocumentList"] == null ? null : UpdateLeadDocumentList.fromJson(json["Update_Lead_DocumentList"]),
        success: json["success"] == null ? null : json["success"],
        productId: json["product_Id"] == null ? null : json["product_Id"],
        leadDocuments: json["leadDocuments"] == null ? null : LeadDocuments.fromJson(json["leadDocuments"]),
        loggedInUserId: json["LOGGED_IN_USER_ID"] == null ? null : json["LOGGED_IN_USER_ID"],
    );

    Map<String, dynamic> toJson() => {
        "menudetails": menudetails == null ? null : menudetails,
        "redirect": redirect == null ? null : redirect,
        "details": details == null ? null : details,
        "languageAssigned": languageAssigned == null ? null : languageAssigned,
        "redirectURL": redirectUrl == null ? null : redirectUrl,
        "sourcedBy": sourcedBy == null ? null : sourcedBy,
        "processesAssigned": processesAssigned == null ? null : processesAssigned,
        "LOGGED_IN_USER_ROLE": loggedInUserRole == null ? null : loggedInUserRole,
        "isProductAsSourceBy": isProductAsSourceBy == null ? null : isProductAsSourceBy,
        "dealer_data_SO": dealerDataSo == null ? null : List<dynamic>.from(dealerDataSo.map((x) => x.toJson())),
        "Post_Disb_DocumentList": postDisbDocumentList == null ? null : postDisbDocumentList.toJson(),
        "UserAuthentication": userAuthentication == null ? null : userAuthentication,
        "app_Version": appVersion == null ? null : appVersion,
        "LOGGED_IN_USER": loggedInUser == null ? null : loggedInUser,
        "token": token == null ? null : token,
        "productToMakeToModelToOnPriceJsonArray": productToMakeToModelToOnPriceJsonArray == null ? null : productToMakeToModelToOnPriceJsonArray,
        "userBranch": userBranch == null ? null : userBranchValues.reverse[userBranch],
        "Service_Inspection_DocumentList": serviceInspectionDocumentList == null ? null : serviceInspectionDocumentList.toJson(),
        "Pre_Disb_DocumentList": preDisbDocumentList == null ? null : preDisbDocumentList.toJson(),
        "Update_Lead_DocumentList": updateLeadDocumentList == null ? null : updateLeadDocumentList.toJson(),
        "success": success == null ? null : success,
        "product_Id": productId == null ? null : productId,
        "leadDocuments": leadDocuments == null ? null : leadDocuments.toJson(),
        "LOGGED_IN_USER_ID": loggedInUserId == null ? null : loggedInUserId,
    };
}

class DealerDataSo {
    DealerDataSo({
        this.bankAccountNumber,
        this.dealerType,
        this.dealershipName,
        this.codeGenerationDate,
        this.isActive,
        this.dealerCode,
        this.dealersBankDetails,
        this.beneficiaryName,
        this.ecsDccAdm,
        this.dealerName,
        this.micrCode,
        this.mobileNo,
        this.dealerState,
        this.dealerBranch,
        this.panNo,
        this.otherCharges1,
        this.isDisbursementActive,
        this.isPrimaryAccount,
        this.pincode,
        this.codeGenerationMnth,
        this.make,
        this.dealerCategorisation,
        this.repaymentMode,
        this.resiAddress,
        this.typeOfAccount,
        this.systemGeneratedDealerCode,
        this.officeAddress,
        this.branchName,
        this.isSourcingActive,
        this.bankName,
        this.districtname,
        this.ifscCode,
        this.country,
        this.id,
        this.location,
        this.undefined,
        this.isSelectAllMake,
        this.modelVariant,
    });

    String bankAccountNumber;
    DealerType dealerType;
    String dealershipName;
    String codeGenerationDate;
    bool isActive;
    String dealerCode;
    List<DealersBankDetail> dealersBankDetails;
    String beneficiaryName;
    dynamic ecsDccAdm;
    String dealerName;
    dynamic micrCode;
    String mobileNo;
    DealerState dealerState;
    UserBranch dealerBranch;
    String panNo;
    dynamic otherCharges1;
    bool isDisbursementActive;
    IsPrimaryAccount isPrimaryAccount;
    String pincode;
    String codeGenerationMnth;
    List<Make> make;
    DealerCategorisation dealerCategorisation;
    RepaymentMode repaymentMode;
    String resiAddress;
    TypeOfAccount typeOfAccount;
    String systemGeneratedDealerCode;
    String officeAddress;
    String branchName;
    bool isSourcingActive;
    String bankName;
    UserBranch districtname;
    String ifscCode;
    Country country;
    Id id;
    Location location;
    String undefined;
    String isSelectAllMake;
    String modelVariant;

    factory DealerDataSo.fromJson(Map<String, dynamic> json) => DealerDataSo(
        bankAccountNumber: json["Bank_Account_Number"] == null ? null : json["Bank_Account_Number"],
        dealerType: json["Dealer_Type"] == null ? null : dealerTypeValues.map[json["Dealer_Type"]],
        dealershipName: json["Dealership_Name"] == null ? null : json["Dealership_Name"],
        codeGenerationDate: json["Code_Generation_Date"] == null ? null : json["Code_Generation_Date"],
        isActive: json["isActive"] == null ? null : json["isActive"],
        dealerCode: json["Dealer_Code"] == null ? null : json["Dealer_Code"],
        dealersBankDetails: json["DealersBankDetails"] == null ? null : List<DealersBankDetail>.from(json["DealersBankDetails"].map((x) => DealersBankDetail.fromJson(x))),
        beneficiaryName: json["Beneficiary_Name"] == null ? null : json["Beneficiary_Name"],
        ecsDccAdm: json["ECS_DCC_ADM"],
        dealerName: json["Dealer_Name"] == null ? null : json["Dealer_Name"],
        micrCode: json["MICR_Code"],
        mobileNo: json["Mobile_No"] == null ? null : json["Mobile_No"],
        dealerState: json["dealer_State"] == null ? null : dealerStateValues.map[json["dealer_State"]],
        dealerBranch: json["dealerBranch"] == null ? null : userBranchValues.map[json["dealerBranch"]],
        panNo: json["PAN_No"] == null ? null : json["PAN_No"],
        otherCharges1: json["OtherCharges1"],
        isDisbursementActive: json["Is_Disbursement_Active"] == null ? null : json["Is_Disbursement_Active"],
        isPrimaryAccount: json["IsPrimaryAccount"] == null ? null : isPrimaryAccountValues.map[json["IsPrimaryAccount"]],
        pincode: json["pincode"] == null ? null : json["pincode"],
        codeGenerationMnth: json["Code_Generation_Mnth"] == null ? null : json["Code_Generation_Mnth"],
        make: json["Make"] == null ? null : List<Make>.from(json["Make"].map((x) => makeValues.map[x])),
        dealerCategorisation: json["Dealer_Categorisation"] == null ? null : dealerCategorisationValues.map[json["Dealer_Categorisation"]],
        repaymentMode: json["RepaymentMode"] == null ? null : repaymentModeValues.map[json["RepaymentMode"]],
        resiAddress: json["Resi_Address"] == null ? null : json["Resi_Address"],
        typeOfAccount: json["Type_Of_Account"] == null ? null : typeOfAccountValues.map[json["Type_Of_Account"]],
        systemGeneratedDealerCode: json["System_generated_Dealer_code"] == null ? null : json["System_generated_Dealer_code"],
        officeAddress: json["Office_Address"] == null ? null : json["Office_Address"],
        branchName: json["Branch_Name"] == null ? null : json["Branch_Name"],
        isSourcingActive: json["Is_Sourcing_Active"] == null ? null : json["Is_Sourcing_Active"],
        bankName: json["Bank_Name"] == null ? null : json["Bank_Name"],
        districtname: json["districtname"] == null ? null : userBranchValues.map[json["districtname"]],
        ifscCode: json["IFSC_Code"] == null ? null : json["IFSC_Code"],
        country: json["Country"] == null ? null : countryValues.map[json["Country"]],
        id: json["_id"] == null ? null : Id.fromJson(json["_id"]),
        location: json["Location"] == null ? null : locationValues.map[json["Location"]],
        undefined: json["undefined"] == null ? null : json["undefined"],
        isSelectAllMake: json["IsSelectAllMake"] == null ? null : json["IsSelectAllMake"],
        modelVariant: json["Model_Variant"] == null ? null : json["Model_Variant"],
    );

    Map<String, dynamic> toJson() => {
        "Bank_Account_Number": bankAccountNumber == null ? null : bankAccountNumber,
        "Dealer_Type": dealerType == null ? null : dealerTypeValues.reverse[dealerType],
        "Dealership_Name": dealershipName == null ? null : dealershipName,
        "Code_Generation_Date": codeGenerationDate == null ? null : codeGenerationDate,
        "isActive": isActive == null ? null : isActive,
        "Dealer_Code": dealerCode == null ? null : dealerCode,
        "DealersBankDetails": dealersBankDetails == null ? null : List<dynamic>.from(dealersBankDetails.map((x) => x.toJson())),
        "Beneficiary_Name": beneficiaryName == null ? null : beneficiaryName,
        "ECS_DCC_ADM": ecsDccAdm,
        "Dealer_Name": dealerName == null ? null : dealerName,
        "MICR_Code": micrCode,
        "Mobile_No": mobileNo == null ? null : mobileNo,
        "dealer_State": dealerState == null ? null : dealerStateValues.reverse[dealerState],
        "dealerBranch": dealerBranch == null ? null : userBranchValues.reverse[dealerBranch],
        "PAN_No": panNo == null ? null : panNo,
        "OtherCharges1": otherCharges1,
        "Is_Disbursement_Active": isDisbursementActive == null ? null : isDisbursementActive,
        "IsPrimaryAccount": isPrimaryAccount == null ? null : isPrimaryAccountValues.reverse[isPrimaryAccount],
        "pincode": pincode == null ? null : pincode,
        "Code_Generation_Mnth": codeGenerationMnth == null ? null : codeGenerationMnth,
        "Make": make == null ? null : List<dynamic>.from(make.map((x) => makeValues.reverse[x])),
        "Dealer_Categorisation": dealerCategorisation == null ? null : dealerCategorisationValues.reverse[dealerCategorisation],
        "RepaymentMode": repaymentMode == null ? null : repaymentModeValues.reverse[repaymentMode],
        "Resi_Address": resiAddress == null ? null : resiAddress,
        "Type_Of_Account": typeOfAccount == null ? null : typeOfAccountValues.reverse[typeOfAccount],
        "System_generated_Dealer_code": systemGeneratedDealerCode == null ? null : systemGeneratedDealerCode,
        "Office_Address": officeAddress == null ? null : officeAddress,
        "Branch_Name": branchName == null ? null : branchName,
        "Is_Sourcing_Active": isSourcingActive == null ? null : isSourcingActive,
        "Bank_Name": bankName == null ? null : bankName,
        "districtname": districtname == null ? null : userBranchValues.reverse[districtname],
        "IFSC_Code": ifscCode == null ? null : ifscCode,
        "Country": country == null ? null : countryValues.reverse[country],
        "_id": id == null ? null : id.toJson(),
        "Location": location == null ? null : locationValues.reverse[location],
        "undefined": undefined == null ? null : undefined,
        "IsSelectAllMake": isSelectAllMake == null ? null : isSelectAllMake,
        "Model_Variant": modelVariant == null ? null : modelVariant,
    };
}

enum Country { INDIA }

final countryValues = EnumValues({
    "India": Country.INDIA
});

enum UserBranch { PUNE, CHINNOOR, HYDERABAD, SOLAPUR }

final userBranchValues = EnumValues({
    "CHINNOOR": UserBranch.CHINNOOR,
    "Hyderabad": UserBranch.HYDERABAD,
    "Pune": UserBranch.PUNE,
    "Solapur": UserBranch.SOLAPUR
});

enum DealerCategorisation { ASC_SUB_DEALER, MAIN_DEALER, DSA, USED, LAV }

final dealerCategorisationValues = EnumValues({
    "ASC - Sub dealer": DealerCategorisation.ASC_SUB_DEALER,
    "DSA": DealerCategorisation.DSA,
    "LAV": DealerCategorisation.LAV,
    "Main dealer": DealerCategorisation.MAIN_DEALER,
    "Used": DealerCategorisation.USED
});

enum DealerState { MAHARASHTRA, TELANGANA }

final dealerStateValues = EnumValues({
    "MAHARASHTRA": DealerState.MAHARASHTRA,
    "TELANGANA": DealerState.TELANGANA
});

enum DealerType { NEW_TW, RFV, USED_TW }

final dealerTypeValues = EnumValues({
    "New TW": DealerType.NEW_TW,
    "RFV": DealerType.RFV,
    "Used TW": DealerType.USED_TW
});

class DealersBankDetail {
    DealersBankDetail({
        this.isPrimaryAccount,
        this.bankAccountNumber,
        this.branchName,
        this.bankName,
        this.ifscCode,
        this.micrCode,
        this.typeOfAccount,
        this.beneficiaryName,
    });

    IsPrimaryAccount isPrimaryAccount;
    String bankAccountNumber;
    String branchName;
    String bankName;
    String ifscCode;
    dynamic micrCode;
    TypeOfAccount typeOfAccount;
    String beneficiaryName;

    factory DealersBankDetail.fromJson(Map<String, dynamic> json) => DealersBankDetail(
        isPrimaryAccount: json["IsPrimaryAccount"] == null ? null : isPrimaryAccountValues.map[json["IsPrimaryAccount"]],
        bankAccountNumber: json["Bank_Account_Number"] == null ? null : json["Bank_Account_Number"],
        branchName: json["Branch_Name"] == null ? null : json["Branch_Name"],
        bankName: json["Bank_Name"] == null ? null : json["Bank_Name"],
        ifscCode: json["IFSC_Code"] == null ? null : json["IFSC_Code"],
        micrCode: json["MICR_Code"],
        typeOfAccount: json["Type_Of_Account"] == null ? null : typeOfAccountValues.map[json["Type_Of_Account"]],
        beneficiaryName: json["Beneficiary_Name"] == null ? null : json["Beneficiary_Name"],
    );

    Map<String, dynamic> toJson() => {
        "IsPrimaryAccount": isPrimaryAccount == null ? null : isPrimaryAccountValues.reverse[isPrimaryAccount],
        "Bank_Account_Number": bankAccountNumber == null ? null : bankAccountNumber,
        "Branch_Name": branchName == null ? null : branchName,
        "Bank_Name": bankName == null ? null : bankName,
        "IFSC_Code": ifscCode == null ? null : ifscCode,
        "MICR_Code": micrCode,
        "Type_Of_Account": typeOfAccount == null ? null : typeOfAccountValues.reverse[typeOfAccount],
        "Beneficiary_Name": beneficiaryName == null ? null : beneficiaryName,
    };
}

enum IsPrimaryAccount { YES }

final isPrimaryAccountValues = EnumValues({
    "Yes": IsPrimaryAccount.YES
});

enum TypeOfAccount { THE_2_CURRENT, THE_1_SAVINGS }

final typeOfAccountValues = EnumValues({
    "1-Savings": TypeOfAccount.THE_1_SAVINGS,
    "2-Current": TypeOfAccount.THE_2_CURRENT
});

class Id {
    Id({
        this.date,
        this.currentCounter,
        this.generatedMachineIdentifier,
        this.machineIdentifier,
        this.generatedProcessIdentifier,
        this.time,
        this.counter,
        this.processIdentifier,
        this.timestamp,
        this.timeSecond,
    });

    String date;
    int currentCounter;
    int generatedMachineIdentifier;
    int machineIdentifier;
    int generatedProcessIdentifier;
    int time;
    int counter;
    int processIdentifier;
    int timestamp;
    int timeSecond;

    factory Id.fromJson(Map<String, dynamic> json) => Id(
        date: json["date"] == null ? null : json["date"],
        currentCounter: json["currentCounter"] == null ? null : json["currentCounter"],
        generatedMachineIdentifier: json["generatedMachineIdentifier"] == null ? null : json["generatedMachineIdentifier"],
        machineIdentifier: json["machineIdentifier"] == null ? null : json["machineIdentifier"],
        generatedProcessIdentifier: json["generatedProcessIdentifier"] == null ? null : json["generatedProcessIdentifier"],
        time: json["time"] == null ? null : json["time"],
        counter: json["counter"] == null ? null : json["counter"],
        processIdentifier: json["processIdentifier"] == null ? null : json["processIdentifier"],
        timestamp: json["timestamp"] == null ? null : json["timestamp"],
        timeSecond: json["timeSecond"] == null ? null : json["timeSecond"],
    );

    Map<String, dynamic> toJson() => {
        "date": date == null ? null : date,
        "currentCounter": currentCounter == null ? null : currentCounter,
        "generatedMachineIdentifier": generatedMachineIdentifier == null ? null : generatedMachineIdentifier,
        "machineIdentifier": machineIdentifier == null ? null : machineIdentifier,
        "generatedProcessIdentifier": generatedProcessIdentifier == null ? null : generatedProcessIdentifier,
        "time": time == null ? null : time,
        "counter": counter == null ? null : counter,
        "processIdentifier": processIdentifier == null ? null : processIdentifier,
        "timestamp": timestamp == null ? null : timestamp,
        "timeSecond": timeSecond == null ? null : timeSecond,
    };
}

enum Location { PUNE_CITY_EAST, PUNE_MOFFUSIL, CHENNUR, PUNE_CITY_WEST, HYDERABAD_CITY, PANDHARPUR, PUNE_CITY }

final locationValues = EnumValues({
    "CHENNUR": Location.CHENNUR,
    "Hyderabad City": Location.HYDERABAD_CITY,
    "Pandharpur": Location.PANDHARPUR,
    "Pune City": Location.PUNE_CITY,
    "Pune City East": Location.PUNE_CITY_EAST,
    "Pune City West": Location.PUNE_CITY_WEST,
    "Pune Moffusil": Location.PUNE_MOFFUSIL
});

enum Make { THE_1_APRILLA, THE_2_BAJAJ, THE_3_HARLEY_DAVIDSON, THE_4_HERO, THE_5_HONDA, THE_6_HYOSUNG, THE_7_KTM, THE_8_MAHINDRA, THE_9_PIAGGIO, THE_13_TVS, THE_18_OKINAWA, THE_19_KAWASAKI, THE_14_YAMAHA, THE_22_UML, THE_23_BMW, THE_15_TATA, THE_16_LML, THE_17_DSK_BENELLI, THE_11_SUZUKI, THE_10_ROYAL_ENFIELD, THE_12_TRIUMPH, THE_25_JAWA, THE_26_AMPERE, EMPTY, THE_007_SUMA_MAKE, THE_008_SUMA_MAKE_ONE, THE_007_SUAMMAKE_TWO, THE_009_SUMA_MAKE_THREE, MAKE_2_BAJAJ }

final makeValues = EnumValues({
    "": Make.EMPTY,
    "2-Bajaj": Make.MAKE_2_BAJAJ,
    "007-SuammakeTwo": Make.THE_007_SUAMMAKE_TWO,
    "007-SumaMake": Make.THE_007_SUMA_MAKE,
    "008-SumaMakeOne": Make.THE_008_SUMA_MAKE_ONE,
    "009-SumaMakeThree": Make.THE_009_SUMA_MAKE_THREE,
    "10-ROYAL ENFIELD": Make.THE_10_ROYAL_ENFIELD,
    "11-Suzuki": Make.THE_11_SUZUKI,
    "12-TRIUMPH": Make.THE_12_TRIUMPH,
    "13-TVS": Make.THE_13_TVS,
    "14-YAMAHA": Make.THE_14_YAMAHA,
    "15-TATA": Make.THE_15_TATA,
    "16-LML": Make.THE_16_LML,
    "17-DSK Benelli": Make.THE_17_DSK_BENELLI,
    "18-Okinawa": Make.THE_18_OKINAWA,
    "19-Kawasaki": Make.THE_19_KAWASAKI,
    "1-Aprilla": Make.THE_1_APRILLA,
    "22-UML": Make.THE_22_UML,
    "23-BMW": Make.THE_23_BMW,
    "25-JAWA": Make.THE_25_JAWA,
    "26-AMPERE": Make.THE_26_AMPERE,
    "2-BAJAJ": Make.THE_2_BAJAJ,
    "3-HARLEY DAVIDSON": Make.THE_3_HARLEY_DAVIDSON,
    "4-Hero": Make.THE_4_HERO,
    "5-Honda": Make.THE_5_HONDA,
    "6-HYOSUNG": Make.THE_6_HYOSUNG,
    "7-KTM": Make.THE_7_KTM,
    "8-MAHINDRA": Make.THE_8_MAHINDRA,
    "9-PIAGGIO": Make.THE_9_PIAGGIO
});

enum RepaymentMode { THE_4_CASH, THE_1_ACH }

final repaymentModeValues = EnumValues({
    "1-ACH": RepaymentMode.THE_1_ACH,
    "4-Cash": RepaymentMode.THE_4_CASH
});

class LeadDocuments {
    LeadDocuments({
        this.the16LoginSheet,
        this.the17Form16Itr,
        this.the1CustomerPhoto,
        this.the18RelationProof,
        this.the2ApplicantAadharCard,
        this.the19Form60,
        this.the23SellerSalesLetter,
        this.the15CoAppOrGrntrOtherIdProof,
        this.the21IncomeProof,
        this.the12CoAppOrGrntrOwnHouseProof,
        this.the20Form61,
        this.the22LatestUtilityBill,
        this.the3ApplicantPanCard,
        this.the4ApplicantAddressProof,
        this.the10CoAppOrGrntrPanCard,
        this.the9CoAppOrGrntrAadharCard,
        this.the5ApplicantOwnHouseProof,
        this.the11CoAppOrGrntrAddressProof,
        this.the8ApplicantOtherIdProof,
        this.the13CoAppOrGrntrSalarySlip,
        this.the7ApplicantBankStatement,
        this.the6ApplicantSalarySlip,
        this.the14CoAppOrGrntrBankStatement,
    });

    String the16LoginSheet;
    String the17Form16Itr;
    String the1CustomerPhoto;
    String the18RelationProof;
    String the2ApplicantAadharCard;
    String the19Form60;
    String the23SellerSalesLetter;
    String the15CoAppOrGrntrOtherIdProof;
    String the21IncomeProof;
    String the12CoAppOrGrntrOwnHouseProof;
    String the20Form61;
    String the22LatestUtilityBill;
    String the3ApplicantPanCard;
    String the4ApplicantAddressProof;
    String the10CoAppOrGrntrPanCard;
    String the9CoAppOrGrntrAadharCard;
    String the5ApplicantOwnHouseProof;
    String the11CoAppOrGrntrAddressProof;
    String the8ApplicantOtherIdProof;
    String the13CoAppOrGrntrSalarySlip;
    String the7ApplicantBankStatement;
    String the6ApplicantSalarySlip;
    String the14CoAppOrGrntrBankStatement;

    factory LeadDocuments.fromJson(Map<String, dynamic> json) => LeadDocuments(
        the16LoginSheet: json["16__LoginSheet"] == null ? null : json["16__LoginSheet"],
        the17Form16Itr: json["17__Form16_ITR"] == null ? null : json["17__Form16_ITR"],
        the1CustomerPhoto: json["1__CustomerPhoto"] == null ? null : json["1__CustomerPhoto"],
        the18RelationProof: json["18__RelationProof"] == null ? null : json["18__RelationProof"],
        the2ApplicantAadharCard: json["2__ApplicantAadharCard"] == null ? null : json["2__ApplicantAadharCard"],
        the19Form60: json["19__Form60"] == null ? null : json["19__Form60"],
        the23SellerSalesLetter: json["23__SellerSalesLetter"] == null ? null : json["23__SellerSalesLetter"],
        the15CoAppOrGrntrOtherIdProof: json["15__CoApp_Or_Grntr_OtherIDProof"] == null ? null : json["15__CoApp_Or_Grntr_OtherIDProof"],
        the21IncomeProof: json["21__IncomeProof"] == null ? null : json["21__IncomeProof"],
        the12CoAppOrGrntrOwnHouseProof: json["12__CoApp_Or_Grntr_OwnHouseProof"] == null ? null : json["12__CoApp_Or_Grntr_OwnHouseProof"],
        the20Form61: json["20__Form61"] == null ? null : json["20__Form61"],
        the22LatestUtilityBill: json["22__LatestUtilityBill"] == null ? null : json["22__LatestUtilityBill"],
        the3ApplicantPanCard: json["3__ApplicantPANCard"] == null ? null : json["3__ApplicantPANCard"],
        the4ApplicantAddressProof: json["4__ApplicantAddressProof"] == null ? null : json["4__ApplicantAddressProof"],
        the10CoAppOrGrntrPanCard: json["10__CoApp_Or_Grntr_PANCard"] == null ? null : json["10__CoApp_Or_Grntr_PANCard"],
        the9CoAppOrGrntrAadharCard: json["9__CoApp_Or_Grntr_AadharCard"] == null ? null : json["9__CoApp_Or_Grntr_AadharCard"],
        the5ApplicantOwnHouseProof: json["5__ApplicantOwnHouseProof"] == null ? null : json["5__ApplicantOwnHouseProof"],
        the11CoAppOrGrntrAddressProof: json["11__CoApp_Or_Grntr_AddressProof"] == null ? null : json["11__CoApp_Or_Grntr_AddressProof"],
        the8ApplicantOtherIdProof: json["8__ApplicantOtherIDProof"] == null ? null : json["8__ApplicantOtherIDProof"],
        the13CoAppOrGrntrSalarySlip: json["13__CoApp_Or_Grntr_SalarySlip"] == null ? null : json["13__CoApp_Or_Grntr_SalarySlip"],
        the7ApplicantBankStatement: json["7__ApplicantBankStatement"] == null ? null : json["7__ApplicantBankStatement"],
        the6ApplicantSalarySlip: json["6__ApplicantSalarySlip"] == null ? null : json["6__ApplicantSalarySlip"],
        the14CoAppOrGrntrBankStatement: json["14__CoApp_Or_Grntr_BankStatement"] == null ? null : json["14__CoApp_Or_Grntr_BankStatement"],
    );

    Map<String, dynamic> toJson() => {
        "16__LoginSheet": the16LoginSheet == null ? null : the16LoginSheet,
        "17__Form16_ITR": the17Form16Itr == null ? null : the17Form16Itr,
        "1__CustomerPhoto": the1CustomerPhoto == null ? null : the1CustomerPhoto,
        "18__RelationProof": the18RelationProof == null ? null : the18RelationProof,
        "2__ApplicantAadharCard": the2ApplicantAadharCard == null ? null : the2ApplicantAadharCard,
        "19__Form60": the19Form60 == null ? null : the19Form60,
        "23__SellerSalesLetter": the23SellerSalesLetter == null ? null : the23SellerSalesLetter,
        "15__CoApp_Or_Grntr_OtherIDProof": the15CoAppOrGrntrOtherIdProof == null ? null : the15CoAppOrGrntrOtherIdProof,
        "21__IncomeProof": the21IncomeProof == null ? null : the21IncomeProof,
        "12__CoApp_Or_Grntr_OwnHouseProof": the12CoAppOrGrntrOwnHouseProof == null ? null : the12CoAppOrGrntrOwnHouseProof,
        "20__Form61": the20Form61 == null ? null : the20Form61,
        "22__LatestUtilityBill": the22LatestUtilityBill == null ? null : the22LatestUtilityBill,
        "3__ApplicantPANCard": the3ApplicantPanCard == null ? null : the3ApplicantPanCard,
        "4__ApplicantAddressProof": the4ApplicantAddressProof == null ? null : the4ApplicantAddressProof,
        "10__CoApp_Or_Grntr_PANCard": the10CoAppOrGrntrPanCard == null ? null : the10CoAppOrGrntrPanCard,
        "9__CoApp_Or_Grntr_AadharCard": the9CoAppOrGrntrAadharCard == null ? null : the9CoAppOrGrntrAadharCard,
        "5__ApplicantOwnHouseProof": the5ApplicantOwnHouseProof == null ? null : the5ApplicantOwnHouseProof,
        "11__CoApp_Or_Grntr_AddressProof": the11CoAppOrGrntrAddressProof == null ? null : the11CoAppOrGrntrAddressProof,
        "8__ApplicantOtherIDProof": the8ApplicantOtherIdProof == null ? null : the8ApplicantOtherIdProof,
        "13__CoApp_Or_Grntr_SalarySlip": the13CoAppOrGrntrSalarySlip == null ? null : the13CoAppOrGrntrSalarySlip,
        "7__ApplicantBankStatement": the7ApplicantBankStatement == null ? null : the7ApplicantBankStatement,
        "6__ApplicantSalarySlip": the6ApplicantSalarySlip == null ? null : the6ApplicantSalarySlip,
        "14__CoApp_Or_Grntr_BankStatement": the14CoAppOrGrntrBankStatement == null ? null : the14CoAppOrGrntrBankStatement,
    };
}

class PostDisbDocumentList {
    PostDisbDocumentList({
        this.the2Invoice,
        this.the11FiRecordingPermanentAddress,
        this.the12CoAppPermanentAddressFiReport,
        this.the6Form61,
        this.the7IncomeProof,
        this.the16CoAppResidenceAndOfficeFiReport,
        this.the13CoAppFiRecordingPermanentAddress,
        this.the14ResidenceAndOfficeFiReport,
        this.the3Insurance,
        this.the15FiRecordingResidenceAndOffice,
        this.the4Rto,
        this.the8LatestUtilityBill,
        this.the5Form60,
        this.the9SellerSalesLetter,
        this.the10PermanentAddressFiReport,
        this.the1Rc,
        this.the17CoAppFiRecordingResidenceAndOffice,
    });

    String the2Invoice;
    String the11FiRecordingPermanentAddress;
    String the12CoAppPermanentAddressFiReport;
    String the6Form61;
    String the7IncomeProof;
    String the16CoAppResidenceAndOfficeFiReport;
    String the13CoAppFiRecordingPermanentAddress;
    String the14ResidenceAndOfficeFiReport;
    String the3Insurance;
    String the15FiRecordingResidenceAndOffice;
    String the4Rto;
    String the8LatestUtilityBill;
    String the5Form60;
    String the9SellerSalesLetter;
    String the10PermanentAddressFiReport;
    String the1Rc;
    String the17CoAppFiRecordingResidenceAndOffice;

    factory PostDisbDocumentList.fromJson(Map<String, dynamic> json) => PostDisbDocumentList(
        the2Invoice: json["2__Invoice"] == null ? null : json["2__Invoice"],
        the11FiRecordingPermanentAddress: json["11__FIRecordingPermanentAddress"] == null ? null : json["11__FIRecordingPermanentAddress"],
        the12CoAppPermanentAddressFiReport: json["12__CoApp_PermanentAddress_FIReport"] == null ? null : json["12__CoApp_PermanentAddress_FIReport"],
        the6Form61: json["6__Form61"] == null ? null : json["6__Form61"],
        the7IncomeProof: json["7__IncomeProof"] == null ? null : json["7__IncomeProof"],
        the16CoAppResidenceAndOfficeFiReport: json["16__CoApp_ResidenceAndOffice_FIReport"] == null ? null : json["16__CoApp_ResidenceAndOffice_FIReport"],
        the13CoAppFiRecordingPermanentAddress: json["13__CoAppFIRecordingPermanentAddress"] == null ? null : json["13__CoAppFIRecordingPermanentAddress"],
        the14ResidenceAndOfficeFiReport: json["14__ResidenceAndOffice_FIReport"] == null ? null : json["14__ResidenceAndOffice_FIReport"],
        the3Insurance: json["3__Insurance"] == null ? null : json["3__Insurance"],
        the15FiRecordingResidenceAndOffice: json["15__FIRecordingResidenceAndOffice"] == null ? null : json["15__FIRecordingResidenceAndOffice"],
        the4Rto: json["4__RTO"] == null ? null : json["4__RTO"],
        the8LatestUtilityBill: json["8__LatestUtilityBill"] == null ? null : json["8__LatestUtilityBill"],
        the5Form60: json["5__Form60"] == null ? null : json["5__Form60"],
        the9SellerSalesLetter: json["9__SellerSalesLetter"] == null ? null : json["9__SellerSalesLetter"],
        the10PermanentAddressFiReport: json["10__PermanentAddress_FIReport"] == null ? null : json["10__PermanentAddress_FIReport"],
        the1Rc: json["1__RC"] == null ? null : json["1__RC"],
        the17CoAppFiRecordingResidenceAndOffice: json["17__CoAppFIRecordingResidenceAndOffice"] == null ? null : json["17__CoAppFIRecordingResidenceAndOffice"],
    );

    Map<String, dynamic> toJson() => {
        "2__Invoice": the2Invoice == null ? null : the2Invoice,
        "11__FIRecordingPermanentAddress": the11FiRecordingPermanentAddress == null ? null : the11FiRecordingPermanentAddress,
        "12__CoApp_PermanentAddress_FIReport": the12CoAppPermanentAddressFiReport == null ? null : the12CoAppPermanentAddressFiReport,
        "6__Form61": the6Form61 == null ? null : the6Form61,
        "7__IncomeProof": the7IncomeProof == null ? null : the7IncomeProof,
        "16__CoApp_ResidenceAndOffice_FIReport": the16CoAppResidenceAndOfficeFiReport == null ? null : the16CoAppResidenceAndOfficeFiReport,
        "13__CoAppFIRecordingPermanentAddress": the13CoAppFiRecordingPermanentAddress == null ? null : the13CoAppFiRecordingPermanentAddress,
        "14__ResidenceAndOffice_FIReport": the14ResidenceAndOfficeFiReport == null ? null : the14ResidenceAndOfficeFiReport,
        "3__Insurance": the3Insurance == null ? null : the3Insurance,
        "15__FIRecordingResidenceAndOffice": the15FiRecordingResidenceAndOffice == null ? null : the15FiRecordingResidenceAndOffice,
        "4__RTO": the4Rto == null ? null : the4Rto,
        "8__LatestUtilityBill": the8LatestUtilityBill == null ? null : the8LatestUtilityBill,
        "5__Form60": the5Form60 == null ? null : the5Form60,
        "9__SellerSalesLetter": the9SellerSalesLetter == null ? null : the9SellerSalesLetter,
        "10__PermanentAddress_FIReport": the10PermanentAddressFiReport == null ? null : the10PermanentAddressFiReport,
        "1__RC": the1Rc == null ? null : the1Rc,
        "17__CoAppFIRecordingResidenceAndOffice": the17CoAppFiRecordingResidenceAndOffice == null ? null : the17CoAppFiRecordingResidenceAndOffice,
    };
}

class PreDisbDocumentList {
    PreDisbDocumentList({
        this.the7AgreementAndTtoSet,
        this.the3ApplicantAddressPhoto,
        this.the10ApprovalDeviationCompliance,
        this.the1ApplicantOtherIdProof,
        this.the8RepaymentMode,
        this.the9CustomerSignatures,
        this.the12DisbursementMemoPreview,
        this.the4IncomeDocument,
        this.the5DeviationCompliance,
        this.the2ApplicantAddressProof,
        this.the6ApplicationForm,
        this.the11DealerDocuments,
    });

    String the7AgreementAndTtoSet;
    String the3ApplicantAddressPhoto;
    String the10ApprovalDeviationCompliance;
    String the1ApplicantOtherIdProof;
    String the8RepaymentMode;
    String the9CustomerSignatures;
    String the12DisbursementMemoPreview;
    String the4IncomeDocument;
    String the5DeviationCompliance;
    String the2ApplicantAddressProof;
    String the6ApplicationForm;
    String the11DealerDocuments;

    factory PreDisbDocumentList.fromJson(Map<String, dynamic> json) => PreDisbDocumentList(
        the7AgreementAndTtoSet: json["7__AgreementAndTTOSet"] == null ? null : json["7__AgreementAndTTOSet"],
        the3ApplicantAddressPhoto: json["3__Applicant_Address_Photo"] == null ? null : json["3__Applicant_Address_Photo"],
        the10ApprovalDeviationCompliance: json["10__Approval_Deviation_Compliance"] == null ? null : json["10__Approval_Deviation_Compliance"],
        the1ApplicantOtherIdProof: json["1__ApplicantOtherIDProof"] == null ? null : json["1__ApplicantOtherIDProof"],
        the8RepaymentMode: json["8__Repayment_Mode"] == null ? null : json["8__Repayment_Mode"],
        the9CustomerSignatures: json["9__CustomerSignatures"] == null ? null : json["9__CustomerSignatures"],
        the12DisbursementMemoPreview: json["12__DisbursementMemoPreview"] == null ? null : json["12__DisbursementMemoPreview"],
        the4IncomeDocument: json["4__IncomeDocument"] == null ? null : json["4__IncomeDocument"],
        the5DeviationCompliance: json["5__DeviationCompliance"] == null ? null : json["5__DeviationCompliance"],
        the2ApplicantAddressProof: json["2__ApplicantAddressProof"] == null ? null : json["2__ApplicantAddressProof"],
        the6ApplicationForm: json["6__ApplicationForm"] == null ? null : json["6__ApplicationForm"],
        the11DealerDocuments: json["11__DealerDocuments"] == null ? null : json["11__DealerDocuments"],
    );

    Map<String, dynamic> toJson() => {
        "7__AgreementAndTTOSet": the7AgreementAndTtoSet == null ? null : the7AgreementAndTtoSet,
        "3__Applicant_Address_Photo": the3ApplicantAddressPhoto == null ? null : the3ApplicantAddressPhoto,
        "10__Approval_Deviation_Compliance": the10ApprovalDeviationCompliance == null ? null : the10ApprovalDeviationCompliance,
        "1__ApplicantOtherIDProof": the1ApplicantOtherIdProof == null ? null : the1ApplicantOtherIdProof,
        "8__Repayment_Mode": the8RepaymentMode == null ? null : the8RepaymentMode,
        "9__CustomerSignatures": the9CustomerSignatures == null ? null : the9CustomerSignatures,
        "12__DisbursementMemoPreview": the12DisbursementMemoPreview == null ? null : the12DisbursementMemoPreview,
        "4__IncomeDocument": the4IncomeDocument == null ? null : the4IncomeDocument,
        "5__DeviationCompliance": the5DeviationCompliance == null ? null : the5DeviationCompliance,
        "2__ApplicantAddressProof": the2ApplicantAddressProof == null ? null : the2ApplicantAddressProof,
        "6__ApplicationForm": the6ApplicationForm == null ? null : the6ApplicationForm,
        "11__DealerDocuments": the11DealerDocuments == null ? null : the11DealerDocuments,
    };
}

class ServiceInspectionDocumentList {
    ServiceInspectionDocumentList({
        this.the5InvoiceImage,
        this.the4SellerSalesLetterImage,
        this.the7OtherRegistrationDocumentImage,
        this.the3InsuranceImage,
        this.the8VehicleInspectionVideoVideo,
        this.the10IndexImage,
        this.the10IndexNull,
        this.the1RcImage,
        this.the6VehiclePhotoImage,
        this.the9OtherSiImage,
        this.the2NocImage,
    });

    String the5InvoiceImage;
    String the4SellerSalesLetterImage;
    String the7OtherRegistrationDocumentImage;
    String the3InsuranceImage;
    String the8VehicleInspectionVideoVideo;
    String the10IndexImage;
    String the10IndexNull;
    String the1RcImage;
    String the6VehiclePhotoImage;
    String the9OtherSiImage;
    String the2NocImage;

    factory ServiceInspectionDocumentList.fromJson(Map<String, dynamic> json) => ServiceInspectionDocumentList(
        the5InvoiceImage: json["5__Invoice__image"] == null ? null : json["5__Invoice__image"],
        the4SellerSalesLetterImage: json["4__SellerSalesLetter__image"] == null ? null : json["4__SellerSalesLetter__image"],
        the7OtherRegistrationDocumentImage: json["7__Other_Registration_Document__image"] == null ? null : json["7__Other_Registration_Document__image"],
        the3InsuranceImage: json["3__Insurance__image"] == null ? null : json["3__Insurance__image"],
        the8VehicleInspectionVideoVideo: json["8__VehicleInspectionVideo__video"] == null ? null : json["8__VehicleInspectionVideo__video"],
        the10IndexImage: json["10__Index__image"] == null ? null : json["10__Index__image"],
        the10IndexNull: json["10__Index__null"] == null ? null : json["10__Index__null"],
        the1RcImage: json["1__RC__image"] == null ? null : json["1__RC__image"],
        the6VehiclePhotoImage: json["6__VehiclePhoto__image"] == null ? null : json["6__VehiclePhoto__image"],
        the9OtherSiImage: json["9__Other_SI__image"] == null ? null : json["9__Other_SI__image"],
        the2NocImage: json["2__NOC__image"] == null ? null : json["2__NOC__image"],
    );

    Map<String, dynamic> toJson() => {
        "5__Invoice__image": the5InvoiceImage == null ? null : the5InvoiceImage,
        "4__SellerSalesLetter__image": the4SellerSalesLetterImage == null ? null : the4SellerSalesLetterImage,
        "7__Other_Registration_Document__image": the7OtherRegistrationDocumentImage == null ? null : the7OtherRegistrationDocumentImage,
        "3__Insurance__image": the3InsuranceImage == null ? null : the3InsuranceImage,
        "8__VehicleInspectionVideo__video": the8VehicleInspectionVideoVideo == null ? null : the8VehicleInspectionVideoVideo,
        "10__Index__image": the10IndexImage == null ? null : the10IndexImage,
        "10__Index__null": the10IndexNull == null ? null : the10IndexNull,
        "1__RC__image": the1RcImage == null ? null : the1RcImage,
        "6__VehiclePhoto__image": the6VehiclePhotoImage == null ? null : the6VehiclePhotoImage,
        "9__Other_SI__image": the9OtherSiImage == null ? null : the9OtherSiImage,
        "2__NOC__image": the2NocImage == null ? null : the2NocImage,
    };
}

class UpdateLeadDocumentList {
    UpdateLeadDocumentList({
        this.the16LoginSheet,
    });

    String the16LoginSheet;

    factory UpdateLeadDocumentList.fromJson(Map<String, dynamic> json) => UpdateLeadDocumentList(
        the16LoginSheet: json["16__LoginSheet"] == null ? null : json["16__LoginSheet"],
    );

    Map<String, dynamic> toJson() => {
        "16__LoginSheet": the16LoginSheet == null ? null : the16LoginSheet,
    };
}

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
