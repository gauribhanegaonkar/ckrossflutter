// To parse this JSON data, do
//
//     final getMobileOtpResModel = getMobileOtpResModelFromJson(jsonString);

import 'dart:convert';

GetMobileOtpResModel getMobileOtpResModelFromJson(String str) => GetMobileOtpResModel.fromJson(json.decode(str));

String getMobileOtpResModelToJson(GetMobileOtpResModel data) => json.encode(data.toJson());

class GetMobileOtpResModel {
    GetMobileOtpResModel({
        this.redirect,
        this.success,
        this.details,
        
    });

    bool redirect;
    bool success;
    String details;
   

    factory GetMobileOtpResModel.fromJson(Map<String, dynamic> json) => GetMobileOtpResModel(
        redirect: json["redirect"] == null ? null : json["redirect"],
        success: json["success"] == null ? null : json["success"],
        details: json["details"] == null ? null : json["details"],
    );

    Map<String, dynamic> toJson() => {
        "redirect": redirect == null ? null : redirect,
        "success": success == null ? null : success,
        "details": details == null ? null : details,
    };
}
