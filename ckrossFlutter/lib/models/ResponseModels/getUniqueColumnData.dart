import 'dart:convert';

GetUniqueColumnDataModel getUniqueColumnDataModelFromJson(String str) => GetUniqueColumnDataModel.fromJson(json.decode(str));

String getMobileOtpResModelToJson(GetUniqueColumnDataModel data) => json.encode(data.toJson());

class GetUniqueColumnDataModel {
  GetUniqueColumnDataModel({
    this.redirect,
    this.redirectURL,
    this.success,
    this.details,

  });

  bool redirect;
  String redirectURL;
  bool success;
  List details;


  factory GetUniqueColumnDataModel.fromJson(Map<String, dynamic> json) => GetUniqueColumnDataModel(
    redirect: json["redirect"] == null ? null : json["redirect"],
    redirectURL: json["redirectURL"] == null ? null : json["redirectURL"],
    success: json["success"] == null ? null : json["success"],
    details: json["details"] == null ? null : json["details"],
  );

  Map<String, dynamic> toJson() => {
    "redirect": redirect == null ? null : redirect,
    "redirectURL": redirectURL == null ? null : redirectURL,
    "success": success == null ? null : success,
    "details": details == null ? null : details,
  };
}
