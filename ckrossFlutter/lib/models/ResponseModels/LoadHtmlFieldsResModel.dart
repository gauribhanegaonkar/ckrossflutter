//
//import 'dart:convert';
//
//LoadHtmlFieldsResModel loadHtmlFieldsResModelFromJson(String str) => LoadHtmlFieldsResModel.fromJson(json.decode(str));
//
//String loadHtmlFieldsResModelToJson(LoadHtmlFieldsResModel data) => json.encode(data.toJson());
//
//class LoadHtmlFieldsResModel {
//    LoadHtmlFieldsResModel({
//        this.redirect,
//        this.tabData,
//        this.redirectUrl,
//        this.success,
//        this.details,
//        this.subProcessFieldsData,
//    });
//
//    bool redirect;
//    List<TabDatum> tabData;
//    String redirectUrl;
//    bool success;
//    Details details;
//    List<SubProcessFieldsDatum> subProcessFieldsData;
//
//    factory LoadHtmlFieldsResModel.fromJson(Map<String, dynamic> json) => LoadHtmlFieldsResModel(
//        redirect: json["redirect"] == null ? null : json["redirect"],
//        tabData: json["tabData"] == null ? null : List<TabDatum>.from(json["tabData"].map((x) => TabDatum.fromJson(x))),
//    redirectUrl: json["redirectURL"] == null ? null : json["redirectURL"],
//    success: json["success"] == null ? null : json["success"],
//    details: json["details"] == null ? null : Details.fromJson(json["details"]),
//    subProcessFieldsData: json["subProcessFieldsData"] == null ? null : List<SubProcessFieldsDatum>.from(json["subProcessFieldsData"].map((x) => SubProcessFieldsDatum.fromJson(x))),
//    );
//
//    Map<String, dynamic> toJson() => {
//        "redirect": redirect == null ? null : redirect,
//        "tabData": tabData == null ? null : List<dynamic>.from(tabData.map((x) => x.toJson())),
//        "redirectURL": redirectUrl == null ? null : redirectUrl,
//        "success": success == null ? null : success,
//        "details": details == null ? null : details.toJson(),
//        "subProcessFieldsData": subProcessFieldsData == null ? null : List<dynamic>.from(subProcessFieldsData.map((x) => x.toJson())),
//    };
//}
//
//class Details {
//    Details({
//        this.leadDocuments,
//    });
//
//    LeadDocuments leadDocuments;
//
//    factory Details.fromJson(Map<String, dynamic> json) => Details(
//        leadDocuments: json["leadDocuments"] == null ? null : LeadDocuments.fromJson(json["leadDocuments"]),
//    );
//
//    Map<String, dynamic> toJson() => {
//        "leadDocuments": leadDocuments == null ? null : leadDocuments.toJson(),
//    };
//}
//
//class LeadDocuments {
//    LeadDocuments({
//        this.the1CustomerPhoto,
//        this.the11Index,
//        this.the10Others,
//        this.the9ApplicantSalarySlip,
//        this.the6ApplicantAddressProof,
//        this.the12ApplicantBankStatement,
//        this.the3ApplicantPanCard,
//        this.the2LoginSheet,
//        this.the5ApplicantOtherIdProof,
//        this.the14Rc,
//        this.the8ApplicantOwnHouseProof,
//        this.the7LatestUtilityBill,
//        this.the13Form16Itr,
//        this.the4ApplicantAadharCard,
//        this.the10ApplicantBankDetails,
//    });
//
//    String the1CustomerPhoto;
//    String the11Index;
//    String the10Others;
//    String the9ApplicantSalarySlip;
//    String the6ApplicantAddressProof;
//    String the12ApplicantBankStatement;
//    String the3ApplicantPanCard;
//    String the2LoginSheet;
//    String the5ApplicantOtherIdProof;
//    String the14Rc;
//    String the8ApplicantOwnHouseProof;
//    String the7LatestUtilityBill;
//    String the13Form16Itr;
//    String the4ApplicantAadharCard;
//    String the10ApplicantBankDetails;
//
//    factory LeadDocuments.fromJson(Map<String, dynamic> json) => LeadDocuments(
//        the1CustomerPhoto: json["1__CustomerPhoto"] == null ? null : json["1__CustomerPhoto"],
//        the11Index: json["11__Index"] == null ? null : json["11__Index"],
//        the10Others: json["10__Others"] == null ? null : json["10__Others"],
//        the9ApplicantSalarySlip: json["9__ApplicantSalarySlip"] == null ? null : json["9__ApplicantSalarySlip"],
//        the6ApplicantAddressProof: json["6__ApplicantAddressProof"] == null ? null : json["6__ApplicantAddressProof"],
//        the12ApplicantBankStatement: json["12__ApplicantBankStatement"] == null ? null : json["12__ApplicantBankStatement"],
//        the3ApplicantPanCard: json["3__ApplicantPANCard"] == null ? null : json["3__ApplicantPANCard"],
//        the2LoginSheet: json["2__LoginSheet"] == null ? null : json["2__LoginSheet"],
//        the5ApplicantOtherIdProof: json["5__ApplicantOtherIDProof"] == null ? null : json["5__ApplicantOtherIDProof"],
//        the14Rc: json["14__RC"] == null ? null : json["14__RC"],
//        the8ApplicantOwnHouseProof: json["8__ApplicantOwnHouseProof"] == null ? null : json["8__ApplicantOwnHouseProof"],
//        the7LatestUtilityBill: json["7__LatestUtilityBill"] == null ? null : json["7__LatestUtilityBill"],
//        the13Form16Itr: json["13__Form16_ITR"] == null ? null : json["13__Form16_ITR"],
//        the4ApplicantAadharCard: json["4__ApplicantAadharCard"] == null ? null : json["4__ApplicantAadharCard"],
//        the10ApplicantBankDetails: json["10__ApplicantBankDetails"] == null ? null : json["10__ApplicantBankDetails"],
//    );
//
//    Map<String, dynamic> toJson() => {
//        "1__CustomerPhoto": the1CustomerPhoto == null ? null : the1CustomerPhoto,
//        "11__Index": the11Index == null ? null : the11Index,
//        "10__Others": the10Others == null ? null : the10Others,
//        "9__ApplicantSalarySlip": the9ApplicantSalarySlip == null ? null : the9ApplicantSalarySlip,
//        "6__ApplicantAddressProof": the6ApplicantAddressProof == null ? null : the6ApplicantAddressProof,
//        "12__ApplicantBankStatement": the12ApplicantBankStatement == null ? null : the12ApplicantBankStatement,
//        "3__ApplicantPANCard": the3ApplicantPanCard == null ? null : the3ApplicantPanCard,
//        "2__LoginSheet": the2LoginSheet == null ? null : the2LoginSheet,
//        "5__ApplicantOtherIDProof": the5ApplicantOtherIdProof == null ? null : the5ApplicantOtherIdProof,
//        "14__RC": the14Rc == null ? null : the14Rc,
//        "8__ApplicantOwnHouseProof": the8ApplicantOwnHouseProof == null ? null : the8ApplicantOwnHouseProof,
//        "7__LatestUtilityBill": the7LatestUtilityBill == null ? null : the7LatestUtilityBill,
//        "13__Form16_ITR": the13Form16Itr == null ? null : the13Form16Itr,
//        "4__ApplicantAadharCard": the4ApplicantAadharCard == null ? null : the4ApplicantAadharCard,
//        "10__ApplicantBankDetails": the10ApplicantBankDetails == null ? null : the10ApplicantBankDetails,
//    };
//}
//
//class SubProcessFieldsDatum {
//    SubProcessFieldsDatum({
//        this.isTableExist,
//        this.subProcessKey,
//        this.processName,
//        this.processId,
//        this.subProcessName,
//        this.controllerName,
//        this.subProcessFields,
//        this.id,
//    });
//
//    bool isTableExist;
//    String subProcessKey;
//    String processName;
//    int processId;
//    String subProcessName;
//    String controllerName;
//    List<SubProcessField> subProcessFields;
//    Id id;
//
//    factory SubProcessFieldsDatum.fromJson(Map<String, dynamic> json) => SubProcessFieldsDatum(
//        isTableExist: json["isTableExist"] == null ? null : json["isTableExist"],
//        subProcessKey: json["subProcessKey"] == null ? null : json["subProcessKey"],
//        processName: json["processName"] == null ? null : json["processName"],
//        processId: json["processID"] == null ? null : json["processID"],
//        subProcessName: json["subProcessName"] == null ? null : json["subProcessName"],
//        controllerName: json["controllerName"] == null ? null : json["controllerName"],
//        subProcessFields: json["subProcessFields"] == null ? null : List<SubProcessField>.from(json["subProcessFields"].map((x) => SubProcessField.fromJson(x))),
//    id: json["_id"] == null ? null : Id.fromJson(json["_id"]),
//    );
//
//    Map<String, dynamic> toJson() => {
//        "isTableExist": isTableExist == null ? null : isTableExist,
//        "subProcessKey": subProcessKey == null ? null : subProcessKey,
//        "processName": processName == null ? null : processName,
//        "processID": processId == null ? null : processId,
//        "subProcessName": subProcessName == null ? null : subProcessName,
//        "controllerName": controllerName == null ? null : controllerName,
//        "subProcessFields": subProcessFields == null ? null : List<dynamic>.from(subProcessFields.map((x) => x.toJson())),
//        "_id": id == null ? null : id.toJson(),
//    };
//}
//
//class Id {
//    Id({
//        this.date,
//        this.currentCounter,
//        this.generatedMachineIdentifier,
//        this.machineIdentifier,
//        this.generatedProcessIdentifier,
//        this.time,
//        this.processIdentifier,
//        this.counter,
//        this.timestamp,
//        this.timeSecond,
//    });
//
//    String date;
//    int currentCounter;
//    int generatedMachineIdentifier;
//    int machineIdentifier;
//    int generatedProcessIdentifier;
//    int time;
//    int processIdentifier;
//    int counter;
//    int timestamp;
//    int timeSecond;
//
//    factory Id.fromJson(Map<String, dynamic> json) => Id(
//        date: json["date"] == null ? null : json["date"],
//        currentCounter: json["currentCounter"] == null ? null : json["currentCounter"],
//        generatedMachineIdentifier: json["generatedMachineIdentifier"] == null ? null : json["generatedMachineIdentifier"],
//        machineIdentifier: json["machineIdentifier"] == null ? null : json["machineIdentifier"],
//        generatedProcessIdentifier: json["generatedProcessIdentifier"] == null ? null : json["generatedProcessIdentifier"],
//        time: json["time"] == null ? null : json["time"],
//        processIdentifier: json["processIdentifier"] == null ? null : json["processIdentifier"],
//        counter: json["counter"] == null ? null : json["counter"],
//        timestamp: json["timestamp"] == null ? null : json["timestamp"],
//        timeSecond: json["timeSecond"] == null ? null : json["timeSecond"],
//    );
//
//    Map<String, dynamic> toJson() => {
//        "date": date == null ? null : date,
//        "currentCounter": currentCounter == null ? null : currentCounter,
//        "generatedMachineIdentifier": generatedMachineIdentifier == null ? null : generatedMachineIdentifier,
//        "machineIdentifier": machineIdentifier == null ? null : machineIdentifier,
//        "generatedProcessIdentifier": generatedProcessIdentifier == null ? null : generatedProcessIdentifier,
//        "time": time == null ? null : time,
//        "processIdentifier": processIdentifier == null ? null : processIdentifier,
//        "counter": counter == null ? null : counter,
//        "timestamp": timestamp == null ? null : timestamp,
//        "timeSecond": timeSecond == null ? null : timeSecond,
//    };
//}
//
//class SubProcessField {
//    SubProcessField({
//        this.validationPattern,
//        this.fieldName,
//        this.isDataList,
//        this.maxlength,
//        this.defaultValue,
//        this.validationType,
//        this.validationMessage,
//        this.dependentField,
//        this.masterCollection,
//        this.lable,
//        this.fieldType,
//        this.value,
//        this.key,
//        this.validation,
//        this.isMandatory,
//        this.fieldId,
//        this.onclick,
//        this.customValidationFunction,
//        this.onchange,
//        this.master,
//        this.isAllMasterData,
//        this.isreadonly,
//        this.columnName,
//        this.startOnNewLine,
//        this.dependentFieldMob,
//        this.isMake,
//        this.mobileInputApiKey,
//        this.dependentFieldMobList,
//        this.isMobileInputRequired,
//        this.isMasterData,
//        this.isDealerSpecific,
//        this.isBlur,
//        this.isMultiSelect,
//    });
//
//    String validationPattern;
//    String fieldName;
//    bool isDataList;
//    dynamic maxlength;
//    DefaultValue defaultValue;
//    Validation validationType;
//    String validationMessage;
//    String dependentField;
//    MasterCollection masterCollection;
//    String lable;
//    FieldType fieldType;
//    dynamic value;
//    String key;
//    Validation validation;
//    bool isMandatory;
//    int fieldId;
//    String onclick;
//    String customValidationFunction;
//    String onchange;
//    String master;
//    bool isAllMasterData;
//    bool isreadonly;
//    String columnName;
//    bool startOnNewLine;
//    String dependentFieldMob;
//    bool isMake;
//    List<String> mobileInputApiKey;
//    dynamic dependentFieldMobList;
//    bool isMobileInputRequired;
//    bool isMasterData;
//    bool isDealerSpecific;
//    bool isBlur;
//    bool isMultiSelect;
//
//    factory SubProcessField.fromJson(Map<String, dynamic> json) => SubProcessField(
//        validationPattern: json["validationPattern"] == null ? null : json["validationPattern"],
//        fieldName: json["fieldName"] == null ? null : json["fieldName"],
//        isDataList: json["isDataList"] == null ? null : json["isDataList"],
//        maxlength: json["maxlength"],
//        defaultValue: json["defaultValue"] == null ? null : defaultValueValues.map[json["defaultValue"]],
//        validationType: json["validationType"] == null ? null : validationValues.map[json["validationType"]],
//        validationMessage: json["validationMessage"] == null ? null : json["validationMessage"],
//        dependentField: json["dependentField"] == null ? null : json["dependentField"],
//        masterCollection: json["masterCollection"] == null ? null : masterCollectionValues.map[json["masterCollection"]],
//        lable: json["lable"] == null ? null : json["lable"],
//        fieldType: json["fieldType"] == null ? null : fieldTypeValues.map[json["fieldType"]],
//        value: json["value"],
//        key: json["key"] == null ? null : json["key"],
//        validation: json["validation"] == null ? null : validationValues.map[json["validation"]],
//        isMandatory: json["isMandatory"] == null ? null : json["isMandatory"],
//        fieldId: json["fieldID"] == null ? null : json["fieldID"],
//        onclick: json["onclick"] == null ? null : json["onclick"],
//        customValidationFunction: json["CustomValidationFunction"] == null ? null : json["CustomValidationFunction"],
//        onchange: json["onchange"] == null ? null : json["onchange"],
//        master: json["master"] == null ? null : json["master"],
//        isAllMasterData: json["isAllMasterData"] == null ? null : json["isAllMasterData"],
//        isreadonly: json["isreadonly"] == null ? null : json["isreadonly"],
//        columnName: json["columnName"] == null ? null : json["columnName"],
//        startOnNewLine: json["startOnNewLine"] == null ? null : json["startOnNewLine"],
//        dependentFieldMob: json["dependentFieldMob"] == null ? null : json["dependentFieldMob"],
//        isMake: json["isMake"] == null ? null : json["isMake"],
//        mobileInputApiKey: json["mobileInputAPIKey"] == null ? null : List<String>.from(json["mobileInputAPIKey"].map((x) => x)),
//    dependentFieldMobList: json["dependentFieldMobList"],
//    isMobileInputRequired: json["isMobileInputRequired"] == null ? null : json["isMobileInputRequired"],
//    isMasterData: json["isMasterData"] == null ? null : json["isMasterData"],
//    isDealerSpecific: json["isDealerSpecific"] == null ? null : json["isDealerSpecific"],
//    isBlur: json["isBlur"] == null ? null : json["isBlur"],
//    isMultiSelect: json["isMultiSelect"] == null ? null : json["isMultiSelect"],
//    );
//
//    Map<String, dynamic> toJson() => {
//        "validationPattern": validationPattern == null ? null : validationPattern,
//        "fieldName": fieldName == null ? null : fieldName,
//        "isDataList": isDataList == null ? null : isDataList,
//        "maxlength": maxlength,
//        "defaultValue": defaultValue == null ? null : defaultValueValues.reverse[defaultValue],
//        "validationType": validationType == null ? null : validationValues.reverse[validationType],
//        "validationMessage": validationMessage == null ? null : validationMessage,
//        "dependentField": dependentField == null ? null : dependentField,
//        "masterCollection": masterCollection == null ? null : masterCollectionValues.reverse[masterCollection],
//        "lable": lable == null ? null : lable,
//        "fieldType": fieldType == null ? null : fieldTypeValues.reverse[fieldType],
//        "value": value,
//        "key": key == null ? null : key,
//        "validation": validation == null ? null : validationValues.reverse[validation],
//        "isMandatory": isMandatory == null ? null : isMandatory,
//        "fieldID": fieldId == null ? null : fieldId,
//        "onclick": onclick == null ? null : onclick,
//        "CustomValidationFunction": customValidationFunction == null ? null : customValidationFunction,
//        "onchange": onchange == null ? null : onchange,
//        "master": master == null ? null : master,
//        "isAllMasterData": isAllMasterData == null ? null : isAllMasterData,
//        "isreadonly": isreadonly == null ? null : isreadonly,
//        "columnName": columnName == null ? null : columnName,
//        "startOnNewLine": startOnNewLine == null ? null : startOnNewLine,
//        "dependentFieldMob": dependentFieldMob == null ? null : dependentFieldMob,
//        "isMake": isMake == null ? null : isMake,
//        "mobileInputAPIKey": mobileInputApiKey == null ? null : List<dynamic>.from(mobileInputApiKey.map((x) => x)),
//        "dependentFieldMobList": dependentFieldMobList,
//        "isMobileInputRequired": isMobileInputRequired == null ? null : isMobileInputRequired,
//        "isMasterData": isMasterData == null ? null : isMasterData,
//        "isDealerSpecific": isDealerSpecific == null ? null : isDealerSpecific,
//        "isBlur": isBlur == null ? null : isBlur,
//        "isMultiSelect": isMultiSelect == null ? null : isMultiSelect,
//    };
//}
//
//enum DefaultValue { EMPTY, NEW }
//
//final defaultValueValues = EnumValues({
//    "": DefaultValue.EMPTY,
//    "NEW": DefaultValue.NEW
//});
//
//enum FieldType { TEXT, DROP_DOWN_LIST, TEXT_AREA, LABEL, HIDDEN }
//
//final fieldTypeValues = EnumValues({
//    "dropDownList": FieldType.DROP_DOWN_LIST,
//    "hidden": FieldType.HIDDEN,
//    "label": FieldType.LABEL,
//    "text": FieldType.TEXT,
//    "textArea": FieldType.TEXT_AREA
//});
//
//enum MasterCollection { EMPTY, NEW_TW }
//
//final masterCollectionValues = EnumValues({
//    "": MasterCollection.EMPTY,
//    "NEW_TW": MasterCollection.NEW_TW
//});
//
//enum Validation { NUMERIC, EMPTY, ALPHA_NUMERIC, TEXT }
//
//final validationValues = EnumValues({
//    "alphaNumeric": Validation.ALPHA_NUMERIC,
//    "": Validation.EMPTY,
//    "Numeric": Validation.NUMERIC,
//    "text": Validation.TEXT
//});
//
//class TabDatum {
//    TabDatum({
//        this.process,
//        this.processId,
//        this.tabs,
//        this.id,
//    });
//
//    String process;
//    int processId;
//    List<Tab> tabs;
//    Id id;
//
//    factory TabDatum.fromJson(Map<String, dynamic> json) => TabDatum(
//        process: json["process"] == null ? null : json["process"],
//        processId: json["processID"] == null ? null : json["processID"],
//        tabs: json["tabs"] == null ? null : List<Tab>.from(json["tabs"].map((x) => Tab.fromJson(x))),
//    id: json["_id"] == null ? null : Id.fromJson(json["_id"]),
//    );
//
//    Map<String, dynamic> toJson() => {
//        "process": process == null ? null : process,
//        "processID": processId == null ? null : processId,
//        "tabs": tabs == null ? null : List<dynamic>.from(tabs.map((x) => x.toJson())),
//        "_id": id == null ? null : id.toJson(),
//    };
//}
//
//class Tab {
//    Tab({
//        this.isactive,
//        this.name,
//        this.subProcessesUi,
//        this.isSubTabsExists,
//        this.id,
//        this.subProcesses,
//        this.key,
//    });
//
//    bool isactive;
//    String name;
//    SubProcessesUi subProcessesUi;
//    bool isSubTabsExists;
//    int id;
//    List<String> subProcesses;
//    String key;
//
//    factory Tab.fromJson(Map<String, dynamic> json) => Tab(
//        isactive: json["isactive"] == null ? null : json["isactive"],
//        name: json["name"] == null ? null : json["name"],
//        subProcessesUi: json["subProcessesUI"] == null ? null : SubProcessesUi.fromJson(json["subProcessesUI"]),
//        isSubTabsExists: json["isSubTabsExists"] == null ? null : json["isSubTabsExists"],
//        id: json["id"] == null ? null : json["id"],
//        subProcesses: json["subProcesses"] == null ? null : List<String>.from(json["subProcesses"].map((x) => x)),
//    key: json["key"] == null ? null : json["key"],
//    );
//
//    Map<String, dynamic> toJson() => {
//        "isactive": isactive == null ? null : isactive,
//        "name": name == null ? null : name,
//        "subProcessesUI": subProcessesUi == null ? null : subProcessesUi.toJson(),
//        "isSubTabsExists": isSubTabsExists == null ? null : isSubTabsExists,
//        "id": id == null ? null : id,
//        "subProcesses": subProcesses == null ? null : List<dynamic>.from(subProcesses.map((x) => x)),
//        "key": key == null ? null : key,
//    };
//}
//
//class SubProcessesUi {
//    SubProcessesUi({
//        this.sourcingDetails,
//        this.customerCapacity,
//        this.customerDetails,
//        this.notes,
//        this.loanDetails,
//        this.vehicleDetails,
//    });
//
//    String sourcingDetails;
//    String customerCapacity;
//    String customerDetails;
//    String notes;
//    String loanDetails;
//    String vehicleDetails;
//
//    factory SubProcessesUi.fromJson(Map<String, dynamic> json) => SubProcessesUi(
//        sourcingDetails: json["SourcingDetails"] == null ? null : json["SourcingDetails"],
//        customerCapacity: json["CustomerCapacity"] == null ? null : json["CustomerCapacity"],
//        customerDetails: json["CustomerDetails"] == null ? null : json["CustomerDetails"],
//        notes: json["Notes"] == null ? null : json["Notes"],
//        loanDetails: json["LoanDetails"] == null ? null : json["LoanDetails"],
//        vehicleDetails: json["VehicleDetails"] == null ? null : json["VehicleDetails"],
//    );
//
//    Map<String, dynamic> toJson() => {
//        "SourcingDetails": sourcingDetails == null ? null : sourcingDetails,
//        "CustomerCapacity": customerCapacity == null ? null : customerCapacity,
//        "CustomerDetails": customerDetails == null ? null : customerDetails,
//        "Notes": notes == null ? null : notes,
//        "LoanDetails": loanDetails == null ? null : loanDetails,
//        "VehicleDetails": vehicleDetails == null ? null : vehicleDetails,
//    };
//}
//
//class EnumValues<T> {
//    Map<String, T> map;
//    Map<T, String> reverseMap;
//
//    EnumValues(this.map);
//
//    Map<T, String> get reverse {
//        if (reverseMap == null) {
//            reverseMap = map.map((k, v) => new MapEntry(v, k));
//        }
//        return reverseMap;
//    }
//}


// To parse this JSON data, do
//
//     final loadHtmlFieldsResMode = loadHtmlFieldsResModeFromJson(jsonString);

import 'dart:convert';

LoadHtmlFieldsResModel loadHtmlFieldsResModelFromJson(String str) => LoadHtmlFieldsResModel.fromJson(json.decode(str));

String loadHtmlFieldsResModeToJson(LoadHtmlFieldsResModel data) => json.encode(data.toJson());

class LoadHtmlFieldsResModel {
    LoadHtmlFieldsResModel({
        this.redirect,
        this.tabData,
        this.redirectUrl,
        this.success,
        this.details,
        this.subProcessFieldsData,
    });

    bool redirect;
    List<TabDatum> tabData;
    String redirectUrl;
    bool success;
    Details details;
    List<SubProcessFieldsDatum> subProcessFieldsData;

    factory LoadHtmlFieldsResModel.fromJson(Map<String, dynamic> json) => LoadHtmlFieldsResModel(
        redirect: json["redirect"],
        tabData: List<TabDatum>.from(json["tabData"].map((x) => TabDatum.fromJson(x))),
    redirectUrl: json["redirectURL"],
    success: json["success"],
    details: Details.fromJson(json["details"]),
    subProcessFieldsData: List<SubProcessFieldsDatum>.from(json["subProcessFieldsData"].map((x) => SubProcessFieldsDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "redirect": redirect,
        "tabData": List<dynamic>.from(tabData.map((x) => x.toJson())),
        "redirectURL": redirectUrl,
        "success": success,
        "details": details.toJson(),
        "subProcessFieldsData": List<dynamic>.from(subProcessFieldsData.map((x) => x.toJson())),
    };
}

class Details {
    Details({
        this.leadDocuments,
    });

    LeadDocuments leadDocuments;

    factory Details.fromJson(Map<String, dynamic> json) => Details(
        leadDocuments: LeadDocuments.fromJson(json["leadDocuments"]),
    );

    Map<String, dynamic> toJson() => {
        "leadDocuments": leadDocuments.toJson(),
    };
}

class LeadDocuments {
    LeadDocuments({
        this.the19ApplicantVoterCard,
        this.the1CustomerPhoto,
        this.the11Index,
        this.the18ApplicantAadharCardMaskBack,
        this.the16ApplicantAadharCardBack,
        this.the10Others,
        this.the9ApplicantSalarySlip,
        this.the15ApplicantAadharCardFront,
        this.the6ApplicantAddressProof,
        this.the12ApplicantBankStatement,
        this.the3ApplicantPanCard,
        this.the2LoginSheet,
        this.the5ApplicantOtherIdProof,
        this.the14Rc,
        this.the17ApplicantAadharCardMaskFront,
        this.the8ApplicantOwnHouseProof,
        this.the7LatestUtilityBill,
        this.the13Form16Itr,
        this.the4ApplicantAadharCard,
        this.the10ApplicantBankDetails,
    });

    String the19ApplicantVoterCard;
    String the1CustomerPhoto;
    String the11Index;
    String the18ApplicantAadharCardMaskBack;
    String the16ApplicantAadharCardBack;
    String the10Others;
    String the9ApplicantSalarySlip;
    String the15ApplicantAadharCardFront;
    String the6ApplicantAddressProof;
    String the12ApplicantBankStatement;
    String the3ApplicantPanCard;
    String the2LoginSheet;
    String the5ApplicantOtherIdProof;
    String the14Rc;
    String the17ApplicantAadharCardMaskFront;
    String the8ApplicantOwnHouseProof;
    String the7LatestUtilityBill;
    String the13Form16Itr;
    String the4ApplicantAadharCard;
    String the10ApplicantBankDetails;

    factory LeadDocuments.fromJson(Map<String, dynamic> json) => LeadDocuments(
        the19ApplicantVoterCard: json["19__ApplicantVoterCard"],
        the1CustomerPhoto: json["1__CustomerPhoto"],
        the11Index: json["11__Index"],
        the18ApplicantAadharCardMaskBack: json["18__ApplicantAadharCardMaskBack"],
        the16ApplicantAadharCardBack: json["16__ApplicantAadharCardBack"],
        the10Others: json["10__Others"],
        the9ApplicantSalarySlip: json["9__ApplicantSalarySlip"],
        the15ApplicantAadharCardFront: json["15__ApplicantAadharCardFront"],
        the6ApplicantAddressProof: json["6__ApplicantAddressProof"],
        the12ApplicantBankStatement: json["12__ApplicantBankStatement"],
        the3ApplicantPanCard: json["3__ApplicantPANCard"],
        the2LoginSheet: json["2__LoginSheet"],
        the5ApplicantOtherIdProof: json["5__ApplicantOtherIDProof"],
        the14Rc: json["14__RC"],
        the17ApplicantAadharCardMaskFront: json["17__ApplicantAadharCardMaskFront"],
        the8ApplicantOwnHouseProof: json["8__ApplicantOwnHouseProof"],
        the7LatestUtilityBill: json["7__LatestUtilityBill"],
        the13Form16Itr: json["13__Form16_ITR"],
        the4ApplicantAadharCard: json["4__ApplicantAadharCard"],
        the10ApplicantBankDetails: json["10__ApplicantBankDetails"],
    );

    Map<String, dynamic> toJson() => {
        "19__ApplicantVoterCard": the19ApplicantVoterCard,
        "1__CustomerPhoto": the1CustomerPhoto,
        "11__Index": the11Index,
        "18__ApplicantAadharCardMaskBack": the18ApplicantAadharCardMaskBack,
        "16__ApplicantAadharCardBack": the16ApplicantAadharCardBack,
        "10__Others": the10Others,
        "9__ApplicantSalarySlip": the9ApplicantSalarySlip,
        "15__ApplicantAadharCardFront": the15ApplicantAadharCardFront,
        "6__ApplicantAddressProof": the6ApplicantAddressProof,
        "12__ApplicantBankStatement": the12ApplicantBankStatement,
        "3__ApplicantPANCard": the3ApplicantPanCard,
        "2__LoginSheet": the2LoginSheet,
        "5__ApplicantOtherIDProof": the5ApplicantOtherIdProof,
        "14__RC": the14Rc,
        "17__ApplicantAadharCardMaskFront": the17ApplicantAadharCardMaskFront,
        "8__ApplicantOwnHouseProof": the8ApplicantOwnHouseProof,
        "7__LatestUtilityBill": the7LatestUtilityBill,
        "13__Form16_ITR": the13Form16Itr,
        "4__ApplicantAadharCard": the4ApplicantAadharCard,
        "10__ApplicantBankDetails": the10ApplicantBankDetails,
    };
}

class SubProcessFieldsDatum {
    SubProcessFieldsDatum({
        this.isTableExist,
        this.subProcessKey,
        this.processName,
        this.processId,
        this.subProcessName,
        this.controllerName,
        this.subProcessFields,
        this.seqId,
        this.id,
    });

    bool isTableExist;
    String subProcessKey;
    String processName;
    int processId;
    String subProcessName;
    String controllerName;
    List<SubProcessField> subProcessFields;
    int seqId;
    Id id;

    factory SubProcessFieldsDatum.fromJson(Map<String, dynamic> json) => SubProcessFieldsDatum(
        isTableExist: json["isTableExist"],
        subProcessKey: json["subProcessKey"],
        processName: json["processName"],
        processId: json["processID"],
        subProcessName: json["subProcessName"],
        controllerName: json["controllerName"],
        subProcessFields: List<SubProcessField>.from(json["subProcessFields"].map((x) => SubProcessField.fromJson(x))),
    seqId: json["seq_id"],
    id: Id.fromJson(json["_id"]),
    );

    Map<String, dynamic> toJson() => {
        "isTableExist": isTableExist,
        "subProcessKey": subProcessKey,
        "processName": processName,
        "processID": processId,
        "subProcessName": subProcessName,
        "controllerName": controllerName,
        "subProcessFields": List<dynamic>.from(subProcessFields.map((x) => x.toJson())),
        "seq_id": seqId,
        "_id": id.toJson(),
    };
}

class Id {
    Id({
        this.currentCounter,
        this.date,
        this.generatedMachineIdentifier,
        this.machineIdentifier,
        this.generatedProcessIdentifier,
        this.processIdentifier,
        this.counter,
        this.time,
        this.timeSecond,
        this.timestamp,
    });

    int currentCounter;
    String date;
    int generatedMachineIdentifier;
    int machineIdentifier;
    int generatedProcessIdentifier;
    int processIdentifier;
    int counter;
    int time;
    int timeSecond;
    int timestamp;

    factory Id.fromJson(Map<String, dynamic> json) => Id(
        currentCounter: json["currentCounter"],
        date: json["date"],
        generatedMachineIdentifier: json["generatedMachineIdentifier"],
        machineIdentifier: json["machineIdentifier"],
        generatedProcessIdentifier: json["generatedProcessIdentifier"],
        processIdentifier: json["processIdentifier"],
        counter: json["counter"],
        time: json["time"],
        timeSecond: json["timeSecond"],
        timestamp: json["timestamp"],
    );

    Map<String, dynamic> toJson() => {
        "currentCounter": currentCounter,
        "date": date,
        "generatedMachineIdentifier": generatedMachineIdentifier,
        "machineIdentifier": machineIdentifier,
        "generatedProcessIdentifier": generatedProcessIdentifier,
        "processIdentifier": processIdentifier,
        "counter": counter,
        "time": time,
        "timeSecond": timeSecond,
        "timestamp": timestamp,
    };
}

class SubProcessField {
    SubProcessField({
        this.validationPattern,
        this.fieldName,
        this.isDataList,
        this.maxlength,
        this.defaultValue,
        this.validationType,
        this.validationMessage,
        this.dependentField,
        this.masterCollection,
        this.lable,
        this.fieldType,
        this.value,
        this.key,
        this.validation,
        this.isMandatory,
        this.fieldId,
        this.onclick,
        this.customValidationFunction,
        this.onchange,
        this.master,
        this.isAllMasterData,
        this.isreadonly,
        this.columnName,
        this.startOnNewLine,
        this.dependentFieldMob,
        this.isMake,
        this.mobileInputApiKey,
        this.dependentFieldMobList,
        this.isMobileInputRequired,
        this.isMasterData,
        this.isDealerSpecific,
        this.isBlur,
        this.isMultiSelect,
    });

    String validationPattern;
    String fieldName;
    bool isDataList;
    dynamic maxlength;
    DefaultValue defaultValue;
    Validation validationType;
    String validationMessage;
    String dependentField;
    MasterCollection masterCollection;
    String lable;
    FieldType fieldType;
    dynamic value;
    String key;
    Validation validation;
    bool isMandatory;
    int fieldId;
    String onclick;
    String customValidationFunction;
    String onchange;
    String master;
    bool isAllMasterData;
    bool isreadonly;
    String columnName;
    bool startOnNewLine;
    String dependentFieldMob;
    bool isMake;
    List<String> mobileInputApiKey;
    dynamic dependentFieldMobList;
    bool isMobileInputRequired;
    bool isMasterData;
    bool isDealerSpecific;
    bool isBlur;
    bool isMultiSelect;

    factory SubProcessField.fromJson(Map<String, dynamic> json) => SubProcessField(
        validationPattern: json["validationPattern"],
        fieldName: json["fieldName"],
        isDataList: json["isDataList"],
        maxlength: json["maxlength"],
        defaultValue: defaultValueValues.map[json["defaultValue"]],
        validationType: validationValues.map[json["validationType"]],
        validationMessage: json["validationMessage"],
        dependentField: json["dependentField"],
        masterCollection: masterCollectionValues.map[json["masterCollection"]],
        lable: json["lable"],
        fieldType: fieldTypeValues.map[json["fieldType"]],
        value: json["value"],
        key: json["key"],
        validation: validationValues.map[json["validation"]],
        isMandatory: json["isMandatory"],
        fieldId: json["fieldID"],
        onclick: json["onclick"] == null ? null : json["onclick"],
        customValidationFunction: json["CustomValidationFunction"] == null ? null : json["CustomValidationFunction"],
        onchange: json["onchange"] == null ? null : json["onchange"],
        master: json["master"] == null ? null : json["master"],
        isAllMasterData: json["isAllMasterData"] == null ? null : json["isAllMasterData"],
        isreadonly: json["isreadonly"] == null ? null : json["isreadonly"],
        columnName: json["columnName"] == null ? null : json["columnName"],
        startOnNewLine: json["startOnNewLine"] == null ? null : json["startOnNewLine"],
        dependentFieldMob: json["dependentFieldMob"] == null ? null : json["dependentFieldMob"],
        isMake: json["isMake"] == null ? null : json["isMake"],
        mobileInputApiKey: json["mobileInputAPIKey"] == null ? null : List<String>.from(json["mobileInputAPIKey"].map((x) => x)),
    dependentFieldMobList: json["dependentFieldMobList"],
    isMobileInputRequired: json["isMobileInputRequired"] == null ? null : json["isMobileInputRequired"],
    isMasterData: json["isMasterData"] == null ? null : json["isMasterData"],
    isDealerSpecific: json["isDealerSpecific"] == null ? null : json["isDealerSpecific"],
    isBlur: json["isBlur"] == null ? null : json["isBlur"],
    isMultiSelect: json["isMultiSelect"] == null ? null : json["isMultiSelect"],
    );

    Map<String, dynamic> toJson() => {
        "validationPattern": validationPattern,
        "fieldName": fieldName,
        "isDataList": isDataList,
        "maxlength": maxlength,
        "defaultValue": defaultValueValues.reverse[defaultValue],
        "validationType": validationValues.reverse[validationType],
        "validationMessage": validationMessage,
        "dependentField": dependentField,
        "masterCollection": masterCollectionValues.reverse[masterCollection],
        "lable": lable,
        "fieldType": fieldTypeValues.reverse[fieldType],
        "value": value,
        "key": key,
        "validation": validationValues.reverse[validation],
        "isMandatory": isMandatory,
        "fieldID": fieldId,
        "onclick": onclick == null ? null : onclick,
        "CustomValidationFunction": customValidationFunction == null ? null : customValidationFunction,
        "onchange": onchange == null ? null : onchange,
        "master": master == null ? null : master,
        "isAllMasterData": isAllMasterData == null ? null : isAllMasterData,
        "isreadonly": isreadonly == null ? null : isreadonly,
        "columnName": columnName == null ? null : columnName,
        "startOnNewLine": startOnNewLine == null ? null : startOnNewLine,
        "dependentFieldMob": dependentFieldMob == null ? null : dependentFieldMob,
        "isMake": isMake == null ? null : isMake,
        "mobileInputAPIKey": mobileInputApiKey == null ? null : List<dynamic>.from(mobileInputApiKey.map((x) => x)),
        "dependentFieldMobList": dependentFieldMobList,
        "isMobileInputRequired": isMobileInputRequired == null ? null : isMobileInputRequired,
        "isMasterData": isMasterData == null ? null : isMasterData,
        "isDealerSpecific": isDealerSpecific == null ? null : isDealerSpecific,
        "isBlur": isBlur == null ? null : isBlur,
        "isMultiSelect": isMultiSelect == null ? null : isMultiSelect,
    };
}

enum DefaultValue { EMPTY, NEW }

final defaultValueValues = EnumValues({
    "": DefaultValue.EMPTY,
    "NEW": DefaultValue.NEW
});

enum FieldType { TEXT, DROP_DOWN_LIST, LABEL, HIDDEN, TEXT_AREA }

final fieldTypeValues = EnumValues({
    "dropDownList": FieldType.DROP_DOWN_LIST,
    "hidden": FieldType.HIDDEN,
    "label": FieldType.LABEL,
    "text": FieldType.TEXT,
    "textArea": FieldType.TEXT_AREA
});

enum MasterCollection { EMPTY, NEW_TW }

final masterCollectionValues = EnumValues({
    "": MasterCollection.EMPTY,
    "NEW_TW": MasterCollection.NEW_TW
});

enum Validation { NUMERIC, EMPTY, ALPHA_NUMERIC, TEXT }

final validationValues = EnumValues({
    "alphaNumeric": Validation.ALPHA_NUMERIC,
    "": Validation.EMPTY,
    "Numeric": Validation.NUMERIC,
    "text": Validation.TEXT
});

class TabDatum {
    TabDatum({
        this.process,
        this.processId,
        this.tabs,
        this.id,
    });

    String process;
    int processId;
    List<Tab> tabs;
    Id id;

    factory TabDatum.fromJson(Map<String, dynamic> json) => TabDatum(
        process: json["process"],
        processId: json["processID"],
        tabs: List<Tab>.from(json["tabs"].map((x) => Tab.fromJson(x))),
    id: Id.fromJson(json["_id"]),
    );

    Map<String, dynamic> toJson() => {
        "process": process,
        "processID": processId,
        "tabs": List<dynamic>.from(tabs.map((x) => x.toJson())),
        "_id": id.toJson(),
    };
}

class Tab {
    Tab({
        this.isactive,
        this.name,
        this.subProcessesUi,
        this.isSubTabsExists,
        this.id,
        this.subProcesses,
        this.key,
    });

    bool isactive;
    String name;
    SubProcessesUi subProcessesUi;
    bool isSubTabsExists;
    int id;
    List<String> subProcesses;
    String key;

    factory Tab.fromJson(Map<String, dynamic> json) => Tab(
        isactive: json["isactive"],
        name: json["name"],
        subProcessesUi: SubProcessesUi.fromJson(json["subProcessesUI"]),
        isSubTabsExists: json["isSubTabsExists"],
        id: json["id"],
        subProcesses: List<String>.from(json["subProcesses"].map((x) => x)),
    key: json["key"],
    );

    Map<String, dynamic> toJson() => {
        "isactive": isactive,
        "name": name,
        "subProcessesUI": subProcessesUi.toJson(),
        "isSubTabsExists": isSubTabsExists,
        "id": id,
        "subProcesses": List<dynamic>.from(subProcesses.map((x) => x)),
        "key": key,
    };
}

class SubProcessesUi {
    SubProcessesUi({
        this.sourcingDetails,
        this.customerCapacity,
        this.customerDetails,
        this.notes,
        this.loanDetails,
        this.vehicleDetails,
    });

    String sourcingDetails;
    String customerCapacity;
    String customerDetails;
    String notes;
    String loanDetails;
    String vehicleDetails;

    factory SubProcessesUi.fromJson(Map<String, dynamic> json) => SubProcessesUi(
        sourcingDetails: json["SourcingDetails"],
        customerCapacity: json["CustomerCapacity"],
        customerDetails: json["CustomerDetails"],
        notes: json["Notes"],
        loanDetails: json["LoanDetails"],
        vehicleDetails: json["VehicleDetails"],
    );

    Map<String, dynamic> toJson() => {
        "SourcingDetails": sourcingDetails,
        "CustomerCapacity": customerCapacity,
        "CustomerDetails": customerDetails,
        "Notes": notes,
        "LoanDetails": loanDetails,
        "VehicleDetails": vehicleDetails,
    };
}

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
