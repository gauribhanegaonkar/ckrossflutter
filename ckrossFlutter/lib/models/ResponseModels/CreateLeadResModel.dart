import 'dart:convert';

CreateLeadResModel createLeadResModelFromJson( str) => CreateLeadResModel.fromJson(json.decode(str));

String createLeadResModelToJson(CreateLeadResModel data) => json.encode(data.toJson());

class CreateLeadResModel {
    CreateLeadResModel({
        this.redirect,
        this.redirectUrl,
        this.success,
        this.processesQueueId,
        this.leadId,
        this.productId,
        this.caseID,
        this.error,
        this.isError,
    });

    bool redirect;
    String redirectUrl;
    bool success;
    String processesQueueId;
    var leadId;
    int productId;
    String caseID;
    String error;
    bool isError;

    factory CreateLeadResModel.fromJson(Map<String, dynamic> json) => CreateLeadResModel(
        redirect: json["redirect"] == null ? null : json["redirect"],
        redirectUrl: json["redirectURL"] == null ? null : json["redirectURL"],
        success: json["success"] == null ? null : json["success"],
        processesQueueId: json["PROCESSES_QUEUE_ID"] == null ? null : json["PROCESSES_QUEUE_ID"],
        leadId: json["LeadID"] == null ? null : json["LeadID"],
        productId: json["ProductID"] == null ? null : json["ProductID"],
        caseID: json["caseID"] == null ? null : json["caseID"],
        error: json["error"] == null ? null : json["error"],
        isError: json["isError"] == null ? null : json["isError"],
    );

    Map<String, dynamic> toJson() => {
        "redirect": redirect == null ? null : redirect,
        "redirectURL": redirectUrl == null ? null : redirectUrl,
        "success": success == null ? null : success,
        "PROCESSES_QUEUE_ID": processesQueueId == null ? null : processesQueueId,
        "LeadID": leadId == null ? null : leadId,
        "ProductID": productId == null ? null : productId,
        "caseID": caseID == null ? null : caseID,
        "error": error == null ? null : error,
        "isError": isError == null ? null : isError,
    };
}
