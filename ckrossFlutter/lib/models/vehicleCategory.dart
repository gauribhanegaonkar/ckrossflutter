class VehicleCategory{

  String vehicle_category_id;
  String model_id;
  String vehicle_category;

  VehicleCategory(this.vehicle_category_id,this.model_id,this.vehicle_category);

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

   // map['vehicle_category_id'] = vehicle_category_id;
    map['model_id'] = model_id;
    map['vehicle_category'] = vehicle_category;


    return map;
  }

  // Extract a Category object from a Map object
  VehicleCategory.fromMapObject(Map<String, dynamic> map) {

    this.vehicle_category_id = map['vehicle_category_id'];
    this.model_id = map['model_id'];
    this.vehicle_category = map['vehicle_category'];

  }
}