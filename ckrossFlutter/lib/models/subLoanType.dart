class SubLoanType{

  String sub_loan_id = 'sub_loan_id';
  String sub_loan_type = 'sub_loan_type';
  String loan_type = 'loan_type';

  SubLoanType();

  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

  //  map['sub_loan_id'] = sub_loan_id;
    map['sub_loan_type'] = sub_loan_type;
    map['loan_type'] = loan_type;
    return map;
  }

  // Extract a Category object from a Map object
  SubLoanType.fromMapObject(Map<String, dynamic> map) {

    this.sub_loan_id = map['sub_loan_id'];
    this.sub_loan_type = map['sub_loan_type'];
    this.loan_type = map['loan_type'];

  }
}