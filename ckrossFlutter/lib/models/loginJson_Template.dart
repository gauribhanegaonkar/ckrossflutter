class LoginJsonTemplate{

  String category_Id;
  String login_json_template_id;
  String jsonKey;
  String jsonValue;
  String language;

  //LoginJsonTemplate(this.login_json_template_id,this.jsonKey,this.jsonValue,this.language);
LoginJsonTemplate();
  Map<String,dynamic> toMap(){
    var map = Map<String, dynamic>();

    map['category_Id'] = category_Id;
    map['login_json_template_id'] = login_json_template_id;
    map['jsonKey'] = jsonKey;
    map['jsonValue'] = jsonValue;
    map['language'] = language;


    return map;
  }

  LoginJsonTemplate.fromMapObject(Map<String,dynamic>map){

    this.category_Id= map['category_Id'];
    this.login_json_template_id= map['login_json_template_id'];
    this.jsonKey = map['jsonKey'];
    this.jsonValue = map['jsonValue'];
    this.language = map['language'];


  }
}