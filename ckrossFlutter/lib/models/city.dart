class City{
  String cityId;
  String cityName;

  City(this.cityId,this.cityName);

  Map<String,dynamic> toMap() {
    var map = Map<String, dynamic>();

   // map['city_id'] = cityId;
    map['city_name'] = cityName;

    return map;
  }

  City.fromMapObject(Map<String,dynamic>map){
    this.cityId = map['city_id'];
    this.cityName = map['city_name'];
  }
}