class Model{

  String model_id;
  String make_id;
  String model_name;
  String on_road_price;
  String date;
  String currentCounter;
  String generatedMachineIdentifier;
  String machineIdentifier;
  String generatedProcessIdentifier;
  String time;
  String counter;
  String processIdentifier;
  String timestamp;
  String timeSecond;

  //Model(this.model_id,this.make_id,this.model_name,this.on_road_price,this.date, this.currentCounter, this.generatedMachineIdentifier, this.machineIdentifier,
  //    this.generatedProcessIdentifier, this.time, this.counter, this.processIdentifier, this.timestamp,this.timeSecond);
  Model();
  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

   // map['model_id'] = model_id;
    map['make_id'] = make_id;
    map['model_name'] = model_name;
    map['on_road_price'] = on_road_price;
    map['date'] = date;
    map['currentCounter'] = currentCounter;
    map['generatedMachineIdentifier'] = generatedMachineIdentifier;
    map['machineIdentifier'] = machineIdentifier;
    map['generatedProcessIdentifier'] = generatedProcessIdentifier;
    map['time'] = time;
    map['counter'] = counter;
    map['processIdentifier'] = processIdentifier;
    map['timestamp'] = timestamp;
    map['timeSecond'] = timeSecond;

    return map;
  }

  // Extract a Category object from a Map object
  Model.fromMapObject(Map<String, dynamic> map) {

    this.model_id = map['model_id'];
    this.make_id = map['make_id'];
    this.model_name = map['model_name'];
    this.on_road_price = map['on_road_price'];
    this.date = map['date'];
    this.currentCounter = map['currentCounter'];
    this.generatedMachineIdentifier = map['generatedMachineIdentifier'];
    this.machineIdentifier = map['machineIdentifier'];
    this.generatedProcessIdentifier = map['generatedProcessIdentifier'];
    this.time = map['time'];
    this.counter = map['counter'];
    this.processIdentifier = map['processIdentifier'];
    this.timestamp = map['timestamp'];
    this.timeSecond = map['timeSecond'];


  }
}