
class DealerSO{

  String dealer_id;
  String dealership_name;
  dynamic dealership_data;

  //DealerSO(this.dealer_id,this.dealership_name,this.dealership_data);
DealerSO();
  Map<String,dynamic> toMap(){
    var map = Map<String, dynamic>();

   // map['dealer_id'] = dealer_id;
    map['dealership_name'] = dealership_name;
    map['dealership_data'] = dealership_data;

    return map;
  }

  DealerSO.fromMapObject(Map<String,dynamic>map){

    this.dealer_id= map['dealer_id'];
    this.dealership_name = map['dealership_name'];
    this.dealership_data = map['dealership_data'];


  }

}