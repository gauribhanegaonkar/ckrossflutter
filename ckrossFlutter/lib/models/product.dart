class Product{

  String product_id ;
  String product_name  ;


  //Product(this.product_id,this.product_name);
  Product();
  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

    map['product_id'] = product_id;
    map['product_name'] = product_name;

    return map;
  }

  // Extract a Category object from a Map object
  Product.fromMapObject(Map<String, dynamic> map) {

    this.product_id = map['product_id'];
    this.product_name = map['product_name'];
   

  }
}