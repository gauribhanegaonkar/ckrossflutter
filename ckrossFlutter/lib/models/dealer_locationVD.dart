class DealerLocationVD{

  String dealer_location_id ;
  String dealer_name ;
  String location;
  String branch;
  String dealer_code;
  String vehicle_city;

  //DealerLocationVD(this.dealer_location_id,this.dealer_name,this.location,this.branch,this.dealer_code,this.vehicle_city);
DealerLocationVD();

  Map<String,dynamic> toMap(){
    var map = Map<String, dynamic>();

   // map['dealer_location_id'] = dealer_location_id;
    map['dealer_name'] = dealer_name;
    map['location'] = location;
    map[' branch'] =  branch;
    map['dealer_code'] = dealer_code;
    map['vehicle_city'] = vehicle_city;

    return map;
  }

  DealerLocationVD.fromMapObject(Map<String,dynamic>map){

    this.dealer_location_id= map['dealer_location_id'];
    this.dealer_name = map['dealer_name'];
    this.location = map['location'];
    this. branch = map[' branch'];
    this.dealer_code = map['dealer_code'] ;
    this.vehicle_city = map['vehicle_city'];

  }


}