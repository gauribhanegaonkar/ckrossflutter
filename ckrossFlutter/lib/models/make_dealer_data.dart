class MakeDalerData{

  String make_dealer_data_id ;
  String dealer_type  ;
  String dealership_name ;
  String make_dealer_data_name  ;

  //MakeDalerData(this.make_dealer_data_id,this.dealer_type,this.dealership_name,this.make_dealer_data_name);
  MakeDalerData();
  // Convert a Category object into a Map object
  Map<String, dynamic> toMap() {

    var map = Map<String, dynamic>();

    map['make_dealer_data_id'] = make_dealer_data_id;
    map['dealer_type'] = dealer_type;
    map['dealership_name'] = dealership_name;
    map['make_dealer_data_name'] = make_dealer_data_name;

    return map;
  }

  // Extract a Category object from a Map object
  MakeDalerData.fromMapObject(Map<String, dynamic> map) {

    this.make_dealer_data_id = map['make_dealer_data_id'];
    this.dealer_type = map['dealer_type'];
    this.dealership_name = map['dealership_name'];
    this.make_dealer_data_name = map['make_dealer_data_name'];

  }
}