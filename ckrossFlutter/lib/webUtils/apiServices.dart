import 'dart:convert';
import 'dart:io';

import 'package:ckrossFlutter/Utils/appConstants.dart';
import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/models/RequestModels/DoOtpLoginReqModel.dart';
import 'package:ckrossFlutter/models/RequestModels/GetMobileOTPReqModel.dart';
import 'package:ckrossFlutter/models/RequestModels/GetProductSwitchDataModel.dart';
import 'package:ckrossFlutter/models/RequestModels/LoadHTMLFieldsRequestModel.dart';
import 'package:ckrossFlutter/models/ResponseModels/LoadHtmlFieldsResModel.dart';
import 'package:ckrossFlutter/webUtils/apiConstants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class APIServices {
  static Future<Response> getMobileOTP(String userid) async {
    try {
      FileUtils.printLog('In getMobileOTP - start of function');
      String url = APIConstants.url + APIConstants.GetMobileOTP;

      GetOTPRequestmodel reqModel = new GetOTPRequestmodel();
      reqModel.username = userid;
      reqModel.database = "Sumasoft";
       print(url);
      // var body = reqModel.toJson();
      var body = json.encode(reqModel);
      print(body);
      Map<String, String> headers = {
        'Content-type': 'application/json',
      };

      final response = await post(url, headers: headers, body: body)
          .timeout(new Duration(seconds: 180));
      print(response.body);
      FileUtils.printLog('End getMobileOTP - end of function');
      return response;
    } on Exception catch (e) {
      FileUtils.printLog('Exception in getMobileOTP: $e');
      return null;
    }
  }

  static Future<Response> productChange(var responsecontent) async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    try{
      print("in api");
      print(responsecontent);
      FileUtils.printLog('In productSwitchData - start of function');
      String url = APIConstants.url + APIConstants.productChange;
      GetProductSwitchmodel reqModel = new GetProductSwitchmodel();
      var body = responsecontent;
      print("product body");
      print(body);
      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await post(url, headers: headers, body: body);
      print("response body");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End getProductSwitchData - end of function');
      return response;

    }on Exception catch (e) {
      FileUtils.printLog('Exception in getProductSwitchData: $e');
      return null;
    }
  }

  static Future<Response> GetReportsDateFilter(String query,String requestContent) async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    try{
      print("in api");
      print(requestContent);
      FileUtils.printLog('In getReportDateFilter - start of function');
      String url =  APIConstants.url + query;

      var body = requestContent;
      print(url);
      print("request body");
      print(body);
      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await post(url, headers: headers, body: body);
      print("response body");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End getReportDateFilter - end of function');
      return response;

    }on Exception catch (e){
      FileUtils.printLog('Exception in GetReports: $e');
      return null;
    }
  }

  static Future<Response> GetReports(String query) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    try{
      print("in api");

      FileUtils.printLog('In GetReports - start of function');
      String url = APIConstants.url + query  ;

      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await get(url,headers: headers);
      print("response body in call function");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End GetReports - end of function');
      return response;

    }on Exception catch (e){
      FileUtils.printLog('Exception in GetReports: $e');
      return null;
    }
  }

  static Future<Response> GetReportsPostDisbursementDoc(String query) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    try{
      print("in api");

      FileUtils.printLog('In GetReportsPostDisbursementDoc - start of function');
      String url = APIConstants.url + query  ;

      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await get(url,headers: headers);
      print("response body in call function");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End GetReportsPostDisbursementDoc - end of function');
      return response;

    }on Exception catch (e){
      FileUtils.printLog('Exception in GetReportsPostDisbursementDoc: $e');
      return null;
    }
  }

  static Future<Response> GetUniqueColumnData(String query) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    try{
      print("in api");
      print(query);
      FileUtils.printLog('In GetUniqueColumnData - start of function');
      String url =  APIConstants.url + APIConstants.RequestGetUniqueColumnData;

      var body = query;
      print(url);
      print("request body");
      print(body);
      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await post(url, headers: headers, body: body);
      print("response body");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End GetUniqueColumnData - end of function');
      return response;

    }on Exception catch (e){
      FileUtils.printLog('Exception in GetUniqueColumnData: $e');
      return null;
    }
  }

  static Future<Response> doOTPLogin(String userid, String otp) async {
    try {
      FileUtils.printLog('In doOTPLogin - start of function');
      String url = APIConstants.url + APIConstants.DoOTPLogin;

      doOtpLoginReqModel reqModel = new doOtpLoginReqModel();
      reqModel.username = userid;
      reqModel.database = "Sumasoft";
      reqModel.password = otp;
      reqModel.client = "Sumasoft";
      reqModel.productName = "New TW";
      reqModel.appType = "M";

      var body = json.encode(reqModel);
      Map<String, String> headers = {
        'Content-type': 'application/json',
      };

      final response = await post(url, headers: headers, body: body)
          .timeout(new Duration(seconds: 180));
      print(response.body);
      FileUtils.printLog('End doOTPLogin - end of function');
      return response;
    } on Exception catch (e) {
      FileUtils.printLog('Exception in doOTPLogin: $e');
      return null;
    }
  }

  static Future<Response> loadHtmlFields(String product_Id) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    try {
      FileUtils.printLog('In loadHtmlFields - start of function');
      String url =
          APIConstants.url + APIConstants.LoadHtmlFields + "/" + product_Id;

      LoadHTMLFieldsRequestModel htmlRequestModel =
          new LoadHTMLFieldsRequestModel();

      htmlRequestModel.processID =
          sp.getString(AppConstants.CURRENT_PRODUCT_LEAD_CREATION_ID);
      htmlRequestModel.landing_page =
          sp.getString(AppConstants.CURRENT_PRODUCT_LOAD_HTML);
      htmlRequestModel.product_Id = product_Id;

      var requestContents = "";
      if (htmlRequestModel != null) {
        requestContents = json.encode(htmlRequestModel);
      }
      var token = sp.getString(AppConstants.TOKEN);
      print(url);
      var body = requestContents;
      //print(sp.getString(AppConstants.TOKEN));

      print(body);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await post(url, headers: headers, body: body)
          .timeout(new Duration(seconds: 180));
      print("2222221111");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End loadHtmlFields - end of function');
      return response;
    } on Exception catch (e) {
      FileUtils.printLog('Exception in loadHtmlFields: $e');
      return null;
    }
  }

    static Future<Response> CreateLead(submitRequestJson, url) async{
        FileUtils.printLog("In create Lead API Service");
        FileUtils.printLog(submitRequestJson);
            var response;
        try
            {
         SharedPreferences sp = await SharedPreferences.getInstance();
        var token = sp.getString(AppConstants.TOKEN);
        Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };
      var body = submitRequestJson;
      //var content = new StringContent(requestContent, Encoding.UTF8, "application/json");
        response = await post(url, headers: headers, body: submitRequestJson)
          .timeout(new Duration(seconds: 180));
          print("In api of create");
          print(url);
          
          // LoadHtmlFieldsResModel responseModel =
          // loadHtmlFieldsResModelFromJson(response.toString());
          print(response.body);
          FileUtils.printLog("****"+body);
            }catch ( e)
            {
                print("CreateLead Exception : " + e.toString());
            }
            return response;
    }

static Future<Response> UploadPdfFile (String filePath, String url,fileName) async {
            http.Response response ;
            print("UploadPdfFile : " + url);
            print(fileName);
            try
            {
               // await InitializeAppData();
                SharedPreferences sp = await SharedPreferences.getInstance();
                 var token = sp.getString(AppConstants.TOKEN);
               Map<String, String> headerDict = new Map<String, String>();
                headerDict["Authorization"]= "Bearer " + token;
                var request = http.MultipartRequest('POST', Uri.parse(url));
                 request.files.add(
                        http.MultipartFile(
                          'File',
                          File(filePath).readAsBytes().asStream(),
                          File(filePath).lengthSync(),
                          filename: fileName
                        )
                      );
              // var res = await request.send()
                print("request");
                print(request);
                response = await http.Response.fromStream(await request.send());
                print("Result: ${response.statusCode}");
                print(response);
                print(response.body);
               // response = await CrossFileUploader.Current.UploadFileAsync(url, new FilePathItem("File", filePath), headerDict);
            }
            catch ( e)
            {
                print("UploadPdfFile Exception : " + e.toString());
            }
            return response;
        }

 static UpdateCompleteStatus(String geturl) async {
            print("UpdateCompleteStatus : " + geturl);
            String resp ;
            try
            {
                SharedPreferences sp = await SharedPreferences.getInstance();
              var token = sp.getString(AppConstants.TOKEN);
              Map<String, String> headers = {
                'Content-type': 'application/json',
                'Authorization': 'Bearer ' + token
              };
              String url = geturl  ;
                print("url...");
                print(url);
                var response = await get(url,headers: headers);
                print("response.body UpdateCompleteStatus....... ");

                print(response.statusCode);
                print(response.body);
                if (response.statusCode==200)
                {
                    resp =  response.body;
                    print("response.body UpdateCompleteStatus ");
                    print(response.body);
                    print(resp);
                }
            }
            catch (exception)
            {
                print("UpdateCompleteStatus Exception : " + exception.toString());
            }
            return resp;
        }

  static Future<Response> getGenerateOtp(String url, String requestContent)async
  {
   // logger = DependencyService.Get<ILogManager>().GetLog();
    SharedPreferences sp = await SharedPreferences.getInstance();
    print("GenerateOtp : " + url + " content " + requestContent);
   // HttpResponseMessage response = null;
    try
    {
    //  await InitializeAppData();
     // String url =  APIConstants.url + APIConstants.RequestGetUniqueColumnData;

      var body = requestContent;
      print(url);
      print("request body");
      print(body);
      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await post(url, headers: headers, body: body);
      print("response body");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End GeneteOTP - end of function');
      return response;

    }
     on Exception catch (exception)
    {
    print("GenerateOtp Exception : " + exception.toString());
    //exception.GetBaseException();
    return null;
    }

  }
  static Future<Response>getVerifyOTP(String url, String requestContent)async
  {
    // logger = DependencyService.Get<ILogManager>().GetLog();
    SharedPreferences sp = await SharedPreferences.getInstance();
    print("VerifyOtp : " + url + " content " + requestContent);
    // HttpResponseMessage response = null;
    try {
      //  await InitializeAppData();
      // String url =  APIConstants.url + APIConstants.RequestGetUniqueColumnData;
      var body = requestContent;
      print(url);
      print("request body");
      print(body);
      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await post(url, headers: headers, body: body);
      print("response body");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End VerifyOtp - end of function');
      return response;
    }
    on Exception catch (exception) {
      print("VerifyOtp Exception : " + exception.toString());
      //exception.GetBaseException();
      return null;
    }
  }

  static Future<Response> adharUpload(requestContent,url) async{
    FileUtils.printLog("In adharUpload");
    FileUtils.printLog(requestContent);
    print("adharUpload : " + url + " content " + requestContent);
    var response;
    try
    {
      SharedPreferences sp = await SharedPreferences.getInstance();
      var token = sp.getString(AppConstants.TOKEN);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };
      var body = requestContent;

      response = await post(url, headers: headers, body: requestContent)
          .timeout(new Duration(seconds: 180));
      print("In api of create");
      print(url);
      print(response.body);
      FileUtils.printLog("****"+body);
    }catch ( e)
    {
      print("adharUpload Exception : " + e.toString());
    }
    return response;
  }

  static Future<Response> GetLeadID() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    try{
      print("in api GetLeadID()");

      FileUtils.printLog('In GetLeadID - start of function');
      String url = APIConstants.url + APIConstants.GetLeadID  ;

      var token = sp.getString(AppConstants.TOKEN);
      print(token);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };

      final response = await get(url,headers: headers);
      print("response body in call function");
      print(response.body);
      FileUtils.printLog(url);
      FileUtils.printLog(response.body);
      FileUtils.printLog('End GetLeadID - end of function');
      return response;

    }on Exception catch (e){
      FileUtils.printLog('Exception in GetLeadID: $e');
      return null;
    }
  }


  static Future<Response> checkCibilScore(requestContent,url) async{
    FileUtils.printLog("In checkCibilScore");
    FileUtils.printLog(requestContent);
    print("checkCibilScore : " + url + " content " + requestContent);
    var response;
    try
    {
      SharedPreferences sp = await SharedPreferences.getInstance();
      var token = sp.getString(AppConstants.TOKEN);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + token
      };
      var body = requestContent;

      response = await post(url, headers: headers, body: requestContent)
          .timeout(new Duration(seconds: 180));
      print("In api of create");
      print(url);
      print(response.body);
      FileUtils.printLog("****"+body);
    }catch ( e)
    {
      print("checkCibilScore Exception : " + e.toString());
    }
    return response;
  }

}
