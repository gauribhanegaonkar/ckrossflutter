// import 'package:flutterbafl/models/apiModels/AuthenticationTokenResponseModel.dart';
// import 'package:http/http.dart';
// import 'package:shared_preferences/shared_preferences.dart';

// import 'apiServices.dart';

// class AuthenticationTokenApi {
//   static Future getAndSaveAuthenticationToken() async {
//     try {
//       SharedPreferences sp = await SharedPreferences.getInstance();
//       if (sp != null) {
//         Response response = await APIServices.getAuthenticationToken();
//         if (response != null) {
//           await handleResponse(response);
//         }
//       }
//     } on Exception catch (_) {}
//   }

//   static Future handleResponse(Response response) async {
//     if (response != null) {
//       if (response.statusCode == 200) {
//         AuthenticationTokenResponseModel tokenResponseModel =
//             authenticationTokenResponseModelFromJson(response.body);
//         if (tokenResponseModel != null) {
//           if (tokenResponseModel.flag) {
//             await saveTokenToSharedPref(tokenResponseModel);
//           }
//         }
//       }
//     }
//   }

//   static Future saveTokenToSharedPref(
//       AuthenticationTokenResponseModel responseModel) async {
//     SharedPreferences sp = await SharedPreferences.getInstance();
//     sp.setString('token', responseModel.token);
//     print('Authentication Token : ' + responseModel.token);
//   }
// }
