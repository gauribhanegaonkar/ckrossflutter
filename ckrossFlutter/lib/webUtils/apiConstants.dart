class APIConstants {

  static String baseUrl = 'http://115.124.101.73:8080';
  //static String baseUrl = 'http://125.16.91.134:9090';

  static String path = '/LMS/';
  static String url = baseUrl + path;
  static String CR = "CR";
  static String REQUEST_CREATE_LEAD = url + 'leads';
  static String GetMobileOTP = 'getMobileOTP';
  static String DoOTPLogin = 'doOTPLogin';
  static String LoadHtmlFields = 'loadHtmlFields';
  static String productChange = 'productChange';
  static String GetReportsDateFilter = 'reports';
  static String RequestGetUniqueColumnData = 'getUniqueColumnData';
  static String REQUEST_UPLOAD_PDF = url + "upload";
  static String REQUEST_UPDATE_LEAD = url + "lead";
  static String UPLOAD = "UPLOAD";
  static String REQUEST_UPDATE_STATUS = "updateStatus";
  static String OTP_BASE_URL = url + "WEMI/Karza/";
  static String MOBILE_OTP = "/Applicant/mobileOTP";
  static String UPDATE_DATA = "updateData";
  static String GetLeadID = "getLeadID";
  static String GENERATE_CIBIL_REPORT  = url + "generateCibilReport";

}

