import 'package:connectivity/connectivity.dart';

class NetworkUtils {
  static Future<ConnectivityResult> checkInternetConnectivity() async {
    return await (Connectivity().checkConnectivity());
  }
}