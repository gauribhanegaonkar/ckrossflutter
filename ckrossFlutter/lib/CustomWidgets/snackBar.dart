import 'package:flutter/material.dart';

class CustomSnackBar {
  static void showSnackBar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(new SnackBar(content: Text(message),duration: const Duration(seconds: 3),));
  }
}