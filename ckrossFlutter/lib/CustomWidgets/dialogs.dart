import 'package:ckrossFlutter/screens/newTW/OTPSubmissionPagenewTW.dart';
import 'package:ckrossFlutter/screens/newTW/panImageUpload.dart';
import 'package:flutter/material.dart';
//import 'package:flutterbafl/utils/Colors.dart';

class Dialogs {
  static Future<void> showSimpleDialog(BuildContext context, String message) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child:
              //SimpleDialog(
              //elevation: 5,
              //  backgroundColor: Colors.transparent,
//            shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.all(Radius.circular(8)),
//            ),
              //children: <Widget>[
              Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(height: 10),
                CircularProgressIndicator(),
                Container(height: 20),
                Material(
                  color: Colors.transparent,
                  child: Text(
                    message,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white),
                  ),
                )
              ],
            ),
          ),
          // ],
          //),
          //),
        );
      },
    );
  }

  static dismissSimpleDialog(BuildContext context) {
    print("dismiss");
    if (Navigator.canPop(context)) {
      print("1111");
      Navigator.of(context).pop();
      
     // Navigator.of(context).pushNamed('/');
    }
    
  }

  static Widget DialogWidget(
      BuildContext context, String title, String details) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(title, style: TextStyle(fontSize: 18, color: Colors.black)),
            IconButton(
              icon: Icon(Icons.close, color: Colors.black),
              onPressed: () => Navigator.pop(context),
            )
          ],
        ),
        Padding(
          padding: EdgeInsets.all(10),
          child: Text(
            details,
            style: TextStyle(fontSize: 15, ),
            textAlign: TextAlign.justify,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(
              child: RaisedButton(
                //color: ButtonBackgroundColor,
                shape: StadiumBorder(),
                child: Text(
                  'Close',
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
                onPressed: () => Navigator.pop(context),
              ),
            ),
          ],
        ),
      ],
    );
  }
  static showDialogs(context,text){
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('CKross'),
        content: Text(
                text),
        actions: <Widget>[
          new FlatButton(
            onPressed: () {
              Navigator.of(context, rootNavigator: true)
                  .pop(); // dismisses only the dialog and returns nothing
            },
            child: new Text('OK'),
          ),
        ],
      ),
    );
  }


static showAlertDialog(context,msg,header) {
  print(msg);
  print(header);
 bool result = false;
  // set up the buttons
  Widget cancelButton = FlatButton(
    child: Text("Ok"),
    onPressed:  () {
      //result=true;
       int count = 0;
       Navigator.of(context).popUntil((_) => count++ >= 3);
      
    },
  );
  Widget continueButton = FlatButton(
    child: Text("Cancel"),
    onPressed:  () {
      int count = 0;
       Navigator.of(context).popUntil((_) => count++ >= 3);
      //result=true;
    },
    
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(header),
    content: Text(msg),
    actions: [
      cancelButton,
      continueButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
  
}

  static showdialog(BuildContext context, String text, String title,submitRequestJson) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text("Ok"),
              onPressed: () {
               // Navigator.pushReplacementNamed(context, '/LeadCreationOTPPageNewTW');
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new LeadCreationOTPPageNewTW(submitRequestJson)));
              },
            ),
            FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  static showdialogss(BuildContext context, String text) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          //title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text("Ok"),
              onPressed: () {
               // Navigator.pushReplacementNamed(context, '/LeadCreationOTPPageNewTW');
//                Navigator.push(
//                    context,
//                    new MaterialPageRoute(
//                        builder: (BuildContext context) => new PANUploadImage()));
            },
            ),
            FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }


  static showdialogs1(BuildContext context, String text) {
    showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          //title: Text(title),
          content: new Text(text),
          actions: <Widget>[
            FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
//            FlatButton(
//              child: new Text("Cancel"),
//              onPressed: () {
//                Navigator.pop(context);
//              },
//            ),
          ],
        );
      },
    );
  }



}
