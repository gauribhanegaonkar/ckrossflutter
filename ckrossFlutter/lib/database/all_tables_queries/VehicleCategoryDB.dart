import 'package:ckrossFlutter/models/modelVd.dart';
import 'package:ckrossFlutter/models/vehicleCategory.dart';
import 'package:sqflite/sqflite.dart';

class VehicleCategoryDB{

  String TABLE_VEHICLE_CATEGORY = 'VehicleCategory';

  String vehicle_category_id = 'vehicle_category_id';
  String model_id = 'model_id';
  String vehicle_category = 'vehicle_category';


  Future<int> insertVehicleCategory(VehicleCategory vehicleCategory, Database database) async {
    Database db = database;
    var result = await db.insert(TABLE_VEHICLE_CATEGORY, vehicleCategory.toMap());
    return result;
  }

  Future<int> updateVehicleCategory(VehicleCategory vehicleCategory,Database database) async {
    var db = database;
    var result = await db.update(TABLE_VEHICLE_CATEGORY, vehicleCategory.toMap(), where: '$vehicle_category_id = ?', whereArgs: [vehicleCategory.vehicle_category_id]);
    return result;
  }

}