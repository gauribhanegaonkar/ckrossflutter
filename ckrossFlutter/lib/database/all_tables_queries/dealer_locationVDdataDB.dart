
import 'package:ckrossFlutter/models/dealer_locationVD.dart';
import 'package:sqflite/sqflite.dart';
import 'package:ckrossFlutter/database/database_helper.dart';

class DealerLocationVDDB {

  String TABLE_Dealer_LocationVD = 'Dealer_LocationVD';
  String dealer_location_id = 'dealer_location_id';
  String dealer_name = 'dealer_name';



  //insert data
  Future<int> insertDealerLocationVDData(DealerLocationVD deal,Database database) async {
    Database db = await database;
    var result = await db.insert(TABLE_Dealer_LocationVD, deal.toMap());
    return result;
  }

  Future<int> updateDealerLocationVDData(DealerLocationVD deal,Database database) async {
    var db = database;
    var result = await db.update(TABLE_Dealer_LocationVD, deal.toMap(), where: '$dealer_location_id = ?', whereArgs: [deal.dealer_location_id]);
    return result;
  }

  // Delete Operation: Delete a Note object from database
  Future<int> deleteDealerLocationVDData(int id,Database database) async {
    var db = await database;
    int result = await db.rawDelete('DELETE FROM $TABLE_Dealer_LocationVD WHERE $dealer_location_id = $id');
    return result;
  }



}