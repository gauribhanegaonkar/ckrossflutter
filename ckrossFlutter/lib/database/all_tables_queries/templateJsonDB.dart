import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/templateJson.dart';
import 'package:sqflite/sqflite.dart';

class TemplateJsonTable{
static final dbHelper = DatabaseHelper.instance;
  String TABLE_TEMPLATE_JSON = 'TemplateJson';

  String template_json_id = 'template_json_id';
  String form_name = 'form_name';
  String client_template_id = 'client_template_id';
  String istable_exists = 'istable_exists';
  String controller_name = 'controller_name';
  String mandatory_field_key_array = 'mandatory_field_key_array';
  String mandatory_field_lable_array = 'mandatory_field_lable_array';
  String field_json = 'field_json';
  String other_field_key_array = 'other_field_key_array';


  static Future<int> insertTemplateJson(TemplateJson templateJson) async {
     var db = await dbHelper.database;
   // Database db = database;
    var result = await db.insert("TemplateJson", templateJson.toMap());
    print("insertTemplateJsonnnnnnnnnnnnn");
    var findd = db.rawQuery('SELECT * FROM TemplateJson');
    print(findd.toString());
   // print(result);
    return result;
  }

  Future<int> updateTemplateJson(TemplateJson templateJson,Database database) async {
    var db = database;
    var result = await db.update(TABLE_TEMPLATE_JSON, templateJson.toMap(), where: '$template_json_id= ?', whereArgs: [templateJson.template_json_id]);
    return result;
  }

  static Future<List> executeTemplatejsonData(query) async {
    List returnedd;
  print(query);
  print("searchtemplatejson");
  var db = await dbHelper.database;
  await db.rawQuery('SELECT * FROM TemplateJson').then((val) {
       var map = new List();
       map.addAll(val);
       print("In TemplateJson");
       print(val);
       returnedd=map;
       
     });
      return returnedd;
  }
}