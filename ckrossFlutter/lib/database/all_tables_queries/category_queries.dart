import 'package:ckrossFlutter/models/category.dart';
import 'package:sqflite/sqflite.dart';
import 'package:ckrossFlutter/database/database_helper.dart';

class categoryDB {

  String TABLE_CATEGORY = 'categoryTable';
  String category_Id = 'category_id';
  String category_Name = 'category_name';
  String login_json_template_Id = 'login_json_template_id';



  //insert data
  Future<int> insertCategoryData(Category category,Database database) async {
    Database db = await database;
    var result = await db.insert(TABLE_CATEGORY, category.toMap());
    return result;
  }

  Future<int> updateCategory(Category category,Database database) async {
    var db = database;
    var result = await db.update(TABLE_CATEGORY, category.toMap(), where: '$category_Id = ?', whereArgs: [category.categoryId]);
    return result;
  }

  // Delete Operation: Delete a Note object from database
  Future<int> deletecategoryData(int id,Database database) async {
    var db = await database;
    int result = await db.rawDelete('DELETE FROM $TABLE_CATEGORY WHERE $category_Id = $id');
    return result;
  }

//  Future<List> getCategoryData() async {
//    var result = await database.rawQuery('SELECT * FROM $TABLE_CATEGORY');
//    return result.toList();
//  }

}