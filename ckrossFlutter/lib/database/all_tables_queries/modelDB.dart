
import 'package:ckrossFlutter/models/model.dart';
import 'package:sqflite/sqflite.dart';

import '../database_helper.dart';

class ModelDB{

  String TABLE_MODEL = 'Model';

  String model_id = 'model_id';
  String make_id = 'make_id';
  String model_name = 'model_name';
  String on_road_price = 'on_road_price';
  String date = 'date';
  String currentCounter = 'currentCounter';
  String generatedMachineIdentifier = 'generatedMachineIdentifier';
  String machineIdentifier = 'machineIdentifier';
  String generatedProcessIdentifier = 'generatedProcessIdentifier';
  String time = 'time';
  String counter = 'counter';
  String processIdentifier = 'processIdentifier';
  String timestamp = 'timestamp';
  String timeSecond = 'timeSecond';
  //final dbHelper = DatabaseHelper.instance;
  static final dbHelper = DatabaseHelper.instance;


  Future<int> insertModel(Model model) async {
    var db = await dbHelper.database;
    var result = await db.insert(TABLE_MODEL, model.toMap());
    return result;
  }

  Future<int> updateModel(Model model) async {
    var db = await dbHelper.database;
    var result = await db.update(TABLE_MODEL, model.toMap(), where: '$model_id = ?', whereArgs: [model.model_id]);
    return result;
  }

  Future<List<Map<String,dynamic>>> showModel() async{
    var db = await dbHelper.database;
    print("model data");
    var data = await db.rawQuery('SELECT * FROM $TABLE_MODEL ');
    print(data);
    return data;
  }

  static Future<List> executeQuery(String query) async {
    var db = await dbHelper.database;
    print("query in execute make");
    print(query);
    var returnedd;

 db.rawQuery(query).then((val) {
       var map = new List();
       map.addAll(val);
       print("In Make");
       returnedd=map;
       print(val);
       
       
     });
     return  returnedd;
  }

}