import 'package:ckrossFlutter/models/subLoanType.dart';
import 'package:sqflite/sqflite.dart';

import '../database_helper.dart';

class SubLoanTypeDB{

  String TABLE_SUBLOANTYPE = 'SubLoanType';

  String sub_loan_id = 'sub_loan_id';
  String sub_loan_type = 'sub_loan_type';
  static final dbHelper = DatabaseHelper.instance;



  Future<int> insertSubLoanType(SubLoanType subLoanType) async {
    var db = await dbHelper.database;
    var result = await db.insert(TABLE_SUBLOANTYPE, subLoanType.toMap());
    return result;
  }

  Future<int> updateSubLoanType(SubLoanType subLoanType) async {
    var db = await dbHelper.database;
    var result = await db.update(TABLE_SUBLOANTYPE, subLoanType.toMap(), where: '$sub_loan_id = ?', whereArgs: [subLoanType.sub_loan_id]);
    return result;
  }

  Future<List> showSubloan() async{
    var db = await dbHelper.database;
    var data = await db.rawQuery('SELECT * FROM  $TABLE_SUBLOANTYPE ');
    print(data);
    return data;
  }


  static Future<List> executeQuery(String query) async {
    List returnedd;
    print("query in execute subloan");
    print(query);

    var db = await dbHelper.database;
    await db.rawQuery(query).then((val) {
      //  var map = new List();
      //  map.addAll(val);
      print("In subloan type");
      returnedd=val;
      print(val);


    });
    return  returnedd;
  }

}