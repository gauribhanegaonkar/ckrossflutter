import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/states.dart';
import 'package:sqflite/sqflite.dart';

class StatesDB{

  String TABLE_STATES = 'States';

  String state_id = 'state_id';
  String state_name = 'state_name';
 static final dbHelper = DatabaseHelper.instance;

  Future<int> insertStates(States states, Database database) async {
    Database db = database;
    var result = await db.insert(TABLE_STATES, states.toMap());
    return result;
  }

  Future<int> updateStates(States states,Database database) async {
    var db = database;
    var result = await db.update(TABLE_STATES, states.toMap(), where: '$state_id = ?', whereArgs: [states.state_id]);
    return result;
  }

  static Future<List> GetAllDataAsync() async {
    List returnedd;
 // print(query);
  print("searchClientTemplateData");
 var db = await dbHelper.database;
    await db.rawQuery('SELECT * FROM States').then((val) {
      
      var map = new List();
       map.addAll(val);
       print("In States");
       print(val);
       returnedd=map;
       
       //return  val;
     });
     return returnedd;
  }

}

