import 'package:ckrossFlutter/models/subCategory.dart';
import 'package:sqflite/sqflite.dart';

class SubCategoryDB{

  String TABLE_SUBCATEGORY = 'SubCategory';

  String sub_category_id = 'sub_category_id';
  String subcategory_name = 'subcategory_name';
  String subcategory_sequence_no = 'subcategory_sequence_no';
  String menuType = 'menuType';
  String category_Id = 'category_id';


  Future<int> insertSubCategory(SubCategory subCategory, Database database) async {
    Database db = database;
    var result = await db.insert(TABLE_SUBCATEGORY, subCategory.toMap());
    return result;
  }

  Future<int> updateSubCategory(SubCategory subCategory,Database database) async {
    var db = database;
    var result = await db.update(TABLE_SUBCATEGORY, subCategory.toMap(), where: '$sub_category_id = ?', whereArgs: [subCategory.sub_category_id]);
    return result;
  }

}