
import 'package:ckrossFlutter/models/city.dart';
import 'package:sqflite/sqflite.dart';
import 'package:ckrossFlutter/database/database_helper.dart';

class cityDB {

  String TABLE_CITY = 'city_Table';
  String cityId = "city_id";
  String cityName = "city_name";

  //Database database;


  //insert data
  Future<int> insertCityData(City city,Database database) async {
    Database db = await database;
    var result = await db.insert(TABLE_CITY, city.toMap());
    return result;
  }

  Future<int> updateCityData(City city,Database database) async {
    var db = database;
    var result = await db.update(TABLE_CITY, city.toMap(), where: '$cityId = ?', whereArgs: [city.cityId]);
    return result;
  }

  // Delete Operation: Delete a Note object from database
  Future<int> deleteCityData(int id,Database database) async {
    var db = await database;
    int result = await db.rawDelete('DELETE FROM $TABLE_CITY WHERE $cityId = $id');
    return result;
  }



}