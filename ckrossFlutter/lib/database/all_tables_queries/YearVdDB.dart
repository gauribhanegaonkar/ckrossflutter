import 'package:ckrossFlutter/models/modelVd.dart';
import 'package:ckrossFlutter/models/yearVD.dart';
import 'package:sqflite/sqflite.dart';

import '../database_helper.dart';

class YearVDDB{

  String TABLE_YearVD = 'YearVD';

  String year_id = 'year_id';
  String model_id = 'model_id';
  String year = 'year';
  static final dbHelper = DatabaseHelper.instance;

  Future<int> insertYearVD(YearVD yearVD) async {
    var db = await dbHelper.database;
    var result = await db.insert(TABLE_YearVD, yearVD.toMap());
    return result;
  }

  Future<List<Map<String,dynamic>>> showYearVD() async{
    var db = await dbHelper.database;
    var data = await db.rawQuery('SELECT * FROM $TABLE_YearVD ');
    print(data);
    return data;
  }

  Future<int> updateYearVD(YearVD yearVD) async {
    var db = await dbHelper.database;
    var result = await db.update(TABLE_YearVD, yearVD.toMap(), where: '$year_id = ?', whereArgs: [yearVD.year_id]);
    return result;
  }

}