import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/modelVd.dart';
import 'package:sqflite/sqflite.dart';

class ModelVdDB{

  String TABLE_MODEL_VD = 'ModelVD';

  String model_id = 'model_id';
  String make_id = 'make_id';
  String model_name = 'model_name';
  static final dbHelper = DatabaseHelper.instance;


  Future<int> insertModelVD(ModelVD modelVd) async {
    var db = await dbHelper.database;
    var result = await db.insert(TABLE_MODEL_VD, modelVd.toMap());
    return result;
  }

  Future<List<Map<String,dynamic>>> showMake() async{
    var db = await dbHelper.database;
    var data = await db.rawQuery('SELECT * FROM $TABLE_MODEL_VD ');
    print(data);
    return data;
  }

  Future<int> updateModelVD(ModelVD modelVd) async {
    var db = await dbHelper.database;
    var result = await db.update(TABLE_MODEL_VD, modelVd.toMap(), where: '$model_id = ?', whereArgs: [modelVd.model_id]);
    return result;
  }

}