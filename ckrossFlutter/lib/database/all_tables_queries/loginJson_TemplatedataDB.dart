
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:sqflite/sqflite.dart';
import 'package:ckrossFlutter/database/database_helper.dart';

class  LoginJsonTemplateDB {

  String TABLE_LoginJsonTemplate = 'LoginJsonTemplate';
  String login_json_template_id = 'login_json_template_id';
  String jsonValue = 'jsonValue';
  String jsonKey = 'jsonKey';
  String category_Id = 'category_Id';
  Database database;
  static final dbHelper = DatabaseHelper.instance;


  //insert data
  Future<int> insertLoginJsonData( LoginJsonTemplate loginData) async {
    var db = await dbHelper.database;
    var result = await db.insert(TABLE_LoginJsonTemplate, loginData.toMap());
    return result;
  }

  Future<int> updateLoginJsonTemplateData( LoginJsonTemplate loginData,Database database) async {
    var db = await dbHelper.database;
    var result = await db.update(TABLE_LoginJsonTemplate, loginData.toMap(), where: '$login_json_template_id = ?', whereArgs: [loginData.login_json_template_id]);
    return result;
  }

  // Delete Operation: Delete a Note object from database
  Future<int> deleteLoginJsonTemplateData(int id) async {
    var db = await dbHelper.database;
    int result = await db.rawDelete('DELETE FROM $TABLE_LoginJsonTemplate WHERE $login_json_template_id = $id');
    return result;
  }

static Future<List> executeLoginJsonTemplateData(query) async {
  List returnedd;
     var db = await dbHelper.database;
   await db.rawQuery(query).then((val) {
       var map = new List();
       map.addAll(val);
       print("In executeLoginJsonTemplateData");
       print(query);
       print(val);
       returnedd=map;
       print("returned:$returnedd");
       
     });
     
     return returnedd;
  }

  static Future<List> executeLoginJsonTemplateImageData(query) async {
    List<Map<String,dynamic>> returnedd;
    var db = await dbHelper.database;
    await db.rawQuery(query).then((val) {
      var map = new List();
      map.addAll(val);
      print("In executeLoginJsonTemplateData");
      print(query);
      print(val);
      returnedd=val;
      print("returned:$returnedd");

    });

    return returnedd;
  }

}