
import 'package:ckrossFlutter/database/database_helper.dart';
import 'package:ckrossFlutter/models/make_dealer_data.dart';
import 'package:sqflite/sqflite.dart';

class MakeDealerDataDB{

  String TABLE_MAKE_DEALER_DATA = 'Make_Dealer_Data';

  String make_dealer_data_id  = 'make_dealer_data_id';
  String dealer_Type = 'Dealer_Type';
  String dealership_Name = 'Dealership_Name';
  String make_dealer_data_name = 'make_dealer_data_name ';
  final dbHelper = DatabaseHelper.instance;

  Future<int> insertMakeDealerData(MakeDalerData makeDalerData) async {
    var db = await dbHelper.database;
    var result = await db.insert(TABLE_MAKE_DEALER_DATA, makeDalerData.toMap());
    return result;
  }

  Future<int> updateMakeDealerData(MakeDalerData makeDalerData,Database database) async {
    var db = await dbHelper.database;
    var result = await db.update(TABLE_MAKE_DEALER_DATA, makeDalerData.toMap(), where: '$make_dealer_data_id = ?', whereArgs: [makeDalerData.make_dealer_data_id]);
    return result;
  }

  Future<List<dynamic>> show() async{
    var db = await dbHelper.database;
    var data = await db.rawQuery('SELECT * FROM $TABLE_MAKE_DEALER_DATA ');
    print(data);
    return data;
  }


}


