
import 'package:ckrossFlutter/models/dyanamic_table_field.dart';
import 'package:sqflite/sqflite.dart';
import 'package:ckrossFlutter/database/database_helper.dart';

class DynamicTableFieldsDB {

  String TABLE_DynamicTableFields = 'DynamicTableFields';
  String dynamic_field_table_id = 'dynamic_field_table_id';
  String table_name = 'table_name';
  static final dbHelper = DatabaseHelper.instance;


  //insert data
  Future<int> insertDynamicFieldsData(DynamicTableFields dynamic,Database database) async {
    Database db = await database;
    var result = await db.insert(TABLE_DynamicTableFields, dynamic.toMap());
    return result;
  }

  Future<int> updateDynamicFieldsData(DynamicTableFields dynamic,Database database) async {
    var db = database;
    var result = await db.update(TABLE_DynamicTableFields, dynamic.toMap(), where: '$dynamic_field_table_id = ?', whereArgs: [dynamic.dynamic_field_table_id]);
    return result;
  }

  // Delete Operation: Delete a Note object from database
  Future<int> deleteDynamicFieldsData(int id,Database database) async {
    var db = await database;
    int result = await db.rawDelete('DELETE FROM $TABLE_DynamicTableFields WHERE $dynamic_field_table_id = $id');
    return result;
  }

static Future<List> ExecuteQueryDynamicTableFields(query) async {
   List returnedd;
  
 var db = await dbHelper.database;
    
     await db.rawQuery(query).then((val) {
       var map = new List();
       map.addAll(val);
       print("In ExecuteQueryDynamicTableFields");
       print(val);
       returnedd=map;
       
     });
      return returnedd;
  }

}