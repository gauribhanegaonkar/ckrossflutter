import 'dart:convert';
import 'dart:ffi';

import 'package:ckrossFlutter/Utils/fileUtils.dart';
import 'package:ckrossFlutter/models/clientTemplate_data.dart';
import 'package:ckrossFlutter/models/dealerso_data.dart';
import 'package:ckrossFlutter/models/dyanamic_table_field.dart';
import 'package:ckrossFlutter/models/loginJson_Template.dart';
import 'package:ckrossFlutter/models/make_dealer_data.dart';
import 'package:ckrossFlutter/models/product.dart';
import 'package:ckrossFlutter/models/templateJson.dart';
import 'package:flutter/cupertino.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class DatabaseHelper {

  static final _databaseName = "ckrossWheelsemi.db";
  static final _databaseVersion = 1;

  static DatabaseHelper _databaseHelper;    // Singleton DatabaseHelper
  static Database _database;
  DatabaseHelper();
  // --------------- Table Names ----------------

  String TABLE_CATEGORY = 'categoryTable';
  String TABLE_CITY = 'city_Table';
  String TABLE_ClientTemplate = 'ClientTemplate';
  String TABLE_Dealer_LocationVD = 'Dealer_LocationVD';
  String TABLE_DealerSO = 'DealerSO';
  String TABLE_CkrossCase = 'CkrossCase_Table';
  String TABLE_DynamicTableFields = 'DynamicTableFields';
  String TABLE_LoginJsonTemplate = 'LoginJsonTemplate';


  String TABLE_MAKE_DEALER_DATA = 'Make_Dealer_Data';
  String TABLE_MODEL = 'Model';
  String TABLE_MODEL_VD = 'ModelVD';
  String TABLE_PRODUCT = 'Product';
  String TABLE_STATES = 'States';
  String TABLE_SUBCATEGORY = 'SubCategory';
  String TABLE_SUBLOANTYPE = 'SubLoanType';
  String TABLE_TEMPLATE_JSON = 'TemplateJson';
  String TABLE_VEHICLE_CATEGORY = 'VehicleCategory';
  String TABLE_YearVD = 'YearVD';
  String TABLE_MAKE = 'Make';


  // ------------------------ Column Names ----------------
  String category_Id = 'category_id';
  String category_Name = 'category_name';
  String login_json_template_Id = 'login_json_template_id';

  String cityId = "city_id";
  String cityName = "city_name";

  String caseRowId = 'case_row_id';
  String caseId = 'case_id';
  String caseType = 'case_type';
  String applicantJson = 'applicant_json';
  String isSubmit = 'is_submit' ;
  String tempCaseId = 'temp_case_id';
  String customerName = 'customer_name';
  String imageLstFrmServerCnt  = 'imageLstFrmServerCnt';
  String userId = 'user_id ';
  String productId = 'product_id';

  String client_template_id = 'client_template_id';
  String client_name = 'client_name';
  String template_name = 'template_name';
  String form_name_array = 'form_name_array';
  String version = 'version';
  String language = 'language';
  String is_deprecated = 'is_deprecated';
  String applicant_json = 'applicant_json';
  String details_json  = 'details_json ';

  String dealer_location_id = 'dealer_location_id';
  String dealer_name = 'dealer_name';
  String location = 'location';
  String branch = 'branch';
  String dealer_code = 'dealer_code';
  String vehicle_city = 'vehicle_city';

  String dealer_id = 'dealer_id';
  String dealership_name = 'dealership_name ';
  String dealership_data = 'dealership_data';

  String login_json_template_id = 'login_json_template_id';
  String jsonKey = 'jsonKey';
  String jsonValue = 'jsonValue';

  String dynamic_field_table_id = 'dynamic_field_table_id';
  String table_name = 'table_name';
  //String template_name = 'template_name';
  String table_headers_array = 'table_headers_array';
  String table_keys_array = 'table_keys_array';
  String mandatory_fields = 'mandatory_fields';
  String mandatory_table_headers = 'mandatory_table_headers';
  String template_json_id = 'template_json_id';
  String clear_subprocess_fields = 'clear_subprocess_fields';

  String make_dealer_data_id  = 'make_dealer_data_id';
  String dealer_Type = 'Dealer_Type';
  String dealership_Name = 'Dealership_Name';
  String make_dealer_data_name = 'make_dealer_data_name ';
  String model_id = 'model_id';
  String make_id = 'make_id';
  String model_name = 'model_name';
  String on_road_price = 'on_road_price';
  String date = 'date';
  String currentCounter = 'currentCounter';
  String generatedMachineIdentifier = 'generatedMachineIdentifier';
  String machineIdentifier = 'machineIdentifier';
  String generatedProcessIdentifier = 'generatedProcessIdentifier';
  String time = 'time';
  String counter = 'counter';
  String processIdentifier = 'processIdentifier';
  String timestamp = 'timestamp';
  String timeSecond = 'timeSecond';
  String product_id = 'product_id';
  String product_name = 'product_name';
  String state_id = 'state_id';
  String state_name = 'state_name';
  String sub_category_id = 'sub_category_id';
  String subcategory_name = 'subcategory_name';
  String subcategory_sequence_no = 'subcategory_sequence_no';
  String menuType = 'menuType';
  String sub_loan_id = 'sub_loan_id';
  String sub_loan_type = 'sub_loan_type';
  String loan_type = 'loan_type';
  // String template_json_id = 'template_json_id';
  String form_name = 'form_name';
  String make_name = 'make_name';
  // String client_template_id = 'client_template_id';
  String istable_exists = 'istable_exists';
  String controller_name = 'controller_name';
  String mandatory_field_key_array = 'mandatory_field_key_array';
  String mandatory_field_lable_array = 'mandatory_field_lable_array';
  String field_json = 'field_json';
  String other_field_key_array = 'other_field_key_array';
  String vehicle_category_id = 'vehicle_category_id';
  String vehicle_category = 'vehicle_category';
  String year_id = 'year_id';
  String year = 'year';
  String city_id='city_id';
  String city_name = 'city_name';

  static TemplateJson cKrossStoreTemplateJson ;
  DatabaseHelper._privateConstructor();
  Database db;
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app-wide reference to the database

  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _createDb);
  }


  void _createDb(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $TABLE_CATEGORY($category_Id INTEGER PRIMARY KEY AUTOINCREMENT,$category_Name TEXT, $login_json_template_Id TEXT)');
    await db.execute(
        'CREATE TABLE City(city_id INTEGER PRIMARY KEY AUTOINCREMENT,city_name TEXT)');
    await db.execute(
        'CREATE TABLE $TABLE_CkrossCase($caseRowId INTEGER PRIMARY KEY AUTOINCREMENT,$caseId TEXT ,$caseType TEXT, $applicantJson TEXT,$isSubmit TEXT,$tempCaseId TEXT,$customerName TEXT,$imageLstFrmServerCnt INTEGER,$userId TEXT,$productId TEXT)');
    await db.execute(
        'CREATE TABLE $TABLE_ClientTemplate($client_template_id INTEGER PRIMARY KEY AUTOINCREMENT,$client_name TEXT ,$template_name TEXT, $form_name_array TEXT,$version TEXT,$language TEXT,$is_deprecated TEXT,$applicant_json TEXT,$details_json TEXT )');
    await db.execute(
        'CREATE TABLE $TABLE_Dealer_LocationVD($dealer_location_id INTEGER PRIMARY KEY AUTOINCREMENT,$dealer_name TEXT ,$location TEXT, $branch TEXT,$dealer_code TEXT,$vehicle_city TEXT )');
    await db.execute(
        'CREATE TABLE $TABLE_DealerSO($dealer_id INTEGER PRIMARY KEY AUTOINCREMENT,$dealership_name TEXT ,$dealership_data TEXT)');
    await db.execute(
        'CREATE TABLE $TABLE_DynamicTableFields($dynamic_field_table_id INTEGER PRIMARY KEY AUTOINCREMENT,$table_name TEXT ,$template_name TEXT, $table_headers_array TEXT,$table_keys_array TEXT,$mandatory_fields TEXT,$mandatory_table_headers TEXT,$template_json_id TEXT,$clear_subprocess_fields TEXT )');
    await db.execute(
        'CREATE TABLE $TABLE_LoginJsonTemplate($category_Id INTEGER PRIMARY KEY AUTOINCREMENT,$login_json_template_id TEXT, $jsonKey  TEXT, $jsonValue TEXT, $language TEXT)');
//  var queryy = 'SELECT * FROM $TABLE_ClientTemplate';
//  var findd = db.query(queryy);
//  print("find");
//  print(db.query(queryy));


    String CREATE_TABLE_MAKE_DEALER_DATA = "CREATE TABLE $TABLE_MAKE_DEALER_DATA( " +
        make_dealer_data_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        dealer_Type + " TEXT, " +
        dealership_Name + " TEXT, " +
        make_dealer_data_name + " TEXT)";

    String CREATE_TABLE_Model = "CREATE TABLE $TABLE_MODEL( " +
        model_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        make_id + " INTEGER, " +
        model_name + " TEXT, " +
        on_road_price + " TEXT, " +
        date + " TEXT, " +
        currentCounter + " TEXT, " +
        generatedMachineIdentifier + " TEXT, " +
        machineIdentifier + " TEXT, " +
        generatedProcessIdentifier + " TEXT, " +
        time + " TEXT, " +
        counter + " TEXT, " +
        processIdentifier + " TEXT, " +
        timestamp + " TEXT, " +
        timeSecond + " TEXT)";

    String CREATE_TABLE_MODEL_VD = "CREATE TABLE $TABLE_MODEL_VD( " +
        model_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        make_id + " INTEGER, " +
        model_name + " TEXT)";

    String CREATE_TABLE_PRODUCT = "CREATE TABLE $TABLE_PRODUCT( " +
        product_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        product_name + " TEXT)";

    String CREATE_TABLE_STATES = "CREATE TABLE $TABLE_STATES( " +
        state_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        state_name + " TEXT)";

    String CREATE_TABLE_SUBCATEGORY = "CREATE TABLE $TABLE_SUBCATEGORY( " +
        sub_category_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        category_Id + " INTEGER, " +
        subcategory_name + " TEXT, " +
        subcategory_sequence_no + " TEXT, " +
        menuType + " TEXT)";

    String CREATE_TABLE_SUBLOANTYPE = "CREATE TABLE $TABLE_SUBLOANTYPE( " +
        sub_loan_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + sub_loan_type + " TEXT," + loan_type + " TEXT)";

    String CREATE_TABLE_TEMPLATE_JSON = "CREATE TABLE $TABLE_TEMPLATE_JSON( " +
        template_json_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        form_name + " TEXT, " +
        client_template_id + " TEXT, " +
        istable_exists + " TEXT, " +
        controller_name + " TEXT, " +
        mandatory_field_key_array + " TEXT, " +
        mandatory_field_lable_array + " TEXT, " +
        field_json + " TEXT, " +
        other_field_key_array + " TEXT)";

    String CREATE_TABLE_VEHICLE_CATEGORY = "CREATE TABLE $TABLE_VEHICLE_CATEGORY( " +
        vehicle_category_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        model_id + " INTEGER, " +
        vehicle_category + " TEXT)";

    String CREATE_TABLE_YearVD = "CREATE TABLE $TABLE_YearVD( " +
        year_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        model_id + " INTEGER, " +
        year + " TEXT)";

    String CREATE_TABLE_MAKE = "CREATE TABLE $TABLE_MAKE( " +
        make_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        product_id + " INTEGER, " +
        make_name + " TEXT)";

    db.execute(CREATE_TABLE_MAKE_DEALER_DATA);
    db.execute(CREATE_TABLE_Model);
    db.execute(CREATE_TABLE_MODEL_VD);
    db.execute(CREATE_TABLE_PRODUCT);
    db.execute(CREATE_TABLE_STATES);
    db.execute(CREATE_TABLE_SUBCATEGORY);
    db.execute(CREATE_TABLE_SUBLOANTYPE);
    db.execute(CREATE_TABLE_TEMPLATE_JSON);
    db.execute(CREATE_TABLE_VEHICLE_CATEGORY);
    db.execute(CREATE_TABLE_YearVD);
    db.execute(CREATE_TABLE_MAKE);
    // db.execute(CREATE_TABLE_CITY);
    print("Create all tables");
    await FileUtils.printLog("All Table Created");
  }

  Future<List> execution(query) async {
    Database db = await this.database;
    print("query");
    print(query);
    db.rawQuery(query).then((val){
      // print("val:$val");
      return val ;
    });
  }

  Future<List> rawquery(query) async {
    Database db = await this.database;
    var id= await db.rawQuery(query) ;
    print("in rawquery");
    print(id);
    return id;
  }

  Future updation(tableName,tempjson) async {
    Database db = await this.database;
    print("in update");
    print(tempjson);
    var result = await db.update(tableName, tempjson);
    print(result);
    //  var result = await db.query(campaignTable, orderBy: '$colPriority ASC');
    return result;
  }
  //  Future add(tableName, values) async {
  //       Database db = await this.database;
  //     print("insert");
  //      Map<String, String> val = values;
  //      print(val);


  //  // print(json.decode(values));
  //     await db.insert(tableName, val);
  //    //  var res = db.rawQuery('SELECT * FROM tableName');
  //     // print("ressssssssss"+res.toString());
  //   }

  Future<List<Map<String, dynamic>>> search() async {
    Database db = await this.database;
    print("in TemplateJson  search");
    var res=await db.rawQuery( "SELECT * FROM TemplateJson");
    print( res.toString());




    //  var result = await db.query(campaignTable, orderBy: '$colPriority ASC');
    //return result;
  }

  insert(String tablename,ClientTemplate clientTemplate) async {
    Database db = await this.database;
    print("in reult insert");
    print(clientTemplate.toMap());
    print(tablename);
    await db.insert(tablename, clientTemplate.toMap());

    // print(result);
    //  var result = await db.query(campaignTable, orderBy: '$colPriority ASC');
    //return result;
  }

  insertdynamicFieldTable(String tablename,DynamicTableFields dynamicTableFields) async {
    Database db = await this.database;
    print("in reult insert");
    print(dynamicTableFields.toMap());
    print(tablename);
    await db.insert(tablename, dynamicTableFields.toMap());
  }

  insertLoginJsonTemplate(String tablename,LoginJsonTemplate loginJsonTemplate) async {
    Database db = await this.database;
    print("in reult insert");

    var insertresult = await db.insert(tablename, loginJsonTemplate.toMap());
    return insertresult;
    print("loginjsontemplate");
    print(insertresult);

  }
  Future<List<Map<String, dynamic>>> search3() async {
    Database db = await this.database;

    await db.rawQuery('SELECT * FROM LoginJsonTemplate').then((val) {
      var map = new List();
      map.addAll(val);
      print("In LoginJsonTemplate");
      print(val[1]);
    });
  }

  Future<List> search4(query) async {
    Database db = await this.database;
    List returnedd;
    await db.rawQuery(query).then((val) {
      var map = new List();
      map.addAll(val);
      print("In ClientTemplate");
      // print(val);
      returnedd=map;

    });
    return returnedd;
  }

  insertDealerSO(String tablename,DealerSO dealerSO) async {
    Database db = await this.database;
    print("in reult insert");
    print(tablename);
    print(dealerSO.toMap());
    await db.insert(tablename, dealerSO.toMap());


  }

//  static Future<List> executeClientTemplateData(query) async {
//    List returnedd;
//    print(query);
//    print("searchClientTemplateData");
//    Database db = await this.database;
//    print(await db.rawQuery('SELECT * FROM ClientTemplate').then((val) {
//      var map = new List();
//      map.addAll(val);
//      print("In ClientTemplate");
//
//    }));
//    await db.rawQuery(query).then((val) {
//      var map = new List();
//      map.addAll(val);
//      print("In ClientTemplate");
//      // print(val);
//      returnedd=map;
//
//    });
//    return returnedd;
//  }


  insertMakeDalerData(String tablename,MakeDalerData makeDalerData) async {
    Database db = await this.database;
    print("in reult insert");

    await db.insert(tablename, makeDalerData.toMap());

  }
  Future<List<Map<String, dynamic>>> search1() async {
    Database db = await this.database;
    List lst;
    await db.rawQuery('SELECT * FROM LoginJsonTemplate ').then((val){
      for(int i= 0;i<val.length;i++){
        print(val[0]);
        print("search1 data");
      }
    });
    return lst;
  }

  Future<List<Map<String, dynamic>>> search2() async {
    Database db = await this.database;

    await db.rawQuery('SELECT * FROM Make_Dealer_Data ').then((val) {
      var map = new List();
      map.addAll(val);
      print("In TABLE_MAKE_DEALER_DATA");
      print(val[1]);
    });
  }

  Future execute(query) async {
    Database db = await this.database;
    print("in execute");
    print(query);
    List result = await db.rawQuery(query);

    print(result);
    //  var result = await db.query(campaignTable, orderBy: '$colPriority ASC');
    return result;
  }
}