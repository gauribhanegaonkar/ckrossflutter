import 'package:camera/camera.dart';
import 'package:ckrossFlutter/providers/cc_leads_provider.dart';
import 'package:ckrossFlutter/providers/creadit_refer_provider.dart';
import 'package:ckrossFlutter/providers/post_dis_query_res_provider.dart';
import 'package:ckrossFlutter/providers/post_disbustment_doc_upload_provider.dart';
import 'package:ckrossFlutter/providers/pre_dis_advice_upload_provider.dart';
import 'package:ckrossFlutter/providers/pre_disbustment_query_resolution.dart';
import 'package:ckrossFlutter/providers/pre_disbustment_ut_provider.dart';
import 'package:ckrossFlutter/providers/query_resolution.dart';
import 'package:ckrossFlutter/providers/service_inspection_video_provider.dart';
import 'package:ckrossFlutter/screens/LeadCreationData.dart';
import 'package:ckrossFlutter/screens/LeadCreationImage.dart';
import 'package:ckrossFlutter/screens/StatusReportUsedTW.dart';
import 'package:ckrossFlutter/screens/homeScreen.dart';
import 'package:ckrossFlutter/screens/leadCreationImagePage.dart';
import 'package:ckrossFlutter/screens/leadCreationPages/LeadCreationData_seq_id_4.dart';
import 'package:ckrossFlutter/screens/leadCreationPages/LeadCreation_seq_id_2.dart';
import 'package:ckrossFlutter/screens/leadCreationPages/LeadCreation_seq_id_3.dart';
import 'package:ckrossFlutter/screens/newTW/CCLeadsNTW.dart';
import 'package:ckrossFlutter/screens/newTW/CreditReferQueueNTW.dart';
import 'package:ckrossFlutter/screens/newTW/OTPSubmissionPagenewTW.dart';
import 'package:ckrossFlutter/screens/newTW/PostDisQueryResolutionNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PostDisbursementDocUploadLeadListNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PreDisAdviceUploadNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PreDisQueryResolutionNTW.dart';
import 'package:ckrossFlutter/screens/newTW/PreDisbursementDocUploadLeadListNTW.dart';
import 'package:ckrossFlutter/screens/newTW/Pre_approvalQueryResolutionNTW.dart';
import 'package:ckrossFlutter/screens/newTW/aadharOCRupload.dart';
import 'package:ckrossFlutter/screens/newTW/panImageUpload.dart';
import 'package:ckrossFlutter/screens/permissionList.dart';
import 'package:ckrossFlutter/screens/postLoginDashboard.dart';
import 'package:ckrossFlutter/screens/selectGalleryVideo.dart';
import 'package:ckrossFlutter/screens/statusReportData.dart';
import 'package:ckrossFlutter/screens/statusReportDate.dart';
import 'package:ckrossFlutter/screens/statusReportDetails.dart';
import 'package:ckrossFlutter/screens/usedtw/QueryResolutionUTW.dart';
import 'package:ckrossFlutter/screens/usedtw/cc_leads_UTW.dart';
import 'package:ckrossFlutter/screens/usedtw/creadit_refer_UTW.dart';
import 'package:ckrossFlutter/screens/usedtw/post_dis_query_res_UTW.dart';
import 'package:ckrossFlutter/screens/usedtw/post_disbursement_upload.dart';
import 'package:ckrossFlutter/screens/usedtw/pre_dis_query_resolution.dart';
import 'package:ckrossFlutter/screens/usedtw/service_inspection_videoUpload.dart';
import 'package:provider/provider.dart';
import './screens/usedtw/pre_disbursement_upload.dart';
import 'package:ckrossFlutter/screens/userSignIn.dart';
import 'package:ckrossFlutter/screens/videoRecordingPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_player/video_player.dart';

import 'Utils/fileUtils.dart';
import 'database/database_helper.dart';

// import 'package:flutterbafl/screens/PostLogin/StatementsScreen.dart';

//import 'package:cameragallery/pages/about.dart';
List<CameraDescription> cameras = [];
void main() async {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent, // status bar color
  ));

//  try {
//    WidgetsFlutterBinding.ensureInitialized();
//    cameras = await availableCameras();
//  } on CameraException catch (e) {
//   // logError(e.code, e.description);
//  }

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale locale) {
    _myState state = context.findAncestorStateOfType<_myState>();
    print(state);
    state.setLocale(locale);
  }

  @override
  _myState createState() => _myState();
}

class _myState extends State<MyApp> {
  Locale _locale;

  var db;
  final dbHelper = DatabaseHelper.instance;
  var flag = false;
  List<String> datesSelected;
  VideoPlayerController _controller;
  String videoPath;
  String submitRequestJson;
  var imageName;
  Map mapFormData = Map();
  Map mapFormData1 = Map();

  @override
  Future<void> initState() {
    // TODO: implement initState
    super.initState();
    checkPermisson();

    db = dbHelper.database;
  }

  Future permissionAcessPhone() async {
    PermissionService().requestPermission(onPermissionDenied: () {
      print('Permission has been denied');
    });
  }

  Future<void> checkPermisson() async {
    final PermissionHandler _permissionHandler = PermissionHandler();
    var result =
        await _permissionHandler.requestPermissions([PermissionGroup.storage]);
    if (result[PermissionGroup.storage] == PermissionStatus.granted) {
      FileUtils.saveLogFile();
      print("File Created");
      permissionAcessPhone();
    } else {
      checkPermisson();
    }
  }

  void setLocale(Locale locale) {
    print("loccccc" + locale.toString());
    setState(() {
      _locale = locale;
    });
  }

  int num;
  @override
  Widget build(BuildContext contextP) {
    print(_locale.toString());
    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(
            value: QueryResolutionUTData(),
          ),
          ChangeNotifierProvider.value(
            value: PreDisbursmentUTData(),
          ),
          ChangeNotifierProvider.value(
            value: PostDisbursmentUTData(),
          ),
          ChangeNotifierProvider.value(
            value: PreDisQueryResolutionUTData(),
          ),
          ChangeNotifierProvider.value(
            value: PostDisQueryResolutionUTData(),
          ),
          ChangeNotifierProvider.value(
            value: CCLeadsUTData(),
          ),
          ChangeNotifierProvider.value(
            value: CreditReferQueueUTData(),
          ),
          ChangeNotifierProvider.value(
            value: PreDisAdvUplUTData(),
          ),
          ChangeNotifierProvider.value(
            value: ServiceInspVideoUTData(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          locale: _locale,
          localeResolutionCallback: (deviceLocale, supportedLocles) {
            for (var locale in supportedLocles) {
              if (locale.languageCode == deviceLocale.languageCode) {
                return deviceLocale;
              }
            }

            return supportedLocles.first;
          },
          initialRoute: '/',
          routes: {
            '/': (context) => UserSignIn(),
            '/PostLoginDashboard': (context) => PostLoginDashboard(),
            '/homeScreen': (context) => HomeScreen(),
            '/leadCreationImagePage': (context) => LeadCreationImagePage(),
            '/leadCreationImage': (context) => LeadCreationImage(),
            '/statusReport': (context) => StatusReportDate(),
            '/statusReportDetail': (context) =>
                StatusRepoetListView(datesSelected),
            '/statusUsedTWReport': (context) =>
                StatusReportUsedTW(datesSelected),
            '/videoRecording': (context) => VideoExampleHome(),
            '/videoFromGallery': (context) =>
                VideoPlayerFromGallery(_controller, videoPath),
            '/LeadCreationData': (context) => LeadCreationData(),
            '/LeadCreationDataSeqid2': (context) => LeadCreationDataSeqid2(mapFormData),
            '/LeadCreationDataSeqid3': (context) => LeadCreationDataSeqid3(mapFormData1),
            '/LeadCreationDataSeqid4': (context) => LeadCreationDataSeqid4(),
            '/LeadCreationOTPPageNewTW': (context) => LeadCreationOTPPageNewTW(submitRequestJson),
            '/ckrossLabel': (context) => TextField(),
            '/PreDisbursementDocUpload': (context) =>
                PreDisbursmentDocUploadNTW(),
            '/PANUploadImage': (context) =>  PANUploadImage(imageName),
            '/AdharUploadImage': (context) =>  AdharUploadImage(imageName),
            PreDisbursmentDocUploadUsedTW.routeName: (context) =>
                PreDisbursmentDocUploadUsedTW(),
            PostDisbursmentDocUploadNTW.routeName: (context) =>
                PostDisbursmentDocUploadNTW(),
            PostDisbursmentDocUploadUTW.routeName: (context) =>
                PostDisbursmentDocUploadUTW(),
            QueryResolutionNTW.routeName: (context) => QueryResolutionNTW(),
            QueryResolutionUTW.routeName: (context) => QueryResolutionUTW(),
            PreDisbursmentQueryResUTW.routeName: (context) =>
                PreDisbursmentQueryResUTW(),
            PreDisbursmentQueryResNTW.routeName: (context) =>
                PreDisbursmentQueryResNTW(),
            PostDisbursmentQueryResNTW.routeName: (context) =>
                PostDisbursmentQueryResNTW(),
            PostDisbursmentQueryResUTW.routeName: (context) =>
                PostDisbursmentQueryResUTW(),
            CCLeadsUTW.routeName: (context) => CCLeadsUTW(),
            CCLeadsNTW.routeName: (context) => CCLeadsNTW(),
            CreditReferUTW.routeName: (context) => CreditReferUTW(),
            CreditReferNTW.routeName: (context) => CreditReferNTW(),
            PreDisAdvUploadNTW.routeName: (context) => PreDisAdvUploadNTW(),
            ServiceInspVideoUTW.routeName: (context) => ServiceInspVideoUTW(),
          },
        ));
  }
}
